/**
 * This module exports a set of common helper functions.
 */
module utils;

public import core.sys.windows.com : GUID, CLSID, LPCOLESTR, S_OK;
public import core.sys.windows.windows : HRESULT;

private import std.traits : EnumMembers;


/**
 * Converts the specified string inth the CLSID instance
 */
extern(Windows) HRESULT CLSIDFromString(const(LPCOLESTR) lpsz, CLSID* pclsid) nothrow;

/**
 * Returns an index of enum element
 */
pragma(inline, true)
size_t rank(E)(E e) nothrow
    if (is(E == enum))
{
	
	foreach (i, member; EnumMembers!E)  {
		if (e == member) {
			return i;
		}
	}
	assert(0, "Not an enum member");
}

/**
 * Returns a first key of associative array that's related to specified value
 */
pragma(inline, true)
string get_key_by_value(E)(E[string] container, E value) {
	foreach (key, item; container) {
		if (item == value) {
			return key;
		}
	}
	return null;
}
