/**
 * This module exports an entry point of application.
 * 
 * Initialization steps: $(OL
 *   $(LI Bind STDIN/STDOUT if the application is run in window mode.)
 *   $(LI Change current directory to parent directory where configuration file is stored.)
 *   $(LI Load configuration file.)
 *   $(LI Setup SIGINT/SIGTERM handlers.)
 *   $(LI Check existing of named event to avoid duplicate processes.)
 *   $(LI Start database server.)
 *   $(LI Start web server.)
 *   $(LI Wait 1 second and open a browser with application page.)
 *   $(LI Run a thread that call database function to remove outdated files. Defined by retention policy.)
 *   $(LI Run a thread that poll a list of USB devices.)
 * )
 */
module app;

import core.thread;
import core.memory : GC;
import core.runtime : Runtime;
import core.sys.windows.windows;
import std.process : browse;

import std.string : toStringz, format, replace, indexOf;
import std.utf : toUTF16z;
import std.conv : to;

import std.file : chdir, getcwd, thisExePath;
import std.path : dirName, dirSeparator;

import config;
import utils;
import util.log;

import camera : CameraDevice;
import media : Framework;
import database : Database;
import web : WebServer;

// Extra dependencies
pragma(lib, "gdi32.lib");
pragma(lib, "winmm.lib");
pragma(lib, "user32.lib");
pragma(lib, "ole32.lib");

alias void* HDEVNOTIFY;

/**
 * Registers the device or type of device for which a window will receive notifications.
 */
extern(Windows) HDEVNOTIFY RegisterDeviceNotificationA(HANDLE recipient, void* notification_filter, DWORD flags);

/**
 * Creates or opens a named or unnamed event object.
 */
extern(Windows) HANDLE CreateEventA(
	LPSECURITY_ATTRIBUTES lpEventAttributes, BOOL bManualReset, BOOL bInitialState, LPCTSTR lpName
);


/**
 * Notifies an application of a change to the hardware configuration of a device or the computer.
 */
enum WM_DEVICECHANGE = 0x0219;

/**
 * The recipient parameter (RegisterDeviceNotification) is a window handle.
 */
enum DEVICE_NOTIFY_WINDOW_HANDLE = 0;

/**
 * The type of filter defines a logical volume.
 * This structure is a DEV_BROADCAST_VOLUME structure.
 */
enum DBT_DEVTYP_VOLUME = 2;

/**
 * The type of filter defines a class of devices.
 * This structure is a DEV_BROADCAST_DEVICEINTERFACE structure.
 */
enum DBT_DEVTYP_DEVICEINTERFACE = 5;

/**
 * A device or piece of media has been inserted and is now available.
 */
enum DBT_DEVICEARRIVAL = 0x8000;

/**
 * A device or piece of media has been removed.
 */
enum DBT_DEVICEREMOVECOMPLETE = 0x8004;


/**
 * Serves as a standard header for information related to a device event
 */
struct DEV_BROADCAST_HDR {
	DWORD dbch_size;
	DWORD dbch_devicetype;
	DWORD dbch_reserved;
}

/**
 * Contains information about a class of devices.
 */
struct DEV_BROADCAST_DEVICEINTERFACE {
	DWORD dbcc_size;
	DWORD dbcc_devicetype;
	DWORD dbcc_reserved;
	GUID dbcc_classguid;
	char[1] dbcc_name;
}

/**
 * Contains information about a logical volume.
 */
struct DEV_BROADCAST_VOLUME {
	DWORD dbcv_size;
	DWORD dbcv_devicetype;
	DWORD dbcv_reserved;
	DWORD dbcv_unitmask;
	WORD  dbcv_flags;
}


/**
 * An entry point of application
 */
int main() {
	int result = 0;

	try {
		Runtime.initialize();
		result = init(GetModuleHandle(null));
	}
	catch (Throwable e) {
		result = -1;
		error("Unable to initialize an application: " ~ e.toString());
		MessageBoxA(null, "Unable ", "Error", MB_OK | MB_ICONEXCLAMATION);
	}

	return result;
}

/**
 * Initializes the CEF application
 */
int init(HINSTANCE hInstance) {
	import sdk : setup_hotplug_callback;
	import core.thread : thread_joinAll;

	// Prepare console for Windows subsystem
	version (Windows) {
		import std.stdio: stdin, stdout, stderr, File, _IONBF;
		import core.sys.windows.windows : STD_INPUT_HANDLE, STD_OUTPUT_HANDLE, 
			STD_ERROR_HANDLE, GetStdHandle;

		// Use the console of the parent of the current process.
		enum ATTACH_PARENT_PROCESS = cast(DWORD)-1;

		if (AttachConsole(ATTACH_PARENT_PROCESS)) {
			// Bind standard output stream
			File dup_stdout;
			dup_stdout.windowsHandleOpen(GetStdHandle(STD_OUTPUT_HANDLE), "w");
			stdout = dup_stdout;
			stdout.setvbuf(0, _IONBF);

			// Bind standard error stream
			File dup_stderr;
			dup_stderr.windowsHandleOpen(GetStdHandle(STD_ERROR_HANDLE), "w");
			
			// Redirect STDERR to STDOUT in case of any error
			try {
				dup_stderr.tell();
				stderr = dup_stderr;
			}
			catch (Exception ignore) {
				stderr = dup_stdout;
			}
			stderr.setvbuf(0, _IONBF);
		}
	}

	// Change current directory where executable file is stored
	auto app_path = dirName(thisExePath) ~ dirSeparator ~ "..";
	chdir(app_path);
	info("APP path: " ~ getcwd());

	load_default_config(detect_config());
	set_signal_handlers();

	// Allow only one instance of application
	auto event = CreateEventA(null, true, false, APP_NAME);
	if (GetLastError() == ERROR_ALREADY_EXISTS) {
		CloseHandle(event); 

		auto cfg = get_default_config();
		browse("http://" ~ cfg.app_host);
		return 1;
	}

	// Initialize http and database backend
	db_server = new Database();
	db_server.start();

	http_server = new WebServer();
	http_server.start();

	// Display web interface
	Thread.sleep(dur!("seconds")(1));
	auto cfg = get_default_config();
	browse("http://" ~ cfg.app_host);

	// Run cleaner of outdated files
	new Thread({
		while (true) {
			try {
				delete_outdated_files();
				Thread.sleep(dur!("minutes")(1));
			}
			catch (Exception e) {
				error(e.msg);
			}
		}
	}).start();

	// Stay in message loop
	setup_hotplug_callback!CameraDevice();
	info("Run message loop");
	thread_joinAll();

	// Clean up resources
	info("Shutdown");

	http_server.stop();
	http_server = null;

	db_server.stop();
	db_server = null;

	GC.collect();
	return 0;
}

/**
 * Deletes outdated files which are defined by retention policy
 */
protected void delete_outdated_files() {
	import database : Database, PQfinish, ExecStatusType, PQexec, PQclear,
		PQresultStatus, PQresultErrorMessage;

	// Prepare connection
	auto connection = Database.get_connection();
	scope(exit) {
		if (connection) {
			PQfinish(connection);
		}
	}

	auto result = PQexec(connection, "SELECT delete_outdated_files()".toStringz());
	scope(exit) PQclear(result);

	if (PQresultStatus(result) != ExecStatusType.TUPLES_OK) {
		throw new Exception(
			"Unable to deleted outdated files: "
				~ to!string(PQresultErrorMessage(result))
		);
	}
}

extern (C) {
	/**
	 * Represents a signal callback
	 */
	private alias void function(int) nothrow sigfn_t;

	/**
	 * Sets a handler for the specified signal.
	 * 
	 * Params:
	 * 		signal = a number of signal
	 * 		callback = a callback to call when the signal will be raised
	 */
	private sigfn_t signal(int signal, sigfn_t callback) nothrow;

	/**
	 * A reference to previous segfault handler
	 */
	private __gshared sigfn_t old_segfault_handler;

	/**
	 * A reference to previous termination handler
	 */
	private __gshared sigfn_t old_sigterm_handler;

	/**
	 * A reference to previous interrupt handler
	 */
	private __gshared sigfn_t old_sigint_handler;

}

/**
 * Sets an application handler for signals
 */
private void set_signal_handlers() {
	import core.stdc.stdlib : atexit;
	import util.runtime : load_library, get_last_error;
	import core.stdc.signal : SIGSEGV, SIGTERM, SIGINT, SIG_ERR, SIG_DFL, raise;

	atexit(&terminate_handler);

	// Setup a termination handler
	old_sigint_handler = signal(SIGINT, (signo) {
		terminate_handler();

		// Jump to previous handler
		if (old_sigint_handler is null) {
			signal(SIGINT, SIG_DFL);
		}
		else {
			signal(SIGINT, old_sigint_handler);
		}

		raise(SIGINT);
	});

	if (old_sigint_handler == SIG_ERR) {
		fatal("Unable to set a SIGINT handler: " ~ get_last_error());
	}

	// Setup a termination handler
	old_sigterm_handler = signal(SIGTERM, (signo) {
		terminate_handler();

		// Jump to previous handler
		if (old_sigterm_handler is null) {
			signal(SIGTERM, SIG_DFL);
		}
		else {
			signal(SIGTERM, old_sigterm_handler);
		}

		raise(SIGTERM);
	});

	if (old_sigterm_handler == SIG_ERR) {
		fatal("Unable to set a SIGTERM handler: " ~ get_last_error());
	}

	// Setup a fault handler
	// TODO: merge with application handler
	version (Posix) {
		try {
			load_library("libSegFault.so");
		}
		catch (Exception e) {
			error("Unable to set a SIGSEGV handler. " ~ e.msg);
		}
	}
	else {
		old_segfault_handler = signal(SIGSEGV, (signo) {
			error("Segmentation fault");

			// Jump to previous handler
			if (old_segfault_handler is null) {
				signal(SIGSEGV, SIG_DFL);
			}
			else {
				signal(SIGSEGV, old_segfault_handler);
			}

			raise(SIGSEGV);
		});

		if (old_segfault_handler == SIG_ERR) {
			fatal("Unable to set a SIGSEGV handler: " ~ get_last_error());
		}
	}
}

/**
 * This function is called when termination signal has occurred.
 */
protected extern(C) void terminate_handler() nothrow {
	import util.runtime : shutdown_callbacks;
	foreach (callback; shutdown_callbacks) {
		try {
			callback();
		}
		catch (Exception e) {
			error(e.msg);
		}
	}
}

/**
 * Returns a reader of configuration file.
 */
private auto detect_config() {
	import std.stdio : File;
	import core.stdc.stdlib : getenv;
	import std.file : thisExePath, exists;
	import std.path : base_name = baseName, build_path = buildNormalizedPath;

	auto app_name = base_name(thisExePath, ".exe");
	auto paths = [
		build_path("./" ~ app_name ~ ".cfg"),
		build_path(to!string(getenv("HOME")), app_name ~ ".cfg"),
		build_path("/etc", app_name ~ ".cfg")
	];

	foreach (path; paths) {
		if (exists(path)) {
			return File(path, "r");
		}
	}

	fatal("Unable to find a valid configuration");
	assert(0);
}


/**
 * The default instance of database server
 */
private __gshared Database db_server;

/**
 * The default instance of HTTP server
 */
private __gshared WebServer http_server;
