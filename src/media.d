/**
 * This module provides a proxy interface to FFMPEG.
 */
module media;

import std.stdio : stdin, stdout, stderr, tmpnam;

import std.process : Pid, ProcessPipes, Config, Redirect, wait, kill, 
	pipeProcess, spawnProcess;
import core.sync.mutex : Mutex;

import std.outbuffer : OutBuffer;
import std.string : format;
import std.conv : to;
import std.json : parseJSON;
import std.algorithm : countUntil, remove;
import core.time : Duration, dur;

import config : FFMPEG_SPEED;

/**
 * A mutex for synchronization access to list of ffmpeg tasks
 */
private __gshared Mutex mutex;

/**
 * A list of ffmpeg tasks
 */
private __gshared Pid[] tasks;

static this() {
	mutex = new Mutex;
}

/**
 * Implements multimedia framework
 */
class Framework {

	/**
	 * Encodes the specified video file to WEBM format.
	 */
	/*public static void transcode_video(string input, string output) {
		auto command = [
			"ffmpeg/ffmpeg.exe", "-i", input, "-c:v", "libvpx", "-crf", "10", 
			"-b:v", "1M", "-c:a", "libvorbis", "-f", "webm", 
			"-speed", FFMPEG_SPEED, output
		];

		// Try to execute a command
		auto pid = spawnProcess(
			command, stdin, stdout, stderr, null, Config.suppressConsole
		);

		synchronized (mutex) {
			tasks ~= pid;
		}

		// Check the exit status
		auto status = wait(pid);
		synchronized (mutex) {
			tasks = remove(tasks, countUntil(tasks, pid));
		}

		if (status != 0) {
			throw new Exception(
				"Unable to transcode video from " ~ input ~ " to " ~ output 
					~ " with result " ~ to!string(status)
			);
		}
	}*/

	/**
	 * Encodes the specified audio file to WEBM format.
	 */
	public static void transcode_audio(string input, string output) {
		auto command = [
			"ffmpeg/ffmpeg.exe", "-nostdin", "-i", input, "-c:a", "libmp3lame", 
			"-f", "mp3", "-qscale:a", "2", "-speed", FFMPEG_SPEED, output
		];

		// Try to execute a command
		auto pid = spawnProcess(
			command, stdin, stdout, stderr, null, Config.suppressConsole
		);

		synchronized (mutex) {
			tasks ~= pid;
		}

		// Check the exit status
		auto status = wait(pid);
		synchronized (mutex) {
			tasks = remove(tasks, countUntil(tasks, pid));
		}

		if (status != 0) {
			throw new Exception(
				"Unable to transcode audio from " ~ input ~ " to " ~ output 
					~ " with result " ~ to!string(status)
			);
		}
	}

	/**
	 * Returns the data of thumbnail for specified video frame
	 */
	public static ubyte[] get_video_thumbnail(string input, Duration position = dur!"seconds"(1)) {
		// Prepare command
		auto seek_pos = position.split!("hours", "minutes", "seconds")();
		auto seek_position = format(
			"%02d:%02d:%02d", seek_pos.hours, seek_pos.minutes, seek_pos.seconds
		);

		auto command = [
			"ffmpeg/ffmpeg.exe", "-nostdin", "-i", input, "-ss", seek_position, 
			"-vframes", "1", "-f", "mjpeg", "pipe:1"
		];

		// Try to execute command
		auto pipes = pipeProcess(
			command, Redirect.stdout, null, Config.suppressConsole
		);

		synchronized (mutex) {
			tasks ~= pipes.pid;
		}

		// Read data of thumbnail
		auto container = new OutBuffer();
		try {
			foreach (buffer; pipes.stdout.byChunk(new ubyte[4096])) {
				container.write(buffer);
			}
		}
		catch (Exception e) {
			wait(pipes.pid);
			throw e;
		}

		// Check the exit status
		auto status = wait(pipes.pid);
		synchronized (mutex) {
			tasks = remove(tasks, countUntil(tasks, pipes.pid));
		}

		if (status != 0) {
			throw new Exception(
				"Unable to fetch thumbnail from " ~ input ~ 
					" with result " ~ to!string(status)
			);
		}
		return container.toBytes();
	}

	/**
	 * Immediately terminates all tasks
	 */
	public static void terminate() {
		if (tasks is null) {
			return;
		}

		synchronized (mutex) {
			foreach (task; tasks) {
				try {
					kill(task);
				}
				catch (Exception ignore) {
				}
			}

			tasks = [];
		}
	}

	/**
	 * Returns the metadata for specified media file
	 */
	public static MediaInfo metadata(string input) {
		auto command = [
			"ffmpeg/ffprobe.exe", "-v", "quiet", "-i", input, "-print_format", "json",
			"-show_format", "-show_streams"
		];

		// Try to fetch metadata
		ProcessPipes pipes;
		auto container = new OutBuffer();
		try {
			pipes = pipeProcess(command, Redirect.stdout, null, Config.suppressConsole);
			foreach (buffer; pipes.stdout.byChunk(new ubyte[4096])) {
				container.write(buffer);
			}
		}
		finally {
			if (pipes.pid) {
				wait(pipes.pid);
			}
		}

		// Try to parse metadata
		MediaInfo info;
		auto data = parseJSON(container.toString());
		info.duration = dur!"seconds"(
			cast(long)to!float(data["format"]["duration"].str())
		);

		foreach (stream; data["streams"].array()) {
			if (stream["codec_type"].str() == "video") {
				info.width = cast(uint)(stream["width"].integer());
				info.height = cast(uint)(stream["height"].integer());
				break;
			}
		}

		return info;
	}

}


/**
 * Represents the information about media file
 */
struct MediaInfo {

	/**
	 * A duration of video/sound stream
	 */
	Duration duration;

	/**
	 * A width of video frame
	 */
	uint width;

	/**
	 * A height of video frame
	 */
	uint height;

}
