/**
 * This module provides a container to start a web server with PHP application.
 */
module web;

import util.log : info, error;

import std.stdio : stdin, stdout, stderr;
import core.thread : Thread;
import std.process : Pid, kill, wait, spawnProcess, Config;
import std.conv : to;

/**
 * Implements container for web server
 */
class WebServer : Thread {

	/**
	 * A process identifier for fastcgi server
	 */
	protected Pid httpd_pid;

	/**
	 * Creates the thread
	 */
	public this() {
        super(() {
			try {
				run();
			}
			catch (Exception e) {
				error("Unable to run webserver:" ~ e.msg);
			}
		});
    }

	/**
	 * Stops the web server and realeses resources
	 */
	public void stop() {
		// Stop FastCGI server
		try {
			if (this.httpd_pid) {
				kill(this.httpd_pid);
			}
		}
		catch (Exception e) {
			error("Unable to stop webserver: " ~ e.msg);
			return;
		}

		info("Web Thread is stopped");
	}

	/**
	 * Starts the web server
	 */
	protected void run() {
		// Run HTTP server
		this.httpd_pid = spawnProcess(
			["apache/bin/httpd.exe"],
			stdin, stdout, stderr, null, Config.suppressConsole
		);

		info("Web Thread is started");
	}

}
