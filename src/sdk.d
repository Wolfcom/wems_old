/**
 * This module provides functions to control Wolfcom cameras.
 * 
 * This module also implement a main hotplug callback for USB devices.
 * Hotplug callback perfoms the following steps: $(OL
 *   $(LI Check every second internal queue for USB events from `dev.usb` module.)
 *   $(LI Verify vendor and product identifiers to detect Wolfcom device.)
 *   $(LI Create an instance of $(D_INLINECODE camera) module in case of new device is found.)
 *   $(LI Trigger a function $(D_INLINECODE camera.on_volume_mounted) in case of mass storage device is found, 
 *     otherwise trigger a function $(D_INLINECODE camera.on_connected).)
 *   $(LI Trigger a function $(D_INLINECODE camera.on_volume_unmounted) in case of mass storage device is disconnected, 
 *     otherwise trigger a function $(D_INLINECODE camera.on_disconnected).)
 * )
 */
module sdk;

private import config : DEVICE_TYPE, SUPPORTED_DEVICES;
private import util.log : info, error, fatal;
private import std.conv : to;
private import device.usb : Device;

/**
 * A time to wait response from the device
 */
enum USB_TIMEOUT = 3000;

/**
 * A list of registered cameras
 */
protected __gshared void*[uint] CAMERAS;

/**
 * Initializes the USB library and installs hotplug callback.
 */
void setup_hotplug_callback(T)() {
	import std.string : format;
	import core.time : dur;
	import util.thread : Thread;
	import net.bus : locate;
	import device.usb : HotplugEvent, libusb_hotplug_event;

	// Check for connected devices
	// TODO
	//libusb_get_device_list ();

	// Listen for USB events
	auto component = locate("USB");
	if (!component) {
		throw new Exception("USB component isn't published");
	}

	auto task = new Thread(() nothrow {
		while (true) {
			try {
				auto event = component.receive!HotplugEvent();
				immutable camera_id = event.device.bus + event.device.port;
				immutable camera_type = is_camera_device(event.device);

				// Skip unknown devices
				if (camera_type == DEVICE_TYPE.UNKNOWN) {
					continue;
				}

				if (camera_type == DEVICE_TYPE.STORAGE) {
					// Small delay before system will mount drive
					Thread.sleep(dur!"seconds"(1));

					// Detect existing instance, otherwise create a new
					T* instance;
					if (camera_id in CAMERAS) {
						instance = cast(T*)(camera_id in CAMERAS);
					}
					else {
						auto tmp = new T(event.device);
						instance = &tmp;
						CAMERAS[camera_id] = cast(void*)tmp;
					}

					immutable camera_bus = event.device.bus;
					immutable camera_port = event.device.port;

					// Detect drive letter
					import core.sys.windows.windows : GetLogicalDrives;
					char drive_letter = '\0';
					foreach (drive; get_drive_letters(GetLogicalDrives())) {
						info(format("CHECK DRIVE: %c", drive));
						try {
							auto dev_location = get_device_location(drive);
							if (dev_location.bus == camera_bus && dev_location.port == camera_port) {
								drive_letter = drive;
								break;
							}

							info(format(
								"IGNORE DRIVE %c (BUS = %d, PORT = %d)",
								drive, dev_location.bus, dev_location.port
							));
						}
						catch (Exception e) {
							error(e.msg);
						}
					}

					final switch (event.type)  {
						// Found new storage device
						case libusb_hotplug_event.DEVICE_ARRIVED:
							// Ignore invalid drive
							if (drive_letter == '\0') {
								error(format(
									"Unable to mount a device (BUS = %d, PORT = %d) as drive",
									camera_bus, camera_port
								));
							}
							else {
								instance.on_volume_mounted(drive_letter);
							}
							break;

						// Storage device is disconnected
						case libusb_hotplug_event.DEVICE_LEFT:
							instance.on_volume_unmounted(drive_letter);
							break;
					}
				}
				else {
					final switch (event.type)  {
						// Found new generic device
						case libusb_hotplug_event.DEVICE_ARRIVED:
							auto instance = new T(event.device);
							instance.on_connected();

							CAMERAS[camera_id] = cast(void*)instance;
							break;

						// Generic device is disconnected
						case libusb_hotplug_event.DEVICE_LEFT:
							auto instance = camera_id in CAMERAS;
							if (instance) {
								(cast(T*)instance).on_disconnected();
							}
							break;
					}
				}
			}
			catch (Exception e) {
				error(e.msg);
				Thread.sleep(dur!"seconds"(1));
			}
		}
	});

	task.start();
	task.set_name("WOLFCOM_LISTENER");
}

/**
 * Checks whether the specified device is the camera device
 */
DEVICE_TYPE is_camera_device(Device* device) {
	return is_camera_device(device.vendor, device.product);
}

/**
 * Checks whether the specified identifier of device is the camera device
 */
DEVICE_TYPE is_camera_device(ushort vendor_id, ushort product_id) {
	foreach (device; SUPPORTED_DEVICES) {
		if (vendor_id == device["Vendor"] && product_id == device["Product"]) {
			return to!DEVICE_TYPE(device["Type"]);
		}
	}
	return DEVICE_TYPE.UNKNOWN;
}

/**
 * Returns the identifier of device
 */
string get_serial_no(Device* device) {
	char[255] buffer;
	fetch_buffer(device, device_command.GET_DEVICE_NUMBER, buffer);
	return to!string(buffer.ptr);
}

/**
 * Returns the number of files in the device memory
 */
uint get_files_count(Device* device) {
	return fetch_int(device, device_command.GET_FILES_NUMBER);
}

/**
 * Switch camera from operation mode to storage mode.
 */
void enable_mass_storage(Device* device) {
	fetch_int(device, device_command.SET_MASSSTORAGE);
}

/**
 * An identifier of internal command
 */
protected enum device_command : ubyte {
	SET_DEVICE_TIME	= 1,

	SET_DEVICE_NUMBER = 2,
	GET_DEVICE_NUMBER = 3,

	SET_POLICE_NUMBER = 4,
	GET_POLICE_NUMBER = 5,

	SET_DEVICE_PASSWORD	= 6,
	GET_FILES_NUMBER = 7,

	GET_FILE_INFO_LIST = 8,
	GET_DEVICE_FILE	= 9,

	SET_MASSSTORAGE	= 10,
	GET_DEVICE_PASSWORD	= 11,
	SET_DEVICE_POWEROFF	= 12,

	GET_DEVICE_SETUP = 13,
	SET_DEVICE_SETUP = 14,

	SET_DEVICE_DEFAULT = 15,
	SET_DEV_FILE_FORMAT	= 16,
	SET_DEVICE_GPS = 17
}

/**
 * An internal format of USB packet
 */
protected struct Packet {
	int magic_head = 0x67023a45;
	int state;
	int size;
	int magic_tail = 0x98fdc5ba;
}

protected enum RAW_READ = 0xabc;
protected enum RAW_WRITE = 0x123;

/**
 * Executes a command and stores the result into specified buffer.
 */
protected void fetch_buffer(Device* device, device_command command, void[] buffer) {
	// Try to open a device
	device.claim();
	scope (exit) {
		device.release();
	}

	// Send a command to the device
	Packet packet;
	auto raw_packet = (cast(ubyte*)&packet)[0 .. Packet.sizeof];

	packet.state = RAW_READ;
	packet.size = command;

	device.send(raw_packet, USB_TIMEOUT);

	// Read a result of command
	auto received = device.receive(buffer, USB_TIMEOUT);
	(cast(byte[])buffer)[received] = 0;

	// Clear internal state of device
	device.receive(raw_packet, USB_TIMEOUT);
	if (packet.state != RAW_WRITE) {
		throw new Exception("Unable to validate a message from the device");
	}
}

/**
 * Executes a command and returns the number
 */
protected int fetch_int(Device* device, device_command command) {
	// Try to open a device
	device.claim();
	scope (exit) {
		device.release();
	}

	// Send a command to the device
	ubyte[5] cmd_buffer = [command, '0', '0', '0', 0];
	Packet packet;
	auto raw_packet = (cast(ubyte*)&packet)[0 .. Packet.sizeof];

	packet.state = RAW_WRITE;
	packet.size = cmd_buffer.length;

	device.send(raw_packet, USB_TIMEOUT);
	device.send(cmd_buffer, USB_TIMEOUT);

	// Read a result of command
	device.receive(raw_packet, USB_TIMEOUT);
	if (packet.state != RAW_READ) {
		throw new Exception("Unable to validate a message from the device");
	}

	return packet.size;
}

protected:
version (Windows) :

private import core.sys.windows.windows :  UINT, ULONG, DWORD, PSTR;
private import std.regex : ctRegex;

/**
 * Represents the hardware location of device
 */
struct Location {
	int bus;
	int port;
}

/**
 * Returns the drive letter for the specified drive mask
 */
char get_drive_letter(int mask) nothrow {
	int count = 0;

	while (mask > 1) {
		mask >>= 1;
		count++;
	}

	return "ABCDEFGHIJKLMNOPQRSTUVWXYZ"[count];
}

/**
 * Returns the list of drive letters for the specified mask
 */
char[] get_drive_letters(int mask) nothrow {
	char[] drives;
	for (auto bit = 0; bit < 26; bit++) {
		auto unit_mask = mask & (1 << bit - 1);
		// Skip empty mask
		if (unit_mask) {
			drives ~= get_drive_letter(unit_mask);
		}		
	}
	return drives;
}

/**
 * Contains information about a storage.
 */
struct STORAGE_DEVICE_NUMBER {
	UINT DeviceType;
	ULONG DeviceNumber;
	ULONG PartitionNumber;
}

/**
 * Retrieves the device type, device number, and, for a partitionable device, 
 * the partition number of a device.
 */
enum IOCTL_STORAGE_GET_DEVICE_NUMBER = 0x2D1080;

/**
 * Returns the hardware location for specifed drive letter
 */
Location get_device_location(char drive_letter) {
	import std.string : toStringz, format;
	import core.sys.windows.windows : DWORD, GUID, INVALID_HANDLE_VALUE,
		GetLastError, OPEN_EXISTING,
		DRIVE_REMOTE, FILE_SHARE_READ, FILE_SHARE_WRITE,
		DeviceIoControl, GetDriveTypeA, CreateFileA, CloseHandle;

	// Ignore network drives
	if (GetDriveTypeA((drive_letter ~ ":\\").toStringz()) == DRIVE_REMOTE) {
		throw new Exception(format(
			"Network driver %s cannot has any hardware location",
			drive_letter
		));
	}

	// Get the device number
	auto volume = CreateFileA(
		("\\\\.\\" ~ drive_letter ~ ":").toStringz(),
		0, 									// no access to the drive
		FILE_SHARE_READ | FILE_SHARE_WRITE, // shared mode
		null,								// default security attributes
		OPEN_EXISTING,						// disposition
		0,									// file attributes
		null
	);
	scope(exit) CloseHandle(volume);

	if (volume == INVALID_HANDLE_VALUE) {
		throw new Exception(
			"Unable to open the volume " ~ drive_letter ~ " of camera: " ~ 
			to!string(GetLastError())
		);
	}

	STORAGE_DEVICE_NUMBER drive_info;
	DWORD buffer_length = 0;
	auto device_status = DeviceIoControl(
		volume, IOCTL_STORAGE_GET_DEVICE_NUMBER, 
		null, 0, 					// not used
		&drive_info, drive_info.sizeof,
		&buffer_length, null		// not used
	);

	if (!device_status) {
		throw new Exception(
			"Unable to get a device number for volume " ~ drive_letter ~ ": "
			 ~ to!string(GetLastError())
		);
	}

	return get_device_location(drive_info.DeviceNumber);
}

/**
 * This class includes hard disk drives.
 */
enum DISK_DRIVE_CLASS = "{4d36e967-e325-11ce-bfc1-08002be10318}";

/**
 * This device interface class is defined for hard disk storage devices.
 */
enum DISK_DRIVE_INTERFACE_CLASS = "{53F56307-B6BF-11D0-94F2-00A0C91EFB8B}";


/**
 * Returns the hardware location for specifed device number
 */
Location get_device_location(ULONG device_number) {
	import core.memory : GC;
	import std.utf : toUTF16z;
	import util.runtime : get_last_error;

	import core.sys.windows.windows : DWORD, GUID, INVALID_HANDLE_VALUE,
		OPEN_EXISTING, FILE_SHARE_READ, FILE_SHARE_WRITE,
		DeviceIoControl, CreateFileA, CloseHandle,
		CLSIDFromString;

	import core.sys.windows.setupapi : SP_DEVICE_INTERFACE_DETAIL_DATA, 
		SP_DEVINFO_DATA, SP_DEVICE_INTERFACE_DATA, DIGCF_PRESENT, DIGCF_DEVICEINTERFACE, 
		SPDRP_LOCATION_INFORMATION, SetupDiGetDeviceRegistryProperty,
		SetupDiGetClassDevsA, SetupDiDestroyDeviceInfoList,
		SetupDiEnumDeviceInfo, SetupDiEnumDeviceInterfaces, SetupDiGetDeviceInterfaceDetailA;

	// Prepare device filter of camera drive
	GUID disk_interface_guid;
	CLSIDFromString(
		cast(wchar*)DISK_DRIVE_INTERFACE_CLASS.toUTF16z(), &disk_interface_guid
	);

	// Get device interface handle for all devices attached to system
	auto info_set = SetupDiGetClassDevsA(
		&disk_interface_guid, null, null, DIGCF_PRESENT | DIGCF_DEVICEINTERFACE
	);
	scope(exit) SetupDiDestroyDeviceInfoList(info_set);

	if (info_set == INVALID_HANDLE_VALUE) {
		throw new Exception("Unable to find a list of removable disks. " ~ get_last_error());
	}

	// Iterate through all devices
	SP_DEVINFO_DATA dev_data = {
		cbSize: SP_DEVINFO_DATA.sizeof
	};

	int device_idx = 0;
	DWORD buffer_length = 0;
	while (SetupDiEnumDeviceInfo(info_set, device_idx, &dev_data)) {
		// Get the interface of device
		SP_DEVICE_INTERFACE_DATA interface_data = {
			cbSize: SP_DEVICE_INTERFACE_DATA.sizeof
		};
		auto interface_status = SetupDiEnumDeviceInterfaces(
			info_set, &dev_data, &disk_interface_guid, 0, &interface_data
		);
		if (!interface_status) {
			error("Unable to get an interface of device. " ~ get_last_error());
			device_idx++;
			continue;
		}

		// Get required buffer size before request interface details (device path)
		SetupDiGetDeviceInterfaceDetailA(
			info_set, &interface_data, null, 0, &buffer_length, null
		);
		if (buffer_length < 64 || buffer_length > 1024) {
			error("Unable to get a size of device buffer: " ~ to!string(buffer_length));
			device_idx++;
			continue;
		}

		// Get interface details (device path)
		auto dev_extra_data = cast(SP_DEVICE_INTERFACE_DETAIL_DATA*)GC.malloc(buffer_length);
		// (offsetof(SP_DEVICE_INTERFACE_DETAIL_DATA, DevicePath) + sizeof(TCHAR)) bytes
		version (Win32) {
			dev_extra_data.cbSize = 4 + char.sizeof;
		}
		else {
			dev_extra_data.cbSize = 8;
		}

		auto req_result = SetupDiGetDeviceInterfaceDetailA(
			info_set, &interface_data, dev_extra_data, buffer_length, &buffer_length, &dev_data
		);
		if (!req_result) {
			error("Unable to get information about device. " ~ get_last_error());
			device_idx++;
			continue;
		}

		// Compare device numbers
		auto drive = CreateFileA(
			dev_extra_data.DevicePath, 
			0, FILE_SHARE_READ | FILE_SHARE_WRITE, 
			null, OPEN_EXISTING, 0, null
		);
		scope(exit) CloseHandle(drive);

        if (drive == INVALID_HANDLE_VALUE) {
			error("Unable to open drive of device. " ~ get_last_error());
			device_idx++;
			continue;
		}

		STORAGE_DEVICE_NUMBER sdn;
		auto device_status = DeviceIoControl(
			drive, IOCTL_STORAGE_GET_DEVICE_NUMBER, null, 0, 
			&sdn, sdn.sizeof, &buffer_length, null
		);
		if (!device_status) {
			error("Unable to retrieve a device instance. " ~ get_last_error());
			device_idx++;
			continue;
		}

		if (device_number != sdn.DeviceNumber) {
			device_idx++;
			continue;
		}

		// Drive device is found, so try to find related USB device
		DWORD usb_device;
		CM_Get_Parent(&usb_device, dev_data.DevInst, 0);
		if (!usb_device) {
			throw new Exception("Unable to get a parent device: " ~ get_last_error());
		}

		char[255] usb_dev_id;
		auto cm_result = CM_Get_Device_IDA(
			usb_device, usb_dev_id.ptr, usb_dev_id.length, 0
		);
		if (cm_result != CR_SUCCESS) {
			throw new Exception(
				"Unable to get a device path of parent device: " ~ get_last_error()
			);
		}

		// Try tp fetch location of device
		return get_device_location(to!string(usb_dev_id.ptr));
	}

	throw new Exception("Unable to find a device: " ~ to!string(device_number));
}

/**
 * A regular expressions to parse a port number from the 
 */
enum PORT_NUMBER = ctRegex!(r"Port_#(\d+)", "i");

/**
 * A regular expressions to parse a port number from the 
 */
enum HUB_NUMBER = ctRegex!(r"Hub_#(\d+)", "i");

/**
 * Returns the hardware location for specified device identifier
 */
Location get_device_location(string device_id) {
	import std.string : toStringz, format;
	import std.regex : matchFirst;
	import util.runtime : get_last_error;
	import core.sys.windows.windows : INVALID_HANDLE_VALUE;

	import core.sys.windows.setupapi : DIGCF_ALLCLASSES, DIGCF_PRESENT, DIGCF_DEVICEINTERFACE,
		SP_DEVINFO_DATA, SPDRP_LOCATION_INFORMATION, SPDRP_BUSNUMBER, SPDRP_ADDRESS,
		SetupDiGetDeviceRegistryProperty,
		SetupDiEnumDeviceInfo, SetupDiGetClassDevsA, SetupDiDestroyDeviceInfoList;

	// Get device interface handle for all devices related to specified device id
	auto info_set = SetupDiGetClassDevsA(
		null, device_id.toStringz(), null, 
		DIGCF_PRESENT | DIGCF_DEVICEINTERFACE | DIGCF_ALLCLASSES
	);
	scope(exit) SetupDiDestroyDeviceInfoList(info_set);

	if (info_set == INVALID_HANDLE_VALUE) {
		throw new Exception(format(
			"Unable to find device (%s): %s", device_id, get_last_error()
		));
	}

	// Try to fetch first device node
	SP_DEVINFO_DATA dev_data = {
		cbSize: SP_DEVINFO_DATA.sizeof
	};

	if (SetupDiEnumDeviceInfo(info_set, 0, &dev_data)) {
		Location location;

		// Detect device port
		char[255] buffer;
		immutable reg_result = SetupDiGetDeviceRegistryProperty(
			info_set, &dev_data, SPDRP_LOCATION_INFORMATION, null,
			cast(ubyte*)buffer.ptr, buffer.length, null
		);

		if (!reg_result) {
			throw new Exception(format(
				"Unable to retrieve a device location (%s). %s",
				device_id, get_last_error()
			));
		}

		auto device_location = to!string(buffer.ptr);
		auto match_port = matchFirst(device_location, PORT_NUMBER);

		if (!match_port) {
			throw new Exception("Invalid location of device: " ~ device_location);
		}
		location.port = to!int(match_port[1]);

		// Detect device bus
		DWORD hub_device;
		CM_Get_Parent(&hub_device, dev_data.DevInst, 0);
		if (!hub_device) {
			throw new Exception("Unable to get a parent device: " ~ get_last_error());
		}

		char[255] hub_dev_id;
		auto cm_result = CM_Get_Device_IDA(
			hub_device, hub_dev_id.ptr, hub_dev_id.length, 0
		);
		if (cm_result != CR_SUCCESS) {
			throw new Exception(
				"Unable to get a device path of parent device: " ~ get_last_error()
			);
		}

		// Use bus number of parent device
		auto hub_id = to!string(hub_dev_id.ptr);
		if (is_external_hub(hub_id)) {
			immutable hub_location = get_device_location(hub_id);
			location.bus = hub_location.bus;
		}

		// Use bus number of device
		else {
			auto match_hub = matchFirst(device_location, HUB_NUMBER);
			if (!match_hub) {
				throw new Exception("Invalid location of device: " ~ device_location);
			}

			location.bus = to!int(match_hub[1]);
		}

		return location;
	}

	throw new Exception("Unable to find a device: " ~ device_id);
}

/**
 * A regular expression to parse a system device identity of parent device
 */
enum DEVICE_IDENTITY = ctRegex!(r"^USB\\VID_([0-9a-f]+)&PID_([0-9a-f]+)\\[0-9a-f]+", "i");

/**
 * Checks whether the specified device identifier is external hub
 */
bool is_external_hub(string device_id) nothrow {
	import std.regex : matchFirst;
	import config : SUPPORTED_HUBS;

	try {
		// Extract device identity
		auto match = matchFirst(device_id, DEVICE_IDENTITY);
		if (!match) {
			return false;
		}

		// Check device identity
		auto vendor_id = match[1].to!int(16);
		foreach (hub; SUPPORTED_HUBS) {
			if (vendor_id == hub["Vendor"]) {
				return true;
			}
		}
	}
	catch (Exception e) {
		error("Unable to detect a device hub: " ~ e.msg);
	}

	return false;
}


/**
 * Obtains a device instance handle to the parent node of a specified device node 
 * in the local machine's device tree
 */
extern(Windows) DWORD CM_Get_Parent(DWORD* pdnDevInst, DWORD dnDevInst, ULONG ulFlags) nothrow @nogc;

/**
 * Retrieves the device instance ID for a specified device instance on the local machine
 */
extern(Windows) DWORD CM_Get_Device_IDA(
	DWORD dnDevInst, PSTR Buffer, ULONG BufferLen, ULONG ulFlags
) nothrow @nogc;

/**
 * Indicates successful PnP operation
 */
enum CR_SUCCESS = 0;
