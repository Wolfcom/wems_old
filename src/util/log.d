/**
 * This module exports a logger API.
 * 
 * License: $(WEB www.opensource.org/licenses/mit-license.php, MIT).
 * Authors: Eugene Vatulin <zenixan@gmail.com>
 */
module util.log;

private import std.conv : to;
private import std.string : toStringz;

private import core.stdc.stdlib : exit;
private import util.runtime_common : ExitStatus, get_last_error;

private import std.path : baseName;
private import std.file : thisExePath;
private import std.stdio : stderr;


version (Posix) {
	private import core.sys.posix.syslog : openlog, syslog, LOG_USER, 
		LOG_ERR, LOG_INFO, LOG_PID, LOG_NDELAY;
}
else {
	private import core.sys.windows.windows : BOOL, HANDLE, WORD, DWORD, ULONG, LPCTSTR, LPVOID;

	alias void* PSID;

	/**
	 * The type of event to be logged.
	 */
	enum EVENT_LOG : WORD {
		SUCCESS = 0,
		ERROR = 1,
		WARNING = 2,
		INFO = 4,
		AUDIT_SUCCESS = 8,
		AUDIT_FAILURE = 0x10		
	}

	/**
	 * Retrieves a registered handle to the specified event log.
	 * 
	 * Params:
	 * 		lpUNCServerName = a name of remote server on which operation is to be performed
	 * 		lpSourceName = a name of the event source
	 * 
	 * Returns: the handle of event source
	 */
	extern(Windows) HANDLE RegisterEventSourceA(
		in LPCTSTR lpUNCServerName, in LPCTSTR lpSourceName
	) nothrow;

	/**
	 * Writes an entry at the end of the specified event log.
	 * 
	 * Params:
	 * 		hEventLog = a handle of event log
	 * 		wType = a type of event
	 * 		wCategory = a category of event
	 * 		dwEventID = an identifier of event
	 * 		lpUserSid = an optional reference to user's security identifier
	 * 		wNumStrings = a number of log strings
	 * 		lpStrings = a reference to log strings
	 * 		lpRawData = an optional reference to log buffer
	 * 
	 * Returns: the status of operation 
	 * See_Also: https://msdn.microsoft.com/en-us/library/windows/desktop/aa363651(v=vs.85).aspx
	 */
	extern(Windows) BOOL ReportEventA(
		in HANDLE hEventLog, in WORD wType, in WORD wCategory, in DWORD dwEventID,
		in PSID lpUserSid, in WORD wNumStrings, in DWORD dwDataSize,
		in LPCTSTR* lpStrings, in LPVOID lpRawData
	) nothrow;

	/**
	 * A handle of event log
	 */
	private __gshared HANDLE SYSLOG_HANDLE;

}


/**
 * Prints a specified message and terminates the current application.
 * 
 * Params:
 * 		message = a message to display
 */
void fatal(string message) {
	scope(exit) exit(ExitStatus.UNKNOWN_ERROR);

	// Duplicate message to standard error stream
	auto tagged_message = "FATAL: " ~ message;
	stderr.writeln(tagged_message);

	version (Posix) {
		report_event(LOG_ERR, tagged_message);
	}
	else {
		report_event(EVENT_LOG.ERROR, tagged_message);
	}
}

/// ditto
void fatal(string* message) {
	if (message is null) {
		fatal("<EMPTY MESSAGE>");
	}
	else {
		fatal(*message);
	}
}

/**
 * Prints an error message.
 * 
 * Params:
 * 		message = a message to display
 */
void error(string message) nothrow {
	// Duplicate message to standard error stream
	auto tagged_message = "ERROR: " ~ message;
	try {
		stderr.writeln(tagged_message);
	}
	catch (Exception ignore) {
		// STDERR is closed
	}

	version (Posix) {
		report_event(LOG_ERR, tagged_message);
	}
	else {
		report_event(EVENT_LOG.ERROR, tagged_message);
	}
}

/// ditto
void error(string* message) nothrow @trusted {
	if (message is null) {
		error("<EMPTY MESSAGE>");
	}
	else {
		error(*message);
	}
}

/**
 * Prints an informational message.
 * 
 * Params:
 * 		message = a message to display
 */
void info(string message) nothrow @trusted {
	// Duplicate message to standard error stream
	auto tagged_message = "INFO: " ~ message;
	try {
		stderr.writeln(tagged_message);
	}
	catch (Exception ignore) {
		// STDERR is closed
	}

	version (Posix) {
		report_event(LOG_INFO, tagged_message);
	}
	else {
		report_event(EVENT_LOG.INFO, tagged_message);
	}
}

/// ditto
void info(string* message) nothrow {
	if (message is null) {
		info("<EMPTY MESSAGE>");
	}
	else {
		info(*message);
	}
}

/**
 * Initializes a connection with syslog service
 */
shared static this() nothrow {
	// Detect the name of log source
	immutable(char)* app_name;
	try {
		app_name = baseName(thisExePath, ".exe").toStringz();
	}
	catch (Exception e) {
		try {
			stderr.writeln("Unable to detect an application name: " ~ e.msg);
		}
		catch (Exception ignore) {
			// STDERR is closed
		}
	}

	// Try to open a log source
	version (Posix) {
		openlog(app_name, LOG_PID | LOG_NDELAY, LOG_USER);
	}
	else {
		SYSLOG_HANDLE = RegisterEventSourceA(null, app_name);
		if (SYSLOG_HANDLE is null) {
			try {
				stderr.writeln(
					"Unable to register an event source: " ~ get_last_error()
				);
			}
			catch (Exception ignore) {
				// STDERR is closed
			}
		}
	}
}

/**
 * Sends a message to syslog service.
 * 
 * Params:
 * 		level = an importance level
 * 		message = a message to display
 * 
 * Returns: true if the message is sent, otherwise false
 */
private bool report_event(T)(T level, string message) nothrow {
	auto raw_message = message.toStringz();
	version (Posix) {
		syslog(level, raw_message);
	}
	else {
		LPCTSTR[1] messages = [raw_message];

		auto status = ReportEventA(
			SYSLOG_HANDLE, level, 0, 0, null,
			cast(WORD)messages.length, 0, messages.ptr, null
		);

		if (!status) {
			try {
				stderr.writeln("Unable to report an event: "  ~ get_last_error());
			}
			catch (Exception ignore) {
				// STDERR is closed
			}

			return false;
		}
	}

	return true;
}
