/**
 * This module exposes a system information.
 * 
 * License: $(WEB www.opensource.org/licenses/mit-license.php, MIT).
 * Authors: Eugene Vatulin <zenixan@gmail.com>
 */
module util.runtime;

public import util.runtime_common;
public import std.system : Endian;

private import std.path : isAbsolute, dirName, dirSeparator;
private import std.process : Pid, pipe, spawnProcess, wait;
private import util.log : error;

private import std.typecons : Typedef;
private import std.meta : AliasSeq;
private import std.traits : isArray, isSomeString;

private import std.conv : to;
private import std.string : toStringz, lastIndexOf, strip;


/**
 * A full path to program that configure dynamic linker run-time bindings
 */
enum LD_CONFIG = "/sbin/ldconfig";

/**
 * Represents a total size for composite types
 */
alias ByteSize = Typedef!ubyte;
/// ditto
alias ShortSize = Typedef!ushort;
/// ditto
alias IntSize = Typedef!uint;
/// ditto
alias LongSize = Typedef!ulong;

/**
 * Represents a null-terminated atring
 */
alias CString = Typedef!string;

/**
 * User defined attribute to define an order of module initialization
 */
static struct MODULE_INIT_FUNCTION {
	int order;
}


/**
 * Converts the static array into the sequence of values
 */
template array_to_tuple(alias items) 
	if (isArray!(typeof(items)))
{
	static if (items.length == 0) {
		alias AliasSeq!() array_to_tuple;
	}
	else {
		alias AliasSeq!(items[0], array_to_tuple!(items[1 .. $])) array_to_tuple;
	}
}

/**
 * Returns an object for specified field of the class.
 * 
 * Example:
 * ----------------------------------------------------
 * auto socket = get_object_from_field!(Class)(
 * 		Class.init.field.offsetof, POINTER
 * );
 * ----------------------------------------------------
 * 
 * Params:
 * 		offset = an offset of field
 * 		field = a pointer to field
 * 
 * Returns: an instance of class
 */
C get_object_from_field(C)(size_t offset, void* field) nothrow @trusted {
	return cast(C)(field - offset);
}

/**
 * A helper for to_bytes function
 */
private template canSwapEndianness(T) {
	import std.traits : isIntegral, isSomeChar, isBoolean, Unqual,
		isFloatingPoint, FloatingPointTypeOf;

	enum canSwapEndianness = isIntegral!T || isSomeChar!T || isBoolean!T 
		|| (isFloatingPoint!T && !is(Unqual!(FloatingPointTypeOf!T) == real));
}

/**
 * Converts the specified data to byte array.
 * 
 * Params:
 * 		data = a data to convert
 * 
 * Returns: a byte representation of data
 */
ubyte[] to_bytes(Endian E, T)(T data) pure nothrow
	if (canSwapEndianness!T)
{
	import std.bitmanip : nativeToBigEndian, nativeToLittleEndian;

	static if (E == Endian.bigEndian) {
		return nativeToBigEndian(data).dup;
	}
	else {
		return nativeToLittleEndian(data).dup;
	}
}

/// ditto
ubyte[] to_bytes(Endian E, T)(T data) pure nothrow
	if (is(T == CString))
{
	return cast(ubyte[])data ~ 0;
}

/// ditto
ubyte[] to_bytes(Endian E, T)(T data) pure nothrow
	if (isSomeString!T)
{
	return cast(ubyte[])data;
}

/// ditto
ubyte[] to_bytes(Endian E, T)(T data) pure nothrow
	if (!canSwapEndianness!T && !is(T == CString) && !isSomeString!T)
{
	import std.array : appender;
	import std.bitmanip : append;

	auto buffer = appender!(ubyte[])();
	foreach (member; data.tupleof) {
		// Put size of structure
		static if (
			is(typeof(member) == ByteSize) ||
			is(typeof(member) == ShortSize) ||
			is(typeof(member) == IntSize) ||
			is(typeof(member) == LongSize)
		) {
			typeof(member) size = 0;
			foreach (field; data.tupleof) {
				static if (is(CString == typeof(field))) {
					if (field.length) size += (field.length + 1) * field[0].sizeof;
				}
				else {
					static if (isArray!(typeof(field))) {
						if (field.length) size += field.length * field[0].sizeof;
					}
					else {
						size += field.sizeof;
					}
				}
			}

			buffer.put(size.to_bytes!E());
		}

		// Put field of structure
		else {
			buffer.put(member.to_bytes!E());
		}
	}
	return buffer.data;
}

/**
 * Loads a dynamic library by specified path.
 * 
 * Params:
 * 		path = a path to library
 * 
 * Returns: a reference to the library
 */
void* load_library(string path) {
	import std.file : exists, thisExePath;
	version (Posix) {
		import core.sys.posix.dlfcn : RTLD_NOW, dlopen, dlerror;
	}
	else {
		import core.runtime : Runtime;
	}

	// Detect the full path to the library
	string full_path;
	if (isAbsolute(path)) {
		full_path = path;
	}
	else {
		foreach (lib_path; LD_LIBRARY_PATH) {
			auto tmp_path = lib_path ~ dirSeparator ~ path;
			if (exists(tmp_path)) {
				full_path = tmp_path;
				break;
			}
		}
	}

	if (full_path is null) {
		throw new Exception("Unable to find a full path for specified library");
	}

	// Try to load library
	version (Posix) {
		auto lib = dlopen(full_path.toStringz(), RTLD_NOW);
	}
	else {
		auto lib = Runtime.loadLibrary(full_path);
	}

	if (lib is null) {
		version (Posix) {
			throw new Exception(to!string(dlerror()));
		}
		else {
			throw new Exception(get_last_error());
		}
	}
	return lib;
}

/**
 * Returns a reference to the symbol in the library
 * 
 * Param:
 * 		library = a reference to the library
 * 		name = a name of symbol
 * 
 * Returns: a reference to the symbol
 */
T get_lib_symbol(T)(void* library, string name) {
	version (Posix) {
		import core.sys.posix.dlfcn : dlsym, dlerror;
	}
	else {
		import core.sys.windows.windows : GetProcAddress;
	}

	version (Posix) {
		auto symbol = cast(T)dlsym(library, name.toStringz());
	}
	else {
		auto symbol = cast(T)GetProcAddress(library, name.toStringz());
	}

	if (symbol is null) {
		version (Posix) {
			throw new Exception(to!string(dlerror()));
		}
		else {
			throw new Exception(get_last_error());
		}
	}
	return symbol;
}
/// ditto
T get_lib_symbol(alias S, T = typeof(S))(void* library) {
	return get_lib_symbol!T(library, __traits(identifier, S));
}

/**
 * A list of full paths to system libraries
 */
protected __gshared void delegate()[] shutdown_callbacks;

/**
 * Registers a function for execution on shutdown.
 * 
 * Params:
 * 		callback = a function to execute
 */
void register_shutdown_function(void delegate() callback) nothrow {
	shutdown_callbacks ~= callback;
}

/**
 * Converts the string to C-style null-terminated string.
 * 
 * Params:
 * 		data = a string to convert
 * 
 * Returns: a pointer to the C-style string
 */
char* toCString(string data) nothrow {
	import core.stdc.stdlib : malloc;
	import core.stdc.string : strncpy;

	char* buffer = cast(char*)malloc(data.length + 1);
	strncpy(buffer, data.ptr, data.length);
	buffer[data.length] = 0;
	return buffer;
}


/**
 * A list of full paths to system libraries
 */
private __gshared string[] LD_LIBRARY_PATH;

/**
 * Setups a list of paths to system libraries
 */
shared static this() nothrow {
	import std.file : exists, thisExePath;

	version (Posix) {
		import std.algorithm : canFind;
		import std.stdio : stdin;

		if (!exists(LD_CONFIG)) {
			error("Unable to find 'ldconfig'");
			return;
		}

		// Try to get all paths
		Pid process;
		try {
			auto uni_pipe = pipe();
			process = spawnProcess([LD_CONFIG, "-p"], stdin, uni_pipe.writeEnd);
			foreach (line; uni_pipe.readEnd.byLine()) {
				auto separator_idx = line.lastIndexOf("=>");
				if (separator_idx < 1) {
					// Line doesn't have path to library
					continue;
				}

				auto path = line[separator_idx + 2 .. $].strip().dirName();
				if (canFind(LD_LIBRARY_PATH, path)) {
					// Skip duplicate paths
					continue;
				}

				LD_LIBRARY_PATH ~= to!string(path);
			}
		}
		catch (Exception e) {
			error(e.msg);

			if (process !is null) {
				// Destroy any zombie process
				try {
					wait(process);
				}
				catch (Exception ignore) {
				}
			}

			return;
		}

		// Destroy any zombie process
		try {
			wait(process);
		}
		catch (Exception ignore) {
		}
	}
	else {
		import core.sys.windows.windows : GetSystemDirectoryA;

		// Search in current directory
		try {
			LD_LIBRARY_PATH ~= dirName(thisExePath);
		}
		catch (Exception e) {
			error("Unable to detect a current directory of application: " ~ e.msg);
		}

		// Search in system directory
		char[255] buffer;
		if (GetSystemDirectoryA(buffer.ptr, buffer.length) > 1) {
			LD_LIBRARY_PATH ~= to!string(buffer.ptr);
		}
	}
}
