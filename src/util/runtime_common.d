/**
 * This module exposes a system information.
 * 
 * License: $(WEB www.opensource.org/licenses/mit-license.php, MIT).
 * Authors: Eugene Vatulin <zenixan@gmail.com>
 */
module util.runtime_common;

private import std.conv : to;

version (Posix) {
	private import core.stdc.errno : errno;
	private import core.stdc.string : strerror;
}
else {
	private import core.sys.windows.windows : GetLastError, FormatMessageA,
		GetProcAddress, GetSystemDirectoryA,
		FORMAT_MESSAGE_FROM_SYSTEM, FORMAT_MESSAGE_MAX_WIDTH_MASK, 
		MAKELANGID, LANG_NEUTRAL, SUBLANG_DEFAULT;
}


/**
 * A set of exit status codes
 */
enum ExitStatus : ubyte {

	/**
	 * An operation has finished successfully
	 */
	SUCCESS = 0,

	/**
	 * An unknown error has occurred
	 */
	UNKNOWN_ERROR = 1,

	/**
	 * Misuse of shell builtins
	 */
	SYNTAX_ERROR = 2,

	/**
	 * Command line usage error
	 */
	USAGE_ERROR = 64,

	/**
	 * Invalid format of data
	 */
	DATA_ERROR = 65,

	/**
	 * Unable to open an input resource
	 */
	INPUT_ERROR = 66,

	/**
	 * An unknown addressee
	 */
	NO_USER = 67,

	/**
	 * An unknown host name
	 */
	NO_HOST = 68,

	/**
	 * A service is unavailable
	 */
	UNAVAILABLE = 69,

	/**
	 * An internal software error
	 */
	SOFTWARE_ERROR = 70,

	/**
	 * A system error
	 */
	SYSTEM_ERROR = 71,

	/**
	 * A critical system file is missing
	 */
	FILE_ERROR = 72,

	/**
	 * Unable to create an output file
	 */
	CREATE_ERROR = 73,

	/**
	 * An input/output error
	 */
	IO_ERROR = 74,

	/**
	 * A temporary failure
	 */
	TEMP_FAILURE = 75,

	/**
	 * A remote error in protocol
	 */
	PROTOCOL_ERROR = 76,

	/**
	 * Permission denied
	 */
	NO_PERMISSION = 77,

	/**
	 * An error of configuration
	 */
	CONFIG_ERROR = 78,

	/**
	 * Unable to invoke a command
	 */
	INVOKE_ERROR = 126,

	/**
	 * Command not found
	 */
	PATH_ERROR = 127,

	/**
	 * Terminal interrupt
	 */
	SIGINT = 130

}

/**
 * A set of exit status codes for the init script
 */
enum InitStatus : ubyte {

	/**
	 * A program is running or service is OK
	 */
	SUCCESS = 0,

	/**
	 * A program is dead and /var/run pid file exists
	 */
	PID_ERROR = 1,

	/**
	 * A program is dead and /var/lock lock file exists
	 */
	LOCK_ERROR = 2,

	/**
	 * A program is not running
	 */
	NOT_RUNNING = 3,

	/**
	 * A program or service status is unknown
	 */
	UNKNOWN = 4

}


/**
 * Returns a code or description of last system error
 */
T get_last_error(T = string)() nothrow {
	version (Posix) {
		static if (is(T == string)) {
			return to!string(strerror(errno));
		}
		else {
			return errno;
		}
	}
	else {
		static if (is(T == string)) {
			char[256] message;
			auto last_error = GetLastError();

			FormatMessageA(
				FORMAT_MESSAGE_FROM_SYSTEM | FORMAT_MESSAGE_MAX_WIDTH_MASK, 
				null, last_error, MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT),
				message.ptr, message.length, null
			);

			return to!string(message.ptr);
		}
		else {
			return GetLastError();
		}
	}
}
