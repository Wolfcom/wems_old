module util.queue;

private import std.traits : hasIndirections;

/**
 * Implements an unlimited circular queue
 */
struct CircularQueue(T) {

	/**
	 * A length of queue
	 */
	protected size_t _length;

	/**
	 * An index of first element
	 */
	protected size_t first_idx;

	/**
	 * An index of last element
	 */
	protected size_t last_idx;

	/**
	 * An internal container of queue
	 */
	protected T[] container = [T.init];
 
	/**
	 * Creates a queue from specified list of elements
	 * 
	 * Params:
	 * 		items = a list to copy
	 */
	this(T[] items) pure nothrow @safe {
		foreach (item; items) {
			push(item);
		}
	}
 
	/**
	 * Checks whether the current queue is empty.
	 * 
	 * Returns: true if the queue is empty, otherwise false
	 */
	@property bool empty() const pure nothrow @safe @nogc {
		return length == 0;
	}

	/**
	 * Returns the length of current queue.
	 */
	@property typeof(_length) length() const pure nothrow @safe @nogc {
		return _length;
	}

	/**
	 * Adds an element to the queue
	 * 
	 * Params:
	 * 		item = an element to add
	 */
	void push(T item) pure nothrow @safe {
		// Increase the size of queue
		if (length >= container.length) {
			immutable old_length = container.length;
			container.length *= 2;

			// Update pointer to the last element
			if (last_idx < first_idx) {
				container[old_length .. old_length + last_idx + 1] = container[0 .. last_idx + 1];
				static if (hasIndirections!T) {
					container[0 .. last_idx + 1] = T.init;
				}

				last_idx += old_length;
			}
		}

		// Put the element in the end of queue
		last_idx = (last_idx + 1) & (container.length - 1);
		container[last_idx] = item;
		_length++;
	}
 
	/**
	 * Returns a next element from the queue
	 */
	@property T pop() pure nothrow @safe @nogc {
		auto item = container[first_idx];
		_length--;

		// Clear reference
		static if (hasIndirections!T) {
			container[first_idx] = T.init;
		}

		// Updater pointer to next element
		first_idx = (first_idx + 1) & (container.length - 1);

		return item;
	}

}
