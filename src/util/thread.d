/**
 * This module exports a wrapper for system threads.
 * 
 * License: $(WEB www.opensource.org/licenses/mit-license.php, MIT).
 * Authors: Eugene Vatulin <zenixan@gmail.com>
 */
module util.thread;

private import core.thread : StdThread = Thread;
private import std.string : toStringz;

version (CRuntime_Glibc) {
	private import core.sys.posix.pthread : pthread_t;

	/**
	 * Sets the name of a thread
	 * 
	 * Params:
	 * 		thread = a thread identifier to update
	 * 		name = a name to set
	 */
	extern(C) int pthread_setname_np(pthread_t thread, const(char)* name) nothrow;
}

/**
 * A wrapper for system threads.
 */
class Thread : StdThread {

	/**
	 * Creates a thread.
	 * 
	 * Params:
	 * 		callback = a thread function
	 * 		size = an optional stack size
	 */
	this(void function() callback, size_t size = 0) {
		super(callback, size);
	}

	/**
	 * Creates a thread.
	 * 
	 * Params:
	 * 		callback = a thread function
	 * 		size = an optional stack size
	 */
	this(void delegate() callback, size_t size = 0) {
		super(callback, size);
	}

	/**
	 * Sets the system name of current thread.
	 * 
	 * Params:
	 * 		name = a name to set
	 */
	void set_name(string name) {
		this.name = name;

		version (CRuntime_Glibc) {
			pthread_setname_np(this.id(), name.toStringz());
		}
	}

}
