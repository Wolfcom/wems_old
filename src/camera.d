/**
 * Implements a synchronization files from Wolfcom devices.
 * 
 * Synchronization is divided in few steps by type of device: $(UL
 *   $(LI Vision devices have two interfaces: common usb interface to send a configuration commands,
 *     and mass storage interface to copy files from device.)
 *   $(LI 3RD-EYE devices can be connected only as mass storage interface.)
 * )
 * 
 * Workflow for Vision devices: $(OL
 *   $(LI Method `CameraDevice.on_connected` is triggered by hotplug callback.)
 *   $(LI Fetch device identifier and user identifier.)
 *   $(LI Update status of device as 'ONLINE' in the database.)
 *   $(LI Check a number of files in the device. If the number is greater than zero,
 *      then send a command to switch device as mass storage. 
 *      At this step device will send shutdown event, and if everything is normal will send
 *      an event to hotplug callback about new mass storage device.
 *   )
 *   $(LI Method `CameraDevice.on_volume_mounted` is triggered by hotplug callback.)
 *   $(LI Update status of device as 'SYNC' if the number of files is greater than zero,
 *     otherwise set the status as 'ONLINE'.)
 *   $(LI For 'SYNC' status fetch the list of files.)
 *   $(LI For each file detect a type: picture, video, audio.)
 *   $(LI Copy a file to local file system and import to a database.)
 *   $(LI Check for GPS files related to the current file.)
 *   $(LI If GPS data is found then try to match it by date/time of file creation.)
 *   $(LI Delete a file if there are no exceptions.)
 *   $(LI Set a status 'ONLINE' after all files are imported.)
 * )
 * 
 * Workflow for 3RD-EYE devices: $(OL
 *   $(LI Method `CameraDevice.on_volume_mounted` is triggered by hotplug callback.)
 *   $(LI Detect device identifier from name of drive.)
 *   $(LI Next synchronization step are same as for Vision devices.)
 * )
 */
module camera;

import config;
import utils;

import device.usb : Device;

import media : Framework;
import database : Oid, INV_WRITE, PGconn, Database, ExecStatusType, 
	PQerrorMessage, PQresultErrorMessage, PQexec, PQcmdTuples, PQresultStatus, 
	PQntuples, PQgetvalue, PQclear, PQfinish, PQescapeByteaConn, 
	lo_import, lo_unlink;

import std.string : toStringz, toLower, indexOf, lastIndexOf, format, splitLines;

import std.conv : to;
import std.regex : matchFirst, ctRegex;

import util.log : info, error;
import core.time : Duration, dur;
import std.datetime : SysTime;

import std.array : join, replace, array;
import std.uuid : randomUUID;
import std.algorithm : countUntil;

import core.thread : Thread;
import std.stdio : File, tmpnam;
import std.file : dirEntries, SpanMode, DirEntry, read, readText, 
	copy, remove, tempDir, exists;
import std.path : extension, stripExtension, baseName;


/**
 * Returns the full name of volume for the specified drive letter
 */
protected string get_drive_name(char drive) {
	import core.sys.windows.windows : GetVolumeInformationA;

	char[255] buffer;
	GetVolumeInformationA(
		toStringz(drive ~ ":\\"), buffer.ptr, buffer.length,
		null, null, null, null, 0
	);

	return to!string(buffer.ptr);
}

/**
 * Represents the status of device in the device table
 */
enum DEVICE_STATUS : string {

	ONLINE = "ONLINE",
    SYNC = "SYNC",
    OFFLINE = "OFFLINE"

}

/**
 * Represents a type of camera type
 */
enum CAMERA_TYPE : string {

	VISION = "VISION",
	THIRD_EYE = "THIRD_EYE"

}

/**
 * Implements control interface to the camera device
 */
class CameraDevice {

	/**
	 * A system USB descriptor
	 */
	protected Device* device;

	/**
	 * A drive letter of device if the camera is mount as device
	 */
	protected char drive_letter = '\0';

	/**
	 * An identifier of device
	 */
	protected string device_id;

	/**
	 * A type of connected device
	 */
	protected CAMERA_TYPE device_type;

	/**
	 * An owner of connected device
	 */
	protected string user_id;

	/**
	 * Creates the instance of device
	 */
	this(Device* device) {
		this.device = device;
	}

	/**
	 * Returns the USB descriptor
	 */
	Device* get_device() {
		return this.device;
	}

	/**
	 * Returns the current drive letter of device
	 */
	char get_drive() {
		return this.drive_letter;
	}

	/**
	 * Updates the status of device
	 */
	protected void set_status(DEVICE_STATUS status, float progress = 0) {
		if (this.device_id.length < 1) {
			return;
		}

		// Prepare connection
		auto connection = Database.get_connection();
		scope(exit) {
			if (connection) {
				PQfinish(connection);
			}
		}
		
		// Try to update status
		auto update_query = format(
			"UPDATE devices SET status = '%s', type = '%s', progress = '%s' WHERE id = '%s'",
			cast(string)status, cast(string)this.device_type, progress, this.device_id
		);

		auto update_result = PQexec(connection, update_query.toStringz());
		auto affected_rows = to!string(PQcmdTuples(update_result));
		scope(exit) PQclear(update_result);

		if (PQresultStatus(update_result) != ExecStatusType.COMMAND_OK) {
			throw new Exception(
				"Unable to update device status (" ~ this.device_id ~ "): "
					~ to!string(PQresultErrorMessage(update_result))
			);
		}

		// Register a new device if it doesn't exist
		if (!affected_rows.length || to!uint(affected_rows) < 1) {
			auto user_id = this.user_id.length ? "'" ~ this.user_id ~ "'" : "null";
			auto reg_query = format(
				"INSERT INTO devices (id, type, user_id, status) VALUES ('%s', '%s', %s, '%s')",
				this.device_id, cast(string)this.device_type, user_id,
				cast(string)status
			);

			auto reg_result = PQexec(connection, reg_query.toStringz());
			scope(exit) PQclear(reg_result);

			if (PQresultStatus(reg_result) != ExecStatusType.COMMAND_OK) {
				throw new Exception(
					"Unable to register a new device (" ~ this.device_id ~ "): "
						~ to!string(PQresultErrorMessage(reg_result))
				);
			}
		}

		info("Device " ~ this.device_id ~ " has new status " ~ status);
	}

	/**
	 * Determines the identifier of connected device
	 */
	protected void detect_device_id() {
		import std.string : format;
		import sdk : get_serial_no;

		if (this.device_id.length) {
			return;
		}

		if (this.drive_letter != '\0') {
			auto name = get_drive_name(this.drive_letter);
			if (name.length < 3 || name[0 .. 2] != "ID") {
				throw new Exception(format(
					"Unknown identifier for device (BUS = %d, PORT = %d)",
					this.device.bus, this.device.port
				));
			}

			this.device_id = name[2 .. $];
			this.device_type = CAMERA_TYPE.THIRD_EYE;
		}
		else {
			this.device_id = get_serial_no(this.device);
			this.device_type = CAMERA_TYPE.VISION;
		}

		info("Detected device with identifier: " ~ this.device_id);
	}

	/**
	 * Determines the identifier of user for the current device
	 */
	protected void detect_user_id() {
		if (this.user_id.length) {
			return;
		}

		// Prepare connection
		auto connection = Database.get_connection();
		scope(exit) {
			if (connection) {
				PQfinish(connection);
			}
		}

		auto user_query = format(
			"SELECT user_id FROM devices WHERE id = '%s'", this.device_id
		);
		auto user_result = PQexec(connection, user_query.toStringz());
		scope(exit) PQclear(user_result);

		// Get the identifier of user
		auto user_rows = PQntuples(user_result);
		if (user_rows > 0) {
			this.user_id = to!string(PQgetvalue(user_result, 0, 0));
		}
		else {
			error("The device " ~ this.device_id ~ " hasn't an owner");
		}
	}

	/**
	 * Called when the camera is plugged. (Vision device)
	 */
	void on_connected() {
		import std.string : format;
		import sdk : get_files_count, enable_mass_storage;

		info(format(
			"Camera is connected: BUS = %d; PORT = %d", device.bus, device.port
		));

		// Detect identifiers
		this.detect_device_id();
		this.detect_user_id();

		// Mount device as storage if all files are synchronized
		auto files_count = get_files_count(this.device);
		if (files_count > 0 && this.user_id.length) {
			this.set_status(DEVICE_STATUS.SYNC);
			enable_mass_storage(this.device);
		}

		// Otherwise synchronize status
		else {
			info("Device " ~ this.device_id ~ " has " ~ to!string(files_count) ~ " files");
			this.set_status(DEVICE_STATUS.ONLINE);

			/*if (IsDeviceInserted(vision_device)) {
				this.set_status(DEVICE_STATUS.ONLINE);
			}
			else {
				this.set_status(DEVICE_STATUS.OFFLINE);
			}*/
		}
	}

	/**
	 * Called when the camera is unplugged. (Vision device)
	 */
	void on_disconnected() {
		info(format(
			"Camera is disconnected: BUS = %d; PORT = %d", device.bus, device.port
		));
		this.set_status(DEVICE_STATUS.OFFLINE);
	}

	/**
	 * Called when the storage of camera is mounted.
	 */
	void on_volume_mounted(char drive) {
		import std.string : format;
		import std.file : exists;

		this.drive_letter = drive;
		info(format(
			"Camera is mounted as storage '" ~ drive ~ "' (BUS = %d, PORT = %d)",
			this.device.bus, this.device.port
		));

		// Update status of device
		this.detect_device_id();
		this.detect_user_id();

		if (!this.user_id.length) {
			this.set_status(DEVICE_STATUS.ONLINE);
			return;
		}
		else {
			this.set_status(DEVICE_STATUS.SYNC, 0);
		}

		// Check folder for new files
		auto worker = new Thread(() nothrow {
			// Preare DB connection
			PGconn* connection;
			try {
				connection = Database.get_connection();
			}
			catch (Exception e) {
				error("Unable to get a database connection. " ~ e.msg);
				return;
			}

			scope(exit) {
				if (connection) {
					PQfinish(connection);
				}
			}

			// Get the list of files
			DirEntry[] entries;
			try {
				auto path = drive ~ ":/DCIM/100MEDIA/";
				if (!exists(path)) {
					info("Empty device: " ~ this.device_id);
					try {
						this.set_status(DEVICE_STATUS.ONLINE);
					}
					catch (Exception e) {
						error(e.msg);
					}
					return;
				}

				entries = array(dirEntries(path, SpanMode.depth));
			}
			catch (Exception e) {
				error("Unable to read a camera drive. " ~ e.msg);
				return;
			}
			info("Found " ~ to!string(entries.length) ~ " files for device " ~ this.device_id);

			auto index = 1;
			foreach (filename; entries) {
				// Device is disconnected
				if (this.drive_letter == '\0') {
					info("Device is disconnected: " ~ this.device_id);
					break;
				}

				info("Check file " ~ filename ~ " in the device " ~ this.device_id);
				try {
					sync_file(connection, filename);
				}
				catch (Exception e) {
					error("Unable to sync a file: " ~ filename ~ ". " ~ e.msg);
				}

				// Device is disconnected
				if (this.drive_letter == '\0') {
					info("Device is disconnected: " ~ this.device_id);
					break;
				}

				// Update synchronization progress
				index++;
				try {
					auto progress = (index * 100) / entries.length;
					this.set_status(DEVICE_STATUS.SYNC, progress);
				}
				catch (Exception ignore) {
				}
			}

			// Files are synchronized, so update the status of device
			info("Device files " ~ this.device_id ~ " are synchronized");
			try {
				this.set_status(DEVICE_STATUS.ONLINE);
			}
			catch (Exception e) {
				error(e.msg);
				return;
			}
		});
		worker.start();
	}

	/**
	 * Fetches files from the camera device and stores to application storage
	 */
	protected void sync_file(PGconn* connection, string filename) {
		// Detect a table name from the file type
		string table;
		bool is_video_file, is_audio_file;
		auto file_ext = extension(filename).toLower();

		switch (file_ext) {
			case ".mov":
			case ".mp4":
				table = "videos";
				is_video_file = true;
				break;

			case ".wav":
				table = "audios";
				is_audio_file = true;
				break;

			case ".jpg":
				table = "pictures";
				break;

			default:
				throw new Exception("Unknown file type: " ~ file_ext);
		}

		auto file = DirEntry(filename);
		auto file_date = file.timeLastModified.toUTC();

		// Try to check file in database
		auto query = "SELECT 1 FROM " ~ table ~ " WHERE modified = '" 
			~ to!string(file_date) ~ "' AND device_id = '" ~ this.device_id ~ "' LIMIT 1";

		auto result = PQexec(connection, query.toStringz());
		scope(exit) PQclear(result);

		auto status = PQresultStatus(result);
		if (status != ExecStatusType.TUPLES_OK) {
			throw new Exception("Unable to check a file: " ~ filename);
		}

		// Skip synchronized files
		auto rows = PQntuples(result);
		if (rows > 0) {
			info("Skip synchronized file: " ~ filename);
			debug {
			}
			else {
				remove(filename);
				info("File is removed: " ~ filename);
			}
			return;
		}

		// Try to save video file
		info("Copy file: " ~ filename);
		if (is_video_file) {
			this.sync_media_file!(true)(connection, file);
		}

		// Try to save audio file
		else if (is_audio_file) {
			this.sync_media_file!(false)(connection, file);
		}

		// Try to save picture
		else {
			this.sync_picture_file(connection, file);
		}

		info("File is synchronized: " ~ filename);
		// Remove file after successfully synchronization
		debug {
		}
		else {
			remove(filename);
			info("File is removed: " ~ filename);
		}
	}

	/**
	 * Stores the audio/video file in the storage
	 */
	protected void sync_picture_file(PGconn* connection, DirEntry entry) {
		import std.digest.sha : sha256Of;

		// Read file properties
		auto entry_date = entry.timeLastModified.toUTC();
		auto metadata = Framework.metadata(entry.name);
		auto content = cast(ubyte[])read(entry.name);

		// Prepare query to execute
		size_t safe_length;
		auto safe_content = to!string(PQescapeByteaConn(
			connection, content.ptr, content.length, &safe_length
		));

		size_t checksum_length;
		auto raw_checksum = sha256Of(content);
		auto checksum = to!string(PQescapeByteaConn(
			connection, raw_checksum.ptr, raw_checksum.length, &checksum_length
		));

		auto picture_id = randomUUID().toString();
		auto meta_query = format("
INSERT INTO pictures (id, created, modified, content, width, height, device_id, user_id, checksum) VALUES (
	'%s', '%s', '%s', '%s', %d, %d, '%s', '%s', '%s'
)",
			picture_id, entry_date.toString(), entry_date.toString(),
			safe_content, metadata.width, metadata.height, 
			this.device_id, this.user_id, checksum
		);

		// Try to execute query
		auto meta_result = PQexec(connection, meta_query.toStringz());
		scope(exit) PQclear(meta_result);

		if (PQresultStatus(meta_result) != ExecStatusType.COMMAND_OK) {
			throw new Exception("Unable to save picture: " ~ entry.name);
		}

		// Add the log entry to the system log
		auto log_query = format(
"INSERT INTO activity (id, user_id, action, entity_id, entity_type) VALUES (
	'%s', '%s', 'upload_picture', '%s', 'pictures'
)",
			randomUUID().toString(), this.user_id, picture_id
		);

		auto log_result = PQexec(connection, log_query.toStringz());
		scope(exit) PQclear(log_result);

		if (PQresultStatus(log_result) != ExecStatusType.COMMAND_OK) {
			throw new Exception(
				"Unable to register event for file " ~ entry.name
			);
		}
	}

	/**
	 * Stores the audio/video file in the storage
	 */
	protected void sync_media_file(bool IS_VIDEO_FILE)(PGconn* connection, DirEntry file) {
		import std.stdio : chunks;
		import std.digest.sha : SHA256Digest;

		auto file_date = file.timeLastModified.toUTC();
		// Prepare temporary files
		string raw_file = tempDir() ~ to!string(tmpnam(null));
		info("Raw temporary file " ~ raw_file ~ " for " ~ file.name);

		static if (!IS_VIDEO_FILE) {
			string encoded_file = tempDir() ~ to!string(tmpnam(null));
			info("Encoded temporary file " ~ encoded_file ~ " for " ~ file.name);
		}

		// Try to find a GPS data
		auto gps_data = get_gps_data(file);
		if (gps_data.length) {
			info("Found GPS file for " ~ file.name);
		}

		try {
			// Try to copy file to the temporary file before converting
			copy(file.name, raw_file);
			info("Temporary file " ~ raw_file ~ " is synchronized with original file " ~ file.name);
			
			// Parse metadata
			static if (IS_VIDEO_FILE) {
				auto metadata = Framework.metadata(raw_file);
			}
			else {
				// Try to convert audio file
				Framework.transcode_audio(raw_file, encoded_file);
				info("Temporary file " ~ raw_file ~ " is successfully converted to " ~ encoded_file);

				auto metadata = Framework.metadata(encoded_file);
			}

			// Try to parse a GPS data
			string[] gps;
			string gps_path;

			auto file_created = file.timeLastModified - metadata.duration;
			if (gps_data.length) {
				gps = parse_gps_data(
					gps_data, file_created, file.timeLastModified
				);
			}

			if (gps.length) {
				info("Found " ~ to!string(gps.length) ~ " points for file " ~ file.name);
				gps_path = "ARRAY[" ~ join(gps, ",") ~ "]";
			}
			else {
				info("No GPS points for file " ~ file.name);
				gps_path = "null";
			}

			// Prepare thumbnail for video files
			static if (IS_VIDEO_FILE) {
				string file_thumbnail;
				auto content = Framework.get_video_thumbnail(raw_file);

				size_t thumbnail_length;
				file_thumbnail = to!string(PQescapeByteaConn(
					connection, content.ptr, content.length, &thumbnail_length
				));
			}

			// Try to save file to database
			PQclear(PQexec(connection, "BEGIN"));

			static if (IS_VIDEO_FILE) {
				auto file_oid = lo_import(connection, raw_file);
				info("Temporary file " ~ raw_file ~ " is successfully imported to database");
			}
			else {
				auto raw_file_oid = lo_import(connection, raw_file.toStringz());
				info("Temporary file " ~ encoded_file ~ " is successfully imported to database");

				auto file_oid = lo_import(connection, encoded_file.toStringz());
				info("Encoded file " ~ encoded_file ~ " is successfully imported to database");
			}

			// Prepare SHA-256 checksum
			auto hash = new SHA256Digest();
			foreach (ubyte[] buffer; chunks(File(raw_file), 4096)){
				hash.put(buffer);
			}

			auto raw_checksum = hash.finish();
			size_t checksum_length;
			auto checksum = to!string(PQescapeByteaConn(
				connection, raw_checksum.ptr, raw_checksum.length, &checksum_length
			));

			// Save audio/video metadata
			auto media_id = randomUUID().toString();
			static if (IS_VIDEO_FILE) {
				string action_id = "upload_video";
				string media_type = "videos";

				string metadata_query = format(
"INSERT INTO videos (
	id, created, modified, path, content, thumbnail, duration, width, height, 
	device_id, user_id, checksum
) VALUES (
	'%s', '%s', '%s', %s, %d, '%s', INTERVAL '%d SECONDS', %d, %d, '%s', '%s', '%s'
)",
					media_id, file_created, file.timeLastModified,
					gps_path, file_oid, file_thumbnail, metadata.duration.total!"seconds",
					metadata.width, metadata.height,
					this.device_id, this.user_id, checksum
				);
			}
			else {
				string action_id = "upload_audio";
				string media_type = "audios";

				string metadata_query = format(
"INSERT INTO audios (id, created, modified, content, raw_content, duration, device_id, user_id, checksum) VALUES (
	'%s', '%s', '%s', %d, %d, INTERVAL '%s SECONDS', '%s', '%s', '%s'
)",
					media_id, file_created, file.timeLastModified,
					file_oid, raw_file_oid, metadata.duration.total!"seconds",
					this.device_id, this.user_id, checksum
				);
			}

			auto meta_result = PQexec(connection, metadata_query.toStringz());
			scope(exit) PQclear(meta_result);

			if (PQresultStatus(meta_result) != ExecStatusType.COMMAND_OK) {
				throw new Exception(
					"Unable to save metadata of file: " ~ file.name
				);
			}

			// Add the log entry to the system log
			auto log_query = format(
"INSERT INTO activity (id, user_id, action, entity_id, entity_type) VALUES (
	'%s', '%s', '%s', '%s', '%s'
)",
				randomUUID().toString(), this.user_id, action_id,
				media_id, media_type
			);

			auto log_result = PQexec(connection, log_query.toStringz());
			scope(exit) PQclear(log_result);

			if (PQresultStatus(log_result) != ExecStatusType.COMMAND_OK) {
				throw new Exception(
					"Unable to register event for file " ~ file.name
				);
			}

			// Finish transaction
			auto commit_result = PQexec(connection, "COMMIT");
			scope(exit) PQclear(commit_result);

			if (PQresultStatus(commit_result) != ExecStatusType.COMMAND_OK) {
				throw new Exception("Unable to save a file: " ~ file.name);
			}
		}
		catch (Exception e) {
			PQclear(PQexec(connection, "ROLLBACK"));
			throw e;
		}
		finally {
			// Remove temporary files
			if (exists(raw_file)) {
				remove(raw_file);
			}

			static if (!IS_VIDEO_FILE) {
				if (exists(encoded_file)) {
					remove(encoded_file);
				}
			}
		}
	}

	/**
	 * Returns the raw GPS data for specified media file
	 */
	protected string get_gps_data(DirEntry file) {
		// Prepare path to GPS files
		auto file_date = file.timeLastModified;
		auto file_timestamp = file_date.toUnixTime();

		auto gps_folder_vision = format(
			"%s:/GPSINFO/%02d_%02d_%02d", 
			this.drive_letter, file_date.day, file_date.month, file_date.year % 100
		);
		auto gps_folder_eye = format("%s:/GPSLog", this.drive_letter);

		auto gps_file_pattern_vision = format("%02d_*_*.txt", file_date.hour);
		auto gps_file_pattern_eye = format(
			"%04d%02d%02d_%02d*.*", file_date.year, file_date.month, file_date.day,
			file_date.hour
		);

		// Merge GPS file in one string
		string data;
		if (exists(gps_folder_vision)) {
			foreach (gps_file; dirEntries(gps_folder_vision, gps_file_pattern_vision, SpanMode.depth)) {
				data ~= readText(gps_file);
			}
		}
		else if (exists(gps_folder_eye)) {
			foreach (gps_file; dirEntries(gps_folder_eye, gps_file_pattern_eye, SpanMode.depth)) {
				data ~= readText(gps_file);
			}
		}

		return data;
	}

	/**
	 * Called when the storage of camera is mounted
	 */
	void on_volume_unmounted(char drive) {
		import std.string : format;

		info(format(
			"Camera is unmounted as storage '" ~ drive ~ "' (BUS = %d, PORT = %d)",
			this.device.bus, this.device.port
		));

		this.set_status(DEVICE_STATUS.OFFLINE);

		this.user_id = null;
		this.device_id = null;
		this.drive_letter = '\0';
	}

}

/**
 * A regular expressions to parse a GPS line
 */
enum GPS_LINE = ctRegex!(r"^(\d{4}-\d{2}-\d{2}\s+\d{2}:\d{2}:\d{2})\s+(N|W|E)(\d+\.\d+)\s+(N|W|E)(\d+\.\d+)", "i");

/**
 * Returns the list of points with time for specified time range
 */
string[] parse_gps_data(string data, SysTime start_time, SysTime end_time) {
	// Validate input
	string[] gps;
	if (!data.length) {
		error("Empty GPS file");
		return gps;
	}

	auto start_timestamp = start_time.toUnixTime();
	auto end_timestamp = end_time.toUnixTime();

	// Parse every line
	foreach (line; splitLines(data)) {
		// Detect type of line
		auto match = matchFirst(line, GPS_LINE);
		if (!match) {
			// Unknown format
			error("Unknown GPS line: " ~ line);
			continue;
		}

		// Parse time
		auto record_time = SysTime.fromISOExtString(match[1].replace(" ", "T"));
		auto record_timestamp = record_time.toUnixTime();
		if (record_timestamp < start_timestamp || record_timestamp > end_timestamp) {
			// Skip invalid time interval
			error(format(
				"Invalid GPS interval: %d > %d || %d < %d\nLine: %s",
				record_timestamp, start_timestamp, record_timestamp, end_timestamp,
				line
			));
			continue;
		}

		// Parse coordinates
		float longitude;
		float latitude;

		if (to!char(match[2]) == 'N') {
			latitude = to!float(match[3]);

			if (to!char(match[4]) == 'W') {
				longitude = to!float(match[5]) * -1;
			}
			else {
				longitude = to!float(match[5]);
			}
		}

		else if (to!char(match[2]) == 'W') {
			longitude = to!float(match[3]) * -1;
			latitude = to!float(match[5]);
		}

		else if (to!char(match[2]) == 'E') {
			longitude = to!float(match[3]);
			latitude = to!float(match[5]);
		}

		// Prepare gps timestamp
		auto record_interval = (record_time - start_time).split!("hours", "minutes", "seconds")();
		gps ~= format(
			"('%02d:%02d:%02d', '(%f, %f)'::point)::time_point",
			record_interval.hours, record_interval.minutes, record_interval.seconds,
			longitude, latitude
		);
	}

	return gps;
}
