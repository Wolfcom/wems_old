/**
 * This module exports a set of functions for USB devices.
 * 
 * License: $(WEB www.opensource.org/licenses/mit-license.php, MIT).
 * Authors: Eugene Vatulin <zenixan@gmail.com>
 */
module device.usb;

private import std.stdint;
private import std.conv : to;
private import std.traits : isCallable;

private import util.log : error, info;
private import net.bus : Component;


/**
 * A version of USB protocol
 */
enum USB_VERSION {

	UNKNOWN,
	VERSION_1_0,
	VERSION_1_1,
	VERSION_2_0,
	VERSION_3_0,
	VERSION_3_1

}

/**
 * Represents a descriptor of USB device
 */
struct Device {

	/**
	 * An internal identity of device
	 */
	private libusb_device* identity;

	/**
	 * An internal handle of claimed device
	 */
	private libusb_device_handle* handle;

	/**
	 * An identifier of device (bus number << 8 + device address)
	 */
	uint id;

	/**
	 * A number of USB bus
	 */
	ubyte bus;

	/**
	 * A number of USB port according to the bus
	 */
	ubyte port;

	/**
	 * A version of usb
	 */
	USB_VERSION usb_version;

	/**
	 * An identifier of vendor
	 */
	ushort vendor;

	/**
	 * An identifier of product
	 */
	ushort product;

	/**
	 * A name of device manufacturer
	 */
	string manufacturer;

	/**
	 * A serial number of device
	 */
	string serial;

	/**
	 * Creates the descriptor
	 */
	this(libusb_device* identity) nothrow {
		this.identity = identity;
		libusb_ref_device(this.identity);
	}

	/**
	 * Destroys the descriptor
	 */
	~this() nothrow {
		libusb_unref_device(this.identity);
	} 

	/**
	 * Claim an interface on a current device.
	 */
	void claim() {
		import std.string : format;

		if (this.handle) {
			throw new USBException(format(
				"Unable to open a device (VID = %d; PID = %d). Device is busy.",
				this.vendor, this.product
			));
		}

		immutable open_status = libusb_open(this.identity, &this.handle);
		if (open_status != 0) {
			throw new USBException(format(
				"Unable to open a device (VID = %d; PID = %d). %s",
				this.vendor, this.product, to!string(libusb_strerror(open_status))
			));
		}

		immutable claim_status = libusb_claim_interface(this.handle, 0);
		if (claim_status != 0) {
			libusb_close(this.handle);
			this.handle = null;

			throw new USBException(format(
				"Unable to claim an interface of device (VID = %d; PID = %d). %s",
				this.vendor, this.product, to!string(libusb_strerror(claim_status))
			));
		}

		immutable config_status = libusb_set_configuration(this.handle, 1);
		if (config_status != 0) {
			libusb_close(this.handle);
			libusb_release_interface(this.handle, 0);
			this.handle = null;

			throw new USBException(format(
				"Unable to set configuration of device (VID = %d; PID = %d). %s",
				this.vendor, this.product, to!string(libusb_strerror(claim_status))
			));
		}

		libusb_clear_halt(this.handle, libusb_endpoint_direction.OUT | 1);
		libusb_clear_halt(this.handle, libusb_endpoint_direction.IN | 1);
	}

	/**
	 * Release an interface previously claimed 
	 */
	void release() {
		if (!this.handle) {
			return;
		}

		libusb_release_interface(this.handle, 0);
		libusb_close(this.handle);

		this.handle = null;
	}

	/**
	 * Sends the data to the current device.
	 * 
	 * Params:
	 * 		buffer = a data to send
	 * 		timeout = a time to wait response from the device, in millseconds.
	 *  For an unlimited timeout, use value 0.
	 */
	void send(void[] buffer, uint timeout = 0) {
		import std.string : format;

		if (!this.handle) {
			throw new USBException(format(
				"Unable to send a data to the device (VID = %d; PID = %d). Device is not opened.",
				this.vendor, this.product
			));
		}

		int transferred;
		immutable status = libusb_bulk_transfer(
			this.handle, libusb_endpoint_direction.OUT | 1,
			buffer.ptr, buffer.length, &transferred, timeout
		);

		if (status != 0) {
			throw new USBException(
				"Unable to send a data to the device. " ~ to!string(libusb_strerror(status))
			);
		}

		if (transferred != buffer.length) {
			throw new USBException(format(
				"Device has received only %d bytes from the total %d bytes",
				transferred, buffer.length
			));
		}
	}

	/**
	 * Receives the data from the current device.
	 * 
	 * Params:
	 * 		buffer = a container of data to write
	 * 		timeout = a time to wait response from the device, in millseconds.
	 *  For an unlimited timeout, use value 0.
	 * 
	 * Returns: a number of received bytes
	 */
	uint receive(void[] buffer, uint timeout = 0) {
		import std.string : format;

		if (!this.handle) {
			throw new USBException(format(
				"Unable to receive a data from the device (VID = %d; PID = %d). Device is not opened.",
				this.vendor, this.product
			));
		}

		int transferred;
		immutable status = libusb_bulk_transfer(
			this.handle, libusb_endpoint_direction.IN | 1,
			buffer.ptr, buffer.length, &transferred, timeout
		);

		if (status != 0) {
			throw new USBException(
				"Unable to receive a data from the device. " ~ to!string(libusb_strerror(status))
			);
		}

		return transferred;
	}

	void reset() {
		this.claim();
		auto status = libusb_reset_device(this.handle);
		info("RESET: " ~ to!string(status));
		this.release();
	}

}

/**
 * Represents a notification about the arrival and departure of USB devices
 */
struct HotplugEvent {

	/**
	 * A type of event
	 */
	libusb_hotplug_event type;

	/**
	 * A descriptor of USB device
	 */
	Device* device;

}

/**
 * Returns the descriptor of USB device for the specified device identity
 * 
 * Params:
 * 		identity = a device identity
 * 
 * Returns: the USB descriptor
 */
protected auto get_device_descriptor(libusb_device* identity) nothrow {
	import std.string : format;
	
	// Prepare an USB descriptor
	auto device = new Device(identity);
	libusb_device_descriptor descriptor;
	libusb_get_device_descriptor(identity, &descriptor);

	device.bus = libusb_get_bus_number(identity);
	device.port = libusb_get_port_number(identity);

	immutable bus_number = (cast(uint)libusb_get_bus_number(identity)) << 8;
	device.id = bus_number + libusb_get_device_address(identity);

	device.vendor = descriptor.idVendor;
	device.product = descriptor.idProduct;

	// Detect the USB version of device
	switch (descriptor.bcdUSB) {
		case 0x0100:
			device.usb_version = USB_VERSION.VERSION_1_0;
			break;

		case 0x0110:
			device.usb_version = USB_VERSION.VERSION_1_1;
			break;

		case 0x0200:
			device.usb_version = USB_VERSION.VERSION_2_0;
			break;

		case 0x0300:
			device.usb_version = USB_VERSION.VERSION_3_0;
			break;

		case 0x0310:
			device.usb_version = USB_VERSION.VERSION_3_1;
			break;

		default:
			device.usb_version = USB_VERSION.UNKNOWN;
			break;
	}

	// Try to check a device
	libusb_device_handle* handle;
	auto status = libusb_open(identity, &handle);
	scope (exit) {
		if (handle) {
			libusb_close(handle);
		}
	}

	if (status != 0) {
		return device;
	}

	// Detect manufacturer of device
	char[255] buffer;
	if (descriptor.iManufacturer) {
		auto length = libusb_get_string_descriptor_ascii(
			handle, descriptor.iManufacturer, buffer.ptr, buffer.length
		);

		if (length > 0) {
			device.manufacturer = to!string(buffer.ptr);
		}
	}

	// Detect serial number of device
	if (descriptor.iSerialNumber) {
		auto length = libusb_get_string_descriptor_ascii(
			handle, descriptor.iSerialNumber, buffer.ptr, buffer.length
		);

		if (length > 0) {
			device.serial = to!string(buffer.ptr);
		}
	}

	return device;
}

/**
 * Returns the list of devices
 */
Device*[] get_device_list() {
	check_library();
	return DEVICES.values;
}

/**
 * Checks whether the USB library is initialized
 */
private void check_library() {
	if (libusb_strerror is null) {
		throw new USBException("USB library is not initialized");
	}
}

/**
 * Initializes the USB bus
 */
shared static this() nothrow {
	import core.time : dur;
	import util.runtime : load_library, get_lib_symbol;

	import net.bus : attach;
	import util.thread : Thread;


	// Try to load library
	void* libusb;
	libusb_init init_library;
	libusb_set_debug set_debug;
	libusb_hotplug_register_callback register_callback;
	libusb_handle_events handle_events;

	try {
		version (Posix) {
			libusb = load_library("libusb-1.0.so");
		}
		else {
			libusb = load_library("libusb-1.0.dll");
		}

		libusb_strerror = get_lib_symbol!libusb_strerror(libusb);
		init_library = get_lib_symbol!libusb_init(libusb, "libusb_init");
		set_debug = get_lib_symbol!libusb_set_debug(libusb, "libusb_set_debug");

		register_callback = get_lib_symbol!libusb_hotplug_register_callback(
			libusb, "libusb_hotplug_register_callback"
		);
		handle_events = get_lib_symbol!libusb_handle_events(libusb, "libusb_handle_events");

		libusb_get_device_list = get_lib_symbol!libusb_get_device_list(libusb);
		libusb_free_device_list = get_lib_symbol!libusb_free_device_list(libusb);

		libusb_get_config_descriptor = get_lib_symbol!libusb_get_config_descriptor(libusb);
		libusb_get_active_config_descriptor = get_lib_symbol!libusb_get_active_config_descriptor(libusb);
		libusb_free_config_descriptor = get_lib_symbol!libusb_free_config_descriptor(libusb);
		libusb_get_device_descriptor = get_lib_symbol!libusb_get_device_descriptor(libusb);
		libusb_get_string_descriptor_ascii = get_lib_symbol!libusb_get_string_descriptor_ascii(libusb);

		libusb_open = get_lib_symbol!libusb_open(libusb);
		libusb_reset_device = get_lib_symbol!libusb_reset_device(libusb);
		libusb_clear_halt = get_lib_symbol!libusb_clear_halt(libusb);
		libusb_claim_interface = get_lib_symbol!libusb_claim_interface(libusb);
		libusb_release_interface = get_lib_symbol!libusb_release_interface(libusb);
		libusb_bulk_transfer = get_lib_symbol!libusb_bulk_transfer(libusb);
		libusb_close = get_lib_symbol!libusb_close(libusb);

		libusb_ref_device = get_lib_symbol!libusb_ref_device(libusb);
		libusb_unref_device = get_lib_symbol!libusb_unref_device(libusb);
		libusb_get_bus_number = get_lib_symbol!libusb_get_bus_number(libusb);
		libusb_get_device_address = get_lib_symbol!libusb_get_device_address(libusb);
		libusb_get_port_number = get_lib_symbol!libusb_get_port_number(libusb);
		libusb_get_configuration = get_lib_symbol!libusb_get_configuration(libusb);
		libusb_set_configuration = get_lib_symbol!libusb_set_configuration(libusb);
	}
	catch (Exception e) {
		error("Unable to load an USB module. " ~ e.msg);
		return;
	}

	// Initialiaze a library
	auto status = init_library(null);
	if (status != 0) {
		error(
			"Unable to initialize an USB module. " ~ to!string(libusb_strerror(status))
		);

		libusb_strerror = null;
		libusb_get_device_list = null;
		libusb_free_device_list = null;

		libusb_get_config_descriptor = null;
		libusb_get_active_config_descriptor = null;
		libusb_free_config_descriptor = null;
		libusb_get_device_descriptor = null;
		libusb_get_string_descriptor_ascii = null;

		libusb_open = null;
		libusb_reset_device = null;
		libusb_clear_halt = null;
		libusb_claim_interface = null;
		libusb_release_interface = null;
		libusb_bulk_transfer = null;
		libusb_close = null;

		libusb_ref_device = null;
		libusb_unref_device = null;
		libusb_get_bus_number = null;
		libusb_get_device_address = null;
		libusb_get_configuration = null;
		libusb_set_configuration = null;
		return;
	}

	// Publish an USB component
	try {
		USB_COMPONENT = attach("USB");
	}
	catch (Exception e) {
		error("Unable to publish a hotplug interface. " ~ e.msg);
		return;
	}

	USB_COMPONENT.address = () {
		return "";
	};

	USB_COMPONENT.is_alive = () {
		return true;
	};

	// Try to enable hotplug interface
	libusb_hotplug_callback_fn hotplug_callback = (context, identity, event, user_data) {
		// TODO
		info("NEW DEVICE EVENT");
		return 0;
	};

	status = register_callback(
		null,
		libusb_hotplug_event.DEVICE_ARRIVED | libusb_hotplug_event.DEVICE_LEFT, 
		//libusb_hotplug_flag.HOTPLUG_NO_FLAGS, 
		libusb_hotplug_flag.HOTPLUG_ENUMERATE,
		LIBUSB_HOTPLUG_MATCH_ANY, LIBUSB_HOTPLUG_MATCH_ANY, LIBUSB_HOTPLUG_MATCH_ANY,
		hotplug_callback, null, null
	);
	if (status != 0) {
		info(
			"Switch to manual polling, because unable to enable LibUSB hotplug interface. " 
			~ to!string(libusb_strerror(status))
		);

		handle_events = null;
	}

	// Use libusb polling of devices
	bool delegate() poll_task;
	if (handle_events) {
		poll_task = () {
			auto status = handle_events(null);
			if (status != 0) {
				error(
					"Unable to receive an USB event. " ~ to!string(libusb_strerror(status))
				);
			}
			return false;
		};
	}

	// Use manual polling of devices
	else {
		// Prepare initial list of connected devices
		libusb_device** identities;
		immutable count = libusb_get_device_list(null, &identities);
		scope (exit) {
			if (identities) {
				libusb_free_device_list(identities, 1);
			}
		}

		for (int i = 0; i < count; i++) {
			auto identity = identities[i];
			auto device = get_device_descriptor(identity);
			DEVICES[device.id] = device;
		}

		// Compare lists of devices in some period
		poll_task = () {
			libusb_device** identities;
			auto count = libusb_get_device_list(null, &identities);
			if (count < 0) {
				error(
					"Unable to retrieve a new list of devices. " 
					~ to!string(libusb_strerror(cast(int)count))
				);
				return false;
			}

			// Detect connected devices
			foreach (identity; identities[0 .. count]) {
				immutable bus_number = (cast(uint)libusb_get_bus_number(identity)) << 8;
				immutable device_id = bus_number + libusb_get_device_address(identity);

				if ((device_id in DEVICES) is null) {
					// Register the device
					auto descriptor = get_device_descriptor(identity);
					DEVICES[descriptor.id] = descriptor;

					// Send USB event
					try {
						info("NEW DEVICE");
						USB_COMPONENT.send(
							HotplugEvent(libusb_hotplug_event.DEVICE_ARRIVED, descriptor)
						);
					}
					catch (Exception e) {
						error("Unable to send USB arrival notifcation. " ~ e.msg);
					}
				}
			}

			// Detect disconnected devices	: Check events are not in race condition.
			info("DETECTING DISCONNECTED DEVICES");			
			foreach (old_device_id; DEVICES.keys) {
				bool is_found = false;
				foreach (identity; identities[0 .. count]) {
					immutable bus_number = (cast(uint)libusb_get_bus_number(identity)) << 8;
					immutable new_device_id = bus_number + libusb_get_device_address(identity);

					if (new_device_id == old_device_id) {
						is_found = true;
						break;
					}
				}

				if (!is_found) {
					// Send USB event
					try {
						info("DEVICE IS DISCONNECTED");
						auto descriptor = DEVICES.get(old_device_id, null);
						USB_COMPONENT.send(
							HotplugEvent(libusb_hotplug_event.DEVICE_LEFT, descriptor)
						);
					}
					catch (Exception e) {
						error("Unable to send USB left notifcation. " ~ e.msg);
					}

					// Unregister the device
					DEVICES.remove(old_device_id);
				}
			}

			libusb_free_device_list(identities, 1);
			return true;
		};
	}

	// Wait for USB events
	try {
		auto task = new Thread(() nothrow {
			while (true) {
				try {
					if (!poll_task()) {
						break;
					}
				}
				catch (Exception e) {
					error(e.msg);
					break;
				}

				Thread.sleep(dur!"msecs"(250));
			}
		});

		task.start();
		task.set_name("USB_LISTENER");
	}
	catch (Exception e) {
		error("Unable to run an USB listener. " ~ e.msg);
		return;
	}
}

/**
 * A USB component on the system bus
 */
protected __gshared Component* USB_COMPONENT;

/**
 * A current list of device addresses
 */
protected __gshared Device*[uint] DEVICES;

/**
 * Represents a USB error
 */
class USBException : Exception {

	/**
	 * Creates the exception.
	 * 
	 * Params:
	 * 		message = an error message
	 */
	this(string message) @safe {
		super(message);
	}

}

extern (System):

/**
 * Represents the type of error
 */
private enum libusb_error {
	/**
	 * Success (no error)
	 */
	SUCCESS = 0,

	/**
	 * Input/output error
	 */
	ERROR_IO = -1,

	/**
	 * Invalid parameter
	 */
	ERROR_INVALID_PARAM = -2,

	/**
	 * Access denied (insufficient permissions)
	 */
	ERROR_ACCESS = -3,

	/**
	 * No such device (it may have been disconnected)
	 */
	ERROR_NO_DEVICE = -4,

	/**
	 * Entity not found
	 */
	ERROR_NOT_FOUND = -5,

	/**
	 * Resource busy
	 */
	ERROR_BUSY = -6,

	/**
	 * Operation timed out
	 */
	ERROR_TIMEOUT = -7,

	/**
	 * Overflow
	 */
	ERROR_OVERFLOW = -8,

	/**
	 * Pipe error
	 */
	ERROR_PIPE = -9,

	/**
	 * System call interrupted (perhaps due to signal)
	 */
	ERROR_INTERRUPTED = -10,

	/**
	 * Insufficient memory
	 */
	ERROR_NO_MEM = -11,

	/**
	 * Operation not supported or unimplemented on this platform
	 */
	ERROR_NOT_SUPPORTED = -12,

	/**
	 * Other error
	 */
	ERROR_OTHER = -99,

}

private /**
 * A set of descriptor types as defined by the USB specification.
 */
enum libusb_descriptor_type : uint8_t {

	/**
	 * A device descriptor.
	 * 
	 * See_Also: libusb_device_descriptor
	 */
	DEVICE = 0x01,

	/**
	 * A configuration descriptor.
	 * 
	 * See_Also: libusb_config_descriptor
	 */
	CONFIG = 0x02,

	/**
	 * A string descriptor
	 */
	STRING = 0x03,

	/**
	 * A descriptor of interface.
	 * 
	 * See_Also: libusb_interface_descriptor
	 */
	INTERFACE = 0x04,

	/**
	 * An endpoint descriptor.
	 * 
	 * See_Also: libusb_endpoint_descriptor
	 */
	ENDPOINT = 0x05,

	/**
	 * BOS descriptor
	 */
	BOS = 0x0f,

	/**
	 * Device Capability descriptor
	 */
	DEVICE_CAPABILITY = 0x10,

	/**
	 * HID descriptor
	 */
	HID = 0x21,

	/**
	 * HID report descriptor
	 */
	REPORT = 0x22,

	/**
	 * Physical descriptor
	 */
	PHYSICAL = 0x23,

	/**
	 * A descriptor of hub
	 */
	HUB = 0x29,

	/**
	 * SuperSpeed Hub descriptor
	 */
	SUPERSPEED_HUB = 0x2a,

	/**
	 * SuperSpeed Endpoint Companion descriptor
	 */
	SS_ENDPOINT_COMPANION = 0x30

}

/**
 * Hotplug events
 */
enum libusb_hotplug_event {

	/**
	 * A device has been plugged in and is ready to use
	 */
	DEVICE_ARRIVED = 0x01,

	/**
	 * A device has left and is no longer available.
	 * It is the user's responsibility to call libusb_close on any handle associated with a disconnected device.
	 * It is safe to call libusb_get_device_descriptor on a device that has left
	 */
	DEVICE_LEFT = 0x02,

}

/**
 * Flags for hotplug events
 */
private enum libusb_hotplug_flag {

	/**
	 * Default value when not using any flags.
	 */
	HOTPLUG_NO_FLAGS = 0,

	/**
	 * Arm the callback and fire it for all matching currently attached devices.
	 */
	HOTPLUG_ENUMERATE = 1 << 0,

}


/**
 * Represents a libusb session
 */
private struct libusb_context;

/**
 * Represents an identity of USB device
 */
private struct libusb_device;

/**
 * Represents a handle of USB device
 */
private struct libusb_device_handle;

/**
 * Represents an USB interface
 */
private struct libusb_interface;

/**
 * Callback handle.
 *
 * Callbacks handles are generated by libusb_hotplug_register_callback()
 * and can be used to deregister callbacks. Callback handles are unique
 * per libusb_context and it is safe to call libusb_hotplug_deregister_callback()
 * on an already deregisted callback.
 */
private alias libusb_hotplug_callback_handle = int;

/**
 * A structure representing the standard USB device descriptor.
 * This descriptor is documented in section 9.6.1 of the USB 3.0 specification.
 * All multiple-byte fields are represented in host-endian format.
 */
private struct libusb_device_descriptor {
	
	/**
	 * A size of this descriptor (in bytes)
	 */
	uint8_t bLength;

	/**
	 * A type of descriptor type.
	 */
	libusb_descriptor_type bDescriptorType = libusb_descriptor_type.DEVICE;

	/** 
	 * USB specification release number in binary-coded decimal.
	 * A value of 0x0200 indicates USB 2.0, 0x0110 indicates USB 1.1, etc.
	 */
	uint16_t bcdUSB;

	/**
	 * USB-IF class code for the device.
	 */
	uint8_t bDeviceClass;

	/**
	 * USB-IF subclass code for the device, qualified by the bDeviceClass value
	 */
	uint8_t bDeviceSubClass;

	/**
	 * USB-IF protocol code for the device, qualified by the bDeviceClass and bDeviceSubClass values
	 */
	uint8_t bDeviceProtocol;

	/**
	 * A maximum packet size for endpoint 0
	 */
	uint8_t bMaxPacketSize0;

	/**
	 * USB-IF vendor ID
	 */
	uint16_t idVendor;

	/**
	 * USB-IF product ID
	 */
	uint16_t idProduct;

	/**
	 * Device release number in binary-coded decimal
	 */
	uint16_t bcdDevice;

	/**
	 * An index of string descriptor describing manufacturer
	 */
	uint8_t iManufacturer;

	/**
	 * An index of string descriptor describing product
	 */
	uint8_t iProduct;

	/**
	 * An index of string descriptor containing device serial number
	 */
	uint8_t iSerialNumber;

	/**
	 * A number of possible configurations
	 */
	uint8_t bNumConfigurations;

}

/**
 * A structure represents the standard USB configuration descriptor.
 * This descriptor is documented in section 9.6.3 of the USB 3.0 specification.
 * All multiple-byte fields are represented in host-endian format.
 */
private struct libusb_config_descriptor {

	/**
	 * A size of this descriptor (in bytes)
	 */
	uint8_t bLength;

	/**
	 * A type of descriptor type.
	 */
	libusb_descriptor_type bDescriptorType = libusb_descriptor_type.CONFIG;

	/**
	 * A total length of data returned for this configuration
	 */
	uint16_t wTotalLength;

	/**
	 * A number of interfaces supported by this configuration
	 */
	uint8_t bNumInterfaces;

	/**
	 * An identifier of this configuration
	 */
	uint8_t bConfigurationValue;

	/**
	 * An index of string descriptor describing this configuration
	 */
	uint8_t iConfiguration;

	/**
	 * Configuration characteristics
	 */
	uint8_t bmAttributes;

	/**
	 * A maximum power consumption of the USB device from this bus in this
	 * configuration when the device is fully operation. Expressed in units
	 * of 2 mA when the device is operating in high-speed mode and in units
	 * of 8 mA when the device is operating in super-speed mode
	 */
	uint8_t MaxPower;

	/**
	 * An array of interfaces supported by this configuration.
	 * The length of this array is determined by the bNumInterfaces field.
	 */
	const libusb_interface* interfaces;

	/**
	 * Extra descriptors.
	 * If libusb encounters unknown configuration descriptors, 
	 * it will store them here, should you wish to parse them
	 */
	const void* extra;

	/**
	 * A length of the extra descriptors, in bytes
	 */
	int extra_length;

}

/**
 *  Log message levels.
 */
private enum libusb_log_level {

	/**
	 * No messages ever printed by the library (default)
	 */
	NONE = 0,

	/**
	 * Error messages are printed to stderr
	 */
	ERROR,

	/**
	 * Warning and error messages are printed to stderr
	 */
	WARNING,

	/**
	 * Informational messages are printed to stdout, 
	 * warning and error messages are printed to stderr
	 */
	INFO,

	/**
	 * Debug and informational messages are printed to stdout,
	 * warnings and errors to stderr
	 */
	DEBUG

}


/**
 * Sets log message verbosity.
 * 
 * Params:
 * 		ctx = a reference to session
 * 		level = a debug level to set
 */
private alias libusb_set_debug = void function(libusb_context* ctx, int level) nothrow @nogc;

/**
 * Initialiazes the libusb library
 * 
 * Params:
 * 		context = a reference to session
 * 
 * Returns: 0 on success, or a LIBUSB_ERROR code on failure
 */
private alias libusb_init = int function(libusb_context** context) nothrow @nogc;

/**
 * Returns the descripton of error.
 * 
 * Params:
 * 	code = an code of error
 */
private __gshared immutable(char)* function(int code) nothrow @nogc libusb_strerror;

/**
 * Returns a list of USB devices currently attached to the system.
 * 
 * Params:
 * 		context = a reference to session
 * 		list = a reference to the list of devices
 * 
 * Returns: the number of devices, or any libusb_error code.
 */
private __gshared ptrdiff_t function(
	libusb_context* ctx, libusb_device*** list
) nothrow @nogc libusb_get_device_list;

/**
 * Increments the reference count of a device.
 * 
 * Params:
 * 		dev = a device identity
 */
private __gshared libusb_device* function(libusb_device* dev) nothrow @nogc libusb_ref_device;

/**
 * Decrements the reference count of a device.
 * 
 * Params:
 * 		dev	= a device identity
 */
private __gshared void function(libusb_device* dev) nothrow @nogc libusb_unref_device;


/**
 * Returns the address of the device on the bus.
 * 
 * Params:
 * 		dev = a device identity
 * 
 * Returns: the address of device
 */
private __gshared uint8_t function(libusb_device* dev) nothrow @nogc libusb_get_device_address;

/**
 * Returns the number of the bus that a device is connected to.
 * 
 * Params:
 * 		dev = a device identity
 * 
 * Returns: the bus number
 */
private __gshared uint8_t function(libusb_device* dev) nothrow @nogc libusb_get_bus_number;

/**
 * Returns the number of port that a device is connected to.
 * 
 * Params:
 * 		dev = a device identity
 * 
 * Returns: the port number
 */
private __gshared uint8_t function(libusb_device* dev) nothrow @nogc libusb_get_port_number;


/**
 * Retrieves an identifier of currently active configuration.
 * 
 * Params:
 * 		dev = a device identity
 * 		config = a reference to identifier
 * 
 * Return: 0 on success, otherwise LIBUSB_ERROR code
 */
private __gshared int function(
	libusb_device_handle* dev, int* config
) nothrow @nogc libusb_get_configuration;	

/**
 * Sets an active configuration for a device.
 * 
 * Params:
 * 		dev = a device identity
 * 		configuration = an identifier of configuration
 * 
 * Return: 0 on success, otherwise LIBUSB_ERROR code
 */
private __gshared int function(
	libusb_device_handle* dev, int configuration
) nothrow @nogc libusb_set_configuration;

/**
 * Frees a list of devices.
 * 
 * Params:
 * 		list = the list to free
 *  	unref_devices = whether to unref the devices in the list
 */
private __gshared void function(
	libusb_device** list, int unref_devices
) nothrow @nogc libusb_free_device_list;

/**
 * Retrieves a USB device descriptor for the specified device.
 * 
 * Params:
 * 		dev = an identifier of device
 */
private __gshared int function(
	libusb_device*	dev, libusb_device_descriptor* desc
) nothrow @nogc libusb_get_device_descriptor;

/**
 * Retrieves a string descriptor for the specified device.
 * 
 * Params:
 * 		dev = a device handle
 * 		desc_index = an index of the descriptor
 * 		data = an output buffer
 * 		length = a size of output buffer
 * 
 * Returns: a number of bytes returned in data, or LIBUSB_ERROR code on failure
 */
private __gshared int function(
	libusb_device_handle* dev, uint8_t desc_index, char* data, int length 
) nothrow @nogc libusb_get_string_descriptor_ascii;

/**
 * Opens an USB device.
 * 
 * Params:
 * 		dev = an identifier of device
 * 		handle = a reference to device handle
 * 
 * Returns: 0 on success, otherwise LIBUSB_ERROR code on failure
 */
private __gshared int function(
	libusb_device* dev, libusb_device_handle** handle
) nothrow @nogc libusb_open;


/**
 * Performs a USB port reset to reinitialize a device.
 * 
 * Params:
 * 		dev = a handle of the device
 * 
 * Returns: 0 on success, otherwise LIBUSB_ERROR code
 */
private __gshared int function(libusb_device_handle* dev) nothrow @nogc libusb_reset_device;


/**
 * Closes an USB device.
 * 
 * Params:
 * 		handle = a device handle
 */
private __gshared void function(libusb_device_handle* dev_handle) nothrow @nogc libusb_close;


/**
 * Clears the halt/stall condition for an endpoint.
 * 
 * Params:
 * 		dev = a device handle
 * 		endpoint = an identifier of endpoint
 * 
 * Returns: 0 on success, otherwise LIBUSB_ERROR code
 */
private __gshared int function(
	libusb_device_handle* dev, ubyte endpoint
) nothrow @nogc libusb_clear_halt;

/**
 * Claim an interface on a given device handle.
 * 
 * Params:
 * 		dev = a device handle
 * 		interface_number = an index of the interface
 * 
 * Returns: 0 on success, otherwise LIBUSB_ERROR code
 */
private __gshared int function(
	libusb_device_handle* dev, int interface_number
) nothrow @nogc libusb_claim_interface;

/**
 * Release an interface previously claimed.
 * 
 * Params
 * 		dev = a device handle
 * 		interface_number = an index of the interface
 * 
 * Returns: 0 on success, otherwise LIBUSB_ERROR code
 */
private __gshared int function(
	libusb_device_handle* dev, int interface_number 
) nothrow @nogc libusb_release_interface;

/**
 * Endpoint direction. Values for bit 7 of the libusb_endpoint_descriptor::bEndpointAddress
 * "endpoint address" scheme.
 */
enum libusb_endpoint_direction {
	/**
	 * In: device-to-host
	 */
	IN = 0x80,

	/**
	 * Out: host-to-device
	 */
	OUT = 0x00
}

/**
 * Perform a USB bulk transfer.
 * 
 * Params:
 * 		dev_handle = a handle for the USB device
 * 		endpoint = an address of endpoint
 * 		data = a buffer for either input or output (depending on endpoint)
 * 
 * 		length = for bulk writes, the number of bytes from data to be sent;
 * 			for bulk reads, the maximum number of bytes to receive into the data buffer
 *
 * 		transferred	= an output location for the number of bytes actually transferred
 * 
 * 		timeout = a timeout (in millseconds) that this function should wait 
 * 			before giving up due to no response being received. For an unlimited timeout, use value 0.
 * 
 * Returns: 0 on success (and populates transferred), otherwise LIBUSB_ERROR code
 */
private __gshared int function(
	libusb_device_handle* dev_handle, ubyte endpoint,
	void* data, int length, int* transferred, uint timeout 
) nothrow @nogc libusb_bulk_transfer;

/**
 * Retrieves a USB configuration descriptor based on its index.
 * 
 * Params:
 * 		dev = a device identity
 * 		index = an index of the configuration
 * 		config = a reference to USB configuration descriptor
 * 
 * Returns: 0 on success, otherwise LIBUSB_ERROR code
 */
private __gshared int function(
	libusb_device* dev, uint8_t index, libusb_config_descriptor** config 
) nothrow @nogc libusb_get_config_descriptor;

/**
 * Retrieves a current USB configuration descriptor.
 * 
 * Params:
 * 		dev = a device identity
 * 		config = a reference to USB configuration descriptor
 * 
 * Returns: 0 on success, otherwise LIBUSB_ERROR code
 */
private __gshared int function(
	libusb_device* dev, libusb_config_descriptor** config
) nothrow @nogc libusb_get_active_config_descriptor;

/**
 * Free a configuration descriptor.
 * 
 * Params:
 * 		config = the configuration descriptor to free
 */
private __gshared void function(
	libusb_config_descriptor* config
) nothrow @nogc libusb_free_config_descriptor;


/**
 * Wildcard matching for hotplug events. 
 */
enum LIBUSB_HOTPLUG_MATCH_ANY = -1;	

/**
 * Hotplug callback function type. When requesting hotplug event notifications,
 * you pass a pointer to a callback function of this type.
 *
 * This callback may be called by an internal event thread and as such it is
 * recommended the callback do minimal processing before returning.
 *
 * libusb will call this function later, when a matching event had happened on
 * a matching device. See \ref hotplug for more information.
 *
 * It is safe to call either libusb_hotplug_register_callback() or
 * libusb_hotplug_deregister_callback() from within a callback function.
 *
 * Params:
 * 		ctx = a context of this notification
 * 		device = an identity of device
 * 		event = a type of event that occurred
 * 		user_data = user data provided when this callback was registered
 * 
 * Returns: 1 will cause this callback to be deregistered
 */
private alias libusb_hotplug_callback_fn = int function(
	libusb_context* ctx, libusb_device* device, libusb_hotplug_event event, void* user_data
) nothrow;

/**
 * Registers a hotplug callback function.
 * 
 * Params:
 * 		ctx = an optional libusb context
 * 		events = a bitwise or of events that will trigger this callback
 * 		flags = a hotplug callback flags.
 * 		vendor_id = an optional identifier vendor to match
 * 		product_id = an optional identifier of product to match
 * 		dev_class = an optional device class to match
 * 
 * Returns: LIBUSB_SUCCESS on success LIBUSB_ERROR code on failure
 */
private alias libusb_hotplug_register_callback = int function(
	libusb_context* ctx, libusb_hotplug_event events, libusb_hotplug_flag flags,
	int vendor_id, int product_id, int dev_class, 
	libusb_hotplug_callback_fn cb_fn, void* user_data,
	libusb_hotplug_callback_handle* handle 
) nothrow @nogc;

/**
 * Handle any pending events in blocking mode.
 * 
 * Params:
 * 		ctx = an optional libusb context
 */
private alias libusb_handle_events = int function(libusb_context* ctx) nothrow @nogc;
