/**
 * This module exports a configuration for application.
 * 
 * Copyright: WolfCom Enterprises, Inc.
 * Authors: Eugene Vatulin <yevhen@softcontrol.net>
 */
module config;

private import util.log : fatal, info, error;
private import std.stdio : File;
private import std.conv : ConvException, to;
private import std.algorithm : startsWith;
private import std.array : split;
private import std.string : format, strip, toUpper;


enum APP_NAME = "WolfCom Evidence Management Solution";
enum APP_ICON = 101;	// An identifier of icon in the resource file
enum USER_AGENT = "WolfCom Evidence Management Solution";

enum FFMPEG_SPEED = "3";

/**
 * A static table of supported devices with following keys: $(UL
 *    $(LI Vendor - an identifier of vendor (VID))
 *    $(LI Product - an identifier of product (PID))
 *    $(LI Type - a type of device (common device interface or device storage))
 *  )
 */
enum SUPPORTED_DEVICES = [
	["Vendor": 0x4255, "Product": 0x1, "Type": cast(int)DEVICE_TYPE.GENERIC],			// common device interface
	["Vendor": 0x4255, "Product": 0x1000, "Type": cast(int)DEVICE_TYPE.STORAGE],		// device storage
];

enum SUPPORTED_HUBS = [
	["Vendor": 0x1A40]
];

enum DEVICE_TYPE {
	UNKNOWN = 0,
	GENERIC = 1,
	STORAGE = 2,
}


/**
 * Represents a container with options.
 */
struct Config {

	/**
	 * A database connection string
	 */
	string storage_dsn;

	string app_host;

}

/**
 * Returns a default configuration for the application.
 */
Config* get_default_config() nothrow {
	return &CONFIGURATION;
}

/**
 * An instance of current configuration.
 */
protected __gshared Config CONFIGURATION;

/**
 * Parses the default configuration of application.
 * 
 * Params:
 * 		config = a configuration file
 */
void load_default_config(File config) {
	import std.string : indexOf;

	auto line_number = 0;
	foreach (line; config.byLine()) {
		auto trimmed_line = line.strip();
		line_number++;

		// Skip comments and empty lines
		if (!trimmed_line.length || trimmed_line.startsWith('#') || trimmed_line.startsWith(';')) {
			continue;
		}

		// Ignore section name
		if (trimmed_line.startsWith('[')) {
			continue;
		}

		// Extract key/value
		auto separator_index = trimmed_line.indexOf("=");
		if (separator_index < 1) {
			fatal(format("Unrcognized line %d", line_number));
		}

		// Parse the option
		auto option = to!string(trimmed_line[0 .. separator_index].strip());
		auto value = trimmed_line[(separator_index + 1) .. $].strip().dup;

		switch (option.toUpper()) {
			case "ER_DSN":
				CONFIGURATION.storage_dsn = to!string(value);
				break;

			case "APP_HOST":
				CONFIGURATION.app_host = to!string(value);
				break;

			default:
				fatal(format("Unknown option %s (line %d)", option, line_number));
		}
	}
}
