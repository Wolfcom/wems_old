/**
 * This module implements a system bus that connects different components of application.
 * 
 * License: $(WEB www.opensource.org/licenses/mit-license.php, MIT).
 * Authors: Eugene Vatulin <zenixan@gmail.com>
 */
module net.bus;

private import core.sync.mutex : Mutex;
private import core.sync.rwmutex : ReadWriteMutex;
private import core.sync.condition : Condition;

private import std.variant : Variant;
private import util.queue : CircularQueue;


/**
 * Represents an unit of bus
 */
struct Component {

	/**
	 * Indicates a new message in the data bus
	 */
	protected Condition has_messages;

	/**
	 * A queue of messages in the data bus
	 */
	protected CircularQueue!(Variant*) messages;

	/**
	 * Checks whether the current component is active.
	 * 
	 * Returns: true if the component is active, otherwise false
	 */
	bool delegate() is_alive;

	/**
	 * Returns the network address of current component;
	 */
	string delegate() address;

	/**
	 * Sends a message to the current component.
	 * 
	 * Params:
	 * 		message = a message to send
	 */
	void send(T)(T message) {
		synchronized (this.has_messages.mutex) {
			this.messages.push(new Variant(message));
			this.has_messages.notifyAll();
		}
	}

	/**
	 * Waits for a message related to the current component.
	 * 
	 * Params:
	 * 		T = a type of message to receive
	 * 
	 * Returns: a message from the queue of data bus
	 */
	T receive(T)()	{
		synchronized (this.has_messages.mutex) {
			Variant*[] buffer;
			do {
				// Wait for new message
				if (!this.messages.length) {
					// Push back messages that doesn't match with requested type
					foreach (item; buffer) {
						this.messages.push(item);
					}

					this.has_messages.wait();
				}

				auto message = this.messages.pop();
				if (message.type == typeid(T)) {
					// Push back messages that doesn't match with requested type
					foreach (item; buffer) {
						this.messages.push(item);
					}

					return message.get!T;
				}

				buffer ~= message;
			} while(true);
		}
	}

}

/**
 * Attaches a new component to the bus.
 * 
 * Params:
 * 		name = a name of unit
 * 
 * Returns: the descriptor of new unit
 */
Component* attach(string name) {
	auto component = new Component;
	component.has_messages = new Condition(new Mutex);

	synchronized (lock.writer) {
		if ((name in components) !is null) {
			throw new Exception("The specified unit of bus was created already");
		}

		components[name] = component;
	}
	return component;
}

/**
 * Discovers the unit of bus by the specified name.
 * 
 * Params:
 * 		name = a name of unit
 * 
 * Returns: the descriptor of unit
 */
Component* locate(string name) {
	synchronized (lock.reader) {
		if (auto unit = name in components) {
			auto component = *unit;
			if (component.is_alive && component.is_alive()) {
				return component;
			}
		}
	}
	return null;
}

/**
 * A protection lock for list of components
 */
private __gshared ReadWriteMutex lock;

/**
 * A list of registered units
 */
private __gshared Component*[string] components;


/**
 * Initializes the system bus
 */
shared static this() {
	lock = new ReadWriteMutex;
}
