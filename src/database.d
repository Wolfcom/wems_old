/**
 * This module exports a set of communication functions with PostgreSQL database.
 */
module database;

import config :  get_default_config;
import util.log : info, error;

import core.thread : Thread;
import std.file : exists;
import std.stdio : stdin, stdout, stderr;
import std.process : spawnProcess, wait, Config;


enum PG_CTL_PATH = "pgsql/bin/pg_ctl.exe";

/**
 * A size of video chunk to encrypt
 */
enum CHUNK_SIZE = 4096;


/**
 * Imports the specified file as a large object
 */
Oid lo_import(PGconn* connection, string filename) {
	import std.string : toStringz;
	return lo_import(connection, filename.toStringz());
}

/**
 * Implements container for database process
 */
class Database : Thread {

	/**
	 * Creates the thread
	 */
	public this() {
        super(() {
			try {
				run();
			}
			catch (Exception e) {
				error("Unable to start database: " ~ e.msg);
			}
		});
    }

	/**
	 * Stops the database server and realeses resources
	 */
	public void stop() {
		if (!exists(PG_CTL_PATH)) {
			return;
		}

		auto command = [
			PG_CTL_PATH, "stop", "-m", "fast",	"-D", "data/pgsql", 
			"-l", "data/pgsql/logs/log.txt"
		];

		auto pid = spawnProcess(
			command, stdin, stdout, stderr, null, Config.suppressConsole
		);
		wait(pid);

		info("Database Thread is stopped");
	}

	/**
	 * Starts the database server
	 */
	protected void run() {
		if (!exists(PG_CTL_PATH)) {
			return;
		}

		auto command = [
			PG_CTL_PATH, "start", "-D", "data/pgsql", "-l", "data/pgsql/logs/log.txt"
		];

		auto pid = spawnProcess(
			command, stdin, stdout, stderr, null, Config.suppressConsole
		);
		wait(pid);

		info("Database Thread is started");
	}

	/**
	 * Returns the connection to the database
	 */
	public static PGconn* get_connection() {
		import std.conv : to;
		import std.string : toStringz;

		auto cfg = get_default_config();
		auto connection = PQconnectdb(cfg.storage_dsn.toStringz());
		if ((connection is null) || PQstatus(connection) != ConnStatusType.OK) {
			string details;
			if (connection !is null) {
				details = to!string(PQerrorMessage(connection));
			}

			throw new Exception(
				"Unable to make connection with database: " ~ details
			);
		}
		return connection;
	}

}

extern (C):

/**
 * Object ID is a fundamental type in Postgres.
 */
alias uint Oid;

/**
 * Read access
 */
enum INV_READ = 0x00040000;

/**
 * Write access
 */
enum INV_WRITE = 0x00020000;

/**
 * PGconn encapsulates a connection to the backend.
 * The contents of this struct are not supposed to be known to applications.
 */
alias void* PGconn;

/**
 * PGresult encapsulates the result of a query (or more precisely, of a single
 * SQL command --- a query string given to PQsendQuery can contain multiple
 * commands and thus return multiple PGresult objects).
 * The contents of this struct are not supposed to be known to applications.
 */
alias void* PGresult;

/**
 * Represents the status of connection
 */
enum ConnStatusType {

	OK,
	BAD,

	// Non-blocking mode only below here

	/**
	 * The existence of these should never be relied upon - they should only
	 * be used for user feedback or similar purposes.
	 * Waiting for connection to be made.
	 */
	STARTED,

	/**
	 * Connection OK; waiting to send.
	 */
	MADE,

	/**
	 * Waiting for a response from the postmaster.
	 */
	AWAITING_RESPONSE,

	/**
	 * Received authentication; waiting for backend startup.
	 */
	AUTH_OK,

	/**
	 * Negotiating environment.
	 */
	SETENV,

	/**
	 * Negotiating SSL.
	 */
	SSL_STARTUP,

	/**
	 * Internal state: connect() needed
	 */
	NEEDED

}

enum ExecStatusType {

	/**
	 * Empty query string was executed
	 */
	EMPTY_QUERY = 0,

	/**
	 * A query command that doesn't return anything was executed properly by the backend
	 */
	COMMAND_OK,

	/**
	 * A query command that returns tuples was executed properly by the backend,
	 * PGresult contains the result tuples
	 */
	TUPLES_OK,

	/**
	 * Copy Out data transfer in progress
	 */
	COPY_OUT,

	/**
	 * Copy In data transfer in progress
	 */
	COPY_IN,

	/**
	 * An unexpected response was recv'd from the backend
	 */
	BAD_RESPONSE,

	/**
	 * Notice or warning message
	 */
	NONFATAL_ERROR,

	/**
	 * A query failed
	 */
	FATAL_ERROR,

	/**
	 * Copy In/Out data transfer in progress
	 */
	COPY_BOTH,

	/**
	 * A tuple from larger resultset
	 */
	SINGLE_TUPLE

}


/**
 * Makes a new synchronous connection to the database server.
 */
PGconn* PQconnectdb(immutable(char)* conninfo);

/**
 * Returns the last error message
 */
char* PQerrorMessage(PGconn* connection);

/**
 * Returns the error message associated with the command
 */
char* PQresultErrorMessage(PGresult* result);

/**
 * Checks the status of connected.
 */
ConnStatusType PQstatus(PGconn* connection);

/**
 * Performs a simple synchronous query.
 */
PGresult* PQexec(PGconn* connection, immutable(char)* query);

/**
 * Checks the status of executed query.
 */
ExecStatusType PQresultStatus(PGresult* result);

/**
 * Escapes binary data for use within an SQL command with the type bytea.
 */
char* PQescapeByteaConn(
	PGconn* connection, ubyte* from, size_t from_length, size_t* to_length
);

/**
 * Returns the number of tuples in the result set
 */
int	PQntuples(PGresult* result);

/**
 * Returns the number of rows affected by the SQL command.
 */
char* PQcmdTuples(PGresult* result);

/**
 * Returns a value of field for specified row
 */
char* PQgetvalue(PGresult* result, int row, int field);

/**
 * Delete a PGresult
 */
void PQclear(PGresult* result);

/**
 * Close the current connection and free the PGconn data structure.
 */
void PQfinish(PGconn* connection) nothrow;

/**
 * Creates a new large object.
 */
Oid lo_create(PGconn* connection, int mode);

/**
 * Open an existing large object for reading or writing.
 */
int lo_open(PGconn* connection, Oid lobjId, int mode);

/**
 * Writes length bytes from buffer (which must be of size length) to large object descriptor.
 */
int lo_write(PGconn* connection, int fd, ubyte* buf, size_t length);

/**
 * Imports the specified file as a large object
 */
Oid lo_import(PGconn* connection, immutable(char)* filename);

/**
 * Closes the opened large object
 */
int lo_close(PGconn* connection, int fd);

/**
 * Removes the specified large object
 */
int lo_unlink(PGconn* connection, Oid lobjId);
