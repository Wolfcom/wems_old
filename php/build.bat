@echo off
cd %~dp0

:: Search D installation
for %%i in (C:\Program Files\D\dmd2,D:\D\dmd2) do (
	if exist %%i (
		set "D_PATH=%%i"
	)
)

if "%D_PATH%" == "" (
	echo Unable to find installation of D toolchain
	exit /b 1
)

:: Prepare compilation parameters
set DEBUG=0
set PATH=%D_PATH%\windows\bin;%PATH%

set LIBS=lib\WolfcomVisionLib.lib lib\php7ts.lib 
set LIBS=%LIBS% lib\kernel32.lib lib\shell32.lib lib\uuid.lib


if "%DEBUG%" == "1" (
	set FLAGS=-Isrc -debug -m64 -w -vtls -g -gc -gs -gx
) else (
	set FLAGS=-Isrc -release -m64 -w -vtls -g
)

:: Compile PHP modules
set MODULES=
for %%F in (src\php\*.d) do (
	echo Compile PHP module: %%~nF
	dmd -c %FLAGS% -odbuild\php %%F
	call :list.append MODULES build\php\%%~nF.obj
)

:: Compile extension modules
for %%F in (src\*.d) do (
	echo Compile extension: %%~nF
	dmd -c %FLAGS% -odbuild %%F
	call :list.append MODULES build\%%~nF.obj
)


:: Build executable
echo Build PHP extension
rem ..\tools\rcc.exe src\main.rc -32 -obuild\main.res
rem dmd %FLAGS% -shared -odbuild -ofphp_camera.dll %SRC% %MODULES% src\main.def build\main.res %LIBS%

:: ulink %MODULES% %LIBS%
dmd %FLAGS% -shared -odbuild -ofphp_camera.dll %MODULES% %LIBS%
exit /b

::
:: Adds the specified element to the list
::
:: Arguments:
::		%1 = the reference to list
::		%2 = the element to add
::
:list.append
	if defined %~1 (
		call set "%~1=%%%~1%% %~2"
	) else (
		set "%~1=%~2"
	)
	goto :eof
