/**
 * Implements an exported functions of PHP extension
 *
 * License: $(WEB www.opensource.org/licenses/mit-license.php, MIT).
 * Authors: Eugene Vatulin
 */
module camera;

private import config;
private import php.api;
private import std.variant : Variant;
private import core.time : dur;

mixin PHP_MODULE!camera;

/**
 * Returns the current settings of device
 */
private DeviceSettings* fetch_device_settings(VISION_HANDLE* handle = null) {
	import std.conv : to;

	// Try to connect to the device
	auto vision_device = GetUsbDevice();
	if (handle) {
		*handle = vision_device;
	}

	scope (exit) {
		if (vision_device && !handle) ReleaseUsbDevice(vision_device);
	}

	if (!vision_device) {
		zend_throw_exception("Unable to establish a connection with device");
		return null;
	}

	// Try to fetch device settings
	DeviceSettings* settings = new DeviceSettings;
	auto device_result = GetDeviceSetup(
		vision_device, cast(void*)settings, DeviceSettings.sizeof
	);

	if (device_result < 1) {
		zend_throw_exception("Unable to fetch a device settings");
		return null;
	}

	// Convert timezone to minutes offset
	auto second_digit = cast(char)(settings.timezone >> 8);
	auto first_digit = cast(char)(settings.timezone & 0xFF);
	auto timezone_idx = to!int("" ~ first_digit ~ second_digit);

	if (timezone_idx < 0 || timezone_idx > (DeviceSettings.TIMEZONE.length - 1)) {
		enum UTC = DeviceSettings.TIMEZONE[9];
		settings.timezone = cast(short)UTC.total!"minutes";
	}
	else {
		settings.timezone = cast(short)DeviceSettings.TIMEZONE[timezone_idx].total!"minutes";
	}

	return settings;
}

/**
 * Returns the settings of camera
 */
@PHP_FUNCTION
extern(C) void get_camera_settings(zend_execute_data* execute_data, zval* return_val) {
	auto settings = fetch_device_settings();
	if (!settings) {
		return;
	}

	create_array(settings.to_array(), return_val);
}

/**
 * Returns the badge identifier of camera
 */
@PHP_FUNCTION
extern(C) void get_camera_badge_id(zend_execute_data* execute_data, zval* return_val) {
	import std.conv : to;

	// Try to connect to the device
	auto vision_device = GetUsbDevice();
	scope (exit) {
		if (vision_device) ReleaseUsbDevice(vision_device);
	}

	if (!vision_device) {
		zend_throw_exception("Unable to establish a connection with device");
		return;
	}

	// Try to fetch badge ID
	char[255] buffer;
	auto result = GetDevicePoliceNo(vision_device, buffer.ptr, buffer.length);
	if (result < 1) {
		zend_throw_exception("Unable to fetch a badge ID");
		return;
	}

	create_string(to!string(buffer.ptr), return_val);
}

/**
 * Sets the new settings of camera
 */
@PHP_FUNCTION
extern(C) void set_camera_settings(zend_execute_data* execute_data, zval* return_val) {
	import std.conv : to;
	import std.algorithm : countUntil;

	zval* raw_settings;
	char* badge_id;
	int length;
	if (zend_parse_parameters(zend_num_args(execute_data), "as", &raw_settings, &badge_id, &length) == FAILURE) {
		return;
	}

	// Try to fetch current settings
	VISION_HANDLE vision_device;
	scope (exit) {
		if (vision_device) ReleaseUsbDevice(vision_device);
	}

	auto device_settings = fetch_device_settings(&vision_device);
	if (!device_settings) {
		return;
	}

	// Prepare new settings
	foreach (key, value; raw_settings.to_array()) {
		// Ignore non-string keys
		if (key.type != typeid(string)) {
			zend_throw_exception("Unknown device property: " ~ key.toString());
			return;
		}

		// Locate the property and set the new value
		auto property_name = key.get!string;
		int propety_index = -1;

		foreach (field_index, ref field_value; device_settings.tupleof) {
			static if (__traits(getProtection, DeviceSettings.tupleof[field_index]) == "public") {
				enum field_name = __traits(identifier, DeviceSettings.tupleof[field_index]);
				enum error_message = __traits(getAttributes, DeviceSettings.tupleof[field_index])[0];

				if (field_name == property_name) {
					propety_index = field_index;

					// Special calculation for timezone of device
					static if (field_name == "timezone") {
						try {
							long timezone;
							if (value.type == typeid(long)) {
								timezone = value.get!long;
							}
							else {
								timezone = to!long(value.get!string);
							}

							auto timezone_idx = countUntil(DeviceSettings.TIMEZONE, dur!"minutes"(timezone));
							field_value = cast(short)(
								(((timezone_idx / 10) + '0')) + (((timezone_idx % 10) + '0')  << 8)
							);
						}
						catch (Exception e) {
							zend_throw_exception(error_message);
							return;
						}
					}

					// Set usual fields
					else {
						try {
							field_value = to!(typeof(DeviceSettings.tupleof[field_index]))(value.get!string);
						}
						catch (Exception e) {
							zend_throw_exception(error_message);
							return;
						}
					}

					break;
				}
			}
		}

		if (propety_index == -1) {
			zend_throw_exception("Unknown device property: " ~ property_name);
			return;
		}
	}

	// Try to apply a new settings
	if (SetDeviceTime(vision_device) < 1) {
		zend_throw_exception("Unable to set a new time");
		return;
	}

	if (SetDevicePoliceNo(vision_device, badge_id) < 1) {
		zend_throw_exception("Unable to set a badge id");
		return;
	}

	if (SetDeviceSetup(vision_device, cast(void*)device_settings) < 1) {
		zend_throw_exception("Unable to update a device settings");
		return;
	}
}


/**
 * Vendor-specific API
 */
extern (C) {

	alias void* VISION_HANDLE;

	/**
	 * Represents a container of settings string
	 */
	struct DeviceSettings {
	align(1):

		enum VIDEO_RESOLUTION {
			FPS_30_1920x1080 = '0',
			FPS_60_1280x720 = '1',
			FPS_30_1280x720 = '2',
			FPS_60_848x480 = '3',
			FPS_30_848x480 = '4',
			FPS_30_640x480 = '5'
		}

		enum TIMEZONE = [
			dur!"hours"(-8), dur!"hours"(-7), dur!"hours"(-6), dur!"hours"(-5),
			dur!"hours"(-4), dur!"minutes"(-210), dur!"hours"(-3), dur!"hours"(-2),
			dur!"hours"(-1), dur!"hours"(0), dur!"hours"(1), dur!"hours"(2),
			dur!"hours"(3), dur!"minutes"(210), dur!"hours"(4), dur!"minutes"(270),
			dur!"hours"(5), dur!"minutes"(330), dur!"hours"(6), dur!"minutes"(390),
			dur!"hours"(7), dur!"hours"(8), dur!"hours"(9), dur!"minutes"(570),
			dur!"hours"(10), dur!"minutes"(630), dur!"hours"(11),
			dur!"minutes"(690), dur!"hours"(12)
		];

		enum COMPRESSION_RATE {
			SUPER = '0',
			FINE = '1',
			NORMAL = '2'
		}

		enum SWITCH {
			NO = '0',
			YES = '1'
		}

		enum REVERSE_SWITCH {
			YES = '0',
			NO = '1'
		}

		enum INDICATOR {
			FLASH = '0',
			OFF = '1',
			ON = '2'
		}

		enum DEVICE_MODE {
			NORMAL = '0',
			PRE_RECORD = '1',
			EASY = '2'
		}

		enum VIDEO_FORMAT {
			MOV = '0',
			MP4 = '1'
		}

		enum RECYCLE {
			MIN_30 = '0',
			MIN_10 = '1',
			MIN_5 = '2',
			MIN_3 = '3',
			MIN_1 = '1'
		}

		enum PHOTO_RESOLUTION {
			MPX_16 = '0',
			MPX_12 = '1',
			MPX_8 = '2',
			MPX_5 = '3',
			MPX_3 = '4'
		}

		enum TOUCH_RECORD {
			NORMAL = '0',
			ADVANCE = '1'
		}

		/**
		 * Unused switch: password switch (0 - OFF, 1 - ON)
		 */
		private byte has_password = '0';

		/**
		 * A resolution of video
		 */
		@("Invalid video resolution")
		VIDEO_RESOLUTION video_resolution;

		/**
		 * A compression rate of video
		 */
		@("Invalid compression rate")
		COMPRESSION_RATE compression_rate;

		/**
		 * Audio record	(0 - YES, 1 - NO)
		 */
		@("Invalid state fro audio record")
		REVERSE_SWITCH audio_record;

		/**
		 * Recycle record (30 min, 10 min, 5 min, 3 min, 1 min)
		 */
		@("Invalid time for recycle")
		RECYCLE recycle_record;

		/**
		 * Loop record (0 - YES, 1 - NO)
		 */
		@("Invalid state for loop record")
		REVERSE_SWITCH loop_record;

		/**
		 * Photo resolution	(16M, 12M, 8M, 5M, 3M)
		 */
		@("Invalid photo resolution")
		PHOTO_RESOLUTION photo_resolution;

		/**
		 * Public awareness indicator
		 */
		@("Invalid state of awareness indicator")
		INDICATOR indicator;

		/**
		 * Date & time stamp (0 - NO, 1 - YES)
		 */
		@("Invalid state for date & time stamp")
		SWITCH datestamp;

		/**
		 * Unused switch: default setting (0 - NO, 1 - YES)
		 */
		private byte default_setting = '0';

		/**
		 * A mode of device
		 */
		@("Invalid device mode")
		DEVICE_MODE mode;

		/**
		 * Format memory of device (0 - No, 1 - Yes)
		 */
		private byte format_device = '0';

		/**
		 * A format of video file
		 */
		@("Invalid video format")
		VIDEO_FORMAT video_format;

		/**
		 * A GPS stamp of pictures
		 */
		@("Invalid state for GPS stamp")
		SWITCH gps_stamp;

		/**
		 * Pre-record audio	(0 - ON, 1 - OFF)
		 */
		@("Invalid state for pre-record audio")
		REVERSE_SWITCH pre_record_audio;

		/**
		 * One Touch Record
		 */
		@("Invalid state of one touch record")
		TOUCH_RECORD touch_record;

		/**
		 * A time zone of device
		 */
		@("Invalid time zone")
		short timezone;

		/**
		 * Padding byte
		 */
		private byte zero_byte;

		/**
		 * Converts the current settings to the associative array
		 */
		Variant[string] to_array() {
			import std.conv : to;

			Variant[string] result;
			foreach (index, value; this.tupleof) {
				static if (__traits(getProtection, this.tupleof[index]) == "public") {
					enum key = __traits(identifier, this.tupleof[index]);

					static if (is(typeof(this.tupleof[index]) == enum)) {
						result[key] = to!string(value);
					}
					else {
						result[key] = value;
					}
				}
			}

			return result;
		}

	}

	/**
	 * Get an opaque handle to a Vision device.
	 */
	VISION_HANDLE GetUsbDevice() nothrow;

	/**
	 * Release the handle to a Vision device.
	 */
	void ReleaseUsbDevice(VISION_HANDLE handle) nothrow;

	/**
	 * Checks if the device is connected
	 */
	int IsDeviceInserted(VISION_HANDLE handle);

	/**
	 * Returns the current settings of device.
	 */
	int GetDeviceSetup(VISION_HANDLE handle, void* buffer, int length) nothrow;

	/**
	 * Updates the current settings of device.
	 */
	int SetDeviceSetup(VISION_HANDLE handle, void* buffer) nothrow;

	/**
	 * Sets the current time as device time
	 */
	int SetDeviceTime(VISION_HANDLE handle) nothrow;

	/**
	 * Returns the current device police (badge) number.
	 */
	int GetDevicePoliceNo(VISION_HANDLE handle, char* buffer, int length) nothrow;

	/**
	 * Updates the current device police (badge) number.
	 */
	int SetDevicePoliceNo(VISION_HANDLE handle, char* buffer) nothrow;

}
