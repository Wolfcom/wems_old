/**
 * Represents a set of internal PHP data types
 */
module php.types;

private import std.stdint;
private import php.ast;


/// Define basic types
alias ubyte zend_bool;
alias char zend_uchar;
alias uint zend_uint;
alias int64_t zend_long;
alias uint64_t zend_ulong;


/// regular data types
enum IS_UNDEF = 0;
enum IS_NULL = 1;
enum IS_FALSE = 2;
enum IS_TRUE = 3;
enum IS_LONG = 4;
enum IS_DOUBLE = 5;
enum IS_STRING = 6;
enum IS_ARRAY = 7;
enum IS_OBJECT = 8;
enum IS_RESOURCE = 9;
enum IS_REFERENCE = 10;

/// constant expressions
enum IS_CONSTANT = 11;
enum IS_CONSTANT_AST = 12;


/**
 * Represents the desctructor of container
 */
private alias void function(zval* pDest) dtor_func_t;

/**
 * Represents a counter of references fro GC
 */
struct zend_refcounted_h {

	/**
	 * A reference counter 32-bit
	 */
	uint32_t refcount;

	union _u {
		struct _v {
			version (BigEndian) {
				/**
				 * Keeps GC root number (or 0) and color
				 */
				uint16_t gc_info;

				/**
				 * Used for strings & objects
				 */
				zend_uchar flags;

				zend_uchar type;
			}
			else {
				zend_uchar type;

				/**
				 * Used for strings & objects
				 */
				zend_uchar flags;

				/**
				 * Keeps GC root number (or 0) and color
				 */
				uint16_t gc_info;
			}
		} _v v;

		uint32_t type_info;
	} _u u;
}

struct zend_refcounted {
	zend_refcounted_h gc;
}

/**
 * A main container of PHP variable
 */
struct zval {
	union _value {
		zend_long lval;
		double dval;
		zend_refcounted* counted;
		zend_string* str;
		zend_array* arr;
		zend_object* obj;
		zend_resource* res;
		zend_reference* _ref;
		zend_ast_ref* ast;
		zval* zv;
		void* ptr;
		zend_class_entry* ce;
		zend_function* func;

		struct _ww {
			uint32_t w1;
			uint32_t w2;
		} _ww ww;
	} _value value;

	union _u1 {
		struct _v {
			version (BigEndian) {
				/**
				 * Call info for EX(This)
				 */
				zend_uchar reserved;

				zend_uchar const_flags;
				zend_uchar type_flags;

				/**
				 * Active type
				 */
				zend_uchar type;
			}
			else {
				/**
				 * Active type
				 */
				zend_uchar type;

				zend_uchar type_flags;
				zend_uchar const_flags;

				/**
				 * Call info for EX(This)
				 */
				zend_uchar reserved;
			}
		} _v v;

		uint32_t type_info;
	} _u1 u1;

	union _u2 {

		uint32_t var_flags;

		/**
		 * A hash collision chain
		 */
		uint32_t next;

		/**
		 * A literal cache slot
		 */
		uint32_t cache_slot;

		/**
		 * A number of line (for ast nodes)
		 */
		uint32_t lineno;

		/**
		 * A number of arguments for EX(This)
		 */
		uint32_t num_args;

		/**
		 * A foreach position
		 */
		uint32_t fe_pos;

		/**
		 * foreach iterator index
		 */
		uint32_t fe_iter_idx;

		/**
		 * A class constant access flags
		 */
		uint32_t access_flags;

	} _u2 u2;

}


/**
 * Represents a PHP string
 */
struct zend_string {

	zend_refcounted_h gc;

	/**
	 * A hash value
	 */
	zend_ulong h;

	size_t len;
	char[1] val;

}

alias uint32_t HashPosition;

/**
 * Represents a PHP array.
 * 
 * HashTable Data Layout
 * =====================
 *
 *                 +=============================+
 *                 | HT_HASH(ht, ht->nTableMask) |
 *                 | ...                         |
 *                 | HT_HASH(ht, -1)             |
 *                 +-----------------------------+
 * ht->arData ---> | Bucket[0]                   |
 *                 | ...                         |
 *                 | Bucket[ht->nTableSize-1]    |
 *                 +=============================+
 */
struct zend_array {

	zend_refcounted_h gc;

	union {
		static struct v {
			version (BigEndian) {
				zend_uchar reserve;
				zend_uchar nIteratorsCount;
				zend_uchar nApplyCount;
				zend_uchar flags;				
			}
			else {
				zend_uchar flags;
				zend_uchar nApplyCount;
				zend_uchar nIteratorsCount;
				zend_uchar reserve;
			}
		}

		uint32_t flags;
	}

	uint32_t nTableMask;
	Bucket* arData;
	uint32_t nNumUsed;
	uint32_t nNumOfElements;
	uint32_t nTableSize;
	uint32_t nInternalPointer;
	zend_long nNextFreeElement;
	dtor_func_t pDestructor;

}

/**
 * Represents an element from the container
 */
struct Bucket {

	zval val;

	/**
	 * A hash value (or numeric index)
	 */
	zend_ulong h;
	
	/**
	 * A string key or NULL for numerics
	 */
	zend_string* key;

}

/**
 * Represents a descriptor of resource
 */
struct zend_resource {
	zend_refcounted_h gc;
	int handle;
	int type;
	void* ptr;
}

/**
 * Represents a PHP reference
 */
struct zend_reference {
	zend_refcounted_h gc;
	zval val;
}

/**
 * Represents a reference to the AST node
 */
struct zend_ast_ref {
	zend_refcounted_h gc;
	zend_ast* ast;
}

/**
 * Represents a PHP object
 */
struct zend_object {
	zend_refcounted_h gc;
	uint32_t handle;

	zend_class_entry* ce;
	const zend_object_handlers* handlers;

	zend_array* properties;
	zval[1] properties_table;
}

/**
 * Represents a state of function
 */
struct zend_execute_data {

	/**
	 * Executed opline
	 */
	const zend_op* opline;

	/**
	 * A current call
	 */
	zend_execute_data* call;

	/**
	 * A value to return from function
	 */
	zval* return_value;

	/**
	 * Executed funcrion
	 */
	zend_function* func;

	/**
	 * this + call_info + num_args
	 */
	zval This;

	zend_class_entry* called_scope;
	zend_execute_data* prev_execute_data;
	zend_array* symbol_table;

	version (ZEND_EX_USE_RUN_TIME_CACHE) {
		/**
		 * A cache of op_array->run_time_cache
		 */
		void** run_time_cache;
	}

	version (ZEND_EX_USE_LITERALS) {
		/**
		 * A cache of op_array->literals
		 */
		zval* literals;
	}
}

struct zend_op;
struct zend_class_entry;
private struct zend_object_handlers;
private union zend_function;
