/**
 * This module provides a set of structures for the Abstract Syntax Tree
 */
module php.ast;

private import std.stdint;


/// Common types
alias uint16_t zend_ast_attr;


enum ZEND_AST_SPECIAL_SHIFT = 6;
enum ZEND_AST_IS_LIST_SHIFT = 7;
enum ZEND_AST_NUM_CHILDREN_SHIFT = 8;

/**
 * A type of AST node
 */
enum zend_ast_kind : uint16_t {
	/* special nodes */
	ZVAL = 1 << ZEND_AST_SPECIAL_SHIFT,
	ZNODE,

	/* declaration nodes */
	FUNC_DECL,
	CLOSURE,
	METHOD,
	CLASS,

	/* list nodes */
	ARG_LIST = 1 << ZEND_AST_IS_LIST_SHIFT,
	LIST,
	ARRAY,
	ENCAPS_LIST,
	EXPR_LIST,
	STMT_LIST,
	IF,
	SWITCH_LIST,
	CATCH_LIST,
	PARAM_LIST,
	CLOSURE_USES,
	PROP_DECL,
	CONST_DECL,
	CLASS_CONST_DECL,
	NAME_LIST,
	TRAIT_ADAPTATIONS,
	USE,

	/* 0 child nodes */
	MAGIC_CONST = 0 << ZEND_AST_NUM_CHILDREN_SHIFT,
	TYPE,

	/* 1 child node */
	VAR = 1 << ZEND_AST_NUM_CHILDREN_SHIFT,
	CONST,
	UNPACK,
	UNARY_PLUS,
	UNARY_MINUS,
	CAST,
	EMPTY,
	ISSET,
	SILENCE,
	SHELL_EXEC,
	CLONE,
	EXIT,
	PRINT,
	INCLUDE_OR_EVAL,
	UNARY_OP,
	PRE_INC,
	PRE_DEC,
	POST_INC,
	POST_DEC,
	YIELD_FROM,

	GLOBAL,
	UNSET,
	RETURN,
	LABEL,
	REF,
	HALT_COMPILER,
	ECHO,
	THROW,
	GOTO,
	BREAK,
	CONTINUE,

	/* 2 child nodes */
	DIM = 2 << ZEND_AST_NUM_CHILDREN_SHIFT,
	PROP,
	STATIC_PROP,
	CALL,
	CLASS_CONST,
	ASSIGN,
	ASSIGN_REF,
	ASSIGN_OP,
	BINARY_OP,
	GREATER,
	GREATER_EQUAL,
	AND,
	OR,
	ARRAY_ELEM,
	NEW,
	INSTANCEOF,
	YIELD,
	COALESCE,

	STATIC,
	WHILE,
	DO_WHILE,
	IF_ELEM,
	SWITCH,
	SWITCH_CASE,
	DECLARE,
	USE_TRAIT,
	TRAIT_PRECEDENCE,
	METHOD_REFERENCE,
	NAMESPACE,
	USE_ELEM,
	TRAIT_ALIAS,
	GROUP_USE,

	/* 3 child nodes */
	METHOD_CALL = 3 << ZEND_AST_NUM_CHILDREN_SHIFT,
	STATIC_CALL,
	CONDITIONAL,

	TRY,
	CATCH,
	PARAM,
	PROP_ELEM,
	CONST_ELEM,

	/* 4 child nodes */
	FOR = 4 << ZEND_AST_NUM_CHILDREN_SHIFT,
	FOREACH

}

/**
 * Represents an Abstract Syntax Tree
 */
struct zend_ast {

	/**
	 * A type of the node
	 */
	zend_ast_kind kind;

	/**
	 * Additional attribute, use depending on node type
	 */
	zend_ast_attr attr;

	/**
	 * A number of line 
	 */
	uint32_t lineno;

	/**
	 * Array of children (using struct hack)
	 */
	zend_ast*[1] child;

}
