module php.memory;

extern (C):

/**
 * Allocates a memory block for specified size
 */
pragma(mangle, "_emalloc@@8")
void* _emalloc(size_t size);
/// ditto
alias _emalloc emalloc;
