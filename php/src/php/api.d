module php.api;

private import php.ast;
public import php.types;
public import php.memory;

private import std.string : toStringz;
private import std.variant : Algebraic, Variant;
private import std.traits : Parameters, ParameterIdentifierTuple, isAssignable,
	isBoolean, isIntegral, isFloatingPoint, PointerTarget, hasUDA;

/// A function statuses
enum SUCCESS = 0;
enum FAILURE = -1;

/// User defined attributes
enum PHP_FUNCTION;
enum PHP_MINIT_FUNCTION;
enum PHP_MSHUTDOWN_FUNCTION;

/**
 * Declare an interface of PHP module
 */
mixin template PHP_MODULE(alias MODULE) {

	/**
	 * Define a module descriptor
	 */
	extern(C) __gshared static zend_module_entry module_entry;

	/**
	 * Defines a list of exported functions
	 */
	extern(C) __gshared static zend_function_entry[255] module_entry_functions;

	/**
	 * Publish the PHP module
	 */
	pragma(mangle, "get_module")
	extern(C) @nogc export static zend_module_entry* zif_get_module() nothrow {
		// Try to initialize a module descriptor
		if (!module_entry.size) {
			// Common settings
			module_entry.size = zend_module_entry.sizeof;
			module_entry.zend_api = PHP_MODULE_API;
			module_entry.zend_debug = ZEND_DEBUG;
			module_entry.zts = USING_ZTS;

			// Module name and version
			module_entry.name = EXT_NAME;
			module_entry._version = EXT_VERSION;
			module_entry.build_id = ZEND_MODULE_BUILD_ID;

			// Initialize module callbacks
			module_entry.module_startup_func = &zif_module_startup;
			module_entry.module_shutdown_func = &zif_module_shutdown;

			// Initialize a list of exported functions
			auto index = 0;
			foreach (MEMBER; __traits(allMembers, MODULE)) {
				// Ignore all private symbols
				static if (
					__traits(compiles, __traits(getProtection, mixin(__traits(identifier, MODULE) ~ "." ~ MEMBER))) &&
					__traits(getProtection, mixin(__traits(identifier, MODULE) ~ "." ~ MEMBER)) == "public" &&
					hasUDA!(mixin(__traits(identifier, MODULE) ~ "." ~ MEMBER), PHP_FUNCTION)
				) {
					module_entry_functions[index] = zend_function_entry(
						MEMBER, mixin("&" ~ __traits(identifier, MODULE) ~ "." ~ MEMBER), null,
						cast(zend_uint)(zend_arg_info.sizeof / zend_arg_info.sizeof - 1),
						0
					);
					index++;
				}
			}
			module_entry.functions = module_entry_functions.ptr;
		}

		return &module_entry;
	}

	/**
	 * Called when module is initialiazed
	 */
	extern(C) int zif_module_startup(INIT_FUNC_ARGS) {
		import core.runtime : Runtime;
		Runtime.initialize();

		foreach (MEMBER; __traits(allMembers, MODULE)) {
			// Ignore all private symbols
			static if (
				__traits(compiles, __traits(getProtection, mixin(__traits(identifier, MODULE) ~ "." ~ MEMBER))) &&
				__traits(getProtection, mixin(__traits(identifier, MODULE) ~ "." ~ MEMBER)) == "public" &&
				hasUDA!(mixin(__traits(identifier, MODULE) ~ "." ~ MEMBER), PHP_MINIT_FUNCTION)
			) {
				MEMBER();
			}
		}

		return 1;
	}

	/**
	 * Called when module is destroyed
	 */
	extern(C) int zif_module_shutdown(SHUTDOWN_FUNC_ARGS) {
		import core.runtime : Runtime;

		foreach (MEMBER; __traits(allMembers, MODULE)) {
			// Ignore all private symbols
			static if (
				__traits(compiles, __traits(getProtection, mixin(__traits(identifier, MODULE) ~ "." ~ MEMBER))) &&
				__traits(getProtection, mixin(__traits(identifier, MODULE) ~ "." ~ MEMBER)) == "public" &&
				hasUDA!(mixin(__traits(identifier, MODULE) ~ "." ~ MEMBER), PHP_MSHUTDOWN_FUNCTION)
			) {
				MEMBER();
			}
		}

		Runtime.terminate();
		return 1;
	}

	version(Windows) {
		import core.sys.windows.windows;

		/**
		 * An entry point for linker
		 */
		extern (Windows) BOOL DllMain(HINSTANCE hInstance, ULONG ulReason, LPVOID pvReserved) {
			return true;
		}
	}
	else {
		// TODO: POSIX
	}

}

/**
 * Throws an exception with specified message and code
 */
void zend_throw_exception(string message, long code = 0) {
	zend_throw_exception(zend_exception_get_default(), message.toStringz(), code);
}

/**
 * Returns true if the specified data is an array
 */
bool is_array(zval* data) pure nothrow {
	if (data) {
		return cast(byte)(data.u1.type_info) == IS_ARRAY;
	}
	else {
		return false;
	}
}

/**
 * Returns true if the specified data is a string
 */
bool is_string(zval* data) pure nothrow {
	if (data) {
		return cast(byte)(data.u1.type_info) == IS_STRING;
	}
	else {
		return false;
	}
}

/**
 * Returns true if the specified data is a long number
 */
bool is_long(zval* data) pure nothrow {
	if (data) {
		return cast(byte)(data.u1.type_info) == IS_LONG;
	}
	else {
		return false;
	}
}

alias PHP_ARRAY_KEY = Algebraic!(zend_long, string);
alias PHP_ARRAY_VALUE = Algebraic!(zend_long, string);

/**
 * Converts the PHP string to the usual string
 */
string to_string(zval* data) nothrow {
	import std.conv : to;

	if (is_string(data)) {
		return to!string(data.value.str.val.ptr);
	}
	else {
		return null;
	}
}

/**
 * Converts the PHP array to the associative array
 */
PHP_ARRAY_VALUE[PHP_ARRAY_KEY] to_array(zval* data) {
	import std.conv : to;

	// Checks whether the specified data is array
	PHP_ARRAY_VALUE[PHP_ARRAY_KEY] array;
	if (!is_array(data)) {
		auto message = "Invalid array. Found type: " ~ to!string(data.u1.type_info);
		zend_throw_exception(message);
		return array;
	}

	// Iterate through specified array
	HashPosition pointer;
	zval key;
	zval* value;

	for (
		zend_hash_internal_pointer_reset_ex(data.value.arr, &pointer);
		(value = zend_hash_get_current_data_ex(data.value.arr, &pointer)) != null;
		zend_hash_move_forward_ex(data.value.arr, &pointer)
	) {
		zend_hash_get_current_key_zval_ex(data.value.arr, &key, &pointer);

		// Detect type of key
		PHP_ARRAY_KEY index;
		if (key.u1.type_info == IS_LONG) {
			index = PHP_ARRAY_KEY(key.value.lval);
		}
		else {
			index = PHP_ARRAY_KEY(to!string(key.value.str.val.ptr));
		}

		// Detect type of value
		if (is_string(value)) {
			array[index] = to!string(value.value.str.val.ptr);
		}

		else if (is_long(value)) {
			array[index] = value.value.lval;
		}
		
		else {
			auto message = "Unsupported type: " ~ to!string(value.u1.type_info);
			zend_throw_exception(message);
			return array.init;
		}
	}

	return array;
}

/**
 * Returns the number of function arguments
 */
auto zend_num_args(zend_execute_data* execute_data) pure nothrow {
	if (execute_data) {
		return execute_data.This.u2.num_args;
	}
	else {
		return 0;
	}
}

/**
 * Creates the PHP array from associative array
 */
void create_array(T)(T[string] source, zval* result) {
	array_init(result, 0);
	foreach (string key, value; source) {
		static if (isBoolean!(typeof(value))) {
			add_assoc_bool(result, key, value);
		}
		else static if (isIntegral!(typeof(value))) {
			add_assoc_long(result, key, value);
		}
		else static if (isFloatingPoint!(typeof(value))) {
			add_assoc_double(result, key, value);
		}
		else static if (is(typeof(value) == string)) {
			add_assoc_string(result, key, value);
		}

		else static if (is(typeof(value) == Variant)) {
			if (value.type == typeid(bool)) {
				add_assoc_bool(result, key, value.get!bool);
			}

			else if (
				value.type == typeid(byte) || value.type == typeid(short) ||
				value.type == typeid(int) || value.type == typeid(long)
			) {
				add_assoc_long(result, key, value.get!long);
			}

			else if (value.type == typeid(float) || value.type == typeid(double)) {
				add_assoc_double(result, key, value.get!double);
			}
			else if (value.type == typeid(int[string])) {
				zval tmp;
				create_array(value.get!(int[string]), &tmp);
				add_assoc_zval(result, key, &tmp);
			}
			else if (value.type == typeid(string)) {
				auto string = value.get!string;
				add_assoc_string(result, key, string);
			}
			else {
				auto message = "Unsupported type " ~ value.type.toString() 
					~ " for field " ~ key;
				zend_throw_exception(message);
				return;
			}
		}

		else {
			static assert(true, "Unsupported type: " ~ typeof(value));
		}
	}
}

/**
 * Creates the PHP string
 */
zend_string* create_string(string str, zval* result = null) {
	import core.stdc.string : strncpy;

	auto buffer = cast(zend_string*)emalloc(zend_string.sizeof + str.length);
	buffer.gc.refcount = 1;
	buffer.gc.u.v.type = IS_STRING;

	buffer.h = zend_hash_func(str.ptr, str.length);
	buffer.len = str.length;
	strncpy(buffer.val.ptr, str.ptr, str.length);

	if (result) {
		result.value.str = buffer;
		result.u1.type_info = IS_STRING;
	}
	return buffer;
}

extern (C):

/// Defined default macroses
alias Parameters!(void function(zend_execute_data* execute_data, zval* return_value)) INTERNAL_FUNCTION_PARAMETERS;

alias Parameters!(void function(int type, int module_number)) INIT_FUNC_ARGS;
alias Parameters!(void function(int type, int module_number)) SHUTDOWN_FUNC_ARGS;
alias Parameters!(void function(zend_module_entry* zend_module)) ZEND_MODULE_INFO_FUNC_ARGS;

mixin template ZEND_INI_MH(string NAME) {
	int NAME(
		zend_ini_entry* entry, immutable(char)* new_value, uint new_value_length, 
		void* mh_arg1, void* mh_arg2, void* mh_arg3, int stage
	);
}

/**
 * Represents a configuration from INI file
 */
struct zend_ini_entry {

	int module_number;
	int modifiable;
	immutable(char)* name;
	uint name_length;
	mixin ZEND_INI_MH!("on_modify");

	void* mh_arg1;
	void* mh_arg2;
	void* mh_arg3;

	immutable(char)* value;
	uint value_length;

	immutable(char)* orig_value;
	uint orig_value_length;
	int orig_modifiable;
	int modified;

	void function(zend_ini_entry* ini_entry, int type) displayer;

};

/**
 * Represents a PHP module
 */
struct zend_module_entry {

	ushort size;
	uint zend_api;
	char zend_debug;
	char zts;

	zend_ini_entry* ini_entry;
	zend_module_dep* deps;
	immutable(char)* name;
	zend_function_entry* functions;

	int function(INIT_FUNC_ARGS) module_startup_func;
	int function(SHUTDOWN_FUNC_ARGS) module_shutdown_func;
	int function(INIT_FUNC_ARGS) request_startup_func;
	int function(SHUTDOWN_FUNC_ARGS) request_shutdown_func;
	void function(ZEND_MODULE_INFO_FUNC_ARGS) info_func;

	/**
	 * A version of module
	 */
	immutable(char)* _version;
	size_t globals_size;

	version (ZTS) {
		ts_rsrc_id* globals_id_ptr;
	}
	else {
		void* globals_ptr;
	}

	void function(void* global) globals_ctor;
	void function(void* global) globals_dtor;
	int function() post_deactivate_func;

	int module_started;
	char type;
	void* handle;
	int module_number;
	immutable(char)* build_id;

}

/**
 * Reprensents a module dependency
 */
struct zend_module_dep {

	/**
	 * A module name
	 */
	immutable(char)* name;

	/**
	 * Version relationship: NULL (exists), lt|le|eq|ge|gt (to given version)
	 */
	immutable(char)* rel;

	/**
	 * A module version
	 */
	immutable(char)* _version;

	/**
	 * Dependency type
	 */
	char type;

}

/**
 * Define a descriptor of function argument
 */
struct zend_arg_info {

	immutable(char)* name;
	zend_uint name_len;
	immutable(char)* class_name;
	zend_uint class_name_len;
	zend_uchar type_hint;
	zend_uchar pass_by_reference;
	zend_bool allow_null;
	zend_bool is_variadic;

}

/**
 * Define a function descriptor
 */
struct zend_function_entry {
	immutable(char)* fname;
	void function(INTERNAL_FUNCTION_PARAMETERS) handler;
	zend_arg_info* arg_info;
	zend_uint num_args;
	zend_uint flags;
}


/**
 * Returns the version for specified module
 */
immutable(char)* zend_get_module_version(immutable(char)* module_name) nothrow;

/**
 * Parses the list of function arguments
 */
int zend_parse_parameters(int num_args, immutable(char)* type_spec, ...) nothrow;

/**
 * Creates the array with specified size
 */
pragma(mangle, "_array_init")
int array_init(zval* arg, uint size) nothrow;

/**
 * Sets a boolean value for specified key in the array
 */
int add_assoc_bool_ex(zval* arg, immutable(char)* key, size_t key_len, int b) nothrow;
/// ditto
int add_assoc_bool(zval* arg, string key, bool value) nothrow {
	return add_assoc_bool_ex(arg, key.toStringz(), key.length, value);
}

/**
 * Sets an integer value for specified key in the array
 */
int add_assoc_long_ex(zval* arg, immutable(char)* key, size_t key_len, long n) nothrow;
/// ditto
int add_assoc_long(zval* arg, string key, long value) nothrow {
	return add_assoc_long_ex(arg, key.toStringz(), key.length, value);
}

/**
 * Sets a float-point value for specified key in the array
 */
int add_assoc_double_ex(zval* arg, immutable(char)* key, size_t key_len, double d) nothrow;
/// ditto
int add_assoc_double(zval* arg, string key, double value) nothrow {
	return add_assoc_double_ex(arg, key.toStringz(), key.length, value);
}

/**
 * Sets a string value for specified key in the array
 */
int add_assoc_string_ex(
	zval* arg, immutable(char)* key, size_t key_len, immutable(char)* str, int duplicate
) nothrow;
/// ditto
int add_assoc_string(zval* arg, string key, string value) nothrow {
	return add_assoc_string_ex(arg, key.toStringz(), key.length, value.toStringz(), 1);
}

/**
 * Sets a PHP variable for specified key in the array
 */
int add_assoc_zval_ex(zval* arg, immutable(char)* key, size_t key_len, zval* value) nothrow;
/// ditto
int add_assoc_zval(zval* arg, string key, zval* value) nothrow {
	return add_assoc_zval_ex(arg, key.toStringz(), key.length, value);
}

/**
 * Returns the number of elements in the array
 */
uint zend_array_count(zend_array* array) nothrow;

/**
 * Calculates the hash code of string
 */
zend_ulong zend_hash_func(const char* str, size_t len) nothrow;

/**
 * Sets the internal pointer of an array to its first element
 */
pragma(mangle, "zend_hash_internal_pointer_reset_ex@@16")
void zend_hash_internal_pointer_reset_ex(zend_array* array, HashPosition* pos) nothrow;

/**
 * Advances the internal array pointer of an array
 */
pragma(mangle, "zend_hash_move_forward_ex@@16")
int zend_hash_move_forward_ex(zend_array* array, HashPosition* pos) nothrow;

/**
 * Returns the current element in an array
 */
pragma(mangle, "zend_hash_get_current_data_ex@@16")
zval* zend_hash_get_current_data_ex(zend_array* array, HashPosition* pos) nothrow;

/**
 * Returns the current key in an array
 */
pragma(mangle, "zend_hash_get_current_key_zval_ex@@24")
void zend_hash_get_current_key_zval_ex(zend_array* array, zval* key, HashPosition* pos) nothrow;


/**
 * Returns an instance of Exception class
 */
zend_class_entry* zend_exception_get_default();

/**
 * Throws an exception with specified message and code
 */
zval* zend_throw_exception(zend_class_entry* exception, immutable(char)* message, long code);
