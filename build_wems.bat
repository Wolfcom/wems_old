@echo off
cd %~dp0

:: Search D installation
for %%i in (C:\Program Files\D\dmd2,D:\D\dmd2,C:\D\dmd2) do (
	if exist %%i (
		set "D_PATH=%%i"
	)
)

if "%D_PATH%" == "" (
	echo Unable to find installation of D toolchain
	exit /b 1
)

:: Prepare compilation parameters
set DEBUG=1
set PATH=%D_PATH%\windows\bin;%PATH%
set SRC=src\app.d src\camera.d src\utils.d src\database.d src\web.d src\media.d src\sdk.d src\config.d
set SRC=%SRC% src\util\log.d src\util\thread.d src\util\queue.d src\util\runtime.d src\util\runtime_common.d
set SRC=%SRC% src\device\usb.d src\net\bus.d
set LIBS=lib\setupapi.lib build\libpq.lib lib\cfgmgr32.lib


if "%DEBUG%" == "1" (
	set FLAGS=-Isrc -debug -m32 -w -vtls -g -gc -gs -gx -version=ANSI
) else (
	set FLAGS=-Isrc -release -m32 -w -vtls -g -version=ANSI
)


:: Build executable
echo Build application
tools\rcc.exe src\app.rc -32 -obuild\app.res

if "%DEBUG%" == "1" (
	tools\implib.exe /noi /system build\libpq.lib distrib\debug\libpq.dll
	dmd %FLAGS% -odbuild -ofdistrib\debug\wolfcom.exe %SRC% %MODULES% src\app.def build\app.res %LIBS%
) else (
	tools\implib.exe /noi /system build\libpq.lib distrib\release\libpq.dll
	dmd %FLAGS% -odbuild -ofdistrib\release\wolfcom.exe %SRC% %MODULES% src\app.def build\app.res %LIBS%
)
exit /b

::
:: Adds the specified element to the list
::
:: Arguments:
::		%1 = the reference to list
::		%2 = the element to add
::
:list.append
	if defined %~1 (
		call set "%~1=%%%~1%% %~2"
	) else (
		set "%~1=%~2"
	)
	goto :eof
