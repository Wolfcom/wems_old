--
-- PostgreSQL database dump
--


SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET search_path = public, pg_catalog;
SET default_tablespace = '';
SET default_with_oids = false;


--- Install extensions
CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;
COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';

CREATE EXTENSION IF NOT EXISTS pgcrypto WITH SCHEMA public;
COMMENT ON EXTENSION pgcrypto IS 'cryptographic functions';

CREATE EXTENSION IF NOT EXISTS "uuid-ossp" WITH SCHEMA public;
COMMENT ON EXTENSION "uuid-ossp" IS 'generate universally unique identifiers (UUIDs)';

CREATE EXTENSION IF NOT EXISTS citext WITH SCHEMA public;
COMMENT ON EXTENSION citext IS 'data type for case-insesetive string comparisons';


--- A set of application permissions
CREATE TYPE privilege_type AS(
    read_audio boolean,
    read_picture boolean,
    read_video boolean,
    delete_audio boolean,
    delete_picture boolean,
    delete_video boolean,
    create_user boolean,
    delete_user boolean,
    create_group boolean,
    delete_group boolean,
    update_audio boolean,
    update_picture boolean,
    update_video boolean,
    update_user boolean,
    update_group boolean,
    module_cases boolean,
    module_favorites boolean,
    module_bookmarks boolean,
    module_images boolean,
    module_videos boolean,
    module_files boolean,
    module_profile boolean,
    module_users boolean,
    module_groups boolean,
    module_cameras boolean,
    module_settings boolean,
    password_profile boolean,
    activate_user boolean,
    report_user boolean,
    permissions_group boolean,
    basic_search boolean,
    advanced_search boolean,
    user_search boolean,
    download_video boolean,
    download_audio boolean,
    download_picture boolean,
    read_report boolean,
    download_report boolean,
    snapshot_video boolean,
    crop_video boolean,
    map_video boolean,
    create_bookmark boolean,
    delete_bookmark boolean,
    read_bookmark boolean,
    update_bookmark boolean,
    global_permissions boolean
);

ALTER TYPE privilege_type OWNER TO postgres;
COMMENT ON TYPE privilege_type IS 'a set of application priveleges (action/module)';

-- Represents an identifier of security privilege
CREATE TYPE action AS ENUM (
    'view_video',
    'view_picture',
    'view_audio',
    'update_user_password',
    'delete_audio',
    'delete_picture',
    'delete_video',
    'update_picture',
    'update_audio',
    'update_video',
    'delete_group',
    'update_group',
    'create_group',
    'update_group_permissions',
    'activate_user',
    'delete_user',
    'update_user',
    'create_user',
    'download_picture',
    'download_audio',
    'download_video',
    'create_audio',
    'create_picture',
    'crop_video',
    'upload_picture',
    'upload_audio',
    'upload_video',
    'delete_favorite',
    'add_favorite',
    'update_bookmark',
    'delete_bookmark',
    'add_bookmark'
);

ALTER TYPE action OWNER TO postgres;

--- A type of email address
CREATE DOMAIN email AS character varying COLLATE pg_catalog."default";
ALTER DOMAIN email OWNER TO postgres;
COMMENT ON DOMAIN email IS 'An email address';

--- Represents a login of user
CREATE DOMAIN username AS citext COLLATE pg_catalog."default"
	CONSTRAINT username_check CHECK (length(VALUE::text) > 1 AND length(VALUE::text) < 200 AND VALUE ~ '^[A-Za-z0-9][A-Za-z0-9]+$'::citext);

ALTER DOMAIN username OWNER TO postgres;
COMMENT ON DOMAIN username IS 'represents a login of user';


--- A type of device
CREATE TYPE device_type AS ENUM  ('VISION', 'THIRD_EYE');
ALTER TYPE device_type OWNER TO postgres;
COMMENT ON TYPE device_type IS 'a type of camera device';

--- a set of allowed devic statuses
CREATE TYPE device_status AS ENUM ('ONLINE', 'SYNC', 'OFFLINE');
ALTER TYPE device_status OWNER TO postgres;
COMMENT ON TYPE device_status IS 'a set of allowed device statuses';

--- Represents time and related GPS coordinates
CREATE TYPE time_point AS (
	"time" time without time zone,
	coordinate point
);
ALTER TYPE time_point OWNER TO postgres;

---
--- Functions
---
CREATE OR REPLACE FUNCTION enum_delete(enum_name character varying, enum_elem character varying, enum_schema character varying DEFAULT 'public'::character varying)
  RETURNS void AS
$BODY$
DECLARE
    type_oid INTEGER;
    rec RECORD;
    sql VARCHAR;
    ret INTEGER;
    schemaoid INTEGER;
BEGIN
    SELECT oid INTO schemaoid FROM pg_namespace WHERE nspname = enum_schema;
    IF NOT FOUND THEN
    RAISE EXCEPTION 'Could not find schema ''%''', enum_schema;
    END IF;
    SELECT pg_type.oid
    FROM pg_type
    WHERE typtype = 'e' AND typname = enum_name AND typnamespace = schemaoid
    INTO type_oid;
    IF NOT FOUND THEN
        RAISE EXCEPTION 'Cannot find a enum: %', enum_name;
    END IF;
    -- Check column DEFAULT value references.
    SELECT *
    FROM
        pg_attrdef
        JOIN pg_attribute ON attnum = adnum AND atttypid = type_oid
        JOIN pg_class ON pg_class.oid = attrelid
        JOIN pg_namespace ON pg_namespace.oid = relnamespace
    WHERE
        adsrc = quote_literal(enum_elem) || '::' || quote_ident(enum_name)
    LIMIT 1
    INTO rec;
    IF FOUND THEN
        RAISE EXCEPTION
            'Cannot delete the ENUM element %.%: column %.%.% has DEFAULT value of ''%''',
            quote_ident(enum_name), quote_ident(enum_elem),
            quote_ident(rec.nspname), quote_ident(rec.relname),
            rec.attname, quote_ident(enum_elem);
    END IF;
    -- Check data references.
    FOR rec IN
        SELECT *
        FROM
            pg_attribute
            JOIN pg_class ON pg_class.oid = attrelid
            JOIN pg_namespace ON pg_namespace.oid = relnamespace
        WHERE
            atttypid = type_oid
            AND relkind = 'r'
    LOOP
        sql :=
            'SELECT 1 FROM ONLY '
            || quote_ident(rec.nspname) || '.'
            || quote_ident(rec.relname) || ' '
            || ' WHERE '
            || quote_ident(rec.attname) || ' = '
            || quote_literal(enum_elem)
            || ' LIMIT 1';
        EXECUTE sql INTO ret;
        IF ret IS NOT NULL THEN
            RAISE EXCEPTION
                'Cannot delete the ENUM element %.%: column %.%.% contains references',
                quote_ident(enum_name), quote_ident(enum_elem),
                quote_ident(rec.nspname), quote_ident(rec.relname),
                rec.attname;
        END IF;
    END LOOP;
    -- OK. We may delete.
    DELETE FROM pg_enum WHERE enumtypid = type_oid AND enumlabel = enum_elem;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;


--
-- TOC entry 197 (class 1255 OID 17277)
-- Name: array_search(anyelement, anyarray); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION array_search(needle anyelement, haystack anyarray) RETURNS integer
    LANGUAGE sql STABLE
    AS $_$
    SELECT i
      FROM generate_subscripts($2, 1) AS i
     WHERE $2[i] = $1
  ORDER BY i
$_$;


ALTER FUNCTION public.array_search(needle anyelement, haystack anyarray) OWNER TO postgres;

--
-- TOC entry 198 (class 1255 OID 18587)
-- Name: delete_large_object(); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION delete_large_object() RETURNS trigger
    LANGUAGE plpgsql
    AS $$BEGIN
	IF (TG_OP = 'DELETE') THEN
		PERFORM lo_unlink(OLD.content);
	END IF;

	RETURN NULL; -- result is ignored since this is an AFTER trigger
END;
$$;


ALTER FUNCTION public.delete_large_object() OWNER TO postgres;

--
-- TOC entry 2113 (class 0 OID 0)
-- Dependencies: 198
-- Name: FUNCTION delete_large_object(); Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON FUNCTION delete_large_object() IS 'Removes an unused large object';


--
-- TOC entry 196 (class 1255 OID 16388)
-- Name: lo_size(oid); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION lo_size(oid) RETURNS bigint
    LANGUAGE plpgsql STRICT
    AS $_$
DECLARE
    fd integer;
    sz bigint;
BEGIN
    -- Open the LO; N.B. it needs to be in a transaction otherwise it will close immediately.
    -- Luckily a function invocation makes its own transaction if necessary.
    -- The mode x'40000'::int corresponds to the PostgreSQL LO mode INV_READ = 0x40000.
    fd := lo_open($1, x'40000'::int);
    -- Seek to the end.  2 = SEEK_END.
    PERFORM lo_lseek(fd, 0, 2);
    -- Fetch the current file position; since we're at the end, this is the size.
    sz := lo_tell(fd);
    -- Remember to close it, since the function may be called as part of a larger transaction.
    PERFORM lo_close(fd);
    -- Return the size.
    RETURN sz;
END;
$_$;


ALTER FUNCTION public.lo_size(oid) OWNER TO postgres;


--
-- TABLES
--

--- System log table
CREATE TABLE activity (
  id uuid NOT NULL, -- a primary key
  user_id uuid, -- an optional identifier of user who has been cause this action
  action action NOT NULL, -- an identifier of action
  created timestamp(1) with time zone NOT NULL DEFAULT now(), -- a date/time of event
  user_ip inet, -- an IP address of user
  entity_id uuid, -- an optional entity related to this event
  entity_type regclass, -- a type of entity related to this event
  source_id uuid, -- an original entity of new entity for copy operations
  source_type regclass, -- a type of source entity
  CONSTRAINT activity_pkey PRIMARY KEY (id)
) WITH (OIDS=FALSE);

ALTER TABLE activity OWNER TO postgres;
CREATE INDEX activity_entity_id_idx ON activity USING btree (entity_id);
CREATE INDEX activity_user_id_idx ON activity USING btree (user_id);

COMMENT ON TABLE activity IS 'A list of log entries';
COMMENT ON COLUMN activity.id IS 'a primary key';
COMMENT ON COLUMN activity.user_id IS 'an optional identifier of user who has been cause this action';
COMMENT ON COLUMN activity.action IS 'an identifier of action';
COMMENT ON COLUMN activity.created IS 'a date/time of event';
COMMENT ON COLUMN activity.user_ip IS 'an IP address of user';
COMMENT ON COLUMN activity.entity_id IS 'an optional entity related to this event';
COMMENT ON COLUMN activity.entity_type IS 'a type of entity related to this event';
COMMENT ON COLUMN activity.source_id IS 'an original entity of new entity for copy operations';
COMMENT ON COLUMN activity.source_type IS 'a type of source entity';


--
-- A list of audio recordings
--
CREATE TABLE audios (
  id uuid NOT NULL, -- an primary identifier
  created timestamp(0) with time zone NOT NULL, -- a date and time when audio recording was created
  duration interval(0), -- a duration of audio recording in seconds
  device_id character varying(16) NOT NULL, -- an identifier of device
  user_id uuid NOT NULL, -- an identifier of device user
  content oid NOT NULL, -- a Large Object with audio content in WEBM format
  modified timestamp(0) with time zone, -- a last date and time when audio record was modified
  description text, -- an optional description of audio recording
  caseno character varying(32), -- an optional identifier of case
  title character varying(200), -- an optional title of file
  CONSTRAINT audios_pkey PRIMARY KEY (id)
) WITH ( OIDS=FALSE );

ALTER TABLE audios OWNER TO postgres;
CREATE INDEX audios_user_id_idx ON audios USING btree (user_id);


CREATE TRIGGER delete_audio_file
  AFTER DELETE ON audios
  FOR EACH ROW EXECUTE PROCEDURE delete_large_object();
  
COMMENT ON TABLE audios IS 'A list of audio recordings';
COMMENT ON COLUMN audios.id IS 'an primary identifier';
COMMENT ON COLUMN audios.created IS 'a date and time when audio recording was created';
COMMENT ON COLUMN audios.duration IS 'a duration of audio recording in seconds';
COMMENT ON COLUMN audios.device_id IS 'an identifier of device';
COMMENT ON COLUMN audios.user_id IS 'an identifier of device user';
COMMENT ON COLUMN audios.content IS 'a Large Object with audio content in WEBM format';
COMMENT ON COLUMN audios.modified IS 'a last date and time when audio record was modified';
COMMENT ON COLUMN audios.description IS 'an optional description of audio recording';
COMMENT ON COLUMN audios.caseno IS 'an optional identifier of case';


--
-- A list of bookmarks
--
CREATE TABLE bookmarks (
  id uuid NOT NULL, -- a primary key
  video_id uuid NOT NULL, -- an identifier of video recording
  name character varying(200) NOT NULL, -- a name of bookmark
  description text, -- a details of bookmark
  start interval(0) NOT NULL, -- a start time in the video recording
  user_id uuid NOT NULL, -- an owner of bookmark
  thumbnail bytea, -- a content of video snapshot
  created timestamp(0) with time zone NOT NULL DEFAULT now(), -- a date/time of bookmark creation
  CONSTRAINT bookmarks_pkey PRIMARY KEY (id)
) WITH ( OIDS=FALSE );

ALTER TABLE bookmarks  OWNER TO postgres;

COMMENT ON TABLE bookmarks IS 'a list of bookmarks';
COMMENT ON COLUMN bookmarks.id IS 'a primary key';
COMMENT ON COLUMN bookmarks.video_id IS 'an identifier of video recording';
COMMENT ON COLUMN bookmarks.name IS 'a name of bookmark';
COMMENT ON COLUMN bookmarks.description IS 'a details of bookmark';
COMMENT ON COLUMN bookmarks.start IS 'a start time in the video recording';
COMMENT ON COLUMN bookmarks.user_id IS 'an owner of bookmark';
COMMENT ON COLUMN bookmarks.thumbnail IS 'a content of video snapshot';
COMMENT ON COLUMN bookmarks.created IS 'a date/time of bookmark creation';


---
--- A list of favourites media files
---
CREATE TABLE favorites (
  media_id uuid NOT NULL, -- an identifier of media file
  user_id uuid NOT NULL, -- an identifier of user
  type regclass, -- a type of media file
  CONSTRAINT favorites_pkey PRIMARY KEY (media_id, user_id)
) WITH ( OIDS=FALSE );

ALTER TABLE favorites OWNER TO postgres;

COMMENT ON TABLE favorites IS 'A list of favorites videos';
COMMENT ON COLUMN favorites.media_id IS 'an identifier of video recording';
COMMENT ON COLUMN favorites.user_id IS 'an identifier of user';


--
-- A list of devices
-- 
CREATE TABLE devices (
  id character varying(16) NOT NULL, -- a primary key
  type device_type NOT NULL, -- a type of device
  user_id uuid NOT NULL, -- an owner of device
  status device_status NOT NULL DEFAULT 'OFFLINE'::device_status, -- a current status of device
  progress numeric(5,2) NOT NULL DEFAULT 100, -- a current synchronization progress
  CONSTRAINT devices_pkey PRIMARY KEY (id)
) WITH ( OIDS=FALSE );

ALTER TABLE devices OWNER TO postgres;

COMMENT ON TABLE devices IS 'a list of devices';
COMMENT ON COLUMN devices.id IS 'a primary key';
COMMENT ON COLUMN devices.type IS 'a type of device';
COMMENT ON COLUMN devices.user_id IS 'an owner of device';
COMMENT ON COLUMN devices.status IS 'a current status of device';


--
-- A list of pictures
--
CREATE TABLE pictures (
  id uuid NOT NULL, -- an primary identifier
  created timestamp(0) with time zone NOT NULL DEFAULT now(), -- a date and time when picture was created
  location point, -- a GPS coordinates
  width smallint, -- a picture width
  height smallint, -- a picture height
  device_id character varying(16) NOT NULL, -- an identifier of device
  user_id uuid NOT NULL, -- an identifier of device user
  content bytea NOT NULL, -- a data of picture in JPEG format
  modified timestamp(0) with time zone, -- a last date and time when picture was modified
  description text, -- an optional description of image
  caseno character varying(32), -- an optional identifier of case
  classification classification, -- an optional classification of case
  title character varying(200), -- an optional title of file
  CONSTRAINT pictures_pkey PRIMARY KEY (id)
) WITH ( OIDS=FALSE );

ALTER TABLE pictures OWNER TO postgres;
CREATE INDEX pictures_user_id_idx ON pictures USING btree (user_id);

COMMENT ON TABLE pictures IS 'A list of pictures';
COMMENT ON COLUMN pictures.id IS 'an primary identifier';
COMMENT ON COLUMN pictures.created IS 'a date and time when picture was created';
COMMENT ON COLUMN pictures.location IS 'a GPS coordinates';
COMMENT ON COLUMN pictures.width IS 'a picture width';
COMMENT ON COLUMN pictures.height IS 'a picture height';
COMMENT ON COLUMN pictures.device_id IS 'an identifier of device';
COMMENT ON COLUMN pictures.user_id IS 'an identifier of device user';
COMMENT ON COLUMN pictures.content IS 'a data of picture in JPEG format';
COMMENT ON COLUMN pictures.modified IS 'a last date and time when picture was modified';
COMMENT ON COLUMN pictures.description IS 'an optional description of image';
COMMENT ON COLUMN pictures.caseno IS 'an optional identifier of case';
COMMENT ON COLUMN pictures.classification IS 'an optional classification of case';


--
-- A list of sessions
--
CREATE TABLE sessions (
  id character varying(40) NOT NULL,
  ip_address character varying(45) NOT NULL,
  "timestamp" bigint NOT NULL DEFAULT 0,
  data text NOT NULL DEFAULT ''::text,
  CONSTRAINT ci_sessions_pkey PRIMARY KEY (id)
) WITH ( OIDS=FALSE );

ALTER TABLE sessions OWNER TO postgres;

CREATE INDEX ci_sessions_timestamp ON sessions USING btree("timestamp");

--
-- A list of users
--
CREATE TABLE users (
  id uuid NOT NULL, -- a primary key
  login username NOT NULL, -- a username to authenticate
  password bytea, -- a hash of user password to authenticate (can be optional in case of different authentication protocol)
  is_active boolean NOT NULL DEFAULT true, -- a status of user account
  email email, -- an email address of user account
  created timestamp(0) with time zone NOT NULL DEFAULT now(), -- a date/time of user creation
  CONSTRAINT users_pkey PRIMARY KEY (id),
  CONSTRAINT users_email_key UNIQUE (email),
  CONSTRAINT users_login_key UNIQUE (login)
) WITH ( OIDS=FALSE );

ALTER TABLE users  OWNER TO postgres;

COMMENT ON TABLE users  IS 'A list of user accounts';
COMMENT ON COLUMN users.id IS 'a primary key';
COMMENT ON COLUMN users.login IS 'a username to authenticate';
COMMENT ON COLUMN users.password IS 'a hash of user password to authenticate (can be optional in case of different authentication protocol)';
COMMENT ON COLUMN users.is_active IS 'a status of user account';

INSERT INTO users (id, login, password) VALUES ('0a89862a-ceec-11e5-903c-303a64a40499', 'admin', digest('12345', 'sha256'));

--
-- A list of user profiles
--
CREATE TABLE profiles (
  id uuid NOT NULL, -- an identifier of user account
  first_name character varying(32) NOT NULL, -- a first name of user
  last_name character varying(32), -- a last name of user
  photo bytea, -- an optional photo of user
  "position" character varying(32), -- an optional position of user in the user's department
  department character varying(32), -- an optional department of user
  created timestamp(0) with time zone NOT NULL DEFAULT now(), -- a date/time of profile creation
  CONSTRAINT profiles_pkey PRIMARY KEY (id)
) WITH ( OIDS=FALSE );

ALTER TABLE profiles OWNER TO postgres;

COMMENT ON TABLE profiles IS 'A list of user profiles';
COMMENT ON COLUMN profiles.id IS 'an identifier of user account';
COMMENT ON COLUMN profiles.first_name IS 'a first name of user';
COMMENT ON COLUMN profiles.last_name IS 'a last name of user';
COMMENT ON COLUMN profiles.photo IS 'an optional photo of user';
COMMENT ON COLUMN profiles."position" IS 'an optional position of user in the user''s department';
COMMENT ON COLUMN profiles.department IS 'an optional department of user';
COMMENT ON COLUMN profiles.created IS 'a date/time of profile creation';

INSERT INTO profiles (id, first_name, last_name) VALUES ('0a89862a-ceec-11e5-903c-303a64a40499', 'Admin', 'Admin');

--
-- A list of user groups
--
CREATE TABLE groups (
  id uuid NOT NULL, -- a primarry key
  name character varying(32) NOT NULL, -- a name of group
  users uuid[] NOT NULL DEFAULT ARRAY[]::uuid[], -- a list of users in the current group
  permissions privilege_type, -- a set of group permissions
  created timestamp(0) with time zone NOT NULL DEFAULT now(), -- a date/time of group creation
  CONSTRAINT groups_pkey PRIMARY KEY (id)
) WITH ( OIDS=FALSE );

ALTER TABLE groups OWNER TO postgres;

COMMENT ON TABLE groups IS 'A list of user groups';
COMMENT ON COLUMN groups.id IS 'a primarry key';
COMMENT ON COLUMN groups.name IS 'a name of group';
COMMENT ON COLUMN groups.created IS 'a date/time of group creation';
COMMENT ON COLUMN groups.users IS 'a list of users in the current group';
COMMENT ON COLUMN groups.permissions IS 'a set of group permissions';

INSERT INTO groups (id, name, users) VALUES (
	'd2ee53ca-cfa0-11e5-b5dc-303a64a40499', 'Administrators', ARRAY['0a89862a-ceec-11e5-903c-303a64a40499']::uuid[]
);

--
-- A list of video recordings
--
CREATE TABLE videos (
  id uuid NOT NULL, -- a primary identifier
  path time_point[], -- a list with associated GPS coordinates
  created timestamp(0) with time zone NOT NULL, -- a date and time when video recording was created
  duration interval, -- a duration of video recording in seconds
  width smallint, -- a frame width
  height smallint, -- a frame height
  device_id character varying(16) NOT NULL, -- an identifier of device
  user_id uuid NOT NULL, -- an identifier of device user
  content oid NOT NULL, -- a Large Object with video content in WEBM format
  thumbnail bytea NOT NULL, -- a data of thumbnail for first video frame
  modified timestamp(0) with time zone, -- a last date and time when video record was modified
  description text, -- an optional description of video recording
  caseno character varying(32), -- an optional identifier of case
  title character varying(200), -- an optional title of file
  CONSTRAINT videos_pkey PRIMARY KEY (id)
) WITH ( OIDS=FALSE );

ALTER TABLE videos OWNER TO postgres;
CREATE INDEX videos_user_id_idx ON videos USING btree (user_id);

CREATE TRIGGER delete_video_file
  AFTER DELETE ON videos
  FOR EACH ROW EXECUTE PROCEDURE delete_large_object();
  
COMMENT ON TABLE videos IS 'A list of video recordings';
COMMENT ON COLUMN videos.id IS 'a primary identifier';
COMMENT ON COLUMN videos.path IS 'a list with associated GPS coordinates';
COMMENT ON COLUMN videos.created IS 'a date and time when video recording was created';
COMMENT ON COLUMN videos.duration IS 'a duration of video recording in seconds';
COMMENT ON COLUMN videos.width IS 'a frame width';
COMMENT ON COLUMN videos.height IS 'a frame height';
COMMENT ON COLUMN videos.device IS 'an identifier of device';
COMMENT ON COLUMN videos.user_id IS 'an identifier of device user';
COMMENT ON COLUMN videos.content IS 'a Large Object with video content in WEBM format';
COMMENT ON COLUMN videos.thumbnail IS 'a data of thumbnail for first video frame';
COMMENT ON COLUMN videos.modified IS 'a last date and time when video record was modified';
COMMENT ON COLUMN videos.description IS 'an optional description of video recording';
COMMENT ON COLUMN videos.caseno IS 'an optional identifier of case';


--
-- TOC entry 2104 (class 0 OID 0)
-- Dependencies: 6
-- Name: public; Type: ACL; Schema: -; Owner: postgres
--

REVOKE ALL ON SCHEMA public FROM PUBLIC;
REVOKE ALL ON SCHEMA public FROM postgres;
GRANT ALL ON SCHEMA public TO postgres;
GRANT ALL ON SCHEMA public TO PUBLIC;
