#ifndef WOLFCOMLIB_H_DEFINED
#define WOLFCOMLIB_H_DEFINED

#define USB_VENDOR_ID  			0x4255
#define USB_PRODUCT_ID  		0x0001

typedef struct
{
	unsigned char /*PF_STAT*/ stat[32];
	int fs_type;
	unsigned __int64 fstfz;	/* file size in bytes */
	unsigned short fstact;	/* file last access time */
	unsigned short fstad;	/* last file access date */
	unsigned char  fstautc;	/* last file access date and time UTC offset */
	unsigned short fstut;	/* last file update time */
	unsigned short fstuc;	/* last file update time[10ms] */
	unsigned short fstud;	/* last file update date */
	unsigned char  fstuutc;	/* last file update date and time UTC offset */
	unsigned short fstct;	/* file create time */
	unsigned short fstcd;	/* file crreate date */
	unsigned short fstcc;	/* file create component time (ms) */
	unsigned char  fstcutc;	/* file create date and time UTC offset */
	unsigned short fstat;	/* file attribute */
	unsigned char filename[13];
} FF_STAT;


// this callback function define for transfer file information
//typedef unsigned long (__stdcall *PROGRESS)(unsigned __int64 TotalBytesTransferred);
typedef void(*PROGRESS)(unsigned __int64 TotalBytesTransferred);

#ifndef ETRANSFER_TIMEDOUT
#define ETRANSFER_TIMEDOUT 116
#endif

#ifndef HANDLE
#define HANDLE void*
#endif

///////////////////////////////////////////////////////////////////////////
//Wolfcom device interface
#ifdef __cplusplus
extern "C" {
#endif

#ifdef _EXPORTING
#define DECLSPEC    __declspec(dllexport)
#else
#define DECLSPEC    __declspec(dllimport)
#endif

/*!
* Get an opaque handle to a Vision device.
* All methods that access a camera take a handle parameter.
*
* @return A handle to a open device.
*/
	DECLSPEC HANDLE GetUsbDevice();	//first call this function before use this class
	DECLSPEC void ReleaseUsbDevice(HANDLE handle);
	DECLSPEC int IsDeviceInserted(HANDLE handle);//detect device if connect to PC


	/*!
	* Release the handle to a Vision device.
	*
	* @param handle  The Vision handle of the device.
	* @return 1 if the handle was released, otherwise 0.
	*/	DECLSPEC int SetDevicePoweroff(HANDLE handle); // turn off device
	DECLSPEC int GetDeviceSetup(HANDLE handle, unsigned char *buf, int buflen);

	DECLSPEC int SetDevicePoweroff(HANDLE handle); // turn off device
	DECLSPEC int GetDevicePassword(HANDLE handle, unsigned char *buf, int buflen);
	DECLSPEC int GetDeviceSerialNo(HANDLE handle, unsigned char *buf, int buflen);
	DECLSPEC int GetDevicePoliceNo(HANDLE handle, unsigned char *buf, int buflen);
	DECLSPEC int GetDeviceFileCount(HANDLE handle);
	DECLSPEC int SetDeviceSetup(HANDLE handle, char* strSetup);
	DECLSPEC int SetDevicePassword(HANDLE handle, char* strPassword);
	DECLSPEC int SetDeviceSerialNo(HANDLE handle, char* strSerialNo);
	DECLSPEC int SetDevicePoliceNo(HANDLE handle, char* strPoliceNo);
	DECLSPEC int SetDeviceTime(HANDLE handle);
	DECLSPEC int SetDeviceMassStorage(HANDLE handle);
	DECLSPEC int GetDeviceFile(HANDLE handle, FF_STAT *pff_stat, int nIndex);
	DECLSPEC char* GetDeviceError(int err);
	DECLSPEC int ImportDeviceFile(HANDLE handle, int nIndex, char* filename, PROGRESS progressCallback);

	//-----------------------------------------------------
	//this function get setting of device.
	//the setting is a number string 
	//the default setting string is 02001020100011021
	//bit 1: password switch			(0 - OFF, 1 - ON)
	//bit 2: video resolution			(1920*1080, 1280*720(60FPS), 1280*720(30FPS), 848*480, 640*480 )
	//bit 3: compression rate			(SUPER, FINE, NORMAL)
	//bit 4: audio record				(0 - YES, 1 - NO )
	//bit 5: recycle record				(30 min, 10 min, 5 min, 3 min, 1 min)
	//bit 6: loop record				(0 - YES, 1 - NO )
	//bit 7: photo resolution			(16M, 12M, 8M, 5M, 3M)
	//bit 8: public awareness indicator (FLASH, OFF, ON)
	//bit 9: date & time stamp			(0 - NO, 1 - YES)
	//bit 10: default setting			(0 - NO, 1 - YES)
	//bit 11: device mode				(0 - normal, 1 - pre record, 2 - easy)
	//bit 12: format memory of device	(0 - No, 1 - Yes)
	//bit 13: video file format			(0 - MOV, 1 - MP4)
	//bit 14: GPS stamp					(0 - OFF, 1 - ON)
	//bit 15: pre-record audio			(0 - ON, 1 - OFF)
	//bit 16-17: Time zone 0-28  => -8 -7 -6 -5 -4 -3.5 -3 -2 -1 0 1 2 3 3.5 4 4.5 5 5.5 6 6.5 7 8 9 9.5 10 10.5 11 11.5 12
	//-----------------------------------------------------

#ifdef __cplusplus
}
#endif


#endif