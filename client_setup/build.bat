@echo off
cd %~dp0

:: Prepare compilation parameters
set EXT=-ext WixUIExtension -ext WixUtilExtension

set APP_MANUFACTURER=WolfCom
set APP_MANUFACTURER_TITLE=WolfCom Enterprises, Inc.

set APP_PRODUCT=Evidence Management Solution
set APP_VERSION=1.3.0


echo Make WEMS client installation package (X86)
set OBJ=build\x86\setup.wixobj

candle.exe -arch x86 -nologo -out build\x86\setup.wixobj %EXT% src\setup.wxs
light.exe -nologo -out bin\setup.%APP_VERSION%.msi %EXT% %OBJ%
