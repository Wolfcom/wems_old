WolfCom Evidence Management Solution.

Structure of project directories:

	legacy
	├── assets					Logo and banners
	├── build					Temporary object files during compilation
	│
	├── client_setup			A client MSI package
	│   ├── build				Destination folder for MSI package
	│   ├── lib					Old Microsoft Runtimes
	│   └── src					Sources of setup file
	│
	├── distrib
	│   ├── 3rd_eye_driver		amd64/x86 LibUSB drivers for 3RD-EYE devices
	│   ├── apache				Binary files of web server (Apache) and PHP 7.0
	│   │
	│   ├── app					A PHP/JS/CSS files of WEB interface
	│   │   ├── application
	│   │   │   ├── config			CodeIgniter configuration
	│   │   │   ├── controllers		A set of application controllers
	│   │   │   ├── helpers			Helper functions
	│   │   │   ├── hooks			Implements a hook for system log
	│   │   │   ├── models			A set of wrappers to hide SQL queries
	│   │   │   └── views			A set of HTML templates
	│   │   ├── assets			CSS/Images/Javascript files (main.js contains JS callbacks for all actions)
	│   │   └── system			Core files of CodeIgniter framework
	│   │
	│   ├── cdrecord			Console application to burn CD/DVD
	│   ├── data				A clean dump of database
	│   ├── debug				A debug version of server application
	│   ├── ffmpeg				FFMPEG application to fetch information about video/audio files
	│   ├── licenses			A set of various application licenses
	│   ├── release				A release version of server application
	│   ├── usb_driver			A common amd64/x86 LibUSB drivers
	│   └── vision_driver		amd64/x86 LibUSB drivers for Vision devices
	│
	├── docs
	│   ├── database			index.html file to describe database structure
	│   ├── html				app.html file to describe structure of server application
	│   └── tools				A set of tools to generate the documentation
	│
	├── full_setup				A server MSI package of application
	│   ├── bin					MSI package
	│   ├── build				Temporary object files during building of package
	│   ├── lib					Old Microsoft Runtimes
	│   ├── sql					A set of scripts to upgrade database
	│   └── src					Sources of setup file
	│
	├── lib						A library object for server application
	├── php						A php extension to update settings of camera
	├── src						Source files of server application
	├── tools					A set of tools to build server application
