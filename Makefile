#
# Detect folder structure
#
ROOT_DIR := $(dir $(abspath $(lastword $(MAKEFILE_LIST))))
ifeq ($(OS), Windows_NT)
	ROOT_DIR := $(shell cygpath -m $(ROOT_DIR))
endif

#
# Creates a HTML documentations
#
.PHONE: html
html:
	rm -rf $(ROOT_DIR)/docs/html/*.html
	rdmd -version=ASCII $(ROOT_DIR)/docs/tools/bootDoc/generate.d $(ROOT_DIR)/src --bootdoc=$(ROOT_DIR)/docs/tools/bootDoc/ --modules=$(ROOT_DIR)/docs/tools/modules.ddoc --settings=$(ROOT_DIR)/docs/tools/settings.ddoc --output=$(ROOT_DIR)/docs/html/
	java -jar $(ROOT_DIR)/docs/tools/schemaSpy_5.0.0.jar -norows -t pgsql -o $(ROOT_DIR)/docs/database/ -host localhost -s public -db wolfcom -u postgres -p X2ZrneLyN4xB6PxU -desc "WolfCom Evidence Management Solution"
