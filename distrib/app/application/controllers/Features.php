<?php

require_once dirname(__FILE__) . '/Basic.php';

class Features extends Basic_Controller {

	/**
	 * Initialiazes the controller
	 */
	function __construct() {
		parent::__construct();
	}

	/**
	 * Displays the list of features
	 */
	function index() {
		// Check the user request
		$this->check_user_session();

		// Prepare templat variables
		$view = array(
			'page_id' => 'modules_page',
			'header_title' => "Modules",

			'current_user_id' => $this->session->user_id,
			'current_user_login' => $this->session->user_login,
			'current_permissions' => $this->session->user_group->permissions,
			'current_features' => $this->features,
			'app_user_list' => $this->get_user_list(),

			'features' => $this->features
		);

		// Render a page
        $this->load->view('template/header', $view);
        $this->load->view('view-modules', $view);
        $this->load->view('template/footer', $view);
	}

	/**
	 * Updates the set of features
	 * 
	 * POST variables: features
	 */
	function update() {
		// Check the user request
		$this->check_user_session();

		// Prepare the set of permissions to update
		$all_features = $this->feature_list();
		$features = array();
		foreach ($all_features as $feature) {
			$features []= empty($_POST['features'][$feature]) ? 'false' : 'true';
		}

		$features_sql = '(' . implode(', ', $features) . ')';

		$this->db->query(<<<EOL
ALTER DATABASE wolfcom SET app.features = '{$features_sql}';
SET app.features = '{$features_sql}';
EOL
		);

		// Send a response
		echo json_encode(array('status' => 'success'));
	}

	/**
	 * Returns the list of features.
	 * TODO: cache results
	 */
	protected function feature_list() {
		// Prepare main query
		$query = <<<EOL
SELECT attname FROM pg_attribute a
INNER JOIN pg_type t ON t.typname = 'features' AND a.attrelid = t.typrelid
WHERE a.atttypid != 0
ORDER BY a.attnum
EOL;

		// Try to fetch data
		$results = $this->db->query($query);
		$features = array();
		foreach ($results->result() as $result) {
			$features []= $result->attname;
		}

		return $features;
	}

}
