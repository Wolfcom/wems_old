<?php

require_once dirname(__FILE__) . '/Basic.php';

class Device extends Basic_Controller {

	/**
	 * Initialiazes the controller
	 */
	function __construct() {
		parent::__construct();

		$this->load->model('device_model', 'device');
	}

	/**
	 * Displays the list of devices
	 */
	function index() {
		// Check the user request
		$this->check_user_session();

		// Prepare template variables
		$view = array(
			'page_id' => 'devices_page',
			'header_title' => "Cameras",

			'current_user_id' => $this->session->user_id,
			'current_user_login' => $this->session->user_login,
			'current_permissions' => $this->session->user_group->permissions,
			'current_features' => $this->features,
			'app_user_list' => $this->get_user_list(),

			'device_list' => $this->device->fetch_list()
		);

		// Render a page
		$this->load->view('template/header', $view);
		$this->load->view('device/camera-management', $view);
		$this->load->view('template/footer', $view);
	}

	/**
	 * Returns the current statuses of registered devices
	 */
	function status() {		
		// Check the user request
		$this->check_user_session();

		// Send a response
		echo json_encode(array(
			'status' => 'success',
			'devices' => $this->device->fetch_list()
		));
	}

	/**
	 * Displays the settings page for the specified device
	 * 
	 * @param string $id	an identifier of device
	 */
	function settings($id) {
		// Check the user request
		$this->check_user_session();

		// Prepare template variables
		try {
			$settings = get_camera_settings();
		}
		catch (Exception $e) {
			$this->session->set_flashdata(
				'errors', (array)$this->session->flashdata('errors') + array($e->getMessage())
			);
			redirect('/index.php/device/index/', 'refresh');
		}

		// Prepare the list of timezones
		$timezones = array(
			-(8 * 60) => '-08:00',
			-(7 * 60) => '-07:00',
			-(6 * 60) => '-06:00',
			-(5 * 60) => '-05:00',
			-(4 * 60) => '-04:00',
			-(3 * 60 + 30 * 60) => '-03:30',
			-(3 * 6) => '-03:00',
			-(2 * 60) => '-02:00',
			-(1 * 60) => '-01:00',
			0 => '00:00',
			(1 * 60) => '01:00',
			(2 * 60) => '02:00',
			(3 * 60) => '03:00',
			(4 * 60) => '04:00',
			(4 * 60 + 30) => '04:30',
			(5 * 60) => '05:00',
			(6 * 60) => '06:00',
			(6 * 60 + 30) => '06:30',
			(7 * 60) => '07:00',
			(8 * 60) => '08:00',
			(9 * 60) => '09:00',
			(9 * 60 + 30) => '09:30',
			(10 * 60) => '10:00',
			(10 * 60 + 30) => '10:30',
			(11 * 60) => '11:00',
			(11 * 60 + 30) => '11:30',
			(12 * 60) => '12:00',
		);

		$view = array(
			'page_id' => 'device_settings_page',
			'header_title' => "Device Settings",

			'current_user_id' => $this->session->user_id,
			'current_user_login' => $this->session->user_login,
			'current_permissions' => $this->session->user_group->permissions,
			'app_user_list' => $this->get_user_list(),

			'device_id' => $id,
			'badge_id' => get_camera_badge_id(),
			'settings' => $settings,
			'timezones' => $timezones
		);


		// Render a page
		$this->load->view('template/header', $view);
		$this->load->view('device/settings', $view);
		$this->load->view('template/footer', $view);
	}

	/**
	 * Updates the settings of connected device
	 */
	function update() {
		// Check the user request
		$this->check_user_session();

		// Try to update a settings
		$settings = $_POST;
		$badge_id = $_POST['badge_id'];
		unset($settings['badge_id']);

		try {
			set_camera_settings($settings, $badge_id);

			// Send a successful response
			echo json_encode(array('status' => 'success'));
		}
		catch (Exception $e) {
			// Send a successful response
			echo json_encode(array('status' => 'error', 'message' => $e->getMessage()));
		}
	}


}
