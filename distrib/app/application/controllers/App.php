<?php

require_once dirname(__FILE__) . '/Basic.php';


class App extends Basic_Controller {

	/**
	 * Initialiazes the controller
	 */
	function __construct() {
		parent::__construct();

		// Check the user session
		if (!$this->session->userdata('user_id')) {
			redirect('/index.php/user/login', 'refresh');
		}
	}

	/**
	 * Display the user dashboard
	 */
	function index() {
		$view = array(
			'page_id' => 'dashboard_page',
			'header_title' => "Dashboard",

			'current_user_id' => $this->session->user_id,
			'current_user_login' => $this->session->user_login,
			'current_permissions' => $this->session->user_group->permissions,
			'current_features' => $this->features,
			'app_user_list' => $this->get_user_list()
		);

		$this->load->view('template/header', $view);
		$this->load->view('view-dashboard', $view);
		$this->load->view('template/footer', $view);
	}

	/**
	 * Displays an about box
	 */
	function about() {
		$view = array(
			'title' => 'About',
			'dialog_id' => 'about-box',
			'left_buttons' => array()
		);

		$this->load->view('template/modalheader', $view);
		$this->load->view('view_modal/about', $view);
		$this->load->view('template/modalfooter', $view);
	}

	/**
	 * Displays a dialog to submit a ticket
	 */
	function ticket() {
		$view = array(
			'title' => 'Submit Ticket',
			'dialog_id' => 'ticket-box',
			'left_buttons' => array()
		);

		$this->load->view('template/modalheader', $view);
		$this->load->view('view_modal/ticket', $view);
		$this->load->view('template/modalfooter', $view);
	}

}
