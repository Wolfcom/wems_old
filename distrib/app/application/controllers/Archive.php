<?php

require_once dirname(__FILE__) . '/Basic.php';

class Archive extends Basic_Controller {

	/**
	 * Files per page
	 */
	const PER_PAGE = 10;

	/**
	 * Initialiazes the controller
	 */
	function __construct() {
		parent::__construct();

		// Load dependencies
		$this->load->library('pagination');

		$this->load->model('archive_model', 'archive');
	}

	/**
	 * Displays the list of files.
	 * Default action.
	 */
	function index() {
		$this->check_user_session();

		// Load records
		$total_rows = $this->archive->total();
		$current_offset = empty($_REQUEST['per_page']) ? 0 : intval($_REQUEST['per_page']);
		$view['records'] = $this->archive->fetch_list(self::PER_PAGE, $current_offset);

		// Prepare pager
		$pager_config = array(
			'base_url' => base_url() . '/index.php/archive/',
			'page_query_string' => true,
			'per_page' => self::PER_PAGE,
			'total_rows' => $total_rows
		);

		$pager_config['first_tag_open'] = $pager_config['last_tag_open'] = 
			$pager_config['next_tag_open'] = $pager_config['prev_tag_open'] =
			$pager_config['num_tag_open'] = '<span>';

        $pager_config['first_tag_close'] = $pager_config['last_tag_close'] =
			$pager_config['next_tag_close'] = $pager_config['prev_tag_close'] =
			$pager_config['num_tag_close'] = '</span>';

		$pager_config['cur_tag_open'] = '<span><strong>';
		$pager_config['cur_tag_close'] = '</strong></span>';

		$this->pagination->initialize($pager_config);
		$view['links'] = $this->pagination->create_links();

		// Render a page
		$view['page_id'] = 'files_page';
		$view['header_title'] = 'Archived Files';

		$view['current_user_id'] = $this->session->user_id;
		$view['current_user_login'] = $this->session->user_login;
		$view['current_permissions'] = $this->session->user_group->permissions;
		$view['current_features'] = $this->features;
		$view['app_user_list'] = $this->get_user_list();

		$this->load->view('template/header', $view);
		$this->load->view('view-archive', $view);
		$this->load->view('template/footer', $view);
	}

	function retension() {
		$this->check_user_session();

		// Render a page
		$view['page_id'] = 'files_page';
		$view['header_title'] = 'Retention Policy';

		$view['current_user_id'] = $this->session->user_id;
		$view['current_user_login'] = $this->session->user_login;
		$view['current_permissions'] = $this->session->user_group->permissions;
		$view['current_features'] = $this->features;
		$view['app_user_list'] = $this->get_user_list();

		$view['classifications'] = $this->get_case_classifications();
		$view['policy'] = $this->archive->fetch_policy();

		$this->load->view('template/header', $view);
		$this->load->view('view-retension', $view);
		$this->load->view('template/footer', $view);
	}

	/**
	 * Deletes the retention rule for the specified classification.
	 * 
	 * POST variables: category
	 */
	function unset_archive_policy() {
		// Verify the request
		$this->check_user_session();

		$categories = $this->input->post('category');
		foreach ($categories as $category) {
			$this->archive->unset_archive_policy($category);
		}

		// Send a response
		echo json_encode(array('status' => 'success'));
	}

	/**
	 * Saves the retention rule for the specified classification.
	 * 
	 * POST variables: category, duration
	 */
	function set_archive_policy() {
		// Verify the request
		$this->check_user_session();

		$categories = $this->input->post('category');
		$duration = $this->input->post('duration');
		foreach ($categories as $category) {
			$this->archive->set_archive_policy($category, $duration);
		}

		// Send a response
		echo json_encode(array('status' => 'success'));
	}

	/**
	 * Deletes the retention rule for the specified classification.
	 * 
	 * POST variables: category
	 */
	function unset_post_archive_policy() {
		// Verify the request
		$this->check_user_session();

		$categories = $this->input->post('category');
		foreach ($categories as $category) {
			$this->archive->unset_post_archive_policy($category);
		}

		// Send a response
		echo json_encode(array('status' => 'success'));
	}
	
	/**
	 * Deletes the retention rule for the specified classification.
	 * 
	 * POST variables: category, duration
	 */
	function set_post_archive_policy() {
		// Verify the request
		$this->check_user_session();

		$duration = $this->input->post('duration');
		$categories = $this->input->post('category');
		foreach ($categories as $category) {
			$this->archive->set_post_archive_policy($category, $duration);
		}
		
		// Send a response
		echo json_encode(array('status' => 'success'));
	}

}
