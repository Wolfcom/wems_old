<?php

require_once dirname(__FILE__) . '/Basic.php';

class Settings extends Basic_Controller {

	/**
	 * Initialiazes the controller
	 */
	function __construct() {
		parent::__construct();
	}

	/**
	 * Displays the list of settings
	 */
	function index() {
		// Check the user request
		$this->check_user_session();

		// Prepare templat variables
		$view = array(
			'page_id' => 'settings_page',
			'header_title' => "Settings",

			'current_user_id' => $this->session->user_id,
			'current_user_login' => $this->session->user_login,
			'current_permissions' => $this->session->user_group->permissions,
			'current_features' => $this->features,
			'app_user_list' => $this->get_user_list(),

			'classifications' => $this->get_case_classifications()
		);

		// Render a page
        $this->load->view('template/header', $view);
        $this->load->view('view-settings', $view);
        $this->load->view('template/footer', $view);
	}

	/**
	 * Updates the specified settings
	 * 
	 * POST variables: classification
	 */
	function update() {
		// Check the user request
		$this->check_user_session();

		// Detect differences between old and new sets
		$classification_old = $this->get_case_classifications();
		$classification_new = $this->input->post('classification');

		$items_to_add = array_diff($classification_new, $classification_old);
		$items_to_remove = array_diff($classification_old, $classification_new);

		// Try to add new items
		foreach ($items_to_add as $item) {
			$this->picklist->add_classification($item);
		}

		// Try to delete items
		foreach ($items_to_remove as $item) {
			$this->picklist->remove_classification($item);
		}

		// Send a response
		echo json_encode(array('status' => 'success'));
	}

}
