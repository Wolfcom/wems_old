<?php

require_once dirname(__FILE__) . '/Basic.php';


class Audio extends Basic_Controller {

	/**
	 * A size of video chunk to send in one network packet
	 */
	const CHUNK_SIZE = 4096;

	/**
	 * Initialiazes the controller
	 */
	function __construct() {
		parent::__construct();

		$this->load->model('activity_model', 'activity');
		$this->load->model('audio_model', 'audio');
	}

	/**
	 * Returns the checksum of audio file.
	 * 
	 * @param string $id	an identifier of audio file (UUID)
	 */
	function checksum($id) {
		// Check the user request
		$this->check_user_session();

		$audio = $this->audio->get($id);
		if (empty($video)) {
			header('HTTP/1.1 404 Not Found');
			return;
		}

		// Stream a checksum 
		header('Content-Type: text/plain');
		header('Content-Length: ' . strlen($audio->checksum));
		header("Content-disposition: attachment; filename=\"sha256sum-{$id}.txt\"");

		echo $audio->checksum;
	}


	/**
	 * Serves an audio file
	 * 
	 * @param string $id	an identifier of audio file (UUID)
	 */
	function index($id = null) {
		// Check the user request
		$this->check_user_session();

		// Check the identifier of audio
		$dbh = $this->load->database('default', true);
		$pgsql = $dbh->conn_id;

		$result = $dbh->query(
			'SELECT content, lo_size(content) AS size FROM audios WHERE id = ?',
			array($id)
		);
		if ($result->num_rows() < 1) {
			throw new InvalidArgumentException(
				'The audio recording with specified identifier isn\'t found'
			);
		}

		$row = $result->row_array();
		$file_id = $row['content'];
		$file_size = $row['size'];

		// Output regular HTTP headers
		header('Content-Type: video/webm');
		header('Accept-Ranges: bytes');

		// Check for content range
		if (isset($_SERVER['HTTP_RANGE'])) {
			list($size_unit, $ranges) = explode('=', $_SERVER['HTTP_RANGE'], 2);
			if ($size_unit != 'bytes') {
				header('HTTP/1.1 416 Requested Range Not Satisfiable');
				exit;
			}

			// TODO: multiple ranges
			list($range,) = explode(',', $ranges, 2);
			@list($seek_start, $seek_end) = explode('-', $range, 2);
			$seek_start = abs(intval($seek_start));
			$seek_end = abs(intval($seek_end));
		}

		// Set start/end positions based on range (if set), else set defaults
		if (empty($seek_end)) {
			$seek_end = $file_size - 1;
		}
		else {
			$seek_end = min($seek_end, $file_size - 1);
		}

		if (empty($seek_start) || $seek_end < $seek_start) {
			$seek_start = 0;
		}
		else {
			$seek_start = max($seek_start, 0);
		}

		// Output range headers
		if (isset($_SERVER['HTTP_RANGE'])) {
			header('HTTP/1.1 206 Partial Content');
			header('Content-Range: bytes ' . $seek_start . '-' . $seek_end . '/' . $file_size);

			$content_length = $seek_end - $seek_start + 1;
		}
		else {
			$content_length = $file_size;
		}
		header('Content-Length: ' . $content_length);

		// Force to download
		if (isset($_REQUEST['force'])) {
			$audio = $this->audio->get($id);
			header("Content-disposition: attachment; filename=\"BodyCam-{$audio->device_id}-{$id}.wav\"");

			// Add the event to the event log
			$user_id = $this->session->userdata('user_id');
			$user_ip = $this->input->ip_address();
			$this->activity->add('download_audio', $user_id, $user_ip, $id, 'audios');
		}

		// Try to stream an audio recording.
		// Large Object API requires active transaction before any operation.
		session_write_close();
		$dbh->trans_start();
		$handle = pg_lo_open($pgsql, $file_id, "r");
		pg_lo_seek($handle, $seek_start, PGSQL_SEEK_SET);

		$read_bytes = $content_length;
		while ($read_bytes > 0) {
			echo pg_lo_read($handle, self::CHUNK_SIZE);

			ob_flush();
			flush();

			$read_bytes -= self::CHUNK_SIZE;
		}

		pg_lo_close($handle);
		$dbh->trans_complete();
	}

	/**
	 * Saves a meta information for the specified audio.
	 * 
	 * @param string $id	an identifier of audio recording
	 * 
	 * POST variables: caseno, title, description, classification
	 */
	function save_metadata($id) {
		// Verify the request
		$this->check_user_session();

		$title = empty($_REQUEST['title']) ? null : $_REQUEST['title'];
		$caseno = empty($_REQUEST['caseno']) ? null : $_REQUEST['caseno'];
		$description = empty($_REQUEST['description']) ? null : $_REQUEST['description'];
		$classification = empty($_REQUEST['classification']) ? null : $_REQUEST['classification'];

		// Try to save meta information
		$this->audio->save($id, $caseno, $title, $description, $classification);

		// Send a response
		echo json_encode(array('status' => 'success'));
	}

	/**
	 * Deletes the specified audio recording
	 * 
	 * @param string $id	an identifier of audio recording
	 */
	function delete($id) {
		// Verify the request
		$this->check_user_session();

		// Try to delete
		$this->audio->delete($id);

		// Send a response
		echo json_encode(array('status' => 'success'));
	}

	/**
	 * Displays the player for specified audio recvording.
	 * 
	 * @param string $id	an identifier of audio recording
	 */
	function preview($id) {
		// Verify the request
		$this->check_user_session();

		$audio = $this->audio->get($id);
		if (empty($audio)) {
			throw new Exception("Access Denied");
		}

		// Prepare list of buttons
		$buttons = array();
		$current_permissions = $this->session->user_group->permissions;

		if (!empty($current_permissions->module_favorites)) {
			$buttons []= array(
				'id' => 'AddFavorite',
				'title' => $audio->in_favs ? 'Remove from Favorites' : 'Add to Favorites'
			);
		}

		if (!empty($current_permissions->update_audio)) {
			$buttons []= array(
				'id' => 'EditMetadata',
				'title' => 'Edit Metadata'
			);
		}

		// Prepare template variables
		$classifications = $this->get_case_classifications();
		$view = array(
			'header_title' => "Audio Details",

			'current_user_id' => $this->session->user_id,
			'current_user_login' => $this->session->user_login,
			'current_permissions' => $current_permissions,
			'current_features' => $this->features,
			'app_user_list' => $this->get_user_list(),
			'classifications' => $classifications,

			'id' => $id,
			'audio' => $audio,
			'buttons' => $buttons
		);

		// Render a page
		$this->load->view('template/header', $view);
		$this->load->view('audio/preview', $view);
		$this->load->view('template/footer', $view);
	}

	/**
	 * Displays the audio details.
	 * 
	 * @param string $id	an identifier of audio recording
	 */
	function details($id) {
		// Verify the request
		$this->check_user_session();

		// Prepare template variables
		$audio = $this->audio->get($id);
		$classifications = $this->get_case_classifications();

		$view = array(
			'header_title' => "Audio Details",

			'current_user_id' => $this->session->user_id,
			'current_user_login' => $this->session->user_login,
			'current_permissions' => $this->session->user_group->permissions,
			'current_features' => $this->features,
			'app_user_list' => $this->get_user_list(),
			'classifications' => $classifications,

			'id' => $id,
			'audio' => $audio
		);

		// Render a page
		$this->load->view('template/header', $view);
		$this->load->view('audio/details', $view);
		$this->load->view('template/footer', $view);
	}

}
