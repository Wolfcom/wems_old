<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {

	/**
	 * Initialiazes the controller
	 */
	function __construct() {
		@parent::__construct();

		// Check the user session
		if (!$this->session->userdata('user_id')) {
			redirect('/index.php/user/login', 'refresh');
		}

		$this->load->model('bookmark_model');
	}

	/**
	 * Redirects the user to the dashboard
	 */
	function index() {
		redirect('/index.php/app/index', 'refresh');
	}

}
