<?php

require_once dirname(__FILE__) . '/Basic.php';

class Files extends Basic_Controller {

	/**
	 * Files per page
	 */
	const PER_PAGE = 10;

	/**
	 * Initialiazes the controller
	 */
	function __construct() {
		parent::__construct();

		// Load dependencies
		$this->load->library('pagination');

		$this->load->model('file_model', 'file');
		$this->load->model('activity_model', 'activity');

		$this->load->model('video_model', 'video');
		$this->load->model('audio_model', 'audio');
		$this->load->model('picture_model', 'picture');
		$this->load->model('document_model', 'document');
	}

	/**
	 * Displays the list of files.
	 * Alias for default action.
	 */
	function search() {
		$this->check_user_session();
		$this->display_list();
	}

	/**
	 * Displays the list of files.
	 * Default action.
	 */
	function index() {
		$this->check_user_session();
		$this->display_list();
	}

	protected function display_list() {
		// Prepare the set of filters
		$filters = array();
		if (!empty($_REQUEST['keyword'])) {
			$filters['keyword'] = trim($_REQUEST['keyword']);
		}
		if (!empty($_REQUEST['owner'])) {
			$filters['owner'] = trim($_REQUEST['owner']);
		}
		if (!empty($_REQUEST['type'])) {
			$filters['type'] = $_REQUEST['type'];
		}

		if (!empty($_REQUEST['exact_date'])) {
			$filters['exact_date'] = strtotime(trim($_REQUEST['exact_date']));
		}
		else {
			if (!empty($_REQUEST['start_date'])) {
				$filters['start_date'] = strtotime(trim($_REQUEST['start_date']));
			}
			if (!empty($_REQUEST['end_date'])) {
				$filters['end_date'] = strtotime(trim($_REQUEST['end_date']));
			}
		}


		// Load records
		$total_rows = $this->file->total($filters);
		$current_offset = empty($_REQUEST['per_page']) ? 0 : intval($_REQUEST['per_page']);
		$view['records'] = $this->file->fetch_list($filters, self::PER_PAGE, $current_offset);

		// Prepare pager
		$pager_config = array(
			'base_url' => base_url() . '/index.php/files/',
			'page_query_string' => true,
			'per_page' => self::PER_PAGE,
			'total_rows' => $total_rows
		);

		if ($filters) {
			$pager_config['base_url'] .= '?' . http_build_query($filters);
		}

		$pager_config['first_tag_open'] = $pager_config['last_tag_open'] = 
			$pager_config['next_tag_open'] = $pager_config['prev_tag_open'] =
			$pager_config['num_tag_open'] = '<span>';

        $pager_config['first_tag_close'] = $pager_config['last_tag_close'] =
			$pager_config['next_tag_close'] = $pager_config['prev_tag_close'] =
			$pager_config['num_tag_close'] = '</span>';

		$pager_config['cur_tag_open'] = '<span><strong>';
		$pager_config['cur_tag_close'] = '</strong></span>';

		$this->pagination->initialize($pager_config);
		$view['links'] = $this->pagination->create_links();

		// Render a page
		$view['page_id'] = 'files_page';
		$view['header_title'] = empty($filters) ? 'All Files' : 'Search Result';

		$view['current_user_id'] = $this->session->user_id;
		$view['current_user_login'] = $this->session->user_login;
		$view['current_permissions'] = $this->session->user_group->permissions;
		$view['current_features'] = $this->features;
		$view['app_user_list'] = $this->get_user_list();

		$this->load->view('template/header', $view);
		$this->load->view('view-files', $view);
		$this->load->view('template/footer', $view);
	}

	/**
	 * Burns the specified files to the CD/DVD disc.
	 * 
	 * POST variables: file
	 */
	function burn_disc() {
		// Check the user request
		$this->check_user_session();

		// Detect user details
		$user_id = $this->session->userdata('user_id');
		$user_ip = $this->input->ip_address();

		$tmp_files = array();
		$iso_file = tempnam(sys_get_temp_dir(), 'ISO_ARC');
		foreach (array_unique($_REQUEST['file']) as $index => $media_id) {
			$tmp_file_path = tempnam(sys_get_temp_dir(), 'ISO_FILE');
			$file_name = null;
			$event = null;

			// Detect type of media
			if (empty($file_name)) {
				try {
					$this->video->export_file($media_id, $tmp_file_path);

					$video = $this->video->get($media_id);
					$file_name = "BodyCam-{$video->device_id}-{$media_id}.mp4";

					$media_type = 'videos';
					$event = array(
						'burn_video', $user_id, $user_ip, $media_id, $media_type
					);
				}
				catch (Exception $ignore) {
				}
			}

			if (empty($file_name)) {
				try {
					$this->audio->export_file($media_id, $tmp_file_path);

					$audio = $this->audio->get($media_id);
					$file_name = "BodyCam-{$audio->device_id}-{$media_id}.wav";

					$media_type = 'audios';
					$event = array(
						'burn_audio', $user_id, $user_ip, $media_id, $media_type
					);
				}
				catch (Exception $ignore) {
				}
			}

			if (empty($file_name)) {
				try {
					$this->picture->export_file($media_id, $tmp_file_path);

					$picture = $this->picture->get($media_id);
					$file_name = "BodyCam-{$picture->device_id}-{$media_id}.jpg";

					$media_type = 'pictures';
					$event = array(
						'burn_picture', $user_id, $user_ip, $media_id, $media_type
					);
				}
				catch (Exception $ignore) {
				}
			}

			if (empty($file_name)) {
				try {
					$this->document->export_file($media_id, $tmp_file_path);

					$document = $this->document->get($media_id);
					$file_name = "BodyCam-{$media_id}." . $this->document->get_file_extension($document->mime_type);

					$media_type = 'documents';
					$event = array(
						'burn_document', $user_id, $user_ip, $media_id, $media_type
					);
				}
				catch (Exception $ignore) {
				}
			}

			// Add the event to the event log
			if ($event) {
				call_user_func_array(array($this->activity, 'add'), $event);
			}

			// Add the entry to archive
			if ($file_name) {
				$new_tmp_file_path = sys_get_temp_dir() . DIRECTORY_SEPARATOR . $file_name;
				rename($tmp_file_path, $new_tmp_file_path);
				$tmp_files []= $new_tmp_file_path;
			}
		}
		if ($tmp_files) {
			// Create ISO file
			$mkisofs_path = realpath(dirname(__FILE__) . "/../../../cdrecord/mkisofs.exe");
			if (!file_exists($mkisofs_path)) {
				die(json_encode(array(
					'status' => 'error',
					'message' => 'Installation is broken. Unable to find mkisofs component.'
				)));
			}

			$mkisofs = '"' . $mkisofs_path  . '"';
			$create_iso_cmd = "{$mkisofs} -o {$iso_file} " . implode(' ', $tmp_files);
			$output = shell_exec($create_iso_cmd);

			// Burn cd
			$cdrecord_path = realpath(dirname(__FILE__) . "/../../../cdrecord/cdrecord.exe");
			if (!file_exists($cdrecord_path)) {
				die(json_encode(array(
					'status' => 'error',
					'message' => 'Installation is broken. Unable to find cdrecord component.'
				)));
			}

			$cdrecord = '"' . $cdrecord_path . '"';
			$burn_iso_cmd = "{$cdrecord} {$iso_file}";
			$output = shell_exec($burn_iso_cmd);
		}

		// Remove temporary files
		foreach ($tmp_files as $file) {
			@unlink($file);
		}
		@unlink($iso_file);

		// Send a response
		echo json_encode(array('status' => 'success'));
	}

	/**
	 * Uploads a document to the system storage
	 * 
	 * POST variables: file
	 */
	function upload() {
		// Check the user request
		$this->check_user_session();

		if (empty($_FILES['file']['tmp_name'])) {
			die(json_encode(array(
				'status' => 'error',
				'message' => 'File is not specified'
			)));
		}

		// Try to create a document
		$file_path = $_FILES['file']['tmp_name'];
		$type = $this->document->get_file_type($file_path);
		switch ($type) {
			case 'image':
				$this->picture->upload($this->session->user_id, $file_path);
				break;

			default:
				$this->document->upload($this->session->user_id, $file_path);
		}

		// Send a response
		echo json_encode(array('status' => 'success'));
	}

	/**
	 * Downloads the specified files in the ZIP archive.
	 * 
	 * POST variables: file, include_report
	 */
	function download_archive() {
		// Prepare archive
		$zip_file = tempnam(sys_get_temp_dir(), 'ZIP_ARC');
		$zip = new ZipArchive;

		if (!$zip->open($zip_file, ZipArchive::CREATE)) {
			throw new Exception('Unable to create zip file');
		}

		$tmp_files = array();
		// Nothing to add
		if (empty($_REQUEST['file'])) {
			$zip->addEmptyDir('.');
		}

		// Try to add a list of media files
		else {
			// Detect user details
			$user_id = $this->session->userdata('user_id');
			$user_ip = $this->input->ip_address();

			foreach (array_unique($_REQUEST['file']) as $index => $media_id) {
				$checksum = '';
				$zip_entry = tempnam(sys_get_temp_dir(), 'ZIP_ARC');
				$zip_name = null;
				$event = null;
				$media_type = null;

				// Detect type of media
				if (empty($zip_name)) {
					try {
						$this->video->export_file($media_id, $zip_entry);

						$video = $this->video->get($media_id);
						$zip_name = "BodyCam-{$video->device_id}-{$media_id}.mp4";
						$checksum = $video->checksum;

						$media_type = 'videos';
						$event = array(
							'download_video', $user_id, $user_ip, $media_id, $media_type
						);
					}
					catch (Exception $ignore) {
					}
				}

				if (empty($zip_name)) {
					try {
						$this->audio->export_file($media_id, $zip_entry);

						$audio = $this->audio->get($media_id);
						$checksum = $audio->checksum;
						$zip_name = "BodyCam-{$audio->device_id}-{$media_id}.wav";

						$media_type = 'audios';
						$event = array(
							'download_audio', $user_id, $user_ip, $media_id, $media_type
						);
					}
					catch (Exception $ignore) {
					}
				}

				if (empty($zip_name)) {
					try {
						$this->picture->export_file($media_id, $zip_entry);

						$picture = $this->picture->get($media_id);
						$zip_name = "BodyCam-{$picture->device_id}-{$media_id}.jpg";
						$checksum = $picture->checksum;

						$media_type = 'pictures';
						$event = array(
							'download_picture', $user_id, $user_ip, $media_id, $media_type
						);
					}
					catch (Exception $ignore) {
					}
				}

				if (empty($zip_name)) {
					try {
						$this->document->export_file($media_id, $zip_entry);

						$document = $this->document->get($media_id);
						$zip_name = "BodyCam-{$media_id}." . $this->document->get_file_extension($document->mime_type);
						$checksum = $document->checksum;

						$media_type = 'documents';
						$event = array(
							'download_document', $user_id, $user_ip, $media_id, $media_type
						);
					}
					catch (Exception $ignore) {
					}
				}

				// Add the audit report
				if (isset($_REQUEST['include_report'])) {
					$zip->addFromString(
						"{$media_id}.txt", $this->activity->report($media_id, $media_type)
					);
				}

				// Add the event to the event log
				if ($event) {
					call_user_func_array(array($this->activity, 'add'), $event);
				}

				// Add the entry to archive
				if ($zip_name) {
					$zip->addFile($zip_entry, $zip_name);
					$zip->addFromString('sha256sum-' . $media_id . '.txt', $checksum);

					$tmp_files []= $zip_entry;
				}
			}
		}

		// Remove temporary files
		$zip->close();
		foreach ($tmp_files as $file) {
			@unlink($file);
		}

		// Prepare HTTP headers
		header('Content-Type: application/zip');
		header('Content-Length: ' . filesize($zip_file));
		header('Content-disposition: attachment; filename="archive.zip"'); 

		// Stream an arhive
		echo file_get_contents($zip_file);
		@unlink($zip_file);
		exit;
	}

}
