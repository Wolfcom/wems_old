<?php

require_once dirname(__FILE__) . '/Basic.php';


class Document extends Basic_Controller {

	/**
	 * Initialiazes the controller
	 */
	function __construct() {
		parent::__construct();

		$this->load->model('activity_model', 'activity');
		$this->load->model('document_model', 'document');
	}

	/**
	 * Returns the checksum of document.
	 * 
	 * @param string $id	an identifier of document file (UUID)
	 */
	function checksum($id) {
		// Check the user request
		$this->check_user_session();

		$document = $this->document->get($id);
		if (empty($document)) {
			header('HTTP/1.1 404 Not Found');
			return;
		}

		// Stream a checksum 
		header('Content-Type: text/plain');
		header('Content-Length: ' . strlen($document->checksum));
		header("Content-disposition: attachment; filename=\"sha256sum-{$id}.txt\"");

		echo $document->checksum;
	}


	/**
	 * Serves a document file.
	 * 
	 * @param string $id	an identifier of document file (UUID)
	 */
	function index($id = null) {
		// Check the user request
		$this->check_user_session();

		// Check the identifier of document
		$dbh = $this->load->database('default', true);
		$pgsql = $dbh->conn_id;

		$result = $dbh->query(
			'SELECT content FROM documents WHERE id = ?', array($id)
		);
		if ($result->num_rows() < 1) {
			throw new InvalidArgumentException(
				'The document with specified identifier isn\'t found'
			);
		}

		$row = $result->row_array();
		$file_data = pg_unescape_bytea($row['content']);
		$file_size = strlen($file_data);

		// Output regular HTTP headers
		header('Content-Type: image/jpeg');
		header('Content-Length: ' . $file_size);
		header('Expires: ', gmstrftime("%a, %d %b %Y %H:%M:%S GMT", time() + 365 * 86440));

		// Force to download
		if (isset($_REQUEST['force'])) {
			$document = $this->document->get($id);
			$file_ext = $this->document->get_file_extension($document->mime_type);
			header("Content-disposition: attachment; filename=\"BodyCam-{$id}.{$file_ext}\"");

			// Add the event to the event log
			$user_id = $this->session->userdata('user_id');
			$user_ip = $this->input->ip_address();
			$this->activity->add('download_document', $user_id, $user_ip, $id, 'documents');
		}

		// Try to stream a document
		echo $file_data;
		ob_flush();
		flush();
	}

	/**
	 * Saves a meta information for the specified document.
	 * 
	 * @param string $id	an identifier of document
	 * 
	 * POST variables: caseno, title, description, classification
	 */
	function save_metadata($id) {
		// Verify the request
		$this->check_user_session();

		$title = empty($_REQUEST['title']) ? null : $_REQUEST['title'];
		$caseno = empty($_REQUEST['caseno']) ? null : $_REQUEST['caseno'];
		$description = empty($_REQUEST['description']) ? null : $_REQUEST['description'];
		$classification = empty($_REQUEST['classification']) ? null : $_REQUEST['classification'];

		// Try to save meta information
		$this->document->save($id, $caseno, $title, $description, $classification);

		// Send a response
		echo json_encode(array('status' => 'success'));
	}

	/**
	 * Deletes the specified document.
	 * 
	 * @param string $id	an identifier of document
	 */
	function delete($id) {
		// Verify the request
		$this->check_user_session();

		// Try to delete
		$this->document->delete($id);

		// Send a response
		echo json_encode(array('status' => 'success'));
	}

	/**
	 * Displays the document details.
	 * 
	 * @param string $id	an identifier of document
	 */
	function details($id) {
		// Verify the request
		$this->check_user_session();

		// Prepare template variables
		$document = $this->document->get($id);
		$classifications = $this->get_case_classifications();

		$view = array(
			'header_title' => "Document Details",

			'current_user_id' => $this->session->user_id,
			'current_user_login' => $this->session->user_login,
			'current_permissions' => $this->session->user_group->permissions,
			'current_features' => $this->features,
			'app_user_list' => $this->get_user_list(),
			'classifications' => $classifications,

			'id' => $id,
			'document' => $document
		);

		// Render a page
		$this->load->view('template/header', $view);
		$this->load->view('document/details', $view);
		$this->load->view('template/footer', $view);
	}

}
