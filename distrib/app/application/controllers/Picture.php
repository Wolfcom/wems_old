<?php

require_once dirname(__FILE__) . '/Basic.php';


class Picture extends Basic_Controller {

	/**
	 * Images per page
	 */
	const PER_PAGE = 10;

	/**
	 * Initialiazes the controller
	 */
	function __construct() {
		parent::__construct();

		// Load dependencies
		$this->load->library('pagination');

		$this->load->model('activity_model', 'activity');
		$this->load->model('picture_model', 'picture');
	}

	/**
	 * Returns the checksum of picture file.
	 * 
	 * @param string $id	an identifier of picture file (UUID)
	 */
	function checksum($id) {
		// Check the user request
		$this->check_user_session();

		$picture = $this->picture->get($id);
		if (empty($picture)) {
			header('HTTP/1.1 404 Not Found');
			return;
		}

		// Stream a checksum 
		header('Content-Type: text/plain');
		header('Content-Length: ' . strlen($picture->checksum));
		header("Content-disposition: attachment; filename=\"sha256sum-{$id}.txt\"");

		echo $picture->checksum;
	}

	/**
	 * Serves a picture file
	 * 
	 * @param string $id	an identifier of picture file (UUID)
	 */
	function index($id) {
		// Check the user request
		$this->check_user_session();

		// Check the identifier of audio
		$dbh = $this->load->database('default', true);
		$pgsql = $dbh->conn_id;

		$result = $dbh->query(
			'SELECT content FROM pictures WHERE id = ?', array($id)
		);
		if ($result->num_rows() < 1) {
			throw new InvalidArgumentException(
				'The audio recording with specified identifier isn\'t found'
			);
		}

		$row = $result->row_array();
		$file_data = pg_unescape_bytea($row['content']);
		$file_size = strlen($file_data);

		// Output regular HTTP headers
		header('Content-Type: image/jpeg');
		header('Content-Length: ' . $file_size);
		header('Expires: ', gmstrftime("%a, %d %b %Y %H:%M:%S GMT", time() + 365 * 86440));

		// Force to download
		if (isset($_REQUEST['force'])) {
			$picture = $this->picture->get($id);
			header("Content-disposition: attachment; filename=\"BodyCam-{$picture->device_id}-{$id}.jpg\"");

			// Add the event to the event log
			$user_id = $this->session->userdata('user_id');
			$user_ip = $this->input->ip_address();
			$this->activity->add('download_picture', $user_id, $user_ip, $id, 'pictures');
		}

		// Try to stream a picture.
		echo $file_data;
		ob_flush();
		flush();
	}


	/**
	 * Displays the list of images.
	 */
	function list_pictures() {
		// Verify the request
		$this->check_user_session();

		// Load records
		$total_rows = $this->picture->total();
		$current_offset = empty($_REQUEST['per_page']) ? 0 : intval($_REQUEST['per_page']);
		$view['records'] = $this->picture->fetch_list(self::PER_PAGE, $current_offset);

		// Prepare pager
		$pager_config = array(
			'base_url' => base_url() . '/index.php/picture/list_pictures/',
			'page_query_string' => true,
			'per_page' => self::PER_PAGE,
			'total_rows' => $total_rows
		);

		$pager_config['first_tag_open'] = $pager_config['last_tag_open'] = 
			$pager_config['next_tag_open'] = $pager_config['prev_tag_open'] =
			$pager_config['num_tag_open'] = '<span>';

        $pager_config['first_tag_close'] = $pager_config['last_tag_close'] =
			$pager_config['next_tag_close'] = $pager_config['prev_tag_close'] =
			$pager_config['num_tag_close'] = '</span>';

		$pager_config['cur_tag_open'] = '<span><strong>';
		$pager_config['cur_tag_close'] = '</strong></span>';

		$this->pagination->initialize($pager_config);
		$view['links'] = $this->pagination->create_links();

		// Render a page
		$view['page_id'] = 'pictures_page';
		$view['header_title'] = "My Images";

		$view['current_user_id'] = $this->session->user_id;
		$view['current_user_login'] = $this->session->user_login;
		$view['current_permissions'] = $this->session->user_group->permissions;
		$view['current_features'] = $this->features;
		$view['app_user_list'] = $this->get_user_list();

		$this->load->view('template/header', $view);
		$this->load->view('view-images', $view);
		$this->load->view('template/footer', $view);
	}

	/**
	 * Displays the picture.
	 * 
	 * @param string $id	an identifier of picture
	 */
	function preview($id) {
		// Verify the request
		$this->check_user_session();

		$picture = $this->picture->get($id);
		if (empty($picture)) {
			throw new Exception("Access Denied");
		}

		// Prepare list of buttons
		$current_permissions = $this->session->user_group->permissions;
		$buttons = array();

		if (!empty($current_permissions->module_favorites)) {
			$buttons []= array(
				'id' => 'AddFavorite',
				'title' => $picture->in_favs ? 'Remove from Favorites' : 'Add to Favorites'
			);
		}

		if (!empty($current_permissions->update_picture)) {
			$buttons []= array(
				'id' => 'EditMetadata',
				'title' => 'Edit Metadata'
			);
		}

		// Prepare template variables
		$classifications = $this->get_case_classifications();
		$view = array(
			'header_title' => "Picture Details",

			'current_user_id' => $this->session->user_id,
			'current_user_login' => $this->session->user_login,
			'current_permissions' => $current_permissions,
			'current_features' => $this->features,
			'app_user_list' => $this->get_user_list(),
			'classifications' => $classifications,

			'id' => $id,
			'picture' => $picture,
			'buttons' => $buttons
		);

		// Render a page
		$this->load->view('template/header', $view);
		$this->load->view('picture/preview', $view);
		$this->load->view('template/footer', $view);
	}

	/**
	 * Saves a meta information for the specified picture.
	 * 
	 * @param string $id	an identifier of picture (UUID)
	 * 
	 * POST variables: caseno, title, description, classification
	 */
	function save_metadata($id) {
		// Verify the request
		$this->check_user_session();

		$title = trim($this->input->post('title'));
		$caseno = empty($_REQUEST['caseno']) ? null : $_REQUEST['caseno'];
		$description = trim($this->input->post('description'));
		$classification = empty($_REQUEST['classification']) ? null : $_REQUEST['classification'];

		// Try to save meta information
		$this->picture->save($id, $caseno, $title, $description, $classification);

		// Send a response
		echo json_encode(array('status' => 'success'));
	}

	/**
	 * Saves a captured video fram as the picture.
	 * 
	 * POST variables: id, title, time
	 */
	function save_snapshot() {
		// Verify the request
		$this->check_user_session();

		$video_id = trim($this->input->post('id'));
		$title = trim($this->input->post('title'));
		$time = intval($this->input->post('time'));

		// Try to upload a snapshot
		$picture_id = $this->picture->upload_snapshot(
			$this->session->user_id, $video_id, intval($time), $title
		);

		$this->uri->segments[3] = $picture_id;
		$this->uri->segments[4] = $video_id;

		// Send a response
		echo json_encode(array('status' => 'success'));
	}

	/**
	 * Deletes the specified picture
	 * 
	 * @param string $id	an identifier of picture
	 */
	function delete($id) {
		// Verify the request
		$this->check_user_session();

		// Try to delete
		$this->picture->delete($id);

		// Send a response
		echo json_encode(array('status' => 'success'));
	}

	/**
	 * Displays the picture details.
	 * 
	 * @param string $id	an identifier of picture
	 */
	function details($id) {
		// Verify the request
		$this->check_user_session();

		if (empty($id)) {
			throw new InvalidArgumentException('Empty identifier of picture');
		}

		// Prepare template variables
		$picture = $this->picture->get($id);
		$classifications = $this->get_case_classifications();

		$view = array(
			'header_title' => "Picture Details",

			'current_user_id' => $this->session->user_id,
			'current_user_login' => $this->session->user_login,
			'current_permissions' => $this->session->user_group->permissions,
			'current_features' => $this->features,
			'app_user_list' => $this->get_user_list(),
			'classifications' => $classifications,

			'id' => $id,
			'picture' => $picture
		);

		// Render a page
		$this->load->view('template/header', $view);
		$this->load->view('picture/details', $view);
		$this->load->view('template/footer', $view);
	}

}
