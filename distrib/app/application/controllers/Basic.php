<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Provides a helper methods
 */
class Basic_Controller extends CI_Controller {

	/**
	 * A list of users
	 * 
	 * @var stdClass[]
	 */
	protected $user_list = null;

	/**
	 * A list of case classifications
	 * 
	 * @var string[]
	 */
	protected $classification = null;

	/**
	 * A list of application's features
	 * 
	 * @var stdClass
	 */
	protected $features = null;

	/**
	 * Initializes the service
	 */
	function __construct() {
		parent::__construct();

		$this->load->model('profile_model', 'profile');
		$this->load->model('picklist_model', 'picklist');

		// Load a list of features
		$result = @$this->db->query("SELECT to_json(current_setting('app.features')::features) AS features");
		if ($result) {
			$this->features = json_decode($result->row()->features);
		}

		$this->session->sess_expire_on_close = true;
	}

	/**
	 * Returns the list of users
	 */
	protected function get_user_list() {
		if ($this->user_list === null) {
			$this->user_list = $this->profile->fetch_list();
		}
		return $this->user_list;
	}

	/**
	 * Checks the sessions of user.
	 * 
	 * @param string $privelege		an optional identifier of permission to force the check
	 */
	protected function check_user_session($privilege = null) {
		if (!$this->session->userdata('user_id')) {
			redirect('/index.php/user/login', 'refresh');
		}

		$this->check_group_privileges($privilege);
	}

	/**
	 * Returns a list of case classifications
	 */
	protected function get_case_classifications() {
		if ($this->classification === null) {
			$this->classification = $this->picklist->get_classification();
		}
		return $this->classification;
	}

	/**
	 * Returns the data URI string for the specified raw buffer
	 */
	protected static function get_data_string($buffer) {
		if (empty($buffer)) {
			return null;
		}

		$finfo = new finfo(FILEINFO_MIME_TYPE);
		$type = $finfo->buffer($buffer);
		return "data:{$type};base64," . base64_encode($buffer);
	}

	/**
	 * Checks the current privileges to the requested page.
	 * 
	 * @param string $req_privilege	an optional identifier of permission to force the check
	 */
	private function check_group_privileges($req_privilege = null) {
		// Detect type of requested privelege
		static $priveleges = array(
			'settings' => array(
				'index' => 'module_settings',
				'update' => 'module_settings'
			),
			'features' => array(
				'index' => 'DISALLOWED',
				'update' => 'DISALLOWED'
			),
			'activity' => array(
				'index' => 'module_settings',
				'export' => 'download_report'
			),

			'user' => array(
				'index' => 'module_users',

				'profile' => 'module_profile',
				'update_password' => 'password_profile',

				'create_profile' => 'create_user',
				'update_profile' => 'update_user',

				'delete_profile' => 'delete_user',
				'activate' => 'activate_user',
			),

			'group' => array(
				'privileges' => 'permissions_group',
				'update_permissions' => 'permissions_group',

				'index' => 'module_groups',
				'create' => 'create_group',
				'update' => 'update_group',
				'delete' => 'delete_group'
			),

			'device' => array(
				'index' => 'module_cameras',
				'status' => 'module_cameras',
				'settings' => 'module_cameras',
				'update' => 'module_cameras'
			),

			'files' => array(
				'index'	=> 'module_files',
				'search' => array('basic_search', 'advanced_search'),
				'burn_disc' => 'burn_disc',
				'upload' => 'upload_file'
			),

			'archive' => array(
				'index'	=> 'module_settings',
				'retension' => 'module_settings',

				'unset_archive_policy' => 'module_settings',
				'set_archive_policy' => 'module_settings',
				'unset_post_archive_policy' => 'module_settings',
				'set_post_archive_policy' => 'module_settings'
			),

			'cases' => array(
				'index'	=> 'module_cases',
			),

			'bookmarks' => array(
				'index' => 'module_bookmarks',
				'update' => 'update_bookmark',
				'create' => 'create_bookmark',
				'delete' => 'delete_bookmark'
			),

			'favourites' => array(
				'index'	=> 'module_favorites',
				'add' => 'module_favorites',
				'remove' => 'module_favorites'
			),

			'picture' => array(
				'list_pictures'	=> 'module_images',
				'index' => 'read_picture',
				'preview' => 'read_picture',
				'checksum' => 'read_picture',
				'details' => 'update_picture',

				'save_metadata' => 'update_picture',
				'save_snapshot' => 'snapshot_video',
				'delete' => 'delete_picture'
			),

			'video' => array(
				'list_videos' => 'module_videos',
				'index' => 'read_video',
				'preview' => 'read_video',
				'checksum' => 'read_video',
				'details' => 'update_video',

				'save_metadata' => 'update_video',
				'snapshot' => 'snapshot_video',
				'editor' => 'crop_video',
				'crop' => 'crop_video',
				'delete' => 'delete_video'
			),

			'document' => array(
				'index' => 'read_document',
				'checksum' => 'read_document',
				'details' => 'update_document',
				'save_metadata' => 'update_document',
				'delete' => 'delete_document'
			),

			'audio' => array(
				'index' => 'read_audio',
				'preview' => 'read_audio',
				'checksum' => 'read_audio',
				'details' => 'update_audio',
				'save_metadata' => 'update_audio',
				'delete' => 'delete_audio'
			)
		);

		if ($this->session->userdata('user_id') == User_model::ADMIN_USER) {
			$priveleges['features']['index'] = 'module_settings';
			$priveleges['features']['update'] = 'module_settings';
		}

		// Disable features
		if (empty($this->features->redaction)) {
			$priveleges['video']['crop'] = 'DISALLOWED';
		}

		if (empty($this->features->security)) {
			$priveleges['group']['privileges'] = 'DISALLOWED';
			$priveleges['group']['update_permissions'] = 'DISALLOWED';
		}

		if (empty($this->features->audit)) {
			$priveleges['activity']['index'] = 'DISALLOWED';
			$priveleges['activity']['export'] = 'DISALLOWED';
		}

		$module = strtolower($this->router->class); // Get name of controller
		$method = strtolower($this->router->method); // Get function name in controller

		if (empty($priveleges[$module][$method])) {
			if ($this->input->is_ajax_request()) {
				die(json_encode(array(
					'status' => 'error',
					'message' => 'Unknown action'
				)));
			}
			else {
				throw new Exception('Unknown action');
			}
			return;
		}

		// Checks the requested privilege
		if (empty($req_privilege)) {
			$req_privilege = $priveleges[$module][$method];
		}

		if (is_array($req_privilege)) {
			foreach ($req_privilege as $privilege) {
				if ($this->session->user_group->permissions->{$privilege}) {
					return;
				}
			}
		}
		else if (!empty($this->session->user_group->permissions->{$req_privilege})) {
			return;
		}

		if ($this->input->is_ajax_request()) {
			die(json_encode(array(
				'status' => 'error',
				'message' => 'Access denied'
			)));
		}
		else {
			throw new Exception('Access denied');
		}
	}

}
