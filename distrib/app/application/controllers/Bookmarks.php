<?php

require_once dirname(__FILE__) . '/Basic.php';


class Bookmarks extends Basic_Controller {

	/**
	 * Bookmarks per page
	 */
	const PER_PAGE = 10;

	/**
	 * Initialiazes the controller
	 */
	function __construct() {
		parent::__construct();

		// Load dependencies
		$this->load->library('pagination');
		$this->load->model('bookmark_model', 'bookmark');
		$this->load->model('video_model', 'video');
	}

	/**
	 * Displays the list of bookmarks.
	 */
	function index() {
		$this->check_user_session();

		// Load records
		$total_rows = $this->bookmark->total();
		$current_offset = empty($_REQUEST['per_page']) ? 0 : intval($_REQUEST['per_page']);
		$view['records'] = $this->bookmark->fetch_list(self::PER_PAGE, $current_offset);

		// Prepare pager
		$pager_config = array(
			'base_url' => base_url() . '/index.php/bookmarks/',
			'page_query_string' => true,
			'per_page' => self::PER_PAGE,
			'total_rows' => $total_rows
		);

		$pager_config['first_tag_open'] = $pager_config['last_tag_open'] = 
			$pager_config['next_tag_open'] = $pager_config['prev_tag_open'] =
			$pager_config['num_tag_open'] = '<span>';

        $pager_config['first_tag_close'] = $pager_config['last_tag_close'] =
			$pager_config['next_tag_close'] = $pager_config['prev_tag_close'] =
			$pager_config['num_tag_close'] = '</span>';

		$pager_config['cur_tag_open'] = '<span><strong>';
		$pager_config['cur_tag_close'] = '</strong></span>';

		$this->pagination->initialize($pager_config);
		$view['links'] = $this->pagination->create_links();

		// Render a page
		$view['page_id'] = 'bookmarks_page';
		$view['header_title'] = "My Bookmarks";

		$view['current_user_id'] = $this->session->user_id;
		$view['current_user_login'] = $this->session->user_login;
		$view['current_permissions'] = $this->session->user_group->permissions;
		$view['current_features'] = $this->features;
		$view['app_user_list'] = $this->get_user_list();

		$this->load->view('template/header', $view);
		$this->load->view('view-bookmarks', $view);
		$this->load->view('template/footer', $view);
	}

	/**
	 * Displays the bookmark dialog.
	 * 
	 * POST variables: time
	 * 
	 * @param string $id	an identifier of media file
	 */
	function add($id) {
		// Prepare template variables
		$left_buttons = array(
			array(
				'id' => 'CreateBookmark',
				'title' => 'Save'
			),
		);

		$view = array(
			'title' => 'Add Bookmark',
			'dialog_id' => 'bookmarkEditor',
			'left_buttons' => $left_buttons,

			'id' => $id,
			'time' => intval($this->input->post('time'))
		);

		// Render a page
		$this->load->view('template/modalheader', $view);
		$this->load->view('bookmark/add', $view);
		$this->load->view('template/modalfooter', $view);
	}

	/**
	 * Displays the bookmark editor.
	 * 
	 * @param string $id	an identifier of bookmark
	 */
	function edit($id) {
		// Prepare a view
		$bookmark = $this->bookmark->get($id);
		$left_buttons = array(
			array(
				'id' => 'UpdateBookmark',
				'title' => 'Save'
			),
		);

		$view = array(
			'title' => 'Edit Bookmark',
			'dialog_id' => 'bookmarkEditor',
			'left_buttons' => $left_buttons,

			'thumbnail' => self::get_data_string($bookmark->thumbnail),
			'bookmark' => $bookmark
		);

		// Render a page
		$this->load->view('template/modalheader', $view);
		$this->load->view('bookmark/edit', $view);
		$this->load->view('template/modalfooter', $view);
	}

	/**
	 * Creates a new bookmark for the specified video.
	 * 
	 * POST variables: id, description, time
	 */
	function create() {
		// Verify the request
		$this->check_user_session();

		$video_id = trim($this->input->post('id'));
		$time = intval(trim($this->input->post('time')));
		$description = trim($this->input->post('description'));

		// Try to create a bookmark
		$bookmark = $this->bookmark->create(
			$this->session->user_id, $video_id, $time, $description
		);

		$this->uri->segments[3] = $bookmark->id;
		$this->uri->segments[4] = $video_id;

		// Send a response
		echo json_encode(array(
			'status' => 'success',
			'bookmark' => $bookmark
		));
	}

	/**
	 * Updates a description for the specified bookmark.
	 * 
	 * POST variables: description
	 * 
	 * @param string $id	an identifier of bookmark
	 */
	function update($id) {
		// Verify the request
		$this->check_user_session();

		// Try to create a bookmark
		$description = trim($this->input->post('description'));
		$bookmark = $this->bookmark->update($id, $description);

		$this->uri->segments[4] = $this->bookmark->get($id)->video_id;

		// Send a response
		echo json_encode(array(
			'status' => 'success',
			'bookmark' => $bookmark
		));
	}

	/**
	 * Deletes the specified bookmark.
	 * 
	 * @param string $id	an identifier of bookmark
	 */
	function delete($id) {
		// Verify the request
		$this->check_user_session();

		// Try to delete
		$this->uri->segments[4] = $this->bookmark->get($id)->video_id;
		$this->bookmark->delete($id);

		// Send a response
		echo json_encode(array('status' => 'success'));
	}

}
