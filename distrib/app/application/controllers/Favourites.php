<?php

require_once dirname(__FILE__) . '/Basic.php';

class Favourites extends Basic_Controller {

	/**
	 * Favourites per page
	 */
	const PER_PAGE = 10;

	/**
	 * Initialiazes the controller
	 */
	function __construct() {
		parent::__construct();

		// Load dependencies
		$this->load->library('pagination');
		$this->load->model('favorite_model', 'favorite');
	}

	/**
	 * Displays the list of favourites
	 */
	function index() {
		$this->check_user_session();

		// Load records
		$total_rows = $this->favorite->total();
		$current_offset = empty($_REQUEST['per_page']) ? 0 : intval($_REQUEST['per_page']);
		$view['records'] = $this->favorite->fetch_list(self::PER_PAGE, $current_offset);

		// Prepare pager
		$pager_config = array(
			'base_url' => base_url() . '/index.php/favourites/',
			'page_query_string' => true,
			'per_page' => self::PER_PAGE,
			'total_rows' => $total_rows
		);

		$pager_config['first_tag_open'] = $pager_config['last_tag_open'] = 
			$pager_config['next_tag_open'] = $pager_config['prev_tag_open'] =
			$pager_config['num_tag_open'] = '<span>';

        $pager_config['first_tag_close'] = $pager_config['last_tag_close'] =
			$pager_config['next_tag_close'] = $pager_config['prev_tag_close'] =
			$pager_config['num_tag_close'] = '</span>';

		$pager_config['cur_tag_open'] = '<span><strong>';
		$pager_config['cur_tag_close'] = '</strong></span>';

		$this->pagination->initialize($pager_config);
		$view['links'] = $this->pagination->create_links();

		// Render a page
		$view['page_id'] = 'favorites_page';
		$view['header_title'] = 'My Favorites';

		$view['current_user_id'] = $this->session->user_id;
		$view['current_user_login'] = $this->session->user_login;
		$view['current_permissions'] = $this->session->user_group->permissions;
		$view['current_features'] = $this->features;
		$view['app_user_list'] = $this->get_user_list();

		$view['first_name'] = $this->session->user_first_name;
		$view['last_name'] = $this->session->user_last_name;

		$this->load->view('template/header', $view);
		$this->load->view('view-favorites', $view);
		$this->load->view('template/footer', $view);
	}

	/**
	 * Adds the specified media file to favourites
	 * 
	 * @param string $id   an identifier of media file to add
	 */
	function add($id) {
		// Check the user request
		$this->check_user_session();

		// Try to add a new favourite file
		$this->favorite->add($id);

		// Send a response
		echo json_encode(array('status' => 'success'));
	}

	/**
	 * Removes the specified media file from favourites
	 * 
	 * @param string $id   an identifier of media file to remove
	 */
	function remove($id) {
		// Check the user request
		$this->check_user_session();

		// Try to delete file from favourites
		$this->favorite->remove($id);

		// Send a response
		echo json_encode(array('status' => 'success'));
	}

}
