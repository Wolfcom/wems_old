<?php

require_once dirname(__FILE__) . '/Basic.php';

class User extends Basic_Controller {

	/**
	 * Initialiazes the controller
	 */
	function __construct() {
		parent::__construct();

		$this->load->model('user_model', 'user');
		$this->load->model('profile_model', 'profile');
		$this->load->model('group_model', 'group');
	}

	/**
	 * Performs a user authentication.
	 * 
	 * POST variables: username, password
	 */
	function authenticate() {
		// Try to authenticate an user
		$login = $this->input->post('username');
		$password = $this->input->post('password');
		$user_id = $this->user->authenticate($login, $password);

		if (empty($user_id)) {
			die(json_encode(array(
				'status' => 'error',
				'title' => empty($this->db->conn_id) ? 'System Startup' :  'Invalid login',
				'message' => empty($this->db->conn_id)
					? 'Please wait for the system to startup....' 
					: 'Incorrect username or password, please try again!',
					
			)));
		}

		// The user is authenticated, so setup the user session
		$session = array(
			'user_id' => $user_id,
			'user_login' => $login,
			'user_group' => new stdClass
		);

		$profile = $this->profile->get($user_id);
		if ($profile) {
			$session['user_first_name'] = $profile->first_name;
			$session['user_last_name'] = $profile->last_name;
			$session['user_group'] = $profile->group;
			$session['user_photo'] = self::get_data_string($profile->photo);
		}

		$this->session->set_userdata($session);

		// Send a response
		echo json_encode(array('status' => 'success'));
	}

	/**
	 * Displays the login form
	 */
	function login() {
		// Check the user session
		if ($this->session->userdata('user_id')) {
			// Go to dashboard
			redirect('/index.php/app/index', 'refresh');
		}

		$view['header_title'] = 'Wolfcom Evidence Management System';
		$view['header_data'] = array(
			'class_in_body' => 'login-page login-form-fall'
		);

		// Render the page
		$this->load->view('template/header', $view);
		$this->load->view('view-login', $view);
		$this->load->view('template/footer', $view);
	}

	/**
	 * Destroys the user session
	 */
	function logout() {
		$this->session->sess_destroy();
		redirect('/index.php/user/login', 'refresh');
	}

	/**
	 * Displays the list of users
	 */
	function index() {
		$this->check_user_session();

		// Fetch the list of profiles
		$groups =  $this->group->fetch_list();
		$profiles = $this->profile->fetch_list();
		if ($profiles) {
			foreach ($profiles as $profile) {
				$profile->photo = self::get_data_string($profile->photo);
			}
		}

		$view = array(
			'page_id' => 'users_page',
			'header_title' => "Users",

			'current_user_id' => $this->session->user_id,
			'current_user_login' => $this->session->user_login,
			'current_permissions' => $this->session->user_group->permissions,
			'current_features' => $this->features,
			'current_id' => $this->session->userdata('user_id'),
			'app_user_list' => $this->get_user_list(),

			'user_list' => $profiles,
			'group_list' => $groups
		);

		// Render a page
		$this->load->view('template/header', $view);
		$this->load->view('users/user-management', $view);
		$this->load->view('template/footer', $view);
	}

	/**
	 * Displays the profile page of current user
	 */
	function profile() {
		// Check the user session
		$this->check_user_session();

		// Prepare a profile of user
		$profile = $this->profile->get($this->session->userdata('user_id'));
		$view = array(
			'page_id' => 'profile_page',
			'header_title' => "Profile",

			'current_user_id' => $this->session->user_id,
			'current_user_login' => $this->session->user_login,
			'current_permissions' => $this->session->user_group->permissions,
			'current_features' => $this->features,
			'app_user_list' => $this->get_user_list(),

			'login' => $profile->login,
			'email' => $profile->email,
			'user_photo' => self::get_data_string($profile->photo),

			'device_id' => $profile->device_id,
			'device_type' => $profile->device_type,

			'first_name' => $profile->first_name,
			'last_name' => $profile->last_name,

			'group' => $profile->group,
			'position' => $profile->position,
			'department' => $profile->department
		);

		// Render a page
		$this->load->view('template/header', $view);
		$this->load->view('users/myprofile', $view);
		$this->load->view('template/footer', $view);
	}

	/**
	 * Resets the password of current user.
	 * 
	 * POST variables: password
	 */
	function update_password() {
		// Check the user session
		$this->check_user_session();

		$user_id = $this->session->userdata('user_id');
		$this->uri->segments[3] = $user_id;

		$password = $this->input->post('password');
		if (empty($password)) {
			die(json_encode(array(
				'status' => 'error',
				'message' => 'Empty Password'
			)));
		}

		// Try to update password
		$this->user->update_password($user_id, $password);

		// Send a response
		echo json_encode(array('status' => 'success'));
    }

	/**
	 * Changes the photo of current user.
	 * 
	 * DISABLED at current moment.
	 */
	function update_photo() {
		// Check the user session
		$user_id = $this->session->userdata('user_id');
		$this->check_user_session();

		// Try to update a photo
		$data = file_get_contents($_FILES["file"]["tmp_name"]);
		$this->profile->update_photo($user_id, $data);
		$this->session->set_userdata('user_photo', self::get_data_string($data));

		// Send a response
		echo json_encode(array('status' => 'success'));
	}

	/**
	 * Updates the profile of user.
	 * If the identifier of user isn't specified then updates the profile of current user.
	 * 
	 * POST variables: first_name, last_name, position, department, email, group,
	 * 	device_id, device_type
	 * 
	 * @param string $user_id	 an optional identifier of user to update
	 */
	function update_profile($user_id = null) {
		// Check the user session
		if (!$this->session->userdata('user_id')) {
			die(json_encode(array(
				'status' => 'error',
				'message' => 'Access Denied'
			)));
		}

		if (empty($user_id)) {
			// Update the current user
			$user_id = $this->session->userdata('user_id');
		}
		else {
			$this->check_user_session();
		}

		// Prepare profile fields
		$first_name = trim($this->input->post('first_name'));
		$last_name = trim($this->input->post('last_name'));

		$profile_fields = array(
			'first_name' => $first_name,
			'last_name' => $last_name,
			'position' => trim($this->input->post('position')),
			'department' => trim($this->input->post('department'))
		);

		if (!empty($_FILES["photo"]["tmp_name"])) {
			$profile_fields['photo'] = file_get_contents($_FILES["photo"]["tmp_name"]);
		}

		// Prepare device fields
		$device_id = trim($this->input->post('device_id'));
		$device_fields = array(
			'type' => trim($this->input->post('device_type'))
		);

		// Try to update a profile
		try {
			$this->profile->update_profile(
				$user_id, $profile_fields, $device_id, $device_fields
			);
		}
		catch (Exception $e) {
			die(json_encode(array(
				'status' => 'error',
				'message' => $e->getMessage()
			)));
		}

		if ($user_id == $this->session->user_id) {
			$this->session->set_userdata(array(
				'user_first_name' => $first_name,
				'user_last_name' => $last_name
			));
		}

		// Try to update an account
		$account_fields = array();
		$password = trim($this->input->post('password'));

		if (isset($_REQUEST['email'])) {
			$account_fields['email'] = trim($this->input->post('email'));
		}

		if (isset($_REQUEST['group']) && $user_id != User_model::ADMIN_USER) {
			$account_fields['group'] = trim($this->input->post('group'));
		}

		$this->user->update_account($user_id, $account_fields, $password);

		// Send a response
		echo json_encode(array('status' => 'success'));
    }

	/**
	 * Creates a new user.
	 * 
	 * POST variables: first_name, last_name, position, department, 
	 *  login, password, email, group, device_id, device_type
	 */
	function create_profile() {
		// Check the user session
		$this->check_user_session();

		// Create an user account
		$login = trim($this->input->post('login'));
		$password = trim($this->input->post('password'));
		$email = trim($this->input->post('email'));
		$group = trim($this->input->post('group'));

		$this->db->trans_start();
		$user_id = $this->user->create_account($login, $password, $email, $group);
		$this->uri->segments[3] = $user_id;

		// Prepare an user profile
		$profile_fields = array(
			'first_name' => trim($this->input->post('first_name')),
			'last_name' => trim($this->input->post('last_name')),
			'position' => trim($this->input->post('position')),
			'department' => trim($this->input->post('department'))
		);

		if (!empty($_FILES["photo"]["tmp_name"])) {
			$profile_fields['photo'] = file_get_contents($_FILES["photo"]["tmp_name"]);
		}

		// Prepare device fields
		$device_id = trim($this->input->post('device_id'));
		$device_fields = array(
			'type' => trim($this->input->post('device_type'))
		);

		// Try to create profile
		try {
			$this->profile->update_profile($user_id, $profile_fields, $device_id, $device_fields);
		}
		catch (Exception $e) {
			die(json_encode(array(
				'status' => 'error',
				'message' => $e->getMessage()
			)));
		}
		$this->db->trans_complete();

		// Send a response
		echo json_encode(array('status' => 'success'));
	}

	/**
	 * Deletes the specified user.
	 * 
	 * @param string $id	an identifier of user
	 */
	function delete_profile($id) {
		if ($id == User_model::ADMIN_USER) {
			die(json_encode(array('status' => 'error', 'message' => 'Access Denied')));
		}

		// Check the user session
		$this->check_user_session();

		// Try to delete profile and account
		$this->user->delete_account($id);

		// Send a response
		echo json_encode(array('status' => 'success'));
	}

	/**
	 * Activates the specified user.
	 * 
	 * @param string $id	an identifier of user
	 */
	function activate($id) {
		// Check the user session
		$this->check_user_session();

		// Try to enable account
		$this->user->activate_account($id);

		// Send a response
		echo json_encode(array('status' => 'success'));
	}

}
