<?php

require_once dirname(__FILE__) . '/Basic.php';

class Cases extends Basic_Controller {

	const PER_PAGE = 10;

	/**
	 * Initialiazes the controller
	 */
	function __construct() {
		parent::__construct();

		// Load dependencies
		$this->load->library('pagination');
		$this->load->model('mycase_model');
	}

	/**
	 * Displays the list of cases
	 */
	function index() {
		// Check the user request
		$this->check_user_session();

		// Prepare list of cases with files
		$records = $this->mycase_model->fetch_list();
		$cases = array();
		foreach ($records as $record) {
			$case = $record->caseno;
			$cases[$case] []= $record;
		}

        // Prepare variables of template
		$view = array(
			'page_id' => 'cases_page',
			'header_title' => "My Cases",

			'current_user_id' => $this->session->user_id,
			'current_user_login' => $this->session->user_login,
			'current_permissions' => $this->session->user_group->permissions,
			'current_features' => $this->features,
			'app_user_list' => $this->get_user_list(),

			'links' => '',
			'cases' => $cases
		);

		// Render a page
		$this->load->view('template/header', $view);
		$this->load->view('view-mycase', $view);
		$this->load->view('template/footer', $view);
	}

}
