<?php

require_once dirname(__FILE__) . '/Basic.php';

class Activity extends Basic_Controller {

	/**
	 * Log entries per page
	 */
	const PER_PAGE = 10;

	/**
	 * Initialiazes the controller
	 */
	function __construct() {
		parent::__construct();

		// Load dependencies
		$this->load->library('pagination');
		$this->load->model('activity_model', 'activity');
	}

	/**
	 * Displays the list of system events
	 */
	function index() {
		// Check the user request
		$user_id = $this->input->get_post('user');
		$record_id = $this->input->get_post('record');

		if ($user_id) {
			$this->check_user_session('report_user');
		}
		else if ($record_id) {
			$this->check_user_session('read_report');
		}
		else {
			$this->check_user_session();
		}

		// Prepare the extra filter
		$filter = array();
		if (!empty($_REQUEST['filter-field']) && !empty($_REQUEST['filter-value'])) {
			$key = $_REQUEST['filter-field'];
			$filter[$key] = $_REQUEST['filter-value'];
		}

		// Load records
		$total_rows = $this->activity->total($user_id, $record_id, $filter);
		$current_offset = intval($this->input->get_post('per_page'));

		$view['records'] = $this->activity->fetch_list(
			$user_id, $record_id, self::PER_PAGE, $current_offset, $filter
		);

		// Prepare pager
		$pager_config = array(
			'base_url' => base_url() . '/index.php/activity/index/',
			'page_query_string' => true,
			'per_page' => self::PER_PAGE,
			'total_rows' => $total_rows
		);

		if ($filter) {
			$pager_config['base_url'] .= '?filter-field=' . key($filter) 
				. '&filter-value=' . urlencode(current($filter));
		}

		$pager_config['first_tag_open'] = $pager_config['last_tag_open'] = 
			$pager_config['next_tag_open'] = $pager_config['prev_tag_open'] =
			$pager_config['num_tag_open'] = '<span>';

        $pager_config['first_tag_close'] = $pager_config['last_tag_close'] =
			$pager_config['next_tag_close'] = $pager_config['prev_tag_close'] =
			$pager_config['num_tag_close'] = '</span>';

		$pager_config['cur_tag_open'] = '<span><strong>';
		$pager_config['cur_tag_close'] = '</strong></span>';

		$this->pagination->initialize($pager_config);
		$view['links'] = $this->pagination->create_links();

		// Render a page
		$view['page_id'] = 'activity_page';
		$view['header_title'] = "System Security Logs";
		$view['has_filter'] = empty($user_id) && empty($record_id);

		$view['current_user_id'] = $this->session->user_id;
		$view['current_user_login'] = $this->session->user_login;
		$view['current_permissions'] = $this->session->user_group->permissions;
		$view['current_features'] = $this->features;
		$view['app_user_list'] = $this->get_user_list();

		$this->load->view('template/header', $view);
		$this->load->view('view-activity', $view);
		$this->load->view('template/footer', $view);
	}

	/**
	 * Serves an audit report in the text format.
	 * 
	 * @param string $id	an identifier of entity related to log entries
	 */
	function export($id) {
		// Check the user request
		$this->check_user_session();

		// Send a response
		$report = $this->activity->report($id);

		header('Content-Type: text/plain');
		header('Content-Length: ' . strlen($report));
		header("Content-disposition: attachment; filename=\"report.txt\"");

		echo $report;
	}

}
