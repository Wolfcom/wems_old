<?php

require_once dirname(__FILE__) . '/Basic.php';

class Group extends Basic_Controller {

	/**
	 * Initialiazes the controller
	 */
	function __construct() {
		parent::__construct();

		$this->load->model('group_model', 'group');
		$this->load->model('profile_model', 'profile');
	}

	/**
	 * Displays the list of groups
	 */
	function index() {
		$this->check_user_session();

		// Prepare the list of groups/users
		$groups = $this->group->fetch_list();
		$profiles = $this->profile->fetch_list();
		if ($profiles) {
			foreach ($profiles as $index => $profile) {
				if (!empty($profile->group)) {
					unset($profiles[$index]);
					continue;
				}

				$profile->photo = self::get_data_string($profile->photo);
			}
		}

		$user_ids = array();
		foreach ($profiles as $user) {
			$user_ids []= $user->id;
		}

		$view = array(
			'page_id' => 'groups_page',
			'header_title' => "Groups",

			'current_user_id' => $this->session->user_id,
			'current_user_login' => $this->session->user_login,
			'current_permissions' => $this->session->user_group->permissions,
			'current_features' => $this->features,
			'app_user_list' => $this->get_user_list(),

			'group_list' => $groups,
			'user_list' => $profiles
		);

		// Render a page
        $this->load->view('template/header', $view);
        $this->load->view('users/group-management', $view);
        $this->load->view('template/footer', $view);
	}

	/**
	 * Displays a list of group privileges.
	 * 
	 * @param string $id	an identifier of group
	 */
	function privileges($id) {
		// Very the user request
		$this->check_user_session();

		if (empty($id)) {
			throw new InvalidArgumentException("Empty identifier of group");
		}

		// Prepare template variables
		$group = $this->group->get($id);
		$view = array(
			'page_id' => 'permissions_page',
			'header_title' => "Group Privileges",

			'current_user_id' => $this->session->user_id,
			'current_user_login' => $this->session->user_login,
			'current_permissions' => $this->session->user_group->permissions,
			'current_features' => $this->features,
			'app_user_list' => $this->get_user_list(),

			'group' => $group,
			'permissions' => $group->permissions
		);

		// Render a page
		$this->load->view('template/header', $view);
		$this->load->view('users/user-privileges', $view);
		$this->load->view('template/footer', $view);
	}

	/**
	 * Updates the list of permissions.
	 * 
	 * @param string $id	an identifier of group
	 */
	function update_permissions($id) {
		// Verify the user request
		$this->check_user_session();

		// Try to update a set of permissions
		$permissions = $this->input->post('permissions');
		$this->group->update_permissions($id, $permissions);

		if ($this->session->user_group->id == $id) {
			// Reload current permissions
			$group = $this->session->user_group;
			foreach ($group->permissions as $permission => $old_value) {
				$group->permissions->{$permission} = !empty($permissions[$permission]);
			}
			$this->session->set_userdata(array('user_group' => $group));
		}

		// Send a response
		echo json_encode(array('status' => 'success'));
	}

	/**
	 * Creates the new group
	 * 
	 * POST variables: name, members
	 */
	function create() {
		// Verify the user request
		$this->check_user_session();

		// Try to created a group
		$name = trim($this->input->post('name'));
		$members = empty($_POST['members']) ? array() : $_POST['members'];

		$group_id = $this->group->create($name, $members, true);
		$this->uri->segments[3] = $group_id;

		// Send a response
		echo json_encode(array('status' => 'success'));
	}

	/**
	 * Update information about group, assigns new members.
	 * 
	 * POST variables: name, members
	 * 
	 * @param string $id	an identifier of group
	 */
	function update($id) {
		// Verify the user request
		if ($id == Group_model::ADMIN_GROUP) {
			die(json_encode(array('status' => 'error', 'message' => 'Access Denied')));
		}

		$this->check_user_session();

		// Try to update a group details
		$name = trim($this->input->post('name'));
		$members = empty($_POST['members']) ? array() : $_POST['members'];
		$this->group->update($id, $name, $members);

		// Send a response
		echo json_encode(array('status' => 'success'));
	}

	/**
	 * Deletes the specified group.
	 * 
	 * @param string $id	an identifier of group
	 */
	function delete($id) {
		// Verify the user request
		if ($id == Group_model::ADMIN_GROUP) {
			die(json_encode(array('status' => 'error', 'message' => 'Access Denied')));
		}

		$this->check_user_session();

		// Try to delete profile and account
		$this->group->delete($id);

		// Send a response
		echo json_encode(array('status' => 'success'));
	}

}
