<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class VideoThumbnail extends CI_Controller {

	public function __construct() {
		parent::__construct();
	}

	/**
	 * Serves a thumbnail for video file
	 * 
	 * @param string $id	an identifier of video file (UUID)
	 */
	public function index($id = null) {
		if (empty($id)) {
			die('Empty identifier of video file');
		}

		// Check the identifier of audio
		$dbh = $this->load->database('default', true);
		$pgsql = $dbh->conn_id;

		$result = $dbh->query(
			'SELECT thumbnail FROM videos WHERE id = ?', array($id)
		);
		if ($result->num_rows() < 1) {
			throw new InvalidArgumentException(
				'The video with specified identifier isn\'t found'
			);
		}

		$row = $result->row_array();
		$file_data = pg_unescape_bytea($row['thumbnail']);
		$file_size = strlen($file_data);

		// Output regular HTTP headers
		header('Content-Type: image/jpeg');
		header('Content-Length: ' . $file_size);
		header('Expires: ', gmstrftime("%a, %d %b %Y %H:%M:%S GMT", time() + 365 * 86440));

		// Try to stream a thumbnail.
		echo $file_data;
		ob_flush();
		flush();
		exit;
	}

	/**
	 * Serves a thumbnail for the specified bookmark.
	 * 
	 * @param string $id	an identifier of bookmakr (UUID)
	 */
	public function bookmark($id = null) {
		if (empty($id)) {
			die('Empty identifier of bookmark');
		}

		// Check the identifier of bookmark
		$dbh = $this->load->database('default', true);
		$pgsql = $dbh->conn_id;

		$result = $dbh->query(
			'SELECT thumbnail FROM bookmarks WHERE id = ?', array($id)
		);
		if ($result->num_rows() < 1) {
			throw new InvalidArgumentException(
				'The bookmark with specified identifier isn\'t found'
			);
		}

		$row = $result->row_array();
		$file_data = pg_unescape_bytea($row['thumbnail']);
		$file_size = strlen($file_data);

		// Output regular HTTP headers
		header('Content-Type: image/jpeg');
		header('Content-Length: ' . $file_size);
		header('Expires: ', gmstrftime("%a, %d %b %Y %H:%M:%S GMT", time() + 365 * 86440));

		// Try to stream a thumbnail.
		echo $file_data;
		ob_flush();
		flush();
		exit;
	}

}
