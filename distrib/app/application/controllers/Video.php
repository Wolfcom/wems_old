<?php

require_once dirname(__FILE__) . '/Basic.php';


class Video extends Basic_Controller {

	/**
	 * A size of video chunk to send in one network packet
	 */
	const CHUNK_SIZE = 4096;

	/**
	 * Videos per page
	 */
	const PER_PAGE = 10;

	/**
	 * Initialiazes the controller
	 */
	function __construct() {
		parent::__construct();

		// Load dependencies
		$this->load->library('pagination');

		$this->load->model('activity_model', 'activity');
		$this->load->model('video_model', 'video');
		$this->load->model('bookmark_model', 'bookmark');
	}

	/**
	 * Returns the checksum of video file.
	 * 
	 * @param string $id	an identifier of video file (UUID)
	 */
	function checksum($id) {
		// Check the user request
		$this->check_user_session();

		$video = $this->video->get($id);
		if (empty($video)) {
			header('HTTP/1.1 404 Not Found');
			return;
		}

		// Stream a checksum 
		header('Content-Type: text/plain');
		header('Content-Length: ' . strlen($video->checksum));
		header("Content-disposition: attachment; filename=\"sha256sum-{$id}.txt\"");

		echo $video->checksum;
	}

	/**
	 * Serves a video file.
	 * 
	 * @param string $id	an identifier of video file (UUID)
	 */
	function index($id) {
		// Check the user request
		$this->check_user_session();

		// Check the identifier of video
		$dbh = $this->load->database('default', true);
		$pgsql = $dbh->conn_id;

		$result = $dbh->query(
			'SELECT content, lo_size(content) AS size FROM videos WHERE id = ?',
			array($id)
		);
		if ($result->num_rows() < 1) {
			throw new InvalidArgumentException('The video with specified identifier isn\'t found');
		}

		$row = $result->row_array();
		$file_id = $row['content'];
		$file_size = $row['size'];

		// Output regular HTTP headers
		header('Content-Type: video/mp4');
		header('Accept-Ranges: bytes');

		// Check for content range
		if (isset($_SERVER['HTTP_RANGE'])) {
			list($size_unit, $ranges) = explode('=', $_SERVER['HTTP_RANGE'], 2);
			if ($size_unit != 'bytes') {
				header('HTTP/1.1 416 Requested Range Not Satisfiable');
				exit;
			}

			// TODO: multiple ranges
			list($range,) = explode(',', $ranges, 2);
			@list($seek_start, $seek_end) = explode('-', $range, 2);
			$seek_start = abs(intval($seek_start));
			$seek_end = abs(intval($seek_end));
		}

		// Set start/end positions based on range (if set), else set defaults
		if (empty($seek_end)) {
			$seek_end = $file_size - 1;
		}
		else {
			$seek_end = min($seek_end, $file_size - 1);
		}

		if (empty($seek_start) || $seek_end < $seek_start) {
			$seek_start = 0;
		}
		else {
			$seek_start = max($seek_start, 0);
		}

		// Output range headers
		if (isset($_SERVER['HTTP_RANGE'])) {
			header('HTTP/1.1 206 Partial Content');
			header('Content-Range: bytes ' . $seek_start . '-' . $seek_end . '/' . $file_size);

			$content_length = $seek_end - $seek_start + 1;
		}
		else {
			$content_length = $file_size;
		}
		header('Content-Length: ' . $content_length);

		// Force to download
		if (isset($_REQUEST['force'])) {
			$video = $this->video->get($id);
			header("Content-disposition: attachment; filename=\"BodyCam-{$video->device_id}-{$id}.mp4\"");

			// Add the event to the event log
			$user_id = $this->session->userdata('user_id');
			$user_ip = $this->input->ip_address();
			$this->activity->add('download_video', $user_id, $user_ip, $id, 'videos');
		}

		// Try to stream a video.
		// Large Object API requires active transaction before any operation.
		session_write_close();
		$dbh->trans_start();
		$handle = pg_lo_open($pgsql, $file_id, "r");
		pg_lo_seek($handle, $seek_start, PGSQL_SEEK_SET);

		$read_bytes = $content_length;
		while ($read_bytes > 0) {
			echo pg_lo_read($handle, self::CHUNK_SIZE);

			ob_flush();
			flush();

			$read_bytes -= self::CHUNK_SIZE;
		}

		pg_lo_close($handle);
		$dbh->trans_complete();
	}

	/**
	 * Crops a specified video file.
	 * 
	 * POST variables: id, title
	 */
	function crop() {
		// Checks the user request
		$this->check_user_session();
		$params = $this->input->post();
		
		// Validate input
		if (empty($params['id'])) {
			throw new InvalidArgumentException('Empty identifier of video file');
		}
		else {
			$source_id = trim($params['id']);
		}

		if ($params['start'] === null) {
			throw new InvalidArgumentException('Empty start time');
		}
		else {
			$start = intval($params['start']);
		}

		if (empty($params['end'])) {
			throw new InvalidArgumentException('Empty end time');
		}
		else {
			$end = intval($params['end']);
		}

		if ($start > $end) {
			throw new InvalidArgumentException('Start time is bigger than End time');
		}

		$title = empty($params['title']) ?  null : $params['title'];

		// Try crop video:
		// Check the identifier of video
		$dbh = $this->load->database('default', true);
		$pgsql = $dbh->conn_id;

		$result = $dbh->query('SELECT content FROM videos WHERE id = ?', array($source_id));
		if ($result->num_rows() < 1) {
			throw new InvalidArgumentException('The video with specified identifier isn\'t found');
		}

		$row = $result->row_array();
		$src_file_id = $row['content'];

		// Try to export video to perform edit functions.
		// Large Object API requires active transaction before any operation.
		$dbh->trans_start();
		$src_file = tempnam(sys_get_temp_dir(), 'VIDEO');
		pg_lo_export($pgsql, $src_file_id, $src_file);

		// Try to import modified video into database
		$dst_file = tempnam(sys_get_temp_dir(), 'VIDEO');
		date_default_timezone_set('UTC');
		$start_time = date('H:i:s', $start);
		$duration = date('H:i:s', $end - $start);

		$program = $this->video->FFMPEG;
		$command = "{$program} -i {$src_file} -y -vcodec copy -f mp4 -ss {$start_time} -t {$duration} {$dst_file}";
		$output = shell_exec($command);

		$dst_file_id = pg_lo_import($pgsql, $dst_file);
		$user_id = $this->session->user_id;

		// Try to make thumbnail
		$thumbnail = pg_escape_bytea(
			$this->video->prepare_thumbnail_from_file($dst_file, '00:00:01')
		);

		$new_id = generate_uuid();
		$query = <<<EOL
INSERT INTO videos (id, created, duration, width, height, device_id, user_id, content, thumbnail, title)
SELECT ?, NOW(), ?, width, height, 0, ?, ?, ?, ? FROM videos WHERE id = ?
EOL;
		$params = array(
			$new_id, $duration, $user_id, $dst_file_id, $thumbnail,
			$title,	$source_id
		);
		$dbh->query($query, $params);
		$dbh->trans_complete();

		// Remove temporary files
		@unlink($src_file);
		@unlink($dst_file);

		// Send a response
		$this->uri->segments[3] = $new_id;
		$this->uri->segments[4] = $source_id;

		echo json_encode(array(
			'status' => 'success',
		));
	}

	/**
	 * Displays the list of videos.
	 */
	function list_videos() {
		$this->check_user_session();

		// Load records
		$total_rows = $this->video->total();
		$current_offset = empty($_REQUEST['per_page']) ? 0 : intval($_REQUEST['per_page']);
		$view['records'] = $this->video->fetch_list(self::PER_PAGE, $current_offset);

		// Prepare pager
		$pager_config = array(
			'base_url' => base_url() . '/index.php/video/list_videos/',
			'page_query_string' => true,
			'per_page' => self::PER_PAGE,
			'total_rows' => $total_rows
		);

		$pager_config['first_tag_open'] = $pager_config['last_tag_open'] = 
			$pager_config['next_tag_open'] = $pager_config['prev_tag_open'] =
			$pager_config['num_tag_open'] = '<span>';

        $pager_config['first_tag_close'] = $pager_config['last_tag_close'] =
			$pager_config['next_tag_close'] = $pager_config['prev_tag_close'] =
			$pager_config['num_tag_close'] = '</span>';

		$pager_config['cur_tag_open'] = '<span><strong>';
		$pager_config['cur_tag_close'] = '</strong></span>';

		$this->pagination->initialize($pager_config);
		$view['links'] = $this->pagination->create_links();

		// Render a page
		$view['page_id'] = 'video_page';
		$view['header_title'] = "My Videos";

		$view['current_user_id'] = $this->session->user_id;
		$view['current_user_login'] = $this->session->user_login;
		$view['current_permissions'] = $this->session->user_group->permissions;
		$view['current_features'] = $this->features;
		$view['app_user_list'] = $this->get_user_list();

		$this->load->view('template/header', $view);
		$this->load->view('view-videos', $view);
		$this->load->view('template/footer', $view);
	}

	/**
	 * Displays the video recording.
	 * 
	 * @param string $id	an identifier of video recording
	 */
	function preview($id) {
		// Verify the request
		$this->check_user_session();

		$video = $this->video->get($id);
		if (empty($video)) {
			throw new Exception('Access Denied');
		}

		// Prepare list of buttons
		$current_permissions = $this->session->user_group->permissions;
		$buttons = array();

		if (!empty($current_permissions->module_favorites)) {
			$buttons []= array(
				'id' => 'AddFavorite',
				'title' => $video->in_favs ? 'Remove from Favorites' : 'Add to Favorites'
			);
		}

		if (!empty($current_permissions->update_video)) {
			$buttons []= array(
				'id' => 'EditMetadata',
				'title' => 'Edit Metadata'
			);
		}

		if (!empty($current_permissions->module_bookmarks)) {
			$buttons []= array(
				'id' => 'ShowBookmarks',
				'title' => 'Show Bookmarks'
			);
		}

		if (!empty($current_permissions->map_video) && !empty($this->features->gps_map)) {
			$buttons []= array(
				'id' => 'ShowMap',
				'title' => 'Show Map'
			);
		}

		// Prepare template variables
		$classifications = $this->get_case_classifications();
		$view = array(
			'header_title' => "Video Details",

			'current_user_id' => $this->session->user_id,
			'current_user_login' => $this->session->user_login,
			'current_permissions' => $current_permissions,
			'current_features' => $this->features,
			'app_user_list' => $this->get_user_list(),
			'classifications' => $classifications,

			'id' => $id,
			'video' => $video,
			'buttons' => $buttons
		);

		// Render a page
		$this->load->view('template/header', $view);
		$this->load->view('video/preview', $view);
		$this->load->view('template/footer', $view);
	}

	/**
	 * Displays the video details.
	 * 
	 * @param string $id	an identifier of video recording
	 */
	function details($id) {
		// Verify the request
		$this->check_user_session();

		// Prepare template variables
		$video = $this->video->get($id);
		$classifications = $this->get_case_classifications();

		$view = array(
			'header_title' => "Video Details",

			'current_user_id' => $this->session->user_id,
			'current_user_login' => $this->session->user_login,
			'current_permissions' => $this->session->user_group->permissions,
			'current_features' => $this->features,
			'app_user_list' => $this->get_user_list(),
			'classifications' => $classifications,

			'id' => $id,
			'video' => $video
		);

		// Render a page
		$this->load->view('template/header', $view);
		$this->load->view('video/details', $view);
		$this->load->view('template/footer', $view);
	}

	/**
	 * Saves a meta information for the specified video.
	 * 
	 * @param string $id	an identifier of video recording (UUID)
	 * 
	 * POST variables: caseno, title, description, classification
	 */
	function save_metadata($id) {
		// Verify the request
		$this->check_user_session();

		$title = empty($_REQUEST['title']) ? null : $_REQUEST['title'];
		$caseno = empty($_REQUEST['caseno']) ? null : $_REQUEST['caseno'];
		$description = empty($_REQUEST['description']) ? null : $_REQUEST['description'];
		$classification = empty($_REQUEST['classification']) ? null : $_REQUEST['classification'];

		// Try to save meta information
		$this->video->save($id, $caseno, $title, $description, $classification);

		// Send a response
		echo json_encode(array('status' => 'success'));
	}

	/**
	 * Deletes the specified video recording
	 * 
	 * @param string $id	an identifier of video recording
	 */
	function delete($id) {
		// Verify the request
		$this->check_user_session();

		// Try to delete
		$this->video->delete($id);

		// Send a response
		echo json_encode(array('status' => 'success'));
	}

	/**
	 * Displays the snapshot dialog.
	 * 
	 * POST variables: time
	 * 
	 * @param string $id	an identifier of video file
	 */
	function snapshot($id) {
		// Verify the request
		$this->check_user_session();

		// Prepare template variables
		$left_buttons = array(
			array(
				'id' => 'SavePicture',
				'title' => 'Save Picture'
			),
		);

		$view = array(
			'id' => $id,
			'title' => 'Snapshot',
			'dialog_id' => 'snapshotEditor',
			'left_buttons' => $left_buttons,
		);

		// Render a page
		$this->load->view('template/modalheader', $view);
		$this->load->view('view_modal/screenshot_editor', $view);
		$this->load->view('template/modalfooter', $view);
	}

	/**
	 * Displays the editor for specified video recording.
	 * 
	 * @param string $id	an identifier of video file
	 */
	function editor($id) {
		// Verify the request
		$this->check_user_session();

		// Prepare template variables
		$buttons = array(
			array(
				'id' => 'SetStartTime',
				'title' => 'Set start time'
			),

			array(
				'id' => 'SetEndTime',
				'title' => 'Set end time'
			),

			array(
				'id' => 'CropVideo',
				'title' => 'Crop'
			)
		);

		$view = array(
			'id' => $id,
			'title' => 'Video Editor',
			'dialog_id' => 'videoEditor',
			'left_buttons' => $buttons,
		);

		// Render a page
		$this->load->view('template/modalheader', $view);
		$this->load->view('view_modal/video_editor', $view);
		$this->load->view('template/modalfooter', $view);
	}

}
