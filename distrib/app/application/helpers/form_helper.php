<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Returns the checked attribute if the specified value is true
 * 
 * @param bool $value	a value to test
 * 
 * @return string
 */
function checked($value) {
	return $value ? ' checked ' : '';
}

/**
 * Returns the selected attribute if the specified value is true
 * 
 * @param bool $value	a value to test
 * 
 * @return string
 */
function selected($value) {
	return $value ? ' selected ' : '';
}

/**
 * Returns a human readable string for specified number of bytes
 * 
 * @param int $bytes	a number to convert
 */
function format_bytes($bytes, $precision = 2) { 
	$base = log($bytes, 1024);
	$suffixes = array('', 'K', 'M', 'G', 'T');   
	return round(pow(1024, $base - floor($base)), $precision) .' '. $suffixes[floor($base)];
} 
