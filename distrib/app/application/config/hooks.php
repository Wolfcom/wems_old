<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| Hooks
| -------------------------------------------------------------------------
| This file lets you define "hooks" to extend CI without hacking the core
| files.  Please see the user guide for info:
|
|	http://codeigniter.com/user_guide/general/hooks.html
|
*/

/**
 * Called after the final rendered page is sent to the browser,
 * at the end of system execution after the finalized data is sent to the browser.
 */
$hook['post_system'][] = array(
	'class' => 'LogHook',
	'function' => 'log',
	'filename' => 'LogHook.php',
	'filepath' => 'hooks'
);
