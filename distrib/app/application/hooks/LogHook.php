<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class LogHook {

	/**
	 * Initializes the instance of hook
	 */
	function __construct() {
		$ci =& get_instance();

		// Load dependencies and copy references
		$ci->load->model('activity_model', 'activity');

		if (!isset($ci->session)) {
			$ci->load->library('session');
		}

		$this->input = $ci->input;
		$this->session = $ci->session;
		$this->uri = $ci->uri;
		$this->router = $ci->router;
		$this->activity = $ci->activity;
	}

	/**
	 * Called at every request
	 */
	function log() {
		// Detect action
		$action = $this->get_current_action();
		if (empty($action)) {
			return;
		}

		// Detect user details
		$user_id = $this->session->userdata('user_id');
		$user_ip = $this->input->ip_address();

		// Detect type of entity, and optionally source entity
		$source_id = null;
		$source_type = null;

		$entity_id = $this->uri->segment(3);
		$class = strtolower($this->router->class);
		$method = strtolower($this->router->method);
		switch ($class) {
			case 'user':
				$entity_type = 'users';
				break;

			case 'group':
				$entity_type = 'groups';
				break;

			case 'video':
				$entity_type = 'videos';

				if ($method == 'crop') {
					$source_id = $this->uri->segment(4);
					$source_type = 'videos';
				}

				break;

			case 'audio':
				$entity_type = 'audios';
				break;

			case 'picture':
				$entity_type = 'pictures';

				if ($method == 'save_snapshot') {
					$source_id = $this->uri->segment(4);
					$source_type = 'videos';
				}

				break;

			case 'favourites':
				$entity_type = 'favorites';
				break;

			case 'bookmarks':
				$entity_type = 'bookmarks';
				$source_id = $this->uri->segment(4);
				break;

			case 'document':
				$entity_type = 'documents';
				break;

			default:
				// Unable to detect type of entity
				return;
		}

		// Try to add log entry
		$this->activity->add(
			$action, $user_id, $user_ip, $entity_id, $entity_type,
			$source_id, $source_type
		);
	}

	/**
	 * Returns the identifier of current action
	 */
	protected function get_current_action() {
		static $actions = array(
			'user' => array(
				'update_password' => 'update_user_password',
				'create_profile' => 'create_user',
				'update_profile' => 'update_user',
				'delete_profile' => 'delete_user',
				'activate' => 'activate_user'
			),

			'group' => array(
				'create' => 'create_group',
				'update' => 'update_group',
				'delete' => 'delete_group',
				'update_permissions' => 'update_group_permissions'
			),

			'favourites' => array(
				'add' => 'add_favorite',
				'remove' => 'delete_favorite'
			),

			'bookmarks' => array(
				'create' => 'add_bookmark',
				'delete' => 'delete_bookmark',
				'update' => 'update_bookmark'
			),

			'video' => array(
				'preview' => 'view_video',
				'save_metadata' => 'update_video',
				'crop' => 'crop_video',
				'delete' => 'delete_video'
			),

			'picture' => array(
				'preview' => 'view_picture',
				'save_metadata' => 'update_picture',
				'save_snapshot' => 'create_picture',
				'delete' => 'delete_picture'
			),

			'audio' => array(
				'preview' => 'view_audio',
				'save_metadata' => 'update_audio',
				'delete' => 'delete_audio'
			),

			'document' => array(
				'details' => 'view_document',
				'save_metadata' => 'update_document',
				'delete' => 'delete_document'
			)
		);

		$module = strtolower($this->router->class);
		$method = strtolower($this->router->method);

		if (empty($actions[$module][$method])) {
			// Ignore unknown action
			return;
		}

		return $actions[$module][$method];
	}

}
