<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Represents a row from the table of system logs
 */
class Activity_model extends CI_Model {

	const TABLE_NAME = 'activity';
 
	/**
	 * Initializes the model
	 */
	function __construct() {
		parent::__construct();

		// Load dependencies
		$this->load->model('video_model', 'video');
		$this->load->model('audio_model', 'audio');
		$this->load->model('picture_model', 'picture');
		$this->load->model('document_model', 'document');

		$this->load->model('group_model', 'group');
		$this->load->model('user_model');
		$this->load->model('profile_model');
	}

	/**
	 * Returns the total number of activity.
	 * 
	 * @param string $user_id	an identifier of user to filter events
	 * @param string $record_id	an identifier of entity to filter events
	 * 
	 * @param array $filter		an optional filter, where a key is the name of field to filter
	 */
	function total($user_id = null, $record_id = null, $filter = array()) {
		// Prepare filter
		if ($user_id) {
			$this->db->where('user_id', $user_id);
		}

		if ($record_id) {
			$this->db->where('entity_id', $record_id);
		}

		// Prepare references and optional filters
		$this->db->join(
			User_model::TABLE_NAME, "users.id = activity.user_id", "left"
		);
		$this->prepare_filters($filter);

		// Try to fetch data
		return $this->db->count_all_results(self::TABLE_NAME); 
	}

	/**
	 * Prepares the database filter
	 * 
	 * @param array $filter	an optional filter, where a key is the name of field to filter
	 */
	protected function prepare_filters($filter) {
		if (empty($filter)) {
			return;
		}

		switch (key($filter)) {
			case 'time':
				$value = $this->db->escape(trim(current($filter)));
				$has_date = strstr($value, '-') !== false;
				$has_time = strstr($value, ':') !== false;

				if ($has_date && $has_time) {
					$this->db->where('activity.created', $value, false);
				}
				else if ($has_date) {
					$this->db->where('activity.created::date', $value, false);
				}
				else if ($has_time) {
					$this->db->where('activity.created::time(0)', $value, false);
				}
				else {
					$this->db->where('false', null, false);
				}
				break;

			case 'record':
				$uuid = trim(current($filter));
				if (preg_match('/^\{?[A-Z0-9]{8}-[A-Z0-9]{4}-[A-Z0-9]{4}-[A-Z0-9]{4}-[A-Z0-9]{12}\}?$/i', $uuid)) {
					$this->db->where('activity.entity_id', $uuid);
				}
				else {
					$this->db->where('false', null, false);
				}
				break;

			case 'action':
				$value = strtolower(trim(current($filter)));
				$actions = array();
				foreach (self::$actions as $action_id => $text) {
					if (preg_match("/$value/i", $text)) {
						$actions []= $action_id;
					}
				}

				if ($actions) {
					$this->db->where_in('activity.action', $actions);
				}
				else {
					$this->db->where('false', null, false);
				}
				break;

			case 'user':
				$this->db->where('users.login', trim(current($filter)));
				break;

			case 'ip':
				$this->db->where('activity.user_ip', trim(current($filter)));
				break;
		}
	}

	/**
	 * Fetches the list of log entries.
	 * 
	 * @param string $user_id	an identifier of user to filter events
	 * @param string $record_id	an identifier of entity to filter events
	 * 
	 * @param int $limit		a maximum number of records
	 * @param int $offset		a number of first record in the general list
	 * 
	 * @param array $filter		an optional filter, where a key is the name of field to filter
	 */
	function fetch_list($user_id = null, $record_id = null, $limit = null, $offset = null, $filter = array()) {
		// Prepare list of fields
		$this->db->select('
			activity.id, activity.created, activity.action AS action_id, activity.user_ip,
			activity.source_id, activity.source_type, activity.entity_id, activity.entity_type,
			users.login AS user_login, profiles.first_name, profiles.last_name
		');

		// Prepare references
		$this->db->join(
			User_model::TABLE_NAME, "users.id = activity.user_id", "left"
		);
		$this->db->join(
			Profile_model::TABLE_NAME, "profiles.id = activity.user_id", "left"
		);

		// Prepare filters
		$this->prepare_filters($filter);
		if ($user_id) {
			$this->db->where('activity.user_id', $user_id);
		}

		if ($record_id) {
			$this->db->where('activity.entity_id', $record_id);
		}

		// Try to fetch data
		$this->db->order_by('activity.created', 'desc');
		$query = $this->db->get(self::TABLE_NAME, $limit, $offset);
		$records = $query->result();

		if (empty($records)) {
			return $records;
		}

		foreach ($records as $record) {
			$record->action = self::translate($record->action_id);

			// Build label for entity
			switch ($record->action_id) {
				case 'update_bookmark':
				case 'delete_bookmark':
				case 'add_bookmark':
					try {
						$video = $this->video->get($record->source_id);
					}
					catch (Exception $ignore) {	
					}

				case 'view_video':
				case 'download_video':
				case 'update_video':
				case 'crop_video':
				case 'upload_video':
				case 'delete_video':
				case 'burn_video':
					$video = null;
					try {
						$video = $this->video->get($record->entity_id);
					}
					catch (Exception $ignore) {	
					}

					// Metadata is lost
					if (empty($video)) {
						break;
					}

					if (empty($video->title)) {
						$video_title = "BodyCam-{$video->device_id}-{$video->id}";
					}
					else {
						$video_title = $video->title . ".mp4 ({$video->device_id})";
					}

					if ($video->deleted) {
						$record->entity_label = $video_title;
					}
					else {
						$record->entity_label = "<a href='/index.php/video/preview/{$video->id}'>{$video_title}</a>";
					}

					$video = null;
					break;

				case 'view_audio':
				case 'download_audio':
				case 'update_audio':
				case 'upload_audio':
				case 'delete_audio':
				case 'burn_audio':
					$audio = null;
					try {
						$audio = $this->audio->get($record->entity_id);
					}
					catch (Exception $ignore) {
					}

					// Metadata is lost
					if (empty($audio)) {
						break;
					}

					if (empty($audio->title)) {
						$audio_title = "BodyCam-{$audio->device_id}-{$audio->id}";
					}
					else {
						$audio_title = $audio->title;
					}

					if ($audio->deleted) {
						$record->entity_label = $audio_title . ".wav ({$audio->device_id})";
					}
					else {
						$record->entity_label = "<a href='/index.php/audio/preview/{$audio->id}'>{$audio_title}</a>";
					}
					break;

				case 'view_picture':
				case 'download_picture':
				case 'update_picture':
				case 'upload_picture':
				case 'delete_picture':
				case 'burn_picture':
					$picture = null;
					try {
						$picture = $this->picture->get($record->entity_id);
					}
					catch (Exception $ignore) {
					}

					// Metadata is lost
					if (empty($picture)) {
						break;
					}

					if (empty($picture->title)) {
						$picture_title = "BodyCam-{$picture->device_id}-{$picture->id}";
					}
					else {
						$picture_title = $picture->title;
					}

					if ($picture->deleted) {
						$record->entity_label = $picture_title . ".jpg ({$picture->device_id})";
					}
					else {
						$record->entity_label = "<a href='/index.php/picture/preview/{$picture->id}'>{$picture_title}</a>";
					}
					break;

				case 'view_document':
				case 'download_document':
				case 'update_document':
				case 'upload_document':
				case 'delete_document':
				case 'burn_document':
					$document = null;
					try {
						$document = $this->document->get($record->entity_id);
					}
					catch (Exception $ignore) {
					}

					// Metadata is lost
					if (empty($document)) {
						break;
					}

					if (empty($document->title)) {
						$document_title = "BodyCam-{$document->id}";
					}
					else {
						$document_title = $document->title;
					}

					if ($document->deleted) {
						$file_ext = $this->document->get_file_extension($document->mime_type);
						$record->entity_label = $document_title . ".{$file_ext}";
					}
					else {
						$record->entity_label = "<a href='/index.php/document/details/{$document->id}'>{$document_title}</a>";
					}
					break;

				case 'create_group':
				case 'update_group':
				case 'update_group_permissions':
					$group = $this->group->get($record->entity_id);
					$record->entity_label = "{$group->name}";
					break;

				case 'create_user':
				case 'update_user':
				case 'activate_user':
					$profile = $this->profile_model->get($record->entity_id);
					$record->entity_label = "{$profile->first_name} {$profile->last_name} ({$profile->login})";
					break;

				case 'add_favorite':
				case 'delete_favorite':
					$media_entity = null;
					try {
						$media_entity = $this->video->get($record->entity_id);
						$media_ext = 'mp4';
						$media_module = 'video';
					}
					catch (Exception $ignore) {
					}

					if (empty($media_entity)) {
						try {
							$media_entity = $this->picture->get($record->entity_id);
							$media_ext = 'jpg';
							$media_module = 'picture';
						}
						catch (Exception $ignore) {
						}
					}
					
					if (empty($media_entity)) {
						try {
							$media_entity = $this->audio->get($record->entity_id);
							$media_ext = 'wav';
							$media_module = 'audio';
						}
						catch (Exception $ignore) {
						}
					}

					// Metadata is lost
					if (empty($media_entity)) {
						break;
					}

					if (empty($media_entity->title)) {
						$media_title = "BodyCam-{$media_entity->device_id}-{$media_entity->id}.{$media_ext}";
					}
					else {
						$media_title = $media_entity->title . ".{$media_ext} ({$media_entity->device_id})";
					}

					if ($media_entity->deleted) {
						$record->entity_label = $media_title;
					}
					else {
						$record->entity_label = "<a href='/index.php/{$media_module}/preview/{$media_entity->id}'>{$media_title}</a>";
					}
					break;
			}
		}

		return $records;
	}

	/**
	 * Creates a log entry.
	 * 
	 * @param string $action	an identifier of action
	 * @param string $user_id	an identifier of user who has been caused this action
	 * @param string $user_ip	an IP address of user
	 * 
	 * @param string $entity_id		an identifier of entity related to this event
	 * @param string $entity_type	an type of entity related to this event
	 * 
	 * @param string $source_id		an optional identifier of original entity for copy operations
	 * @param string $source_type	an optional type of source entity
	 */
	function add(
		$action, $user_id, $user_ip, $entity_id, $entity_type,
		$source_id = null, $source_type = null
	) {
		$this->db->set('id', generate_uuid());
		$this->db->set('action', $action);

		$this->db->set('entity_id', $entity_id);
		$this->db->set('entity_type', $entity_type);

		$this->db->set('user_id', $user_id);
		$this->db->set('user_ip', $user_ip);

		if ($source_id) {
			$this->db->set('source_id', $source_id);
			$this->db->set('source_type', $source_type);
		}

		$this->db->insert(self::TABLE_NAME);
	}

	/**
	 * Returns the text report for the specified media file.
	 * 
	 * @param string $id	an identifier of media file
	 * @param string $type	an optional type of media file
	 */
	function report($id, $type = null) {
		// Detect type of entity
		if (empty($type)) {
			if (empty($entity)) {
				try {
					$entity = $this->video->get($id);
				}
				catch (Exception $e) {
				}
			}

			if (empty($entity)) {
				try {
					$entity = $this->audio->get($id);
				}
				catch (Exception $e) {
				}
			}

			if (empty($entity)) {
				try {
					$entity = $this->picture->get($id);
				}
				catch (Exception $e) {
				}
			}
		}
		else {
			switch ($type) {
				case 'videos':
					$entity = $this->video->get($id);
					break;

				case 'audios':
					$entity = $this->audio->get($id);
					break;

				case 'pictures':
					$entity = $this->picture->get($id);
					break;
			}
		}

		// Prepare header fields
		if (!empty($entity)) {
			$file_title =  $entity->title;
			$owner = $entity->user_login . ' - ' . $entity->first_name . ' ' . $entity->last_name;
			$file_created = date('d/m/Y H:i:s', strtotime($entity->created));
		}
		else {
			$file_title = '';
			$file_created = '';
			$owner = '';
		}

		// Define max length of action string
		$max_action_length = max(array_map('strlen', self::$actions));
		$action_filler_title = str_repeat(' ', $max_action_length - 13);

		// Prepare header of report
		$report = <<<EOL
Evidence name: $file_title
Record id: {$id}
Date/Time Created: {$file_created}
Owner: {$owner}

Time                     Action       {$action_filler_title}User              IP Address
===================      ============={$action_filler_title}============      ==============

EOL;

		$records = $this->fetch_list(null, $id);
		foreach ($records as $record) {
			// Prepare fields
			$entry_time = date('d/m/Y H:i:s', strtotime($record->created));

			// Prepare fillers
			$spaces6 = str_repeat(' ', 6);
			$action_filler_value = str_repeat(' ', $max_action_length - strlen($record->action));
			$user_filler = str_repeat(' ', 18 - strlen($record->user_login));

			// Prepare log entry
			$entry_format = "%s{$spaces6}%s{$action_filler_value}%s{$user_filler}%s";
			$entry = trim(sprintf(
				$entry_format, 
				$entry_time, $record->action, $record->user_login, $record->user_ip
			));

			$report .= $entry . "\r\n";
		}

		return $report;
	}

	/**
	 * A list of supported actions
	 */
	protected static $actions = array(
		'update_user_password' => 'User has updated password',
		'create_user' => 'Created user',
		'update_user' => 'Updated user',
		'delete_user' => 'Deactivated user',
		'activate_user' => 'Activated user',

		'create_group' => 'Created group',
		'update_group' => 'Updated group',
		'update_group_permissions' => 'Updated permissions of group',
		'delete_group' => 'Deleted group',

		'add_favorite' => 'Added to favorites',
		'delete_favorite' => 'Removed from favorites',

		'add_bookmark' => 'Added bookmark',
		'update_bookmark' => 'Updated bookmark',
		'delete_bookmark' => 'Deleted bookmark',

		'burn_video' => 'Burned to Disc',
		'burn_audio' => 'Burned to Disc',
		'burn_picture' => 'Burned to Disc',
		'burn_document' => 'Burned to Disc',

		'view_video' => 'Viewed video',
		'view_audio' => 'Listen audio',
		'view_picture' => 'Viewed picture',
		'view_document' => 'Viewed document',

		'download_video' => 'Downloaded video',
		'download_audio' => 'Downloaded audio',
		'download_picture' => 'Downloaded picture',
		'download_document' => 'Downloaded document',

		'create_picture' => 'Created picture',
		'crop_video' => 'Created video',
		'create_audio' => 'Created audio',

		'upload_picture' => 'Uploaded picture',
		'upload_video' => 'Uploaded video',
		'upload_audio' => 'Uploaded audio',
		'upload_document' => 'Uploaded document',

		'update_video' => 'Updated video',
		'update_audio' => 'Updated audio',
		'update_picture' => 'Updated picture',
		'update_document' => 'Updated document',

		'delete_video' => 'Deleted video',
		'delete_audio' => 'Deleted audio',
		'delete_picture' => 'Deleted picture',
		'delete_document' => 'Deteleted document'
	);

	/**
	 * Returns the description for the specified identifier of action
	 * 
	 * @param string $action	an identifier of action
	 */
	static function translate($action) {
		if (!isset(self::$actions[$action])) {
			return 'Unknown action';
		}
		else {
			return self::$actions[$action];
		}
	}

}
