<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Represents a row from the table of user groups
 */
class Group_model extends CI_Model {

	const ADMIN_GROUP = 'd2ee53ca-cfa0-11e5-b5dc-303a64a40499';

	const TABLE_NAME = 'groups';

	/**
	 * Initializes the model
	 */
	function __construct() {
		parent::__construct();

		$this->load->model('profile_model');
		$this->load->model('user_model');
	}

	/**
	 * Fetches the list of groups.
	 * 
	 * @param int $limit	a maximum number of records
	 * @param int $offset	a number of first record in the general list
	 */
	function fetch_list($limit = null, $offset = null) {
		// Prepare list of field
		$this->db->select("
			groups.id, groups.name, groups.created,
			json_agg(json_build_object(
				'id', profiles.id, 'login', users.login, 
				'first_name', first_name, 'last_name', last_name
			)) AS users
		");

		// Try to fetch data
		$this->db->join(Profile_model::TABLE_NAME, "profiles.id = ANY(groups.users)", 'left');
		$this->db->join(User_model::TABLE_NAME, "users.id = profiles.id", 'left');
		$this->db->where('groups.id !=', self::ADMIN_GROUP);
		$this->db->group_by('groups.id'); 

		$result_set = $this->db->get(self::TABLE_NAME, $limit, $offset);
		$groups = $result_set->result();
		foreach ($groups as $group) {
			$group->users = json_decode($group->users);
			foreach ($group->users as $index => $user) {
				if (empty($user->id)) {
					unset($group->users[$index]);
				}
			}
		}

		return $groups;
	}

	/**
	 * Returns the information for specified group
	 *
	 * @param string $id 	an identifier of group
	 * 
	 * @return object  		group information
	 */
	function get($id) {
		// Prepare the list of fields
		$this->db->select('
			groups.id, groups.name,
			row_to_json(groups.permissions) AS permissions
		');

		// Try to fetch data
		$this->db->where('id', $id);
		$result_set = $this->db->get(self::TABLE_NAME);

		$group = $result_set->row();
		if ($group) {
			$group->permissions = json_decode($group->permissions);
		}

		return $group;
	}

	/**
	 * Creates a new group.
	 * 
	 * @param string $name		a name of group
	 * @param string[] members	a list of members
	 * 
	 * @return string	an identifier of new group
	 */
	function create($name, $members, $all_permissions = false) {
		$group_id = generate_uuid();
		if (empty($name)) {
			throw new Exception("Empty name of group");
		}

		// Prepare list of members
		foreach ($members as $index => $member) {
			$members[$index] = $this->db->escape($member);
		}
		$member_list = implode(', ', $members);

		// Try to create a group
		$this->db->set('id', $group_id);
		$this->db->set('name', $name);
		$this->db->set('users', "ARRAY[$member_list]::uuid[]", false);

		$this->db->insert(self::TABLE_NAME);

		// Enable all privileges
		if ($all_permissions) {
			$all_permissions = $this->privileges_list();
			$privileges = array();
			foreach ($all_permissions as $permission) {
				$privileges[$permission] = 'true';
			}

			$this->update_permissions($group_id, $privileges);
		}

		return $group_id;
	}

	/**
	 * Updates name/member list of group.
	 * 
	 * @param string $id		an identifier of group
	 * @param string $name		a name of group
	 * @param string[] members	a list of members
	 */
	function update($id, $name, $members) {
		// Prepare list of members
		foreach ($members as $index => $member) {
			$members[$index] = $this->db->escape($member);
		}
		$member_list = implode(', ', $members);

		$this->db->set('name', $name);
		$this->db->set('users', "ARRAY[$member_list]::uuid[]", false);

		// Try to update details of group
		$this->db->where('id', $id);
		$this->db->update(self::TABLE_NAME);
	}

	/**
	 * Deletes the specified group.
	 * 
	 * @param string $id	an identifier of group
	 */
	function delete($id) {
		$this->db->delete(self::TABLE_NAME, array('id' => $id));
	}

	/**
	 * Updates the list of permissions for the specified group.
	 * 
	 * @param string $id			an identifier of group
	 * @param string $permissions	a set of permissions
	 */
	function update_permissions($id, $permissions) {
		// Prepare the set of permissions to update
		$all_permissions = $this->privileges_list();
		$privileges = array();
		foreach ($all_permissions as $permission) {
			$privileges []= empty($permissions[$permission]) ? 'false' : 'true';
		}

		$privileges_sql = '(' . implode(', ', $privileges) . ')::privilege_type';
		$this->db->set('permissions', $privileges_sql, false);

		// Try to update a set of permissions
		$this->db->where('id', $id);
		$this->db->update(self::TABLE_NAME);
	}

	/**
	 * Returns the list of privileges.
	 * TODO: cache results
	 */
	function privileges_list() {
		// Prepare main query
		$query = <<<EOL
SELECT attname FROM pg_attribute a
INNER JOIN pg_type t ON t.typname = 'privilege_type' AND a.attrelid = t.typrelid
WHERE a.atttypid != 0
ORDER BY a.attnum
EOL;

		// Try to fetch data
		$results = $this->db->query($query);
		$privileges = array();
		foreach ($results->result() as $result) {
			$privileges []= $result->attname;
		}

		return $privileges;
	}

}
