<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Represents a row from the table of documents
 */
class Document_model extends CI_Model {

	const TABLE_NAME = 'documents';

	/**
	 * Initializes the model
	 */
	function __construct() {
		parent::__construct();

		$this->load->model('favorite_model');
		$this->load->model('user_model');
		$this->load->model('profile_model');
	}

	/**
	 * Returns the entity of document.
	 * 
	 * @param string $id	an identifier of document
	 */
	function get($id) {
		// Check permissions
		$user_id = $this->session->user_id;
		$group = $this->session->user_group;
		if (empty($group->permissions->global_permissions)) {
			$this->db->where('documents.user_id', $this->session->user_id);
		}

		// Prepare the list of fields
		$this->db->select("
			documents.id, documents.created, documents.deleted, 
			encode(documents.checksum, 'hex') AS checksum, documents.mime_type,
			documents.user_id, profiles.first_name, profiles.last_name, users.login AS user_login,
			documents.title, documents.description, documents.caseno, documents.classification,
			octet_length(documents.content) AS size,
			EXISTS(SELECT true FROM favorites WHERE favorites.user_id = '$user_id' AND favorites.media_id = documents.id) AS in_favs
		");

		// Prepare references
		$this->db->join(User_model::TABLE_NAME, "users.id = documents.user_id", "left");
		$this->db->join(Profile_model::TABLE_NAME, "profiles.id = documents.user_id", "left");

		// Prepare filters
		$this->db->where('documents.id', $id);

		// Try to fetch a picture
		$result = $this->db->get(self::TABLE_NAME);
		$document = $result->row();

		if ($document) {
			$document->in_favs = $document->in_favs === 't' ? true : false;
			$document->deleted = $document->deleted === 't' ? true : false;
		}

		return $document;
	}

	/**
	 * Creates a new document in the storage.
	 * 
	 * @param string $user_id	an identifier of user
	 * @param string $file		a full path to the local file
	 * 
	 * @return string	an identifier of new document
	 */
	function upload($user_id, $path) {
		// Prepare a content of document
		$document_id = generate_uuid();
		$document = file_get_contents($path);
		$checksum = hash('sha256', $document, true);
		$mime_type = $this->get_file_mime_type($path);

		// Try to create a document
		$this->db->set('id', $document_id);
		$this->db->set('user_id', $user_id);

		$this->db->set('content', pg_escape_bytea($document));
		$this->db->set('checksum', pg_escape_bytea($checksum));
		$this->db->set('mime_type', $mime_type);

		$this->db->insert(self::TABLE_NAME);
		return $document_id;
	}

	/**
	 * Saves a meta information for the specified document.
	 * 
	 * @param string $id	an identifier of document
	 */
	function save($id, $caseno, $title, $description, $classification) {
		// Check permissions
		$group = $this->session->user_group;
		if (empty($group->permissions->global_permissions)) {
			$this->db->where('user_id', $this->session->user_id);
		}

		// Set new data
		$fields = array(
			'title' => $title,
			'caseno' => $caseno,
			'description' => $description,
			'classification' => $classification
		);

		// Try to update
		$this->db->where('id', $id);
		$this->db->update(self::TABLE_NAME, $fields);
	}

	/**
	 * Deletes the specified document.
	 * 
	 * @param string $id	an identifier of document
	 */
	function delete($id) {
		// Check permissions
		$group = $this->session->user_group;
		if (empty($group->permissions->global_permissions)) {
			$this->db->where('user_id', $this->session->user_id);
		}

		// Mark as deleted
		$fields = array(
			'content' => '',
			'deleted' => true
		);

		$this->db->where('id', $id);
		$this->db->update(self::TABLE_NAME, $fields);
	}

	/**
	 * Returns the file extensions for the specified mime type.
	 */
	function get_file_extension($mime_type) {
		switch ($mime_type) {
			case 'text/plain':
				return 'txt';

			case 'application/msword':
				return 'doc';

			case 'vnd.openxmlformats-officedocument.wordprocessingml.document':
			case 'application/vnd.openxmlformats-officedocument.wordprocessingml.document':
				return 'docx';

			case 'application/vnd.ms-excel':
				return 'xls';

			case 'vnd.openxmlformats-officedocument.spreadsheetml.sheet':
			case 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet':
				return 'xlsx';

			case 'application/vnd.ms-powerpoint':
				return 'ppt';

			case 'vnd.openxmlformats-officedocument.presentationml.presentation':
			case 'application/vnd.openxmlformats-officedocument.presentationml.presentation':
				return 'pptx';

			default:
				list(, $file_ext) = explode('/', $mime_type);
				return $file_ext;
		}
	}

	/**
	 * Returns the type for the specified file.
	 */
	function get_file_type($file_path) {
		$mime_type = $this->get_file_mime_type($file_path);
		list($type) = explode('/', $mime_type);
		return $type;
	}

	/**
	 * Returns the mime type for the specified file.
	 */
	function get_file_mime_type($file_path) {
		$finfo = finfo_open(FILEINFO_MIME_TYPE);
		$mime_type = finfo_file($finfo, $file_path);
		finfo_close($finfo);

		switch ($mime_type) {
			case 'application/vnd.openxmlformats-officedocument.wordprocessingml.document':
				return 'vnd.openxmlformats-officedocument.wordprocessingml.document';

			case 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet':
				return 'vnd.openxmlformats-officedocument.spreadsheetml.sheet';

			case 'application/vnd.openxmlformats-officedocument.presentationml.presentation':
				return 'vnd.openxmlformats-officedocument.presentationml.presentation';

			default:
				return $mime_type;
		}
	}

	/**
	 * Exports the document to the specified location.
	 * 
	 * @param string $id	an identifier of document
	 * @param string $path	a full path to the local file
	 */
	function export_file($id, $path) {
		// Try to fetch a BLOB file
		$dbh = $this->load->database('default', true);

		$group = $this->session->user_group;
		if (empty($group->permissions->global_permissions)) {
			$result = $dbh->query(
				'SELECT content FROM documents WHERE id = ? AND user_id = ?', 
				array($id, $this->session->user_id)
			);
		}
		else {
			$result = $dbh->query('SELECT content FROM documents WHERE id = ?', array($id));
		}

		if ($result->num_rows() < 1) {
			throw new InvalidArgumentException('The document with specified identifier isn\'t found');
		}
		$row = $result->row_array();

		// Try to export a picture
		file_put_contents($path, pg_unescape_bytea($row['content']));
	}

}
