<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Represents a row from the table of video recordings
 */
class Video_model extends CI_Model {

	const TABLE_NAME = 'videos';

	/**
	 * A full path to the FFMPEG executable.
	 * 
	 * @var string
	 */
	public $FFMPEG;

	/**
	 * Initializes the model
	 */
	function __construct() {
		parent::__construct();

		$this->FFMPEG = '"' . realpath(dirname(__FILE__) . "/../../../ffmpeg/ffmpeg.exe") . '"';

		$this->load->model('favorite_model');
		$this->load->model('user_model');
		$this->load->model('profile_model');
	}

	/**
	 * Returns the total number of videos
	 */
	function total() {
		// Check permissions
		$group = $this->session->user_group;
		if (empty($group->permissions->global_permissions)) {
			$this->db->where('user_id', $this->session->user_id);
		}

		// Prepare references
		$this->db->join(
			'retention_policy', 
			"retention_policy.archive_duration <> '178000000 years' AND retention_policy.category = videos.classification", 
			"left"
		);

		$this->db->where('deleted', false);
		$this->db->where('
			CASE 
				WHEN retention_policy.archive_duration IS NULL THEN true
				WHEN (NOW() - retention_policy.archive_duration) > videos.created THEN false
				ELSE true
			END',
			null,
			false
		);

		// Try to fetch a number
		return $this->db->count_all_results(self::TABLE_NAME); 
	}

	/**
	 * Fetches the list of videos.
	 * 
	 * @param int $limit		a maximum number of records
	 * @param int $offset		a number of first record in the general list
	 */
	function fetch_list($limit = null, $offset = null) {
		// Check permissions
		$group = $this->session->user_group;
		if (empty($group->permissions->read_video)) {
			return array();
		}

		if (empty($group->permissions->global_permissions)) {
			$this->db->where('videos.user_id', $this->session->user_id);
		}

		// Prepare list of fields
		$this->db->select('
			videos.id, videos.created, videos.duration, 
			videos.user_id, users.login AS user_login,
			profiles.first_name, profiles.last_name,
			videos.title, videos.caseno, lo_size(videos.content) AS size
		');

		// Prepare references
		$this->db->join(
			User_model::TABLE_NAME, "users.id = videos.user_id", "left"
		);
		$this->db->join(
			Profile_model::TABLE_NAME, "profiles.id = videos.user_id", "left"
		);
		$this->db->join(
			'retention_policy', 
			"retention_policy.archive_duration <> '178000000 years' AND retention_policy.category = videos.classification", 
			"left"
		);

		// Try to fetch data
		$this->db->where('deleted', false);
		$this->db->where('
			CASE 
				WHEN retention_policy.archive_duration IS NULL THEN true
				WHEN (NOW() - retention_policy.archive_duration) > videos.created THEN false
				ELSE true
			END',
			null,
			false
		);

		$this->db->order_by('videos.created', 'DESC');

		$query = $this->db->get(self::TABLE_NAME, $limit, $offset);
		return $query->result();
	}

	/**
	 * Returns the entity of video recording.
	 * 
	 * @param string $id	an identifier of picture
	 */
	function get($id) {
		// Check permissions
		$user_id = $this->session->user_id;
		$group = $this->session->user_group;
		if (empty($group->permissions->global_permissions)) {
			$this->db->where('videos.user_id', $this->session->user_id);
		}

		// Prepare the list of fields
		$this->db->select("
			videos.id, videos.created, videos.deleted, videos.device_id, 
			encode(videos.checksum, 'hex') AS checksum,	to_json(videos.path) AS gps_track,
			videos.user_id, profiles.first_name, profiles.last_name, users.login AS user_login,
			videos.title, videos.description, videos.caseno, videos.classification,
			videos.width, videos.height, videos.duration,
			(CASE WHEN videos.content > 0 THEN lo_size(videos.content) ELSE 0 END) as size,
			(CASE WHEN count(bookmarks) = 0 THEN '[]' ELSE 
				json_agg(json_build_object(
					'id', bookmarks.id, 'description', bookmarks.description, 
					'time', EXTRACT(epoch FROM bookmarks.start)
				)) 
			END) as bookmarks,
			EXISTS(SELECT true FROM favorites WHERE favorites.user_id = '$user_id' AND favorites.media_id = videos.id) AS in_favs
		");

		// Prepare references
		$this->db->join(
			'bookmarks', 
			"bookmarks.video_id = videos.id AND bookmarks.user_id = '{$user_id}'",
			'left'
		);
		$this->db->join(User_model::TABLE_NAME, "users.id = videos.user_id", "left");
		$this->db->join(Profile_model::TABLE_NAME, "profiles.id = videos.user_id", "left");

		// Prepare filters
		$this->db->where('videos.id', $id);
		$this->db->group_by(array('videos.id', 'users.id', 'profiles.id'));

		// Try to fetch a picture
		$result = $this->db->get(self::TABLE_NAME);
		$video = $result->row();

		if ($video) {
			$video->in_favs = $video->in_favs === 't' ? true : false;
			$video->deleted = $video->deleted === 't' ? true : false;
			$video->bookmarks = json_decode($video->bookmarks);
		}

		return $video;
	}

	/**
	 * Saves a meta information for the specified video.
	 * 
	 * @param string $id	an identifier of video recording
	 */
	function save($id, $caseno, $title, $description, $classification) {
		// Check permissions
		$group = $this->session->user_group;
		if (empty($group->permissions->global_permissions)) {
			$this->db->where('user_id', $this->session->user_id);
		}

		// Set new data
		$fields = array(
			'title' => $title,
			'caseno' => $caseno,
			'description' => $description,
			'classification' => $classification
		);

		// Try to update
		$this->db->where('id', $id);
		$this->db->update(self::TABLE_NAME, $fields);
	}

	/**
	 * Deletes the specified video recording.
	 * 
	 * @param string $id	an identifier of video recording
	 */
	function delete($id) {
		// Check permissions
		$group = $this->session->user_group;
		if (empty($group->permissions->global_permissions)) {
			$this->db->where('user_id', $this->session->user_id);
		}

		// Try to delete file
		$result = $this->db->query(
			'SELECT content FROM videos WHERE id = ?', array($id)
		);
		if ($result->num_rows() < 1) {
			throw new InvalidArgumentException(
				'The video recording with specified identifier isn\'t found'
			);
		}

		$row = $result->row_array();
		$file_id = $row['content'];
		if ($file_id) {
			pg_lo_unlink($this->db->conn_id, $file_id);
		}

		// Mark as deleted
		$fields = array(
			'content' => 0,
			'thumbnail' => '',
			'deleted' => true
		);

		$this->db->where('id', $id);
		$this->db->update(self::TABLE_NAME, $fields);
	}

	/**
	 * Prepares a thumbnail for the specified video file.
	 * 
	 * @param string $src_file	a source video file
	 * @param string $time		a start time to capture picture
	 * 
	 * @return string	a binary content of thumbnail
	 */
	function prepare_thumbnail_from_file($src_file, $time) {
		$program = $this->FFMPEG;
		$thumb_file = tempnam(sys_get_temp_dir(), 'THUMB');

		// Try to make a thumbnail
		$command = "{$program} -i {$src_file} -y -ss {$time} -vframes 1 -f mjpeg {$thumb_file}";
		$output = shell_exec($command);

		// Try to read a result picture
		$thumbnail = file_get_contents($thumb_file);
		@unlink($thumb_file);
		return $thumbnail;
	}

	/**
	 * Exports the specified video recording to the specified location
	 * 
	 * @param string $id	an identifier of video recording
	 * @param string $path	a full path to the local file
	 */
	function export_file($id, $path) {
		// Try to fetch an identifier of BLOB file
		$dbh = $this->load->database('default', true);
		$pgsql = $dbh->conn_id;

		$group = $this->session->user_group;
		if (empty($group->permissions->global_permissions)) {
			$result = $dbh->query(
				'SELECT content FROM videos WHERE id = ? AND user_id = ?', 
				array($id, $this->session->user_id)
			);
		}
		else {
			$result = $dbh->query('SELECT content FROM videos WHERE id = ?', array($id));
		}

		if ($result->num_rows() < 1) {
			throw new InvalidArgumentException('The video with specified identifier isn\'t found');
		}

		$row = $result->row_array();
		$video_file_id = $row['content'];

		// Try to export video to perform edit functions
		$dbh->trans_start();
		pg_lo_export($pgsql, $video_file_id, $path);
		$dbh->trans_complete();
	}

	/**
	 * Prepares a thumbnail for the specified video recordings.
	 * 
	 * @param string $id	an identifier of video recording
	 * @param string $time	a start time to capture picture
	 * 
	 * @return string	a binary content of thumbnail
	 */
	function prepare_thumbnail_from_record($id, $time) {
		// Try to export video to perform edit functions.		
		$video_file = tempnam(sys_get_temp_dir(), 'VIDEO');
		$this->export_file($id, $video_file);

		// Try to make a thumbnail
		$thumbnail = $this->prepare_thumbnail_from_file($video_file, $time);
		@unlink($video_file);
		return $thumbnail;
	}

}
