<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Represents a row from the table of audio recordings
 */
class Audio_model extends CI_Model {

	const TABLE_NAME = 'audios';

	/**
	 * Initializes the model
	 */
	function __construct() {
		parent::__construct();
	}

	/**
	 * Returns the entity of audio recording.
	 * 
	 * @param string $id	an identifier of audio recording
	 */
	function get($id) {
		// Check permissions
		$user_id = $this->session->user_id;
		$group = $this->session->user_group;
		if (empty($group->permissions->global_permissions)) {
			$this->db->where('audios.user_id', $this->session->user_id);
		}

		// Prepare the list of fields
		$this->db->select("
			audios.id, audios.deleted, audios.created, audios.device_id,
			encode(audios.checksum, 'hex') AS checksum,
			audios.user_id, profiles.first_name, profiles.last_name, users.login AS user_login,
			audios.title, audios.description, audios.caseno, audios.classification,
			audios.duration, 
			(CASE WHEN audios.content > 0 THEN lo_size(audios.content) ELSE 0 END) as size,
			(favorites.type IS NOT NULL) AS in_favs
		");

		// Prepare references
		$this->db->join('favorites', "favorites.user_id = '$user_id' AND favorites.media_id = audios.id", 'left');
		$this->db->join(User_model::TABLE_NAME, "users.id = audios.user_id", "left");
		$this->db->join(Profile_model::TABLE_NAME, "profiles.id = audios.user_id", "left");

		// Prepare filters
		$this->db->where('audios.id', $id);

		// Try to fetch a picture
		$result = $this->db->get(self::TABLE_NAME);
		$audio = $result->row();

		if ($audio) {
			$audio->in_favs = $audio->in_favs === 't' ? true : false;
			$audio->deleted = $audio->deleted === 't' ? true : false;
		}

		return $audio;
	}

	/**
	 * Saves a meta information for the specified audio.
	 * 
	 * @param string $id	an identifier of audio recording
	 */
	function save($id, $caseno, $title, $description, $classification) {
		// Check permissions
		$group = $this->session->user_group;
		if (empty($group->permissions->global_permissions)) {
			$this->db->where('user_id', $this->session->user_id);
		}

		// Set new data
		$fields = array(
			'title' => $title,
			'caseno' => $caseno,
			'description' => $description,
			'classification' => $classification
		);

		// Try to update
		$this->db->where('id', $id);
		$this->db->update(self::TABLE_NAME, $fields);
	}

	/**
	 * Deletes the specified audio recording.
	 * 
	 * @param string $id	an identifier of audio recording
	 */
	function delete($id) {
		// Check permissions
		$group = $this->session->user_group;
		if (empty($group->permissions->global_permissions)) {
			$this->db->where('user_id', $this->session->user_id);
		}

		// Try to delete file
		$result = $this->db->query(
			'SELECT content, raw_content FROM audios WHERE id = ?', array($id)
		);
		if ($result->num_rows() < 1) {
			throw new InvalidArgumentException(
				'The audio recording with specified identifier isn\'t found'
			);
		}

		$row = $result->row_array();
		$file_id = $row['content'];
		$raw_file_id = $row['raw_content'];

		if ($file_id) {
			pg_lo_unlink($this->db->conn_id, $file_id);
		}
		if ($raw_file_id) {
			pg_lo_unlink($this->db->conn_id, $file_id);
		}

		// Mark as deleted
		$fields = array(
			'content' => 0,
			'raw_content' => 0,
			'deleted' => true
		);

		$this->db->where('id', $id);
		$this->db->update(self::TABLE_NAME, $fields);
	}

	/**
	 * Exports the specified audio recording to the specified location
	 * 
	 * @param string $id	an identifier of audio recording
	 * @param string $path	a full path to the local file
	 */
	function export_file($id, $path) {
		// Try to fetch an identifier of BLOB file
		$dbh = $this->load->database('default', true);
		$pgsql = $dbh->conn_id;

		$group = $this->session->user_group;
		if (empty($group->permissions->global_permissions)) {
			$result = $dbh->query(
				'SELECT raw_content FROM audios WHERE id = ? AND user_id = ?', 
				array($id, $this->session->user_id)
			);
		}
		else {
			$result = $dbh->query('SELECT raw_content FROM audios WHERE id = ?', array($id));
		}

		if ($result->num_rows() < 1) {
			throw new InvalidArgumentException('The audio with specified identifier isn\'t found');
		}

		$row = $result->row_array();
		$audio_file_id = $row['raw_content'];

		// Try to export audio
		$dbh->trans_start();
		pg_lo_export($pgsql, $audio_file_id, $path);
		$dbh->trans_complete();
	}

}
