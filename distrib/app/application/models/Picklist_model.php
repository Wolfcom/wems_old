<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Picklist_model extends CI_Model {

	/**
	 * Initializes the model
	 */
	function __construct() {
		parent::__construct();
	}

	/**
	 * Fetches the list of devices.
	 * 
	 * @param int $limit	a maximum number of records
	 * @param int $offset	a number of first record in the general list
	 */
	function fetch_list($limit = null, $offset = null) {
		// Prepare list of field
		$this->db->select("
			devices.id, devices.type, devices.user_id, devices.status, 
			users.login, profiles.first_name, profiles.last_name
		");

		// Try to fetch data
		$this->db->join(User_model::TABLE_NAME, "users.id = devices.user_id", 'left');
		$this->db->join(Profile_model::TABLE_NAME, "profiles.id = devices.user_id", 'left');

		$result_set = $this->db->get(self::TABLE_NAME, $limit, $offset);
		return $result_set->result();
	}

	/**
	 * Returns the classification list
	 */
	function get_classification() {
		$classification = array();
		$result_set = $this->db->query(
			'SELECT unnest AS item FROM unnest(enum_range(NULL::classification))'
		);

		foreach ($result_set->result() as $item) {
			$classification []= $item->item;
		}
		return $classification;
	}

	/**
	 * Adds the new classification
	 * 
	 * @param string $item	a value to add
	 */
	function add_classification($item) {
		$this->db->query(
			'ALTER TYPE classification ADD VALUE IF NOT EXISTS ' . $this->db->escape($item)
		);
	}

	/**
	 * Removes the specified classification
	 * 
	 * @param string $item	a value to remove
	 */
	function remove_classification($item) {
		$item_sql = $this->db->escape($item);
		$this->db->query("SELECT enum_delete('classification', $item_sql)");
	}

}

