<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Represents a row from the table 'pictures'
 */
class Mycase_model extends CI_Model {

	/**
	 * Initializes the model
	 */
	function __construct() {
		parent::__construct();
	}

	/**
	 * Returns the total number of cases.
	 */
	function total() {
		$user_id = $this->session->user_id;

		// Prepare picture filters
		if (empty($group->permissions->read_picture)) {
			$picture_filter = 'WHERE false';
		}
		else {
			$picture_filter = 'WHERE p.caseno IS NOT NULL';
		}

		$picture_filter .= ' AND p.deleted = false';

		if (empty($group->permissions->global_permissions)) {
			$picture_filter .= " AND p.user_id = '{$user_id}'";
		}

		// Prepare video filter
		if (empty($group->permissions->read_video)) {
			$video_filter = 'WHERE false';
		}
		else {
			$video_filter = 'WHERE v.caseno IS NOT NULL';
		}

		$video_filter .= ' AND v.deleted = false';

		if (empty($group->permissions->global_permissions)) {
			$video_filter .= " AND v.user_id = '{$user_id}'";
		}

		// Prepare audio filter
		if (empty($group->permissions->read_audio)) {
			$audio_filter = 'WHERE false';
		}
		else {
			$audio_filter = 'WHERE a.caseno IS NOT NULL';
		}

		$audio_filter .= ' AND a.deleted = false';

		if (empty($group->permissions->global_permissions)) {
			$audio_filter .= " AND a.user_id = '{$user_id}'";
		}

		// Prepare audio filter
		if (empty($group->permissions->read_document)) {
			$document_file = 'WHERE false';
		}
		else {
			$document_filter = 'WHERE d.caseno IS NOT NULL';
		}

		$document_filter .= ' AND d.deleted = false';

		if (empty($group->permissions->global_permissions)) {
			$document_filter .= " AND aduser_id = '{$user_id}'";
		}

		$query = <<<EOL
(SELECT COUNT(*) AS cnt FROM pictures p {$picture_filter})
UNION ALL
(SELECT COUNT(*) AS cnt FROM videos v {$video_filter})
UNION ALL
(SELECT COUNT(*) AS cnt FROM audios a {$audio_filter})
UNION ALL
(SELECT COUNT(*) AS cnt FROM documents d {$document_filter})
EOL;

		$result = $this->db->query($query)->result();
		$count = 0;
		foreach ($result as $row) {
			$count += $row->cnt;
		}
		return $count;
	}

	/**
	 * Fetches the list of cases.
	 * 
	 * @param int $limit	a maximum number of records
	 * @param int $offset	a number of first record in the general list
	 */
	function fetch_list($limit = null, $offset = null) {
		$user_id = $this->session->user_id;
		$group = $this->session->user_group;

		// Prepare picture filters
		if (empty($group->permissions->read_picture)) {
			$picture_filter = 'WHERE false';
		}
		else {
			$picture_filter = 'WHERE p.caseno IS NOT NULL';
		}

		$picture_filter .= ' AND p.deleted = false';

		if (empty($group->permissions->global_permissions)) {
			$picture_filter .= " AND p.user_id = '{$user_id}'";
		}

		// Prepare video filter
		if (empty($group->permissions->read_video)) {
			$video_filter = 'WHERE false';
		}
		else {
			$video_filter = 'WHERE v.caseno IS NOT NULL';
		}

		$video_filter .= ' AND v.deleted = false';

		if (empty($group->permissions->global_permissions)) {
			$video_filter .= " AND v.user_id = '{$user_id}'";
		}

		// Prepare audio filter
		if (empty($group->permissions->read_audio)) {
			$audio_filter = 'WHERE false';
		}
		else {
			$audio_filter = 'WHERE a.caseno IS NOT NULL';
		}

		$audio_filter .= ' AND a.deleted = false';

		if (empty($group->permissions->global_permissions)) {
			$audio_filter .= " AND a.user_id = '{$user_id}'";
		}

		// Prepare document filter
		if (empty($group->permissions->read_document)) {
			$document_filter = 'WHERE false';
		}
		else {
			$document_filter = 'WHERE d.caseno IS NOT NULL';
		}

		$document_filter .= ' AND d.deleted = false';

		if (empty($group->permissions->global_permissions)) {
			$document_filter .= " AND d.user_id = '{$user_id}'";
		}

		// Prepare main query
		$query = <<<EOL
(
 SELECT 'picture' AS type, p.id, p.created, 
   us.login AS user_login, ps.first_name, ps.last_name,
   null AS duration, octet_length(p.content) AS size, p.title, p.caseno
 FROM pictures p
 LEFT JOIN users us ON us.id = p.user_id
 LEFT JOIN profiles ps ON ps.id = p.user_id
 {$picture_filter} 
)

UNION ALL

(
 SELECT 'video' AS type, v.id, v.created, 
   us.login AS user_login, ps.first_name, ps.last_name,
   v.duration, lo_size(v.content) AS size, v.title, v.caseno
 FROM videos v
 LEFT JOIN users us ON us.id = v.user_id
 LEFT JOIN profiles ps ON ps.id = v.user_id
 {$video_filter}
)

UNION ALL

(
 SELECT 'audio' AS type, a.id, a.created, 
   us.login AS user_login, ps.first_name, ps.last_name,
   a.duration, lo_size(a.raw_content) AS size, a.title, a.caseno
 FROM audios a
 LEFT JOIN users us ON us.id = a.user_id
 LEFT JOIN profiles ps ON ps.id = a.user_id
 {$audio_filter}
)

UNION ALL

(
 SELECT 'document' AS type, d.id, d.created, 
   us.login AS user_login, ps.first_name, ps.last_name,
   null AS duration, octet_length(d.content) AS size, d.title, d.caseno
 FROM documents d
 LEFT JOIN users us ON us.id = d.user_id
 LEFT JOIN profiles ps ON ps.id = d.user_id
 {$document_filter}
)
EOL;

		if ($limit) {
			$query .= "LIMIT {$limit}\n";
		}
		if ($offset) {
			$offset .= "OFFSET {$offset}\n";
		}

		// Try to fetch data
		$result = $this->db->query($query);
		return $result->result();
	}

}
