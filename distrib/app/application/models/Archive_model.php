<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Represents a row from the virtual table of archived files
 */
class Archive_model extends CI_Model {

	const TABLE_NAME = 'archive';

	/**
	 * Initializes the model
	 */
	function __construct() {
		parent::__construct();
	}

	/**
	 * Returns the total number of files.
	 */
	function total() {
		// Check permissions
		$group = $this->session->user_group;
		if (empty($group->permissions->global_permissions)) {
			$this->db->where('user_id', $this->session->user_id);
		}

		// Try to fetch a number
		return $this->db->count_all_results(self::TABLE_NAME); 
	}

	/**
	 * Fetches the list of files.
	 * 
	 * @param int $limit		a maximum number of records
	 * @param int $offset		a number of first record in the general list
	 */
	function fetch_list($limit = null, $offset = null) {
		$group = $this->session->user_group;
		if (empty($group->permissions->global_permissions)) {
			$this->db->where('archive.user_id', $this->session->user_id);
		}

		// Prepare list of fields
		$this->db->select('
			archive.type, archive.id, archive.created, archive.archived,
			archive.user_id, users.login AS user_login, 
			profiles.first_name, profiles.last_name,
			archive.title, archive.caseno, archive.size
		');

		// Prepare references
		$this->db->join(
			User_model::TABLE_NAME, "users.id = archive.user_id", "left"
		);
		$this->db->join(
			Profile_model::TABLE_NAME, "profiles.id = archive.user_id", "left"
		);

		// Try to fetch data
		$this->db->order_by('archive.created', 'DESC');

		$query = $this->db->get(self::TABLE_NAME, $limit, $offset);
		return $query->result();
	}

	/**
	 * Returns the retention policy.
	 */
	function fetch_policy() {
		$query = $this->db->get('retention_policy');
		return $query->result();
	}

	/**
	 * Deletes the retention rule for the specified classification.
	 * 
	 * @param string $classification	a category to unset
	 */
	function unset_archive_policy($classification) {
		$data = array(
			'archive_duration' => null
		);

		$this->db->where('category', $classification);
		$this->db->update('retention_policy', $data);
	}

	/**
	 * Saves the retention rule for the specified classification.
	 * 
	 * @param string $classification	a category to set
	 * @param string $duration	a duration to store a file
	 */
	function set_archive_policy($classification, $duration) {
		$data = array(
			'archive_duration' => $duration,
			'category' => $classification
		);

		$this->db->where('category', $classification);
		$this->db->update('retention_policy', $data);

		if (!$this->db->affected_rows()) {
			$this->db->insert('retention_policy', $data);
		}
	}

	/**
	 * Deletes the retention rule for the specified classification.
	 * 
	 * @param string $classification	a category to unset
	 */
	function unset_post_archive_policy($classification) {
		$data = array(
			'post_archive_duration' => null
		);

		$this->db->where('category', $classification);
		$this->db->update('retention_policy', $data);
	}
	
	/**
	 * Deletes the retention rule for the specified classification.
	 * 
	 * @param string $classification	a category to unset
	 * @param string $duration	a duration to store a file
	 */
	function set_post_archive_policy($classification, $duration) {
		$data = array(
			'post_archive_duration' => $duration,
			'category' => $classification
		);

		$this->db->where('category', $classification);
		$this->db->update('retention_policy', $data);

		if (!$this->db->affected_rows()) {
			$this->db->insert('retention_policy', $data);
		}
	}

}
