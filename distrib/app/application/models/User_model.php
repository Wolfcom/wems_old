<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Represents a row from the table of user accounts
 */
class User_model extends CI_Model {

	const ADMIN_USER = '0a89862a-ceec-11e5-903c-303a64a40499';

	const TABLE_NAME = 'users';

	/**
	 * Initializes the model
	 */
	function __construct() {
		parent::__construct();

		$this->load->model('profile_model');
	}

	/**
	 * Performs an user authentication
	 * 
	 * @param string $login		an username to authenticate
	 * @param string $password	a password of user
	 * 
	 * @return string|false		a user identifier
	 */
	function authenticate($login, $password) {
		// Prepare a filter
		$this->db->where('is_active', true);
		$this->db->where('login', $login);

		$password = $this->db->escape($password);
		$this->db->where("password = digest({$password}, 'sha256')");

		// Execute a query
		$this->db->select('id');
		$result = $this->db->get(self::TABLE_NAME);

		// Check the result of query
		if ($result->num_rows()) {
			return $result->row()->id;
		}
		else {
			return false;
		}
	}

	/**
	 * Updates the password for specified user.
	 * 
	 * @param string $id		an identifier of user
	 * @param string $password	a new password of user
	 */
	function update_password($id, $password) {
		$password = $this->db->escape($password);
		$this->db->set('password', "digest({$password}, 'sha256')", false);

		$this->db->where('id', $id);
		$this->db->update(self::TABLE_NAME);
	}

	/**
	 * Updates the account fields of specified user.
	 * 
	 * @param string $id	an identifier of user
	 * @param array $data	a set of fields to update
	 * @param string $password	an optional password to update
	 */
	function update_account($id, $fields, $password = null) {
		// Group field is stored in the separate table
		$has_group = isset($fields['group']);
		if ($has_group) {
			$group_id = $fields['group'];
			unset($fields['group']);
		}

		// Ignore inappropriate fields
		unset($fields['id']);
		unset($fields['password']);
		unset($fields['login']);

		if (isset($fields['email']) && empty($fields['email'])) {
			$fields['email'] = null;
		}

		$this->db->where('id', $id);
		$this->db->update(self::TABLE_NAME, $fields);

		// Try to assign the user into the specified group
		if ($has_group) {
			// Reset the current group of user
			$user_id = $this->db->escape($id);
			$this->db->query("UPDATE groups SET users = array_remove(users, {$user_id})");

			// Try to assign the group of user
			if ($group_id) {
				$group_id = $this->db->escape($group_id);
				$this->db->query(
					"UPDATE groups SET users = array_append(users, {$user_id}) WHERE id = {$group_id}"
				);
			}
		}

		if ($password) {
			$this->update_password($id, $password);
		}
	}

	/**
	 * Activates the specified account.
	 * 
	 * @param string $id	an identifier of user
	 */
	function activate_account($id) {
		$this->db->where('id', $id);
		$this->db->update(self::TABLE_NAME, array('is_active' => true));
	}

	/**
	 * Creates the new user account.
	 * 
	 * @param string $login		an user name
	 * @param string $password	a password of user
	 * @param string $email		an optional email of user
	 * @param string $group 	an optional group of user
	 * 
	 * @return string	an identifier of new user
	 */
	function create_account($login, $password, $email, $group) {
		// Validate data
		$user_id = generate_uuid();
		if (empty($login)) {
			throw new Exception("Empty login");
		}

		// Try to create a user
		$this->db->set('id', $user_id);
		$this->db->set('login', $login);

		if (empty($email)) {
			$this->db->set('email', null);
		}
		else {
			$this->db->set('email', $email);
		}

		$password = $this->db->escape($password);
		$this->db->set('password', "digest({$password}, 'sha256')", false);

		$this->db->insert(self::TABLE_NAME);

		// Try to assign the group of user
		if ($group) {
			$group_id = $this->db->escape($group);
			$this->db->query(
				"UPDATE groups SET users = array_append(users, '{$user_id}') WHERE id = {$group_id}"
			);
		}

		return $user_id;
	}

	/**
	 * Deletes an account of user.
	 * 
	 * @param string $id	an identifier of user
	 */
	function delete_account($id) {
		$this->db->where('id', $id);
		$this->db->update(self::TABLE_NAME, array('is_active' => false));
	}

}
