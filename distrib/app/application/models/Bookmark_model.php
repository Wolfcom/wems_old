<?php

if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Represents a row from the table of user bookmarks
 */
class Bookmark_model extends CI_Model {

    const TABLE_NAME = 'bookmarks';

	/**
	 * Initializes the model
	 */
	function __construct() {
		parent::__construct();
		
		$this->load->model('video_model');
	}

	/**
	 * Creates the new video bookmark.
	 * 
	 * @param string $user_id	an identifier of bookmark owner (UUID)
	 * @param string $video_id	an identifier of related video (UUID)
	 * @param int $time			a start time in the video recording (in seconds)
	 * @param string $description	an optional description of video
	 * 
	 * @return string	an identifier of new bookmark
	 */
	function create($user_id, $video_id, $time, $description) {
		// Prepare a thumbnail of video
		$bookmark_id = generate_uuid();
		$start_time = date('H:i:s', $time);
		$thumbnail = $this->video_model->prepare_thumbnail_from_record(
			$video_id, $start_time
		);

		// Try to create a bookmark
		$this->db->set('id', $bookmark_id);
		$this->db->set('video_id', $video_id);
		$this->db->set('user_id', $user_id);
		$this->db->set('start', $start_time);

		$this->db->set('description', $description);
		$this->db->set('thumbnail', pg_escape_bytea($thumbnail));

		$this->db->insert(self::TABLE_NAME);

		// Return a new entity
		$bookmark = new stdClass;
		$bookmark->id = $bookmark_id;
		$bookmark->raw_time = $time;
		$bookmark->time = date('i:s', $time);
		$bookmark->description = $description;
		return $bookmark;
	}

	/**
	 * Updates the specified bookmark
	 * 
	 * @param string $id			an identifier of bookmark
	 * @param string $description	a new description of bookmark
	 */
	function update($id, $description) {
		$data = array(
			'description' => $description
		);

		$this->db->where('id', $id);
		$this->db->where('user_id', $this->session->user_id);
		$this->db->update(self::TABLE_NAME, $data);

		// Return an updated entity
		$bookmark = new stdClass;
		$bookmark->id = $id;
		$bookmark->description = $description;
		return $bookmark;
	}


	/**
	 * Returns the total number of bookmarks.
	 */
	function total() {
		$this->db->where('user_id', $this->session->user_id);
		return $this->db->count_all_results(self::TABLE_NAME); 
	}

	/**
	 * Fetches the list of bookmarks.
	 * 
	 * @param int $limit		a maximum number of records
	 * @param int $offset		a number of first record in the general list
	 */
	function fetch_list($limit = null, $offset = null) {
		// Prepare list of fields
		$this->db->select('
			bookmarks.id, bookmarks.created, bookmarks.video_id, 
			bookmarks.description, bookmarks.start,
			users.id AS user_id, users.login AS user_login,
			profiles.first_name, profiles.last_name
		');

		// Prepare references
		$this->db->where('bookmarks.user_id', $this->session->user_id);
		$this->db->where('videos.deleted', false);

		$this->db->join('videos', "videos.id = bookmarks.video_id");
		$this->db->join('users', "users.id = bookmarks.user_id", 'left');
		$this->db->join('profiles', "profiles.id = bookmarks.user_id", 'left');

		$query = $this->db->get(self::TABLE_NAME, $limit, $offset);
		return $query->result();
	}

	/**
	 * Returns the entity of bookmakr.
	 * 
	 * @param string $id	an identifier of bookmark
	 */
	function get($id) {
		// Prepare the query
		$this->db->select("
			id, video_id, start, description, created, thumbnail
		");

		$this->db->where('id', $id);
		$this->db->where('user_id', $this->session->user_id);

		// Try to fetch a picture
		$result = $this->db->get(self::TABLE_NAME);
		$bookmark = $result->row();

		if ($bookmark) {
			$bookmark->thumbnail = pg_unescape_bytea($bookmark->thumbnail);
		}

		return $bookmark;
	}

	/**
	 * Deletes the specified bookmark
	 */
	function delete($id) {
        $filters = array(
			'id' => $id,
			'user_id' => $this->session->user_id
        );
		$this->db->delete(self::TABLE_NAME, $filters);
	}

}
