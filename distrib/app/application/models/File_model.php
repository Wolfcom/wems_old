<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Represents a row from the virtual table of all files
 */
class File_model extends CI_Model {

	/**
	 * Initializes the model
	 */
	function __construct() {
		parent::__construct();
	}

	/**
	 * Returns the total number of files.
	 * 
	 * @param array $filters	a set of filters
	 */
	function total($filters) {
		// Prepare main query
		list($picture_filter, $video_filter, $audio_filter, $document_filter) = $this->prepare_filters($filters);
		$query = array();

		if ($picture_filter) {
			$query []= <<<EOL
(
 SELECT COUNT(*) as cnt FROM pictures p
 LEFT JOIN retention_policy rp ON rp.archive_duration <> '178000000 years' AND rp.category = p.classification
 {$picture_filter}
)
EOL;
		}

		if ($video_filter) {
			$query []= <<<EOL
(
 SELECT COUNT(*) as cnt FROM videos v
 LEFT JOIN bookmarks b ON b.video_id = v.id
 LEFT JOIN retention_policy rp ON rp.archive_duration <> '178000000 years' AND rp.category = v.classification
 {$video_filter}
)
EOL;
		}

		if ($audio_filter) {
			$query []= <<<EOL
(
 SELECT COUNT(*) as cnt FROM audios a
 LEFT JOIN retention_policy rp ON rp.archive_duration <> '178000000 years' AND rp.category = a.classification
 {$audio_filter}
)
EOL;
		}

		if ($document_filter) {
			$query []= <<<EOL
(
 SELECT COUNT(*) as cnt FROM documents d
 LEFT JOIN retention_policy rp ON rp.archive_duration <> '178000000 years' AND rp.category = d.classification
 {$document_filter}
)
EOL;
		}
		
		$query = implode(' UNION ALL ', $query);

		// Fetch numbers
		$count = 0;
		$result_set = $this->db->query($query);
		foreach ($result_set->result() as $row) {
			$count += $row->cnt;
		}

		return $count;
	}

	/**
	 * Prepares the set of SQL filters
	 */
	protected function prepare_filters($filters) {
		$user_id = $this->session->user_id;
		$group = $this->session->user_group;

		// Prepare filter by keyword in description, case number
		if (isset($filters['keyword'])) {
			$keyword = $this->db->escape('%' . $filters['keyword'] . '%');
		}
		else {
			$keyword = '';
		}

		// Prepare owner filter
		if (isset($filters['owner'])) {
			$owner_filter = 'user_id = ' . $this->db->escape($filters['owner']);
		}
		else {
			$owner_filter = '';
		}

		// Prepare date range filters
		if (isset($filters['exact_date'])) {
			$date_filter = "created::date = '" .  date('Y-m-d', $filters['exact_date']) . "'";
		}
		else if (isset($filters['start_date']) && isset($filters['end_date'])) {
			$date_filter = "created::date BETWEEN '" . date('Y-m-d', $filters['start_date'])
				. "' AND '" . date('Y-m-d', $filters['end_date']) . "'";
		}
		else if (isset($filters['start_date'])) {
			$date_filter = "created::date >= '" . date('Y-m-d', $filters['start_date']) . "'";
		}
		else if (isset($filters['end_date'])) {
			$date_filter = "created::date >= '" . date('Y-m-d', $filters['end_date']) . "'";
		}
		else {
			$date_filter = '';
		}

		// Prepare filter of documents
		$document_filter = 'WHERE d.deleted = false AND CASE 
				WHEN rp.archive_duration IS NULL THEN true
				WHEN (NOW() - rp.archive_duration) > d.created THEN false
				ELSE true
			END';

		if ($owner_filter && !empty($group->permissions->user_search)) {
			$document_filter .= " AND (d.{$owner_filter})";
		}
		else if (empty($group->permissions->global_permissions)) {
			$document_filter .= " AND d.user_id = '{$user_id}'";
		}

		if ($keyword) {
			$document_filter .= <<<EOL
 AND (
	d.description ILIKE $keyword OR 
	d.caseno ILIKE $keyword OR 
	d.title ILIKE $keyword OR
	d.classification::text ILIKE $keyword
 )
EOL;
		}
		if ($date_filter) {
			$document_filter .= " AND (d.{$date_filter})";
		}


		// Prepare filter of pictures
		if (empty($group->permissions->read_picture)) {
			$picture_filter = 'WHERE false';
		}
		else {
			$picture_filter = 'WHERE true';
		}

		$picture_filter .= ' AND p.deleted = false AND CASE 
				WHEN rp.archive_duration IS NULL THEN true
				WHEN (NOW() - rp.archive_duration) > p.created THEN false
				ELSE true
			END';

		if ($owner_filter && !empty($group->permissions->user_search)) {
			$picture_filter .= " AND (p.{$owner_filter})";
		}
		else if (empty($group->permissions->global_permissions)) {
			$picture_filter .= " AND p.user_id = '{$user_id}'";
		}

		if ($keyword) {
			$picture_filter .= <<<EOL
 AND (
	p.description ILIKE $keyword OR 
	p.caseno ILIKE $keyword OR 
	p.title ILIKE $keyword OR
	p.classification::text ILIKE $keyword
 )
EOL;
		}
		if ($date_filter) {
			$picture_filter .= " AND (p.{$date_filter})";
		}


		// Prepare filter of videos
		if (empty($group->permissions->read_video)) {
			$video_filter = 'WHERE false';
		}
		else {
			$video_filter = 'WHERE true';
		}

		$video_filter .= ' AND v.deleted = false AND CASE 
				WHEN rp.archive_duration IS NULL THEN true
				WHEN (NOW() - rp.archive_duration) > v.created THEN false
				ELSE true
			END';

		if ($owner_filter && !empty($group->permissions->user_search)) {
			$video_filter .= " AND (v.{$owner_filter})";
		}
		else if (empty($group->permissions->global_permissions)) {
			$video_filter .= " AND v.user_id = '{$user_id}'";
		}

		if ($keyword) {
			$video_filter .= <<<EOL
 AND (
	v.description ILIKE $keyword OR
	v.caseno ILIKE $keyword OR
	v.title ILIKE $keyword OR
	v.classification::text ILIKE $keyword OR
	b.description ILIKE $keyword 
 )
EOL;
		}
		if ($date_filter) {
			$video_filter .= " AND (v.{$date_filter})";
		}
		

		// Prepare filter of audios
		if (empty($group->permissions->read_audio)) {
			$audio_filter = 'WHERE false';
		}
		else {
			$audio_filter = 'WHERE true';
		}

		$audio_filter .= ' AND a.deleted = false AND CASE 
				WHEN rp.archive_duration IS NULL THEN true
				WHEN (NOW() - rp.archive_duration) > a.created THEN false
				ELSE true
			END';

		if ($owner_filter && !empty($group->permissions->user_search)) {
			$audio_filter .= " AND (a.{$owner_filter})";
		}
		else if (empty($group->permissions->global_permissions)) {
			$audio_filter .= " AND a.user_id = '{$user_id}'";
		}

		if ($keyword) {
			$audio_filter .= <<<EOL
 AND (
	a.description ILIKE $keyword OR
	a.caseno ILIKE $keyword OR
	a.title ILIKE $keyword OR
	a.classification::text ILIKE $keyword
 )
EOL;
		}
		if ($date_filter) {
			$audio_filter .= " AND (a.{$date_filter})";
		}

		// Prepare media type filter
		if (empty($filters['type'])) {
			return array($picture_filter, $video_filter, $audio_filter, $document_filter);
		}

		$sql_filters = array('', '', '');
		foreach ($filters['type'] as $index => $type) {
			switch ($type) {
				case 'video':
					$sql_filters[1] = $video_filter;
					break;

				case 'audio':
					$sql_filters[2] = $audio_filter;
					break;

				case 'picture':
					$sql_filters[0] = $picture_filter;
					break;

				case 'document':
					$sql_filters[3] = $document_filter;
					break;

				default:
					throw new Exception('Unknown type');
			}
		}
		return $sql_filters;
	}

	/**
	 * Fetches the list of files.
	 * 
	 * @param array $filters	a set of filters
	 * @param int $limit		a maximum number of records
	 * @param int $offset		a number of first record in the general list
	 */
	function fetch_list($filters, $limit = null, $offset = null) {
		// Prepare main query
		list($picture_filter, $video_filter, $audio_filter, $document_filter) = $this->prepare_filters($filters);
		$query = array();

		if ($picture_filter) {
			$query []= <<<EOL
(
 SELECT 'picture' AS type, p.id, p.created, 
   us.login AS user_login, ps.first_name, ps.last_name,
   null AS duration, octet_length(p.content) AS size, p.title, p.caseno
 FROM pictures p
 LEFT JOIN users us ON us.id = p.user_id
 LEFT JOIN profiles ps ON ps.id = p.user_id
 LEFT JOIN retention_policy rp ON rp.archive_duration <> '178000000 years' AND rp.category = p.classification
 {$picture_filter}
)
EOL;
		}

		if ($video_filter) {
			$query []= <<<EOL
(
 SELECT 'video' AS type, v.id, v.created,
   us.login AS user_login, ps.first_name, ps.last_name, 
   v.duration, lo_size(v.content) AS size, v.title, v.caseno
 FROM videos v
 LEFT JOIN users us ON us.id = v.user_id
 LEFT JOIN profiles ps ON ps.id = v.user_id
 LEFT JOIN bookmarks b ON b.video_id = v.id
 LEFT JOIN retention_policy rp ON rp.archive_duration <> '178000000 years' AND rp.category = v.classification
 {$video_filter}
)
EOL;
		}

		if ($audio_filter) {
			$query []= <<<EOL
(
 SELECT 'audio' AS type, a.id, a.created, 
   us.login AS user_login, ps.first_name, ps.last_name, 
   a.duration, lo_size(a.raw_content) AS size, a.title, a.caseno
 FROM audios a
 LEFT JOIN users us ON us.id = a.user_id
 LEFT JOIN profiles ps ON ps.id = a.user_id
 LEFT JOIN retention_policy rp ON rp.archive_duration <> '178000000 years' AND rp.category = a.classification
 {$audio_filter}
)
EOL;
		}

		if ($document_filter) {
			$query []= <<<EOL
(
 SELECT 'document' AS type, d.id, d.created, 
   us.login AS user_login, ps.first_name, ps.last_name,
   null AS duration, octet_length(d.content) AS size, d.title, d.caseno
 FROM documents d
 LEFT JOIN users us ON us.id = d.user_id
 LEFT JOIN profiles ps ON ps.id = d.user_id
 LEFT JOIN retention_policy rp ON rp.archive_duration <> '178000000 years' AND rp.category = d.classification
 {$document_filter}
)
EOL;
		}

		$query = implode(' UNION ALL ', $query) . "\nORDER BY created DESC";
		if ($limit) {
			$query .= " LIMIT {$limit}\n";
		}
		if ($offset) {
			$query .= " OFFSET {$offset}\n";
		}

		// Try to fecth data
		$result_set = $this->db->query($query);
		return $result_set->result();
	}

}
