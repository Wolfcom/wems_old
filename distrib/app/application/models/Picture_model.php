<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Represents a row from the table of pictures
 */
class Picture_model extends CI_Model {

	const TABLE_NAME = 'pictures';

	/**
	 * Initializes the model
	 */
	function __construct() {
		parent::__construct();

		$this->load->model('favorite_model');
		$this->load->model('user_model');
		$this->load->model('profile_model');
		$this->load->model('video_model');
	}

	/**
	 * Returns the total number of pictures
	 */
	function total() {
		// Check permissions
		$group = $this->session->user_group;
		if (empty($group->permissions->global_permissions)) {
			$this->db->where('user_id', $this->session->user_id);
		}

		// Prepare references
		$this->db->join(
			'retention_policy', 
			"retention_policy.archive_duration <> '178000000 years' AND retention_policy.category = pictures.classification", 
			"left"
		);

		$this->db->where('deleted', false);
		$this->db->where('
			CASE 
				WHEN retention_policy.archive_duration IS NULL THEN true
				WHEN (NOW() - retention_policy.archive_duration) > pictures.created THEN false
				ELSE true
			END',
			null,
			false
		);

		// Try to fetch a number
		return $this->db->count_all_results(self::TABLE_NAME); 
	}

	/**
	 * Returns the list of images.
	 * 
	 * @param int $limit	a maximum number of records
	 * @param int $offset	a number of first record in the general list
	 */
	function fetch_list($limit = null, $offset = null) {
		// Check permissions
		$group = $this->session->user_group;
		if (empty($group->permissions->read_picture)) {
			return array();
		}

		if (empty($group->permissions->global_permissions)) {
			$this->db->where('pictures.user_id', $this->session->user_id);
		}

		// Prepare list of fields
		$this->db->select('
			pictures.id, pictures.created,
			pictures.user_id, users.login AS user_login, 
			profiles.first_name, profiles.last_name,
			pictures.title, pictures.caseno, octet_length(pictures.content) AS size
		');

		// Prepare references
		$this->db->join(
			User_model::TABLE_NAME, "users.id = pictures.user_id", "left"
		);
		$this->db->join(
			Profile_model::TABLE_NAME, "profiles.id = pictures.user_id", "left"
		);
		$this->db->join(
			'retention_policy', 
			"retention_policy.archive_duration <> '178000000 years' AND retention_policy.category = pictures.classification", 
			"left"
		);

		// Try to fetch data
		$this->db->where('deleted', false);
		$this->db->where('
			CASE 
				WHEN retention_policy.archive_duration IS NULL THEN true
				WHEN (NOW() - retention_policy.archive_duration) > pictures.created THEN false
				ELSE true
			END',
			null,
			false
		);
		$this->db->order_by('pictures.created', 'DESC');

		$query = $this->db->get(self::TABLE_NAME, $limit, $offset);
		return $query->result();
	}

	/**
	 * Returns the entity of picture.
	 * 
	 * @param string $id	an identifier of picture
	 */
	function get($id) {
		// Check permissions
		$user_id = $this->session->user_id;
		$group = $this->session->user_group;
		if (empty($group->permissions->global_permissions)) {
			$this->db->where('pictures.user_id', $this->session->user_id);
		}

		// Prepare the list of fields
		$this->db->select("
			pictures.id, pictures.created, pictures.deleted, pictures.device_id, 
			encode(pictures.checksum, 'hex') AS checksum,
			pictures.user_id, profiles.first_name, profiles.last_name, users.login AS user_login,
			pictures.title, pictures.description, pictures.caseno, pictures.classification,
			pictures.width, pictures.height, octet_length(pictures.content) AS size,
			EXISTS(SELECT true FROM favorites WHERE favorites.user_id = '$user_id' AND favorites.media_id = pictures.id) AS in_favs
		");

		// Prepare references
		$this->db->join(User_model::TABLE_NAME, "users.id = pictures.user_id", "left");
		$this->db->join(Profile_model::TABLE_NAME, "profiles.id = pictures.user_id", "left");

		// Prepare filters
		$this->db->where('pictures.id', $id);

		// Try to fetch a picture
		$result = $this->db->get(self::TABLE_NAME);
		$picture = $result->row();

		if ($picture) {
			$picture->in_favs = $picture->in_favs === 't' ? true : false;
			$picture->deleted = $picture->deleted === 't' ? true : false;
		}

		return $picture;
	}

	/**
	 * Creates a new picture in the storage.
	 * 
	 * @param string $user_id	an identifier of user
	 * @param string $video_id	an identifier of source video
	 * @param string $time		a time of capture video frame
	 * @param string $title		a title of video
	 * 
	 * @return string	an identifier of new picture
	 */
	function upload_snapshot($user_id, $video_id, $time, $title) {
		// Prepare a thumbnail of video
		$picture_id = generate_uuid();
		$start_time = date('H:i:s', $time);
		$thumbnail = $this->video_model->prepare_thumbnail_from_record(
			$video_id, $start_time
		);

		list($width, $height) = getimagesizefromstring($thumbnail);

		// Try to create a picture
		$this->db->set('id', $picture_id);
		$this->db->set('user_id', $user_id);
		$this->db->set('device_id', '0');

		$this->db->set('title', $title);
		$this->db->set('content', pg_escape_bytea($thumbnail));
		$this->db->set('checksum', pg_escape_bytea(hash('sha256', $thumbnail, true)));
		$this->db->set('width', $width);
		$this->db->set('height', $height);

		$this->db->insert(self::TABLE_NAME);
		return $picture_id;
	}

	/**
	 * Creates a new picture in the storage.
	 * 
	 * @param string $user_id	an identifier of user
	 * @param string $file		a full path to the local file
	 * 
	 * @return string	an identifier of new picture
	 */
	function upload($user_id, $path) {
		// Prepare a content of document
		$picture_id = generate_uuid();
		$picture = file_get_contents($path);
		$checksum = hash('sha256', $picture, true);

		// Try to create a document
		$this->db->set('id', $picture_id);
		$this->db->set('user_id', $user_id);
		$this->db->set('device_id', 0);

		$this->db->set('content', pg_escape_bytea($picture));
		$this->db->set('checksum', pg_escape_bytea($checksum));

		$this->db->insert(self::TABLE_NAME);
		
		// Add the event to the event log
		$user_id = $this->session->userdata('user_id');
		$user_ip = $this->input->ip_address();
		$this->activity->add('upload_picture', $user_id, $user_ip, $picture_id, 'pictures');

		return $picture_id;
	}

	/**
	 * Saves a meta information for the specified picture.
	 * 
	 * @param string $id	an identifier of picture
	 */
	function save($id, $caseno,  $title, $description, $classification) {
		// Check permissions
		$group = $this->session->user_group;
		if (empty($group->permissions->global_permissions)) {
			$this->db->where('user_id', $this->session->user_id);
		}

		// Set new data
		$fields = array(
			'title' => $title,
			'caseno' => $caseno,
			'description' => $description,
			'classification' => $classification
		);

		// Try to update
		$this->db->where('id', $id);
		$this->db->update(self::TABLE_NAME, $fields);
	}

	/**
	 * Deletes the specified picture.
	 * 
	 * @param string $id	an identifier of picture
	 */
	function delete($id) {
		// Check permissions
		$group = $this->session->user_group;
		if (empty($group->permissions->global_permissions)) {
			$this->db->where('user_id', $this->session->user_id);
		}

		// Mark as deleted
		$fields = array(
			'content' => '',
			'deleted' => true
		);

		$this->db->where('id', $id);
		$this->db->update(self::TABLE_NAME, $fields);
	}

	/**
	 * Exports the specified audio recording to the specified location
	 * 
	 * @param string $id	an identifier of audio recording
	 * @param string $path	a full path to the local file
	 */
	function export_file($id, $path) {
		// Try to fetch a BLOB file
		$dbh = $this->load->database('default', true);

		$group = $this->session->user_group;
		if (empty($group->permissions->global_permissions)) {
			$result = $dbh->query(
				'SELECT content FROM pictures WHERE id = ? AND user_id = ?', 
				array($id, $this->session->user_id)
			);
		}
		else {
			$result = $dbh->query('SELECT content FROM pictures WHERE id = ?', array($id));
		}


		if ($result->num_rows() < 1) {
			throw new InvalidArgumentException('The picture with specified identifier isn\'t found');
		}
		$row = $result->row_array();

		// Try to export a picture
		file_put_contents($path, pg_unescape_bytea($row['content']));
	}

}
