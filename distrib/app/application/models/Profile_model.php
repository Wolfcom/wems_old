<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Represents a row from the table of user profiles
 */
class Profile_model extends CI_Model {

	const TABLE_NAME = 'profiles';

	/**
	 * Initializes the model
	 */
	function __construct() {
		parent::__construct();

		$this->load->model('user_model');
		$this->load->model('group_model');
		$this->load->model('device_model');

		// Prepare dependencies
		$this->db->query(<<<EOL
DO $$
BEGIN
	IF NOT EXISTS (SELECT 1 FROM pg_type WHERE typname = 'device_identity') THEN
		CREATE TYPE device_identity AS (
			device_id text,
			device_type device_type
		);
	END IF;
END$$;
EOL
		);
	}

	/**
	 * Returns the entity of user profile.
	 * 
	 * @param string $id	an identifier of user
	 */
	function get($id) {
		// Prepare list of field
		$this->db->select("
			users.id, users.login, users.email, users.created,
			profiles.first_name, profiles.last_name,
			profiles.photo, profiles.position, profiles.department,
			devices.id AS device_id, devices.type AS device_type,
			json_build_object(
				'id', groups.id, 'name', groups.name, 'permissions', groups.permissions
			) AS group
		");

		// Prepare filter
		$this->db->join(User_model::TABLE_NAME, "users.id = profiles.id");
		$this->db->join(Group_model::TABLE_NAME, "users.id = ANY(groups.users)", 'left');
		$this->db->join(Device_model::TABLE_NAME, "devices.user_id = profiles.id", 'left');
		$this->db->where('users.id', $id);

		// Try to fetch data
		$result = $this->db->get(self::TABLE_NAME);
		$profile = $result->row();
		if ($profile) {
			 $profile->photo = pg_unescape_bytea($profile->photo);
			 $profile->group = json_decode($profile->group);
		}

		return $profile;
	}

	/**
	 * Fetches the list of profiles.
	 * 
	 * @param int $limit	a maximum number of records
	 * @param int $offset	a number of first record in the general list
	 */
	function fetch_list($limit = null, $offset = null) {
		// Prepare list of field
		$this->db->select("
			users.id, users.login, users.email, users.created, users.is_active,
			profiles.first_name, profiles.last_name,
			profiles.photo, profiles.position, profiles.department,
			(SELECT CAST(ROW(devices.id, devices.type) AS device_identity)  FROM devices WHERE devices.user_id = profiles.id LIMIT 1).*,
			(SELECT row_to_json(CAST(ROW(groups.*) AS groups)) FROM groups WHERE users.id = ANY(groups.users) LIMIT 1) AS group
		");

		// Try to fetch data
		$this->db->join(User_model::TABLE_NAME, "users.id = profiles.id", 'right');
		$this->db->where('users.id !=', User_model::ADMIN_USER);
		$this->db->order_by('users.created', 'desc');

		$result_set = $this->db->get(self::TABLE_NAME, $limit, $offset);

		// Decode binary columns
		$profiles = $result_set->result();
		if ($profiles) {
			foreach ($profiles as $profile) {
				$profile->photo = pg_unescape_bytea($profile->photo);
				$profile->group = json_decode($profile->group);
				$profile->is_active = $profile->is_active === 't' ? true : false;
			}
		}

		return $profiles;
	}

	/**
	 * Updates the photo of specified user.
	 * 
	 * @param string $id	an identifier of user
	 * @param string $data	a content of photo
	 */
	function update_photo($id, $data) {
		$fields = array(
			'photo' => pg_escape_bytea($data)
		);

		$this->db->where('id', $id);
		$this->db->update(self::TABLE_NAME, $fields);
	}

	/**
	 * Updates the profile fields of specified user.
	 * 
	 * @param string $id		an identifier of user
	 * @param array $data		a set of profile fields to update
	 * @param string $device_id	an identifier of assigned device
	 * @param string $device	a set of device fields to update
	 */
	function update_profile($id, $data, $device_id, $device) {
		// Validate profile fields
		unset($data['id']);
		if (isset($data['photo'])) {
			$data['photo'] = pg_escape_bytea($data['photo']);
		}

		// Try to update a profile
		$this->db->where('id', $id);
		$this->db->update(self::TABLE_NAME, $data);

		// Create a new profile if it doesn't exist
		if (!$this->db->affected_rows()) {
			$data['id'] = $id;
			$this->db->insert(self::TABLE_NAME, $data);
		}

		// Validate device fields
		unset($device['id']);
		if (empty($device['type'])) {
			$device['type'] = null;
		}
		$device['user_id'] = $id;

		// Unassign owner of devices
		$this->db->where('user_id', $id);
		$this->db->update(Device_model::TABLE_NAME, array('user_id' => null));

		// Try to update device details
		if (!empty($device_id)) {
			// Check the device registration
			$this->db->where('id', $device_id);
			$old_device = $this->db->get(Device_model::TABLE_NAME)->row();

			if ($old_device) {
				// Assign owner ony for free devices
				if (!empty($old_device->user_id) && $old_device->user_id != $id) {
					throw new Exception('Camera ID is already assigned to another user');
				}

				$this->db->where('id', $device_id);
				$this->db->update(Device_model::TABLE_NAME, $device);
			}
			else {
				// Register a new device
				$device['id'] = $device_id;
				$this->db->insert(Device_model::TABLE_NAME, $device);
			}
		}
	}

}
