<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Device_model extends CI_Model {

	const TABLE_NAME = 'devices';

	/**
	 * Initializes the model
	 */
	function __construct() {
		parent::__construct();

		$this->load->model('user_model');
		$this->load->model('profile_model');
	}

	/**
	 * Fetches the list of devices.
	 * 
	 * @param int $limit	a maximum number of records
	 * @param int $offset	a number of first record in the general list
	 */
	function fetch_list($limit = null, $offset = null) {
		// Prepare list of field
		$this->db->select("
			devices.id, devices.type, devices.user_id, devices.status, devices.progress,
			users.login, profiles.first_name, profiles.last_name
		");

		// Try to fetch data
		$this->db->join(User_model::TABLE_NAME, "users.id = devices.user_id", 'inner');
		$this->db->join(Profile_model::TABLE_NAME, "profiles.id = devices.user_id", 'left');
		$this->db->order_by('devices.status', 'desc');
		

		$result_set = $this->db->get(self::TABLE_NAME, $limit, $offset);
		return $result_set->result();
	}

}

