<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Represents a row from the table 'favorites'
 */
class Favorite_model extends CI_Model {

	const TABLE_NAME = 'favorites';

	/**
	 * Initializes the model
	 */
	function __construct() {
		parent::__construct();
	}

	/**
	 * Returns the total number of favorites
	 */
	function total() {
		$this->db->where('user_id', $this->session->user_id);
		return $this->db->count_all_results(self::TABLE_NAME); 
	}

	/**
	 * Fetches the list of favorites.
	 * 
	 * @param int $limit	a maximum number of records
	 * @param int $offset	a number of first record in the general list
	 */
	function fetch_list($limit = null, $offset = null) {
		$user_login = $this->session->user_login;
		$user_id = $this->session->user_id;

		// Prepare main query
		$query = <<<EOL
(
 SELECT 'picture' AS type, p.id, p.created, null AS duration, 
  octet_length(p.content) AS size, p.title, p.caseno
 FROM favorites f 
 INNER JOIN pictures p ON p.id = f.media_id AND p.deleted = false
 WHERE f.user_id = '{$user_id}'
)

UNION ALL

(
 SELECT 'video' AS type, v.id, v.created, v.duration, 
   lo_size(v.content) AS size, v.title, v.caseno
 FROM favorites f 
 INNER JOIN videos v ON v.id = f.media_id AND v.deleted = false
 WHERE f.user_id = '{$user_id}'
)

UNION ALL

(
 SELECT 'audio' AS type, a.id, a.created, a.duration, 
   lo_size(a.raw_content) AS size, a.title, a.caseno
 FROM favorites f 
 INNER JOIN audios a ON a.id = f.media_id AND a.deleted = false
 WHERE f.user_id = '{$user_id}'
)
EOL;

		if ($limit) {
			$query .= "LIMIT {$limit}\n";
		}
		if ($offset) {
			$query .= "OFFSET {$offset}\n";
		}

		$result = $this->db->query($query);
		return $result->result();
	}

	/**
	 * Adds the media file to favorites.
	 * 
	 * @param string $id	an identifier of media file
	 */
	function add($id) {
		$type = null;
		$filters = array('id' => $id);

		// Detect type of media
		$query = $this->db->get_where('audios', $filters);
		$result = $query->result();
		if (!empty($result[0])) {
			$type = 'audios';
		}

		if (empty($type)) {
			$query = $this->db->get_where('videos', $filters);
			$result = $query->result();
			if (!empty($result[0])) {
				$type = 'videos';
			}
		}

		if (empty($type)) {
			$query = $this->db->get_where('pictures', $filters);
			$result = $query->result();
			if (!empty($result[0])) {
				$type = 'pictures';
			}
		}

		if (empty($type)) {
			throw new Exception('Unknown media');
		}

		// Create relation
		$array = array(
			'media_id' => $id,
			'user_id' => $this->session->user_id,
			'type' => $type
		);
		$this->db->set($array);
		$this->db->insert(self::TABLE_NAME);
	}

	/**
	 * Removes the media file from favorites.
	 * 
	 * @param string $id	an identifier of media file
	 */
	function remove($id) {
		// Try to delete a favourite
		$this->db->where('media_id', $id);
		$this->db->where('user_id', $this->session->user_id);
		$this->db->delete(self::TABLE_NAME);
	}

}
