<div class="login-container">
    <div class="login-header">
        <div class="login-content">         
            <img src="<?php echo site_url('assets'); ?>/images/Wolfie-logo-White.png" class="logo-login"  alt="Wolfie logo" />
            <p class="description">Hi there, become the authorized user!</p>
                        <!-- progress bar indicator -->
            <div class="login-progressbar-indicator">
                <h3>34%</h3>
                <span>logging in...</span>
            </div>
        </div><!-- login-content -->
    </div><!-- login-header -->
    
    <div class="login-progressbar">
        <div></div>
    </div><!-- login-progressbar -->
    
    <div class="login-form">
        <div class="login-content">

            <?php echo validation_errors('<div class="error">', '</div>'); ?>
            <?php echo $this->session->flashdata('registration_message'); ?>

            <div class="form-login-error">
                <h3>Invalid login</h3>
                <p>Please enter again your login and password.</p>
            </div><!-- form-login-error -->
            
            <!-- <form method="post" role="form" id="form_login"> -->
            <?php echo form_open('index.php/user/newaccount')?>
                <div class="form-group">
                    <div class="input-group">
                        <div class="input-group-addon">
                            <i class="entypo-user"></i>
                        </div>
                        <input type="text" class="form-control" name="username" id="username" placeholder="Username" autocomplete="off" />
                    </div><!-- input-group -->
                </div><!-- form-group -->
                
                <div class="form-group">
                    <div class="input-group">
                        <div class="input-group-addon">
                            <i class="entypo-key"></i>
                        </div>
                        <input type="password" class="form-control" name="password" id="password" placeholder="Password" autocomplete="off" />
                    </div><!-- input-group -->
                </div><!-- form-group -->

                <div class="form-group">
                    <div class="input-group">
                        <div class="input-group-addon">
                            <i class="entypo-key"></i>
                        </div>
                        <input type="password" class="form-control" name="password_conf" id="password" placeholder="Confirm Password" autocomplete="off" />
                    </div><!-- input-group -->
                </div><!-- form-group -->
                
                <div class="form-group">
                    <button type="submit" class="btn btn-primary btn-block btn-login">
                        <i class="entypo-login"></i>
                        Create Account
                    </button>
                </div><!-- form-group -->
             <?php echo form_close(); ?>
           <div class="login-bottom-links">
                <a href="/index.php/user/login" class="link">Already have account?</a>
                <!-- <br /> -->
                <!-- <a href="#">Term of Service</a> - <a href="#">Privacy Policy</a> -->
            </div>
        </div><!-- login-content -->
    </div><!-- login-form -->   
</div><!-- login-container -->
<!-- <p class="text-mute small">Page rendered in {elapsed_time} seconds</p> -->