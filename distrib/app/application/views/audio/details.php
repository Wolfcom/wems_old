<input type="hidden" id="record-module" value="Audio" />
<input type="hidden" id="record-id" value="<?= $id ?> " />

<div class="col-sm-10 main-content body-wrapper tab video-preview">
	<div class="container-fluid">
		<?php require( dirname(__FILE__) . '/../template/navigation.php'); ?>
	</div>

	<div class="video-details-container">
		<div class="head-container">
			<h3><?= empty($audio->title) ? 'No Title' : $audio->title ?></h3>
			<span class="modal-close-icon icon-icn_close_x_01 secondaryColorTextActive" aria-hidden="true" onclick="location.href = '/index.php/audio/preview/<?= $id ?>'"></span>
		</div>

		<div class="video-details-inner">
			<div class="col-sm-4">
				<div class="detail-video">
					<audio  controls preload="true" autoplay>
						<source src="/index.php/audio/index/<?= $id ?>">
					</audio>
				</div>				

				<div class="details-box">
					<h4>Details</h4>
					<label class="d-tiltle">Size : </label>
					<span class="d-content"><?= format_bytes($audio->size) ?></span><br>
					<label class="d-tiltle">Duration : </label>
					<span class="d-content"><?= $audio->duration ?></span><br>
					<label class="d-tiltle">Date Created : </label>
					<span class="d-content"><?= $audio->created ?></span><br>
					<label class="d-tiltle">Date Modified : </label>
					<span class="d-content"></span><br>
					<label class="d-tiltle">Upload By : </label>
					<span class="d-content"><?= $audio->first_name ?> <?= $audio->last_name ?> (<?= $audio->user_login ?>)</span><br>
				</div>
			</div>

			<div class="col-sm-4">
				<label for="record-title" class="s-title">Name</label>
				<input type="text" id="record-title" class="s-select" value="<?= $audio->title ?>">

				<label for="record-classification" class="s-title">Classification</label>
				<select name="s-clacification" id="record-classification">
					<option value="">None</option>
					<?php foreach ($classifications as $item) { ?>
						<option value="<?= $item ?>" <?= selected($item == $audio->classification) ?>><?= $item ?></option>
					<?php } ?>
				</select>

				<label for="record-caseno" class="s-title">Case</label>
				<input type="text" id="record-caseno" class="s-select" value="<?= $audio->caseno ?>">

				<label for="record-description" class="s-title">Description</label>
				<textarea id="record-description" class="text-box"><?= $audio->description ?></textarea>

				<div class="btn-group save-meta-group">
					<button type="button" class="btn btn-primary btn-popup btn-detail" id="SaveMetadata">Save</button>
				</div>
			</div>

			<!--<div class="col-sm-4">
				<h4>Bookmarks</h4>
				<label class="d-tiltle">Size : </label>
				<span class="d-content">376.52 KB</span><br>
				<label class="d-tiltle">Dimensions : </label>
				<span class="d-content">400*270 Pxels</span><br>
				<label class="d-tiltle">Date Uploaded : </label>
				<span class="d-content">2016-02-14</span><br>
			</div>-->
		</div>


	</div>
</div>
