<input type="hidden" id="record-module" value="Audio" />
<input type="hidden" id="record-id" value="<?= $id ?> " />
<input type="hidden" id="in-favs" value="<?= $audio->in_favs ? 1 : 0 ?> " />

<div class="col-sm-10 main-content body-wrapper tab video-preview">
	<div class="container-fluid">
		<?php require( dirname(__FILE__) . '/../template/navigation.php'); ?>
	</div>

	<div class="video-details-container">
		<div class="head-container">
			<h3><?= empty($audio->title) ? 'No Title' : $audio->title ?></h3>
			<span class="modal-close-icon icon-icn_close_x_01 secondaryColorTextActive" aria-hidden="true" onclick="location.href = '/index.php/files'"></span>
		</div>

		<div class="video-details-inner picture-details-inner">
			<audio controls preload="true" autoplay>
				<source src="/index.php/Audio/index/<?= $id ?>" type="audio/mp3">
				Your browser does not support the audio element.
			</audio>

			<div class="btn-box">
			<?php foreach ($buttons as $button) { ?>
				<button type="button" class="btn btn-primary" id="<?= $button['id'] ?>"><?= $button['title'] ?></button>
			<?php }	?>
			</div>
		</div>
	</div>

</div>
