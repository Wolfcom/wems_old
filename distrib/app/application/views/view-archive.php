<script>
	<?php if (empty($current_permissions->read_video)) { ?>
		alert('No permission to preview image files/audio files/video files');
	<?php } ?>
</script>

<div class="col-sm-10 main-content body-wrapper tab">
	<div class="container-fluid">
		<div class="row">
			<?php require('template/navigation.php'); ?>
			<?php require('template/pagination_head.php'); ?>

<div  class="admin-content-page admin-groups-list ng-scope">
	<div class="feature-area">
		<div class="add-new-button">
			<a title="Edit" href="javascript:void(0)" onclick="edit_media_file(get_selected_record())">
				<i class="fa fa-pencil"></i>
			</a>
		</div>

		<div class="add-new-button">
			<a title="Delete" href="javascript:void(0)" onclick="delete_media_file(get_selected_records())"><i class="fa fa-trash"></i></a>
		</div>

		<div class="add-new-button">
			<a title="Download" href="javascript:void(0)" onclick="download_dialog_file(get_selected_records())"><i class="fa fa-download"></i></a>
		</div>

		<div class="add-new-button">
			<a title="More" href="javascript:void(0)" id="#"><i class="fa fa-ellipsis-h"></i></a>

			<?php if (!empty($current_features->audit)) { ?>
				<ul class="user-more-sub">
					<li>
						<a href="javascript:void(0)" onclick="report_dialog(get_selected_records())">Audit Report</a>
					</li>
				</ul>
			<?php } ?>
		</div>
	</div>

	<div class="table-list table-bg">
		<div class="table-responsive">
			<table class="table  table-user-groupe">
				<thead>
					<tr>
						<th>                                                                                
							<input type="checkbox" class="priv-chk" />
						</th>
						<th>Preview</th>
						<th>Type</th>
						<th>Size</th>
						<th>Case No.</th>
						<th>Title</th>
						<th>Uploaded By</th>
						<th>Creation Date</th>
						<th>Archived On</th>
					</tr>
				</thead>

				<tbody>
					<?php
					foreach ($records as $record) {
						$attributes = "class='{$record->type}-row' data-record-id='{$record->id}' data-record-type='{$record->type}' "; ?>
						<tr>
							<td>                                                                                                            
								<input type="checkbox" class="priv-chk record" value="<?= $record->id ?>" data-record-type="<?= $record->type ?>" />
							</td>

							<td <?= $attributes ?> id="pict-row">
								<?php if ($record->type == 'picture') { ?>
									<img src="/index.php/Picture/index/<?= $record->id ?>" class="img-responsive table-img" />
								<?php } else if ($record->type == 'video') { ?>
									<img src="/index.php/VideoThumbnail/index/<?= $record->id ?>" onerror="this.src='<?= site_url("assets") ?>/images/non-video.png'" class="img-responsive table-img" />
								<?php } else { ?>
									<img src="<?= site_url("assets") ?>/images/non-audio.png" class="img-responsive table-img" />
								<?php } ?>
							</td>

							<td><?= $record->type ?></td>
							<td><?= format_bytes($record->size) ?></td>
							<td><?= $record->caseno ?></td>
							<td><?= $record->title ?></td>
							<td><?= $record->first_name ?> <?= $record->last_name ?> (<?= $record->user_login ?>)</td>
							<td><span class="datetime"><?= $record->created ?></span></td>
							<td><span class="datetime"><?= $record->archived ?></span></td>
						</tr>
					<?php } ?>
				</tbody>
			</table>
		</div>
	</div>
</div>

	<?php require('template/pagination_foot.php'); ?>

		</div>
	</div>
</div>

<?php require('template/download_dialog.php'); ?>
<?php require('template/audit_dialog.php'); ?>
