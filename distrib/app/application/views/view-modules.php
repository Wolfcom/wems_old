<div class="col-sm-10 main-content body-wrapper tab">
    <div class="container-fluid">
        <div class="row">            
            <?php require(dirname(__FILE__) . '/template/navigation.php'); ?>
            <div id="privileges-page">
                <div  id="admin-conf-global" class="admin-content-page configuration-global secondaryColorScrollbar ng-scope">
                    <ul class="table-priv">
                            <li>
                                    <h3 class="priv-head">List of Modules & Functions</h3>
                                    <div class="conf-global-table crew_privileges">
                                                    <div class="global-table-title global-table-title-text">Module and Functions that can be enabled or disabled</div>
                                                   <div class="global-table-content ng-scope" >
                                                                <ul>
                                                                    <li  class="ng-scope">
                                                                        <input type="checkbox" class="priv-chk" value="security" <?= checked(!empty($features->security)) ?> />
                                                                        <span class="capability-name ng-binding">Security Module</span>
                                                                    </li>
                                                                    <li  class="ng-scope">
                                                                        <input type="checkbox" class="priv-chk" value="gps_map" <?= checked(!empty($features->gps_map)) ?>/>
                                                                        <span class="capability-name ng-binding">GPS Map</span>
                                                                    </li>
                                                                    <li  class="ng-scope">
                                                                        <input type="checkbox" class="priv-chk" value="burn_disc" <?= checked(!empty($features->burn_disc)) ?>/>
                                                                        <span class="capability-name ng-binding">Burn to Disc</span>
                                                                    </li>
                                                                </ul>
                                                            </div>

                                                            <div class="global-table-content ng-scope" >
                                                                <ul>
                                                                    <li  class="ng-scope">
                                                                        <input type="checkbox" class="priv-chk" value="redaction" <?= checked(!empty($features->redaction)) ?> />
                                                                        <span class="capability-name ng-binding">Redaction</span>
                                                                    </li>
                                                                    <li  class="ng-scope">
                                                                        <input type="checkbox" class="priv-chk" value="audit" <?= checked(!empty($features->audit)) ?>/>
                                                                        <span class="capability-name ng-binding">Audit</span>
                                                                    </li>
                                                                    <li  class="ng-scope">
                                                                        <input type="checkbox" class="priv-chk" value="audit" <?= checked(!empty($features->audit)) ?>/>
                                                                        <span class="capability-name ng-binding">Retention Module</span>
                                                                    </li>
                                                                    
                                                                    <li class="ng-scope">
                                                                        <input type="checkbox" class="priv-chk" value="upload_file" <?= checked(!empty($features->upload_file)) ?>/>
                                                                        <span class="capability-name ng-binding">Upload Module</span>
                                                                    </li>
                                                                </ul>
                                                            </div>
                                    </div>
                            </li>
                    </ul>
                    
                    <div class="button-area">                   
                        <button type="button" class="btn-parent btn-default" onclick = "javascript:update_modules();">Set</button>
                    </div>
                </div>
            </div>
        </div>
		</div>
	</div>
</div> 
