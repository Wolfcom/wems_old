<div class="col-sm-10 main-content body-wrapper tab">
    <div class="container-fluid">
        <div class="row">            
            <?php require(dirname(__FILE__) . '/template/navigation.php'); ?>
            <div id="privileges-page">
                <div  id="admin-conf-global" class="admin-content-page configuration-global secondaryColorScrollbar ng-scope">
                    <ul class="table-priv">
                            <li>
                                    <h3 class="priv-head">Archive</h3>
                                    <div class="conf-global-table crew_privileges crew-one">
                                                    <div class="global-table-title global-table-title-text">Classification</div>
                                                   <div class="global-table-content ng-scope" >
                                                    	<select id="archive_classifications" name="archive_classifications" class="multiple-user-select" multiple="multiple">
														<?php foreach ($classifications as $category) { ?>
                                                    		<option value="<?= $category ?>"><?= $category ?></option>
														<?php } ?>
                                                    	</select>
                                                    </div>
                                       </div>
                                       <div class="conf-global-table crew_privileges crew-two">
                                                    <div class="global-table-title global-table-title-text">Duration</div>

                                                    <div class="global-table-content ng-scope" >
														<input type="number" id="archive_duration" name="archive_duration" value="1" /> Days
                                                    </div>
                                    </div>

                                    <div class="clearfix"></div>

                                    <div class="conf-global-table crew_privileges">
                                                    <div class="global-table-title global-table-title-text globale-one">Classification</div>
                                                    <div class="global-table-title global-table-title-text globale-two">Duration</div>
                                                    <div class="archive-atble-container table-responsive">
                                                   		<table class="table table-archive">
                                                   			<tbody>
															<?php foreach ($policy as $rule) {
																if (empty($rule->archive_duration)) {
																	continue;
																}
															?>
                                                   				<tr>
                                                   					<td><a onclick="select_archive_category('<?= $rule->category ?>')"><?= $rule->category ?></a></td>
                                                   					<td><a onclick="select_archive_category('<?= $rule->category ?>')">
																	<?=
																		$rule->archive_duration == '178000000 years' 
																			? 'Forever' : $rule->archive_duration
																	?></a></td>
                                                   				</tr>
															<?php } ?>
                                                   			</tbody>
                                                   		</table>                                                    	
                                                    </div>
                                  </div>
                            </li>
                    </ul>

                    <div class="button-area">                   
                        <button id="DeleteArchivePolicy" type="button" class="btn-parent btn-default">Delete</button>
                        <button id="SaveArchivePolicy" type="button" class="btn-parent btn-default">Save</button>
                    </div>
	        

	        <ul class="table-priv">
                            <li>
                                    <h3 class="priv-head">Post Archive</h3>
                                    <div class="conf-global-table crew_privileges crew-one">
                                                    <div class="global-table-title global-table-title-text">Classification</div>
                                                   <div class="global-table-content ng-scope" >
                                                    	<select id="post_archive_classifications" name="post_archive_classifications" class="multiple-user-select" multiple="multiple">
														<?php foreach ($classifications as $category) { ?>
                                                    		<option value="<?= $category ?>"><?= $category ?></option>
														<?php } ?>
                                                    	</select>
                                                    </div>
                                       </div>
                                       <div class="conf-global-table crew_privileges crew-two">
                                                    <div class="global-table-title global-table-title-text">Duration</div>

                                                    <div class="global-table-content ng-scope" >
														<input type="number" id="post_archive_duration" name="post_archive_duration" value="1" /> Days
                                                    </div>
                                    </div>

                                    <div class="clearfix"></div>

                                    <div class="conf-global-table crew_privileges">
                                                    <div class="global-table-title global-table-title-text globale-one">Classification</div>
                                                    <div class="global-table-title global-table-title-text globale-two">Duration</div>
                                                    <div class="archive-atble-container table-responsive">
                                                   		<table class="table table-archive">
                                                   			<tbody>
                                                   			<?php foreach ($policy as $rule) {
																if (empty($rule->post_archive_duration)) {
																	continue;
																}
															?>
                                                   				<tr>
                                                   					<td><a onclick="select_post_archive_category('<?= $rule->category ?>')"><?= $rule->category ?></a></td>
                                                   					<td><a onclick="select_post_archive_category('<?= $rule->category ?>')">
																	<?=
																		$rule->post_archive_duration == '178000000 years' 
																			? 'Forever' : $rule->post_archive_duration
																	?>
																	</a></td>
                                                   				</tr>
															<?php } ?>
                                                   			</tbody>
                                                   		</table>                                                    	
                                                    </div>
                                  </div>
                            </li>
                    </ul>
                    
                    <div class="button-area">                   
                        <button id="DeletePostArchivePolicy" type="button" class="btn-parent btn-default">Delete</button>
                        <button id="SavePostArchivePolicy" type="button" class="btn-parent btn-default">Save</button>
                    </div>
                </div>
            </div>
        </div>
		</div>
	</div>
</div> 
