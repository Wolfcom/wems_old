<div class="col-sm-10 main-content body-wrapper tab">
	<div class="container-fluid">
		<div class="row">
			<?php require(dirname(__FILE__) . '/../template/navigation.php'); ?>
			<div  class="admin-content-page admin-groups-list ng-scope">
			    <div class="feature-area">
					<div class="add-new-button">
						<!--<a title="Edit" href="javascript:void(0)" onclick="$('#dialog-edit-' + get_selected_record()).dialog('open')"><i class="fa fa-pencil"></i></a>-->
					</div>

					<div class="add-new-button">
						<a title="More" href="javascript:void(0)" id="more-info"><i class="fa fa-ellipsis-h"></i></a>
						<ul class="user-more-sub">
							<li>
								<a href="javascript:void(0)" onclick="device_settings(get_selected_record())">Settings <i class="fa fa-ellipsis-h"></i></a>
							</li>
						</ul>
					</div>
				</div>

				<div class="table-list table-bg">
					<div class="table-responsive">
						<table class="table  table-user-groupe">
							<thead>
								<tr>
									<th>
										<input type="checkbox" class="priv-chk" />
									</th>

									<th>Device ID</th>
									<th>Device Type</th>
									<th>Badge ID</th>
									<th>Assigned To</th>
									<th>Status</th>
									<th>Progress</th>
								</tr>
							</thead>

							<tbody>
								<?php foreach ($device_list as $device) { ?>
									<tr>
										<td>
											<input type="checkbox" class="priv-chk record" value="<?= $device->id ?>" />
										</td>

										<td><?= $device->id ?></td>
										<td id="device-type-<?= $device->id ?>" data-device-type="<?= $device->type ?>">
											<?php
												switch ($device->type) {
													case 'VISION':
														echo 'Wolfcom Vision';
														break;

													case 'THIRD_EYE':
														echo 'Wolfcom 3rd Eye';
														break;
												}
											?>
										</td>
										<td><?= $device->login ?></td>
										<td><?= $device->first_name ?> <?= $device->last_name ?></td>

										<td id="device-status-<?= $device->id ?>" class="device-<?= $device->status ?>"></td>
										<td id="device-progress-<?= $device->id ?>" class="device-progress">
										<?php
											if ($device->status == 'SYNC') {
												echo $device->progress;
											}
										?>
										</td>
									</tr>
								<?php
									} ?>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
