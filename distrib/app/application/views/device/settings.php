<div class="col-sm-10 main-content body-wrapper tab">
	<?php require dirname(__FILE__) . '/../template/navigation.php'; ?>

	<div class="camera-setting-container">
	<form id="device-settings-form">
		<div class="col-sm-3">
			<h3>Initial Settings</h3>
			<!-- <label for="#" class="badge-label">badge id:</label>
			<input type="text" class="badge-input"> -->


			<label for="device_id" class="badge-label">Device ID:</label>
			<select id="device_id" class="set-select" disabled>
				<option><?= $device_id ?></option>
			</select>

			<label for="badge_id" class="badge-label">Badge ID:</label>
			<input id="badge_id" name="badge_id" value="<?= $badge_id ?>">

			<!--<label for="device_time" class="badge-label">Time:</label>
			<input id="device_time" value="<?= date('H:i:s') ?>" disabled>-->

			<label for="timezone" class="badge-label">Time Zone:</label>
			<!-- <p class="second-set">-8</p><br> -->
			<select name="timezone" id="timezone" class="set-select" required>
				<?php foreach ($timezones as $time => $timezone) { ?>
					<option value="<?= $time ?>" <?= selected($settings['timezone'] == $time) ?>><?= $timezone ?></option>
				<?php } ?>
			</select><br /><br />
		</div>

		<div class="col-sm-9">
			<h3>Mode Select and Settings</h3>
			<div class="col-sm-6">
				<label for="video_resolution" class="set-label">Video Resolution</label>
				<select name="video_resolution" id="video_resolution" class="set-select" required>
					<option value="FPS_30_1920x1080" <?= selected($settings['video_resolution'] == 'FPS_30_1920x1080') ?>>1920x1080</option>
					<option value="FPS_60_1280x720" <?= selected($settings['video_resolution'] == 'FPS_60_1280x720') ?>>1280x720 (60FPS)</option>
					<option value="FPS_30_1280x720" <?= selected($settings['video_resolution'] == 'FPS_30_1280x720') ?>>1280x720 (30FPS)</option>
					<option value="FPS_60_848x480" <?= selected($settings['video_resolution'] == 'FPS_60_848x480') ?>>848x480 (60FPS)</option>
					<option value="FPS_30_848x480" <?= selected($settings['video_resolution'] == 'FPS_30_848x480') ?>>848x480 (30FPS)</option>
					<option value="FPS_30_640x480" <?= selected($settings['video_resolution'] == 'FPS_30_640x480') ?>>640x480</option>
				</select><br />

				<label for="compression_rate" class="set-label">Compression Rate</label>
				<select name="compression_rate" id="compression_rate" class="set-select" required>
					<option value="SUPER" <?= selected($settings['compression_rate'] == 'SUPER') ?>>SUPER</option>
					<option value="FINE" <?= selected($settings['compression_rate'] == 'FINE') ?>>FINE</option>
					<option value="NORMAL" <?= selected($settings['compression_rate'] == 'NORMAL') ?>>NORMAL</option>
				</select><br />

				<label for="video_format" class="set-label">Video Format</label>
				<select name="video_format" id="video_format" class="set-select" required>
					<option value="MOV" <?= selected($settings['video_format'] == 'MOV') ?>>MOV</option>
					<option value="MP4" <?= selected($settings['video_format'] == 'MP4') ?>>MP4</option>
				</select><br />

				<label for="audio_record" class="set-label">Audio Record</label>
				<select name="audio_record" id="audio_record" class="set-select" required>
					<option value="YES"  <?= selected($settings['audio_record'] == 'YES') ?>>YES</option>
					<option value="NO"  <?= selected($settings['audio_record'] == 'NO') ?>>NO</option>
				</select><br />

				<label for="recycle_record" class="set-label">Recycle Record</label>
				<select name="recycle_record" id="recycle_record" class="set-select" required>
					<option value="MIN_30" <?= selected($settings['recycle_record'] == 'MIN_30') ?>>30 min</option>
					<option value="MIN_10" <?= selected($settings['recycle_record'] == 'MIN_10') ?>>10 min</option>
					<option value="MIN_5" <?= selected($settings['recycle_record'] == 'MIN_5') ?>>5 min</option>
					<option value="MIN_3" <?= selected($settings['recycle_record'] == 'MIN_3') ?>>3 min</option>
					<option value="MIN_1" <?= selected($settings['recycle_record'] == 'MIN_1') ?>>1 min</option>
				</select><br />

				<label for="pre_record_audio" class="set-label">Pre-record Audio</label>
				<select name="pre_record_audio" id="pre_record_audio" class="set-select" required>
					<option value="YES" <?= selected($settings['pre_record_audio'] == 'YES') ?>>YES</option>
					<option value="NO" <?= selected($settings['pre_record_audio'] == 'NO') ?>>NO</option>
				</select><br />				
			</div>

			<div class="col-sm-6">
				<label for="loop_record" class="set-label">Loop Record</label>
				<select name="loop_record" id="loop_record" class="set-select" required>
					<option value="YES" <?= selected($settings['loop_record'] == 'YES') ?>>YES</option>
					<option value="NO" <?= selected($settings['loop_record'] == 'NO') ?>>NO</option>
				</select><br />

				<label for="photo_resolution" class="set-label">Photo Resolution</label>
				<select name="photo_resolution" id="photo_resolution" class="set-select" required>
					<option value="MPX_16" <?= selected($settings['photo_resolution'] == 'MPX_16') ?>>16M</option>
					<option value="MPX_12" <?= selected($settings['photo_resolution'] == 'MPX_12') ?>>12M</option>
					<option value="MPX_8" <?= selected($settings['photo_resolution'] == 'MPX_8') ?>>8M</option>
					<option value="MPX_5" <?= selected($settings['photo_resolution'] == 'MPX_5') ?>>5M</option>
					<option value="MPX_3" <?= selected($settings['photo_resolution'] == 'MPX_3') ?>>3M</option>
				</select><br />

				<label for="indicator" class="set-label">Public Awareness Indicator</label>
				<select name="indicator" id="indicator" class="set-select" required>
					<option value="FLASH" <?= selected($settings['indicator'] == 'FLASH') ?>>FLASH</option>
					<option value="OFF" <?= selected($settings['indicator'] == 'OFF') ?>>OFF</option>
					<option value="ON" <?= selected($settings['indicator'] == 'ON') ?>>ON</option>
				</select><br />

				<label for="datestamp" class="set-label">Date & Time Stamp</label>
				<select name="datestamp" id="datestamp" class="set-select" required>
					<option value="YES" <?= selected($settings['datestamp'] == 'YES') ?>>YES</option>
					<option value="NO"  <?= selected($settings['datestamp'] == 'NO') ?>>NO</option>
				</select><br />

				<label for="gps_stamp" class="set-label">GPS Stamp</label>
				<select name="gps_stamp" id="gps_stamp" class="set-select" required>
					<option value="YES" <?= selected($settings['gps_stamp'] == 'YES') ?>>YES</option>
					<option value="NO" <?= selected($settings['gps_stamp'] == 'NO') ?>>NO</option>
				</select><br />

				<label for="touch_record" class="set-label">One Touch Record</label>
				<select name="touch_record" id="touch_record" class="set-select" required>
					<option value="NORMAL" <?= selected($settings['touch_record'] == 'NORMAL') ?>>NORMAL</option>
					<option value="ADVANCE" <?= selected($settings['touch_record'] == 'ADVANCE') ?>>ADVANCE</option>
				</select><br />				
			</div>
		</div>

		<div class="reset-container">
			<button type="submit" class="btn-change-password btn-popup" id="UpdateDeviceSettings">Apply Setting</button>
		</div>
	</form>
	</div>
</div>
