<div id="report-dialog" class="modal fade admin-user-dialog in add_new_user dialog-edit download-pop-radio"  style="z-index: 1050;">
	<div class="modal-header ng-scope">
		<div>Audit Report</div>
		<span class="modal-close-icon icon-icn_close_x_01  secondaryColorTextActive" ></span>
	</div>
	        <div class="modal-body ng-scope">
		<div  class="modal-inner">
		        <form id="createProfile" class="edit-add-form ng-pristine ng-invalid ng-invalid-required">
		            <section>
		                <input type="radio" value="display" name="report_action" id="display_report"  />
		                <label for="display_report">Display</label>
		            </section>

		            <section>
		                <input type="radio" value="download" name="report_action" id="download_report" />
		                <label for="download_report">Download</label>
		            </section>
		        </form>
		</div>

		<div class="modal-footer ng-scope">
			<button class="btn-parent btn-default" onclick="get_file_report(get_selected_record())">Ok</button>
			<button class="btn-parent btn-optional btn-close-edit" >Cancel</button>
		</div>
	</div>
</div>
