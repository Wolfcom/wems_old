<div class="modal fade" id="<?= empty($dialog_id) ? 'playerModal' : $dialog_id ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
<div class="modal-dialog modal-lg">
<div class="modal-content">
<div class="modal-header">
<!-- 	<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
 -->	<span class="modal-close-icon icon-icn_close_x_01  secondaryColorTextActive" data-dismiss="modal" aria-hidden="true"></span>
	<h4 class="modal-title"><?= $title ?></h4>
</div>
<div class="modal-body">
