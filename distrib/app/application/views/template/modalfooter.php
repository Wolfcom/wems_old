</div>
<div class="modal-footer">
	<div class="btn-box">
		<?php 
			foreach ((array)$left_buttons as $index => $button) {
				if ($index == 4) {
					//echo '<br/><br/>';
				}
		?>
				<button type="button" class="btn btn-primary" id="<?= $button['id'] ?>"><?= $button['title'] ?></button>
		<?php
			}
		?>
	</div>
	<div id="notification-message"></div>
	<!--<div class="pull-right">
		<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
		<button type="button" class="btn btn-primary" id="btn_openMap">Open Map</button>
	</div>-->
	<div class="clearfix"></div>
	<div id="screen"></div>
</div>
</div>
<!-- /.modal-content -->
</div>
<!-- /.modal-dialog -->
</div>
