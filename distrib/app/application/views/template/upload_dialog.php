<div id="upload-dialog" class="modal fade admin-user-dialog in add_new_user download-pop dialog-edit"  style="z-index: 1050;">
	<div class="modal-header ng-scope">
		<div>Upload File</div>
		<span class="modal-close-icon icon-icn_close_x_01  secondaryColorTextActive" ></span>
	</div>

	<div class="modal-body ng-scope">
		<div  class="modal-inner">
			<form method="POST" class="edit-add-form ng-pristine ng-invalid ng-invalid-required" enctype="multipart/form-data">
				<div id="files"></div>
				
				<section>
					<p>Please select a document</p>
					<input type="file" name="file" id="file" class="#"/>
				</section>

				<div class="modal-footer ng-scope">
					<input type="submit" class="btn-parent btn-default">
					<button class="btn-parent btn-optional btn-close-edit" type="button">Cancel</button>
				</div>
			</form>
			
    
		</div>
	</div>
</div>
