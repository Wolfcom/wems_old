<div id="download-dialog" class="modal fade admin-user-dialog in add_new_user download-pop dialog-edit"  style="z-index: 1050;">
	<div class="modal-header ng-scope">
		<div>Download File(s)</div>
		<span class="modal-close-icon icon-icn_close_x_01  secondaryColorTextActive" ></span>
	</div>

	<div class="modal-body ng-scope">
		<div  class="modal-inner">
			<form id="createProfile" class="edit-add-form ng-pristine ng-invalid ng-invalid-required">
				<?php if (!empty($current_permissions->download_report)) { ?>
					<section>
						<input type="checkbox" class="priv-chk" id="download-include-report" />
						<label for="download-include-report">Include Audit Report</label>
					</section>
				<?php } ?>

				<section>
					<input type="checkbox" class="priv-chk" id="download-zip-file" />
					<label for="download-zip-file">Zip File</label>
				</section>
			</form>
		</div>

		<div class="modal-footer ng-scope">
			<button class="btn-parent btn-default" onclick="download_file(get_selected_records())">Ok</button>
			<button class="btn-parent btn-optional btn-close-edit" >Cancel</button>
		</div>
	</div>
</div>
