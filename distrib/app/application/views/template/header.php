<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0" />
  <title><?= $header_title ?></title>
  <link rel="stylesheet" type="text/css" href="<?php echo site_url('assets'); ?>/css/font-awesome/css/font-awesome.min.css">
  <link rel="stylesheet" type="text/css" href="<?php echo site_url('assets'); ?>/css/core-min.css">
  <link rel="stylesheet" type="text/css" href="<?php echo site_url('assets'); ?>/css/bootstrap.min.css">
  <link rel="stylesheet" type="text/css" href="<?php echo site_url('assets'); ?>/css/bootstrap_new.min.css">
  <link rel="stylesheet" type="text/css" href="<?php echo site_url('assets'); ?>/css/style.css">
  <link rel="stylesheet" type="text/css" href="<?php echo site_url('assets'); ?>/css/style_new.css">
  <link rel="stylesheet" type="text/css" href="<?php echo site_url('assets'); ?>/bower_components/plyr/dist/plyr.css">
  <link rel="stylesheet" type="text/css" href="<?= site_url('assets') ?>/js/select2/select2.css">
  <link rel="stylesheet" type="text/css" href="<?= site_url('assets') ?>/css/pnotify.custom.min.css">

  <!--[if lt IE 9]><script src="js/ie8-responsive-file-warning.js"></script><![endif]-->

  <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
  <![endif]-->
</head>

<body class="page-body <?php echo (!empty($header_data['class_in_body'])) ? $header_data['class_in_body'] : ''; ?>" data-url="" id="<?= empty($page_id) ? '' : $page_id  ?>">

<script>
<?php if (!empty($current_permissions)) { ?>
	window.current_user_permissions = <?= json_encode($current_permissions) ?>;
<?php } ?>

<?php if (!empty($current_permissions)) { ?>
	window.current_features = <?= json_encode($current_permissions) ?>;
<?php } ?>
</script>

<?php if($this->session->userdata('user_id')) { ?>

<div class="page-container sidebar-collapsed">
    <div class="top-head-t">
      <a href="<?= site_url() ?>" class="col-sm-12 col-md-3">
        <img class="logo-top-menu" src="<?= site_url('assets') ?>/images/wolfcom-levidence.png" alt="Wolfcom Logo" />
      </a>

      <div class="col-xs-12 col-sm-6 col-sm-push-6  col-md-push-6 col-md-3 user-section">
        <div class="user-info-wrap">

		 <div class="user-title-wrap">
            <p class="name"><a href="<?php echo site_url('index.php/user/profile'); ?>"><?= $this->session->userdata('user_first_name') ?> <?= $this->session->userdata('user_last_name') ?></a></p>
            <p class="position">Logged as <strong><i><?php echo $this->session->userdata('user_login'); ?></i></strong></p>
          
            <div class="user-actions-menu">
                <div class="item secondaryColorIconInside ng-hide menu-queries">
                    <div class="text-icon"><i class="fa fa-file-text-o"></i></div>
                    <div class="text">Queries</div>
                    <div class="text-icon"><i class="fa fa-ellipsis-h"></i></div>
                          <div class="secd-user-menu">
                              <div class="item secondaryColorIconInside ng-hide" onclick = "javascript:location.href = '/index.php/app/index';">
                                  <div class="text-icon"><i class="fa fa-th-large"></i></div>
                                  <div class="text">Dashboard</div>
                                  <div class="text-icon" style="float: right;"><i class="fa fa-arrow-left"></i></div>
                              </div>

							<?php if (!empty($current_permissions->module_cases)) { ?>
								<div class="item secondaryColorIconInside ng-hide" onclick = "javascript:location.href = '/index.php/cases';">
									<div class="text-icon"><i class="fa fa-suitcase"></i></div>
									<div class="text">My Cases</div>
								</div>
							<?php } ?>

							<?php if (!empty($current_permissions->module_favorites)) { ?>
								<div class="item secondaryColorIconInside" onclick = "javascript:location.href = '/index.php/favourites';">
									<div class="text-icon"><i class="fa fa-heart"></i></div>
									<div class="text">My Favorites</div>
								</div>
							<?php } ?>

							<?php if (!empty($current_permissions->module_bookmarks)) { ?>
								<div class="item secondaryColorIconInside" onclick = "javascript:location.href = '/index.php/bookmarks';">
									<div class="text-icon"><i class="fa fa-bookmark"></i></div>
									<div class="text">My bookmarks</div>
								</div>
							<?php } ?>

							<?php if (!empty($current_permissions->module_images)) { ?>
								<div class="item secondaryColorIconInside" onclick = "javascript:location.href = '/index.php/picture/list_pictures';">
									<div class="text-icon"><i class="fa fa-picture-o"></i></div>
									<div class="text">My images</div>
								</div>
							<?php } ?>

							<?php if (!empty($current_permissions->module_videos)) { ?>
								<div class="item secondaryColorIconInside" onclick = "javascript:location.href = '/index.php/video/list_videos';">
									<div class="text-icon"><i class="fa fa-film"></i></div>
									<div class="text">My Videos</div>
								</div>
							<?php } ?>

							<?php if (!empty($current_permissions->module_files)) { ?>
								<div class="item secondaryColorIconInside" onclick = "javascript:location.href = '/index.php/files';">
									<div class="text-icon"><i class="fa fa-files-o"></i></div>
									<div class="text">All files</div>
								</div>
							<?php } ?>
                          </div>
                </div>

				<?php if (!empty($current_permissions->module_profile)) { ?>
					<div class="item secondaryColorIconInside" onclick="javascript:location.href = '/index.php/user/profile';">
						<div class="icon-user text-icon"></div>
						<div class="text">Profile</div>
					</div>
				<?php } ?>

				<?php if (!empty($current_permissions->module_users)) { ?>
					<div class="item secondaryColorIconInside" onclick="javascript:location.href = '/index.php/user/';">
						<div class="icon-users text-icon"></div>
						<div class="text">Users</div>
					</div>
				<?php } ?>

				<?php if (!empty($current_permissions->module_groups)) { ?>
					<div class="item secondaryColorIconInside" onclick="javascript:location.href = '/index.php/group/';">
						<div class="icon-cog text-icon"></div>
						<div class="text">Groups</div>
					</div>
				<?php } ?>

				<?php if (!empty($current_permissions->module_cameras)) { ?>
					<div class="item secondaryColorIconInside" onclick="javascript:location.href = '/index.php/device/';">
						<div class="text-icon"><i class="fa fa-camera"></i></div>
						<div class="text">Camera Management</div>
					</div>
				<?php } ?>

				<?php if (!empty($current_permissions->module_settings)) { ?>
					<div class="item secondaryColorIconInside ng-hide menu-queries" onclick="javascript:location.href = '/index.php/archive/';">
						<div class="text-icon"><i class="fa fa-archive"></i></div>
						<div class="text">Asset Archive</div>
					</div>
				<?php } ?>

				<?php if (!empty($current_permissions->module_settings)) { ?>
					<div class="item secondaryColorIconInside ng-hide menu-queries">
						<div class="text-icon"><i class="fa fa-cogs"></i></div>
						<div class="text">Settings</div>
				                    	<div class="text-icon"><i class="fa fa-ellipsis-h"></i></div>
					                          <div class="secd-user-menu setting-sub-menu">
												<?php if ($current_user_id == User_model::ADMIN_USER) { ?>
													<div class="item secondaryColorIconInside ng-hide" onclick = "javascript:location.href = '/index.php/features/index';">
						                                  <div class="text-icon"><i class="fa fa-rocket"></i></div>
						                                  <div class="text">Modules</div>
						                              </div>
												<?php } ?>

						                              <div class="item secondaryColorIconInside ng-hide" onclick = "javascript:location.href = '/index.php/settings/index';">
						                                  <div class="text-icon"><i class="fa fa-list"></i></div>
						                                  <div class="text">Picklist Editor</div>
						                              </div>

						                              <div class="item secondaryColorIconInside ng-hide" onclick = "javascript:location.href = '/index.php/archive/retension';">
						                                  <div class="text-icon"><i class="fa fa-user-secret"></i></div>
						                                  <div class="text">Retention Policies</div>
						                              </div>
						                              
												<?php if (!empty($current_features->audit)) { ?>
						                              <div class="item secondaryColorIconInside ng-hide" onclick = "javascript:location.href = '/index.php/activity/index';">
						                                  <div class="text-icon"><i class="fa fa-history"></i></div>
						                                  <div class="text">System Security Logs</div>
						                              </div>
						                        <?php } ?>

					                           </div>
					</div>
				<?php } ?>

                <!--<div class="item secondaryColorIconInside">
                    <a href="#" target="_blank" style="display:inline-block;width:100%;text-decoration: none;" onclick = "javascript:location.href = 'https://wolfcom.freshdesk.com/support/login';">
                        <div class="icon-help text-icon"></div>
                        <div class="text">Help</div>
                    </a>
                </div>-->
                <div class="item secondaryColorIconInside" onclick="javascript:show_ticket()">
					<div class="icon-help text-icon"></div>
					<div class="text">Submit Ticket</div>
                </div>
                <div class="item secondaryColorIconInside" onclick="javascript:show_about()">
                    <div class="icon-info text-icon"></div>
                    <div class="text">About Us</div>
                </div>
                <div class="item secondaryColorIconInside" onclick = "javascript:location.href = '/index.php/user/logout';">
                    <div class="text-icon"><i class="fa fa-sign-out"></i></div>
                    <div class="text">Logout</div>
                </div>
            </div>


          </div>
        

          <div class="user-profile-wrap">
            <?php  
				if (!$this->session->userdata('user_photo')) { ?>
					<img src="<?= site_url('assets') ?>/images/non-profile.jpg" alt="Profile Image" class="img-responsive profile-img">
			<?php
				} else { ?>
					<img src="<?= $this->session->userdata('user_photo') ?>" alt="Profile Image" class="img-responsive profile-img">
			<?php
				} ?>
          </div>

          </div>
      </div>
 

      <div class="col-xs-12 col-sm-6 col-sm-pull-6 col-md-pull-3  col-md-6 main-head-title">
        <h1><?php echo $header_title; ?></h1>
      </div>

      <!-- PROGRESS BAR -->
      <div id="show-progress-bar"></div>
      <!-- END PROGRESS BAR -->
 
    </div>


<?php } ?>
