	</div><!-- page-container -->

	<!-- Bottom Scripts -->
	<script type="text/javascript" src="<?php echo site_url('assets/node_modules/jquery/dist/'); ?>/jquery.min.js"></script>
	<script type="text/javascript" src="<?php echo site_url('assets/node_modules/bootstrap/dist/js/'); ?>/bootstrap.min.js"></script>
	<script type="text/javascript" src="<?php echo site_url('assets'); ?>/js/main-gsap.js"></script>
	<script type="text/javascript" src="<?php echo site_url('assets'); ?>/js/joinable.js"></script>
	<script type="text/javascript" src="<?php echo site_url('assets'); ?>/js/custom.js"></script>
	<script type="text/javascript" src="<?php echo site_url('assets'); ?>/js/resizeable.js"></script>
	<script type="text/javascript" src="<?php echo site_url('assets'); ?>/js/api.js"></script>	
	<script type="text/javascript" src="<?php echo site_url('assets'); ?>/js/jquery.dataTables.min.js"></script>
	<script type="text/javascript" src="<?php echo site_url('assets'); ?>/js/select2/select2.min.js"></script>
	<script type="text/javascript" src="<?php echo site_url('assets'); ?>/js/jquery.validate.min.js"></script>
	<script type="text/javascript" src="<?php echo site_url('assets'); ?>/js/login.js"></script>

	<script src="<?php echo site_url('assets'); ?>/bower_components/plyr/dist/plyr.js"></script>
	<script src="<?= site_url('assets') ?>/js/pnotify.custom.min.js"></script>
	<script><?php
		if (isset($this->session)) {
			foreach ((array)$this->session->flashdata('errors') as $error) { ?>
				new PNotify({
					text: '<?= addslashes($error) ?>',
					type: 'error'
				});
	<?php
			}
		} ?>
	</script>
	
	<!-- CUSTOM -->
	<script src="<?php echo site_url('assets'); ?>/js/bootstrap.dragable.js"></script>
	<!-- <script src="https://maps.google.com/maps/api/js?sensor=false" type="text/javascript"></script> -->
	<script src="//maps.googleapis.com/maps/api/js?v=3.exp&signed_in=true&libraries=places"></script>
	<script src="<?php echo site_url('assets'); ?>/js/main.js"></script>

	<script type="text/javascript" src="<?php echo site_url('assets'); ?>/js/jquery-ui.min.js"></script>
	<script src="<?php echo site_url('assets'); ?>/js/main_new.js"></script>
	<script type="text/javascript">
                    $('a#open-advanced').click(function() {
                        $('#dialog-advanced').dialog('open');
                    });

                    $('#dialog-advanced').dialog({
                        autoOpen: false,
                        modal: true,
                    });
                        
		$('a#open-edit').click(function() {
		    $('.dialog-edit').dialog('open');
		});

		$('.dialog-edit').dialog({
		    autoOpen: false,
		    modal: true,
		   show:{effect:'drop', direction:'right'},
		   hide:{effect:'drop', direction:'right'},
		    open: function() {
		        $('.btn-close-edit').bind('click',function(){
					jQuery(document.body).removeClass('dialog-open');
		            $('.dialog-edit').dialog('close');
		        });
		        $('.icon-icn_close_x_01').bind('click', function(event) {
					jQuery(document.body).removeClass('dialog-open');
		            $('.dialog-edit').dialog('close');
		        });
		    }
		});
           </script>         
</body>
</html>
