<!-- <nav class="tabs-wrapper">
	<ul class="tabs">
		<li><a href="/index.php/app/index"><i class="fa fa-suitcase"></i> Dashboard</a></li>
		<li><a href="/index.php/cases"><i class="fa fa-suitcase"></i> My Cases</a></li>
		<li><a href="/index.php/favourites"><i class="fa fa-heart"></i> Favourites</a></li>
		<li><a href="/index.php/bookmarks" ><i class="fa fa-bookmark"></i> My bookmarks</a></li>
		<li><a href="/index.php/images" ><i class="fa fa-picture-o"></i> My images</a></li>
		<li><a href="/index.php/videos" ><i class="fa fa-picture-o"></i> My videos</a></li>
		<li><a href="/index.php/files"><i class="fa fa-files-o"></i> All files</a></li>
	</ul>
</nav> -->

                    <div class="nav-area">
                            <div class="nav-search col-sm-6">
								<?php if (!empty($current_permissions->basic_search)) { ?>
									<div class="nav-search-input">
										<input id="nav-keyword" class="keyword thirdColorText thirdColorBorder ng-pristine ng-valid" type="text" name="keyword" placeholder="Search">
										<span class="clickable icon-search thirdColorText" type="button" ><i class="fa fa-search"></i></span>
									</div>
                                <?php } ?>

								<?php if (!empty($current_permissions->advanced_search)) { ?>
									<a href="javascript:void(0)" class="advance-seache" id="open-advanced">Advanced Search</a>
								<?php } ?>
                            </div>

                            <div class="nav-items col-sm-6" help-id="smartalbum">
                                <div class="thirdColorText "><a title="Dashboard" href="/index.php/app/index"><i class="fa fa-th-large"></i></a></div>

                                <?php if (!empty($current_permissions->module_cases)) { ?>
									<div class="thirdColorText "><a title="My Cases" href="/index.php/cases"><i class="fa fa-suitcase"></i></a></div>
								<?php } ?>

								<?php if (!empty($current_permissions->module_favorites)) { ?>
									<div class="thirdColorText"><a title="My Favorites" href="/index.php/favourites"><i class="fa fa-heart"></i></a></div>
								<?php } ?>

								<?php if (!empty($current_permissions->module_bookmarks)) { ?>
									<div class="thirdColorText"><a title="My Bookmarks" href="/index.php/bookmarks"><i class="fa fa-bookmark"></i></a></div>
								<?php } ?>

								<?php if (!empty($current_permissions->module_images)) { ?>
									<div class="thirdColorText"><a title="My Photos" href="/index.php/picture/list_pictures"><i class="fa fa-picture-o"></i></a></div>
								<?php } ?>

								<?php if (!empty($current_permissions->module_videos)) { ?>
									<div class="thirdColorText"><a title="My Videos" href="/index.php/video/list_videos"><i class="fa fa-film"></i></a></div>
								<?php } ?>

								<?php if (!empty($current_permissions->module_files)) { ?>
									<div class="thirdColorText"><a title="All Files" href="/index.php/files"><i class="fa fa-files-o"></i></a></div>
								<?php } ?>
                           </div> 
                    </div>


            <div class="advanced-search-content-area" id="dialog-advanced">
                <ul id="main-menu" class="">
                        <li class="opened root-level has-sub">
                            <a>
                                <i class="fa fa-search"></i>
                                <span style="">Search</span>
                            </a>

                                    <div class="col-xs-12">
                                        <input type="text" class="search-box" id="advanced-search-keyword" placeholder="Search">
                                        <input type="radio" id="advanced-search-owner" name="advanced-search-owner" class="search-chk" value="<?= $current_user_id ?>">
                                        <label for="advanced-search-owner" class="search-label">Search Only Throught my Files</label>
                                    </div>

								<?php if (!empty($current_permissions->user_search)) { ?>
									<div class="col-xs-12">
										<input type="radio" id="advanced-search-owner-user" name="advanced-search-owner" class="search-chk" value="SELECT">
										<label for="advanced-search-owner-user" class="search-label">Search By User</label>

										<?php if (!empty($app_user_list)) { ?>
											<select id="search-user-select" class="search-select" onchange='jQuery("#advanced-search-owner-user").prop("checked", true)'>
											<?php foreach ($app_user_list as $user) { ?>
												<option value="<?= $user->id ?>"><?= $user->first_name ?> <?= $user->last_name ?></option>
											<?php } ?>
											</select>
										<?php } ?>
									</div>
								<?php } ?>
                        </li>
                        <li class="opened root-level has-sub">
                            <a>
                                <i class="fa fa-calendar"></i>
                                <span style="">Time and Date</span>
                            </a>

                                    <div class="col-xs-12">
                                        <input type="radio" id="date-filter" name="advanced-search-date" class="time-chk" value="exact">
                                        <label for="date-filter" class="time-label">Any Date</label><br>
                                        <input type="text" id="first-datepicker" name="advanced-search-exact-date" onchange='jQuery("#date-filter").prop("checked", true)'><br>

                                        <input type="radio" id="range-filter" name="advanced-search-date" class="time-chk" value="range">
                                        <label for="range-filter" class="time-label">Range</label><br>
                                        <input type="text" id="scd-datepicker" name="advanced-search-start-date" onchange='jQuery("#range-filter").prop("checked", true)'>
                                        <input type="text" id="thrd-datepicker" name="advanced-search-end-date" onchange='jQuery("#range-filter").prop("checked", true)'>
                                    </div>

                        </li>
                        <li class="opened root-level has-sub">
                            <a>
                                <i class="fa fa-filter"></i>
                                <span style="">Filters</span>
                            </a>
                                    <div class="col-sm-6 filter-sub">
										<?php if (!empty($current_permissions->read_picture)) { ?>
											<input type="checkbox" id="image-type" name="advanced-search-type" value="picture" class="filter-chk">
											<label for="image-type" class="filter-label">Image</label><br>
										<?php } ?>

										<?php if (!empty($current_permissions->read_audio)) { ?>
											<input type="checkbox" id="audio-type" name="advanced-search-type" value="audio" class="filter-chk">
											<label for="audio-type" class="filter-label">Audio</label><br>
										<?php } ?>
                                    </div>

									<div class="col-sm-6 filter-sub">
										<?php if (!empty($current_permissions->read_video)) { ?>
											<input type="checkbox" id="video-type" name="advanced-search-type" value="video" class="filter-chk">
											<label for="video-type" class="filter-label">Video</label><br>
										<?php } ?>
										<?php if (!empty($current_permissions->read_document)) { ?>
											<input type="checkbox" id="video-type" name="advanced-search-type" value="document" class="filter-chk">
											<label for="video-type" class="filter-label">Other Files</label><br>
										<?php } ?>
									</div>

                                    <div class="search-btn-container">
                                        <button type="button" class="btn-parent btn-default btn-search" onclick="perform_search()">Search</button>
                                    </div>
                        </li>
                </ul>
            </div>
