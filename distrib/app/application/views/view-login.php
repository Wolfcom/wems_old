<div class="login-container">
	<div class="sync-progress">
		<div class="sync-progress-inner">
		</div>
	</div>
	<div class="login-header">
		<div class="login-content">			
			<img src="<?php echo site_url('assets'); ?>/images/Wolfie-logo-White.png" class="logo-login"  alt="Wolfie logo" />
			<p class="description">Welcome to Wolfcom Evidence Management Solution .<br>Please Log in</p>
						<!-- progress bar indicator -->
			<div class="login-progressbar-indicator">
				<h3>34%</h3>
				<span>logging in...</span>
			</div>
		</div><!-- login-content -->
	</div><!-- login-header -->
	
	<div class="login-progressbar">
		<div></div>
	</div><!-- login-progressbar -->
	
	<div class="login-form">
		<div class="login-content">

			<div class="form-login-error">
				<h3 class="login-error-title"></h3>
				<p class="login-error-message"></p>
			</div><!-- form-login-error -->
			
			<form method="post" role="form" id="form_login">
				<div class="form-group">
					<div class="input-group">
						<div class="input-group-addon">
							<i class="entypo-user"></i>
						</div>
						<input type="text" class="form-control" name="username" id="username" placeholder="Username" autocomplete="off" />
					</div><!-- input-group -->
				</div><!-- form-group -->
				
				<div class="form-group">
					<div class="input-group">
						<div class="input-group-addon">
							<i class="entypo-key"></i>
						</div>
						<input type="password" class="form-control" name="password" id="password" placeholder="Password" autocomplete="off" />
					</div><!-- input-group -->
				</div><!-- form-group -->
				
				<div class="form-group">
					<button type="submit" class="btn btn-primary btn-block btn-login">
						<i class="entypo-login"></i>
						Login
					</button>
				</div><!-- form-group -->
			</form>
<!-- 			<div class="login-bottom-links">
 --><!-- 				<a href="/index.php/user/newaccount" class="link">Create New Account?</a>
 -->				<!-- <br /> -->
				<!-- <a href="#">Term of Service</a> - <a href="#">Privacy Policy</a> -->
<!-- 			</div>
 -->		</div><!-- login-content -->
	</div><!-- login-form -->	
</div><!-- login-container -->
<!-- <p class="text-mute small">Page rendered in {elapsed_time} seconds</p> -->
