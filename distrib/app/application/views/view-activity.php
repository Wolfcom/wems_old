<div class="col-sm-10 main-content body-wrapper tab">
	<div class="container-fluid">
		<div class="row">
			<?php require('template/navigation.php'); ?>
			<?php require('template/pagination_head.php'); ?>

<div  class="admin-content-page admin-groups-list ng-scope">
	<?php if ($has_filter) { ?>
		<div class="activity-drop-container">
			<select name="filter-field" class="activity-select">
				<option value="time">Time</option>
				<option value="action">Action</option>
				<option value="record">Record ID</option>
				<option value="user">User</option>
				<option value="ip">IP Address</option>
			</select>
			<input type="text" name="filter-value" class="activity-input">
			<button type="button" class="btn btn-primary btn-activity" id="ActivityFilter">Filter</button>
		</div>
	<?php } ?>

	<div class="table-list table-bg table-up-gap">
		<div class="table-responsive">
			<table class="table  table-user-groupe">
				<thead>
					<tr>
						<th>Time</th>
						<th>Action</th>
						<th>Record ID</th>
						<th>User</th>
						<th>IP Address</th>
					</tr>
				</thead>

				<tbody>
					<?php foreach ($records as $record) { ?>
						<?php if ($record->source_id && in_array($record->action_id, array('create_picture', 'crop_video'))) { ?>
							<tr>
								<td><?= $record->created ?></td>

								<td>
								<?php
									switch ($record->action_id) {
										case 'create_picture':
											echo 'Snapshot from video';
											break;

										case 'crop_video':
											echo 'Cropped video';
											break;
									} ?>
								</td>

								<td><?= $record->source_id ?></td>
								<td><?= $record->first_name ?> <?= $record->last_name ?> (<?= $record->user_login ?>)</td>
								<td><?= $record->user_ip ?></td>
							</tr>
						<?php } ?>

						<tr>
							<td><span class="datetime"><?= $record->created ?></span></td>
							<td><?= $record->action ?></td>
							<td>
							<?php
								if (empty($record->entity_label)) {
									echo $record->entity_id;
								}
								else {
									echo $record->entity_label;
								}
							?>
							</td>
							<td><?= $record->first_name ?> <?= $record->last_name ?> (<?= $record->user_login ?>)</td>
							<td><?= $record->user_ip ?></td>
						</tr>
					<?php } ?>
				</tbody>
			</table>
		</div>
	</div>
</div>

	<?php require('template/pagination_foot.php'); ?>

		</div>
	</div>
</div>
