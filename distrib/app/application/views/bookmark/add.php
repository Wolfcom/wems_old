<input type="hidden" id="record-id" value="<?= $id ?> " />

<div id="bookmarkBlock">
	<div id="bookmark-thumbnail" />

	<form id="bookmarkForm">
		<div class="form-group">
			<label for="bookmark-description">Description:</label>
			<textarea class="form-control" rows="5" id="bookmark-description" required></textarea>
		</div>
	</form>
</div>
