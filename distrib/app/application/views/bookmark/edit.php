<input type="hidden" id="record-id" value="<?= $bookmark->id ?> " />

<div id="bookmarkBlock">
	<img src="<?= $thumbnail?>" />

	<form id="bookmarkForm">
		<div class="form-group">
			<label for="bookmark-description">Description:</label>
			<textarea class="form-control" rows="5" id="bookmark-description" required><?= $bookmark->description ?></textarea>
		</div>
	</form>
</div>
