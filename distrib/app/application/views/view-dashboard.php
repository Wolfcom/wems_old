<div class="col-sm-10 main-content body-wrapper tab">
	<div class="container-fluid">
		<div class="row">
			<?php require('template/navigation.php'); ?>
			<div class="col-xs-12">
				<?php if (!empty($current_permissions->module_cases)) { ?>
					<div class="col-sm-4" onclick = "javascript:location.href = '/index.php/cases';">
						<div class="dash-box">
							<h2><i class="fa fa-suitcase"></i>My Cases</h2>						
						</div>
					</div>
				<?php } ?>

				<?php if (!empty($current_permissions->module_favorites)) { ?>
					<div class="col-sm-4"onclick = "javascript:location.href = '/index.php/favourites';">
						<div class="dash-box">
							<h2><i class="fa fa-heart"></i>Favorites</h2>						
						</div>
					</div>
				<?php } ?>

				<?php if (!empty($current_permissions->module_bookmarks)) { ?>
					<div class="col-sm-4"onclick = "javascript:location.href = '/index.php/bookmarks';">
						<div class="dash-box">
							<h2><i class="fa fa-bookmark"></i>My bookmarks</h2>						
						</div>
					</div>
				<?php } ?>
			</div>

			<div class="col-xs-12">
				<?php if (!empty($current_permissions->module_images)) { ?>
					<div class="col-sm-4"onclick = "javascript:location.href = '/index.php/picture/list_pictures';">
						<div class="dash-box">
							<h2><i class="fa fa-picture-o"></i>My photos</h2>					
						</div>
					</div>
				<?php } ?>

				<?php if (!empty($current_permissions->module_videos)) { ?>
					<div class="col-sm-4"onclick = "javascript:location.href = '/index.php/video/list_videos';">
						<div class="dash-box">
							<h2><i class="fa fa-film"></i>My videos</h2>					
						</div>
					</div>
				<?php } ?>

				<?php if (!empty($current_permissions->module_files)) { ?>
					<div class="col-sm-4"onclick = "javascript:location.href = '/index.php/files';">
						<div class="dash-box">
							<h2><i class="fa fa-files-o"></i>All files</h2>					
						</div>
					</div>
				<?php } ?>
			</div>
			<!--<div class="col-sm-3 dash-box"onclick = "javascript:location.href = '/index.php/app/mycase';">
				<h2><i class="fa fa-folder-open-o"></i>Filer purging Within 30 Days</h2>
			</div>-->
		</div>
	</div>
</div>

<!-- <div class="content-area-dash">
    <div class="row">
        <div class="dash-box">
            <a href="/index.php/app/mycase"><i class="fa fa-suitcase"></i>My Cases</a>
        </div>
        <div class="dash-box">
            <a href="/index.php/favourites"><i class="fa fa-heart"></i>My Favourites</a>
        </div>
        <div class="dash-box">
            <a href="/index.php/bookmarks"><i class="fa fa-bookmark"></i>My bookmarks</a>
        </div>
        <div class="dash-box">
            <a href="/index.php/images"><i class="fa fa-picture-o"></i>My images</a>
        </div>
        <div class="dash-box">
            <a href="/index.php/videos"><i class="fa fa-film"></i>My Videos</a>
        </div>
        <div class="dash-box">
            <a href="/index.php/files"><i class="fa fa-files-o"></i>All files</a>
        </div>
    </div>
</div> -->
