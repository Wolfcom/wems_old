<div class="col-sm-10 main-content body-wrapper tab">
	<div class="container-fluid">
		<div class="row">
			<?php require('template/navigation.php'); ?>

			<div class="picklist">
				<span>Classification Values</span>
				<input id="classification-list" data-tags="<?= htmlentities(json_encode($classifications)) ?>"/>
			</div>
			
			<button class="btn btn-primary btn-popup btn-detail" id="SaveClassification">Save</button>
		</div>
	</div>
</div>
