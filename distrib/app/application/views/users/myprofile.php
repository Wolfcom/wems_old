<?php require(dirname(__FILE__) . '/../template/navigation.php'); ?>

<div class="col-sm-10 main-content body-wrapper tab">
	<div class="profile-container">
		<div class="profile-pic">
			<div class="profile-pic-inner">
				<?php if (empty($user_photo)) { ?>
					<img src="<?php echo site_url('assets'); ?>/images/non-profile.jpg" alt="User Profile Image" class="img-responsive user-prof-img" />
				<?php } else { ?>
					<img src="<?= $user_photo ?>" alt="User Profile Image" class="img-responsive user-prof-img"/>
				<?php } ?>

				<!--<div class="edit">
					<a href="#" data-toggle="modal" data-target="#change-picture-modal">Edit picture<i class="fa fa-pencil fa-lg"></i></a>
				</div>-->
			</div>
			<div class="profile-info-inner">
				<p class="first-part">Username (Badge ID) : <?= $login ?></p>
				<p class="first-part">Camera ID : <?= $device_id ? $device_id : '-' ?></p>
				<p class="first-part">Camera Type : <?= $device_type ? $device_type : '-' ?></p>
				<p class="first-part">First Name : <?= $first_name ?></p>
				<p class="first-part">Last Name : <?= $last_name ?></p>
				<p class="first-part">Email : <?= $email ? $email : '-' ?></p>
				<p class="first-part">Group : <?= empty($group->name) ? '-' : $group->name ?></p>
				<p class="first-part">Department : <?= $department ? $department : '-' ?></p>
				<p class="first-part">Position : <?= $position ? $position : '-' ?></p>
			</div>
		</div>
			<div class="col-xs-12">
				<?php if (!empty($current_permissions->password_profile)) { ?>
					<a href="#" class="btn-change-password" data-toggle="modal" data-target="#change-password-modal"><i class="fa fa-lock"></i> Change Password</a>
				<?php } ?>
			</div>	
		<!-- <div class="info-container">		
		</div> -->
	</div>
</div>

<!-- MODAL -->

<div id="change-password-modal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title"><i class="fa fa-lock"></i> Change Password</h4>
        <span class="modal-close-icon icon-icn_close_x_01  secondaryColorTextActive"></span>
      </div>
      <div class="modal-body">

      <form id="changePasswordForm" name="changePasswordForm">
	      <div class="form-group">
	          <label>New Password</label>
	          <input type="password" class="form-control" name="newPassword" id="newPassword" placeholder="Enter new password..." minlength = "5" required/>
	      </div>

	      <div class="form-group">
	          <label>Confirm New Password</label>
	          <input type="password" class="form-control" name="confirmPassword" id="confirmPassword" placeholder="Confirm password..." required/>
	      </div>
	  </div>

      <div class="modal-footer">
      	<button type="button" class="btn btn-default" onclick="update_password()">Save</button>
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>

      </form>

    </div>

  </div>
</div>

<!-- MODAL -->





<!-- MODAL -->

<div id="update-profile-modal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title"><i class="fa fa-user"></i> Update Profile</h4>
      </div>
      <div class="modal-body">

      <form id="updateProfile" name="updateProfile">
        
	      <div class="form-group">
	          <label>First Name</label>
	          <input type="text" class="form-control" name="first_name" value="<?= $first_name ?>" required />
	      </div>

	      <div class="form-group">
	          <label>Last name</label>
	          <input type="text" class="form-control" name="last_name"  value="<?= $last_name ?>" />
	      </div>

	      <div class="form-group">
	          <label>Position</label>
	          <input type="text" class="form-control" name="position" value="<?= $position ?>" />
	      </div>

	      <div class="form-group">
	          <label>Department</label>
	          <input type="text" class="form-control" name="department" value="<?= $department ?>" />
	      </div>

	      <!--<div class="form-group">

	          <label>Badge ID</label>
	          <input type="text" class="form-control" name = "badgeID" id="badgeID" placeholder="Please enter something..." value = "<?php echo $user_profile['badgeID']; ?>" required/>

	      </div>

	      <div class="form-group">

	          <label>Camera ID</label>
	          <input type="text" class="form-control" name = "cameraID" id="cameraID" placeholder="Please enter something..." value = "<?php echo $user_profile['cameraID']; ?>" required/>

	      </div>
			-->

	  </div>
      <div class="modal-footer">
      	<button type="button" class="btn btn-default">Save</button>
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>

      </form>

    </div>

  </div>
</div>

<!-- MODAL -->



<!-- MODAL -->

<div id="change-picture-modal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title"><i class="fa fa-picture-o"></i> Change Profile Picture</h4>
      </div>
      <div class="modal-body">

      <form id = "changePictureForm" name = "changePictureForm" enctype="multipart/form-data">
        
	      <div class="form-group">

	          <label>Select Your Picture</label>
	          <input type="file" class="form-control" name = "picture_file" id="picture_file" accept="image/*" onchange="javascript:changePicture('preview');" />
	          <p class = "help-block"><i>Remark : File size not over 1 MB</i></p>
	      </div>

	      <div id = "previewPicture" align = "center"></div>

	  </div>
      <div class="modal-footer">
      	<button type="button" class="btn btn-default">Save</button>
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>

      </form>

    </div>

  </div>
</div>

<!-- MODAL -->

