<div class="col-sm-10 main-content body-wrapper tab">
    <div class="container-fluid">
        <div class="row">            
            <?php require(dirname(__FILE__) . '/../template/navigation.php'); ?>
            <div id="privileges-page">


                <div  id="admin-conf-global" class="admin-content-page configuration-global secondaryColorScrollbar ng-scope">
                <br>
                <div class="feature-title">Group Permissions : <?= $group->name?>
                </div>
                    <br>
                    <br>
                    <ul class="table-priv">
                            <li>
                                    <h3 class="priv-head">Module and Pages</h3>
                                    <div class="conf-global-table crew_privileges">
                                                    <div class="global-table-title global-table-title-text">Module and Pages permitted to access</div>
                                                   <div class="global-table-content ng-scope" >
                                                                <ul>
                                                                    <li  class="ng-scope">
                                                                        <input type="checkbox" class="priv-chk" value="module_cases" <?= checked(!empty($permissions->module_cases)) ?> />
                                                                        <span class="capability-name ng-binding">My Cases</span>
                                                                    </li>
                                                                    <li  class="ng-scope">
                                                                        <input type="checkbox" class="priv-chk" value="module_favorites" <?= checked(!empty($permissions->module_favorites)) ?>/>
                                                                        <span class="capability-name ng-binding">My Favorites</span>
                                                                    </li>
                                                                    <li  class="ng-scope">
                                                                        <input type="checkbox" class="priv-chk" value="module_bookmarks" <?= checked(!empty($permissions->module_bookmarks)) ?>/>
                                                                        <span class="capability-name ng-binding">My Bookmarks</span>
                                                                    </li>
                                                                    <li  class="ng-scope">
                                                                        <input type="checkbox" class="priv-chk" value="module_files" <?= checked(!empty($permissions->module_files)) ?>/>
                                                                        <span class="capability-name ng-binding">All Files</span>
                                                                    </li>
                                                                    <li  class="ng-scope">
                                                                        <input type="checkbox" class="priv-chk" value="module_groups" <?= checked(!empty($permissions->module_groups)) ?> />
                                                                        <span class="capability-name ng-binding">Groups</span>
                                                                    </li>
                													<li  class="ng-scope">
                                                                        <input type="checkbox" class="priv-chk" value="module_settings" <?= checked(!empty($permissions->module_settings)) ?> />
                                                                        <span class="capability-name ng-binding">Settings</span>
                                                                    </li>
                                                                </ul>
                                                            </div>

                                                            <div class="global-table-content ng-scope" >
                                                                <ul>
                                                                    <li  class="ng-scope">
                                                                        <input type="checkbox" class="priv-chk" value="module_images" <?= checked(!empty($permissions->module_images)) ?> />
                                                                        <span class="capability-name ng-binding">My Images</span>
                                                                    </li>
                                                                    <li  class="ng-scope">
                                                                        <input type="checkbox" class="priv-chk" value="module_videos" <?= checked(!empty($permissions->module_videos)) ?>/>
                                                                        <span class="capability-name ng-binding">My Videos</span>
                                                                    </li>
                                                                    <li  class="ng-scope">
                                                                        <input type="checkbox" class="priv-chk" value="module_users" <?= checked(!empty($permissions->module_users)) ?> />
                                                                        <span class="capability-name ng-binding">Users</span>
                                                                    </li>
                                                                    <li  class="ng-scope">
                                                                        <input type="checkbox" class="priv-chk" value="module_profile" <?= checked(!empty($permissions->module_profile)) ?> />
                                                                        <span class="capability-name ng-binding">Profile</span>
                                                                    </li>
                                                                    <li  class="ng-scope">
                                                                        <input type="checkbox" class="priv-chk" value="module_cameras" <?= checked(!empty($permissions->module_cameras)) ?> />
                                                                        <span class="capability-name ng-binding">Cameras</span>
                                                                    </li>
                                                                    <li  class="ng-scope">
                                                                        <input type="checkbox" class="priv-chk" value="module_cameras" <?= checked(!empty($permissions->module_cameras)) ?> />
                                                                        <span class="capability-name ng-binding">Asset Archive</span>
                                                                    </li>
                                                                </ul>
                                                            </div>
                                    </div>
                            </li>
                    </ul>

                    <ul class="table-priv">
                            <li>
                                    <h3 class="priv-head">Profile Managment</h3>
                                    <div class="conf-global-table crew_privileges">
                                                    <div class="global-table-title global-table-title-text">Action permitted in to perform</div>
                                                   <div class="global-table-content ng-scope" >
                                                                <ul>
                                                                    <li  class="ng-scope">
                                                                        <input type="checkbox" class="priv-chk" value="password_profile" <?= checked(!empty($permissions->password_profile)) ?> />
                                                                        <span class="capability-name ng-binding">Change Password</span>
                                                                    </li>
                                                                </ul>
                                                            </div>
                                    </div>
                            </li>
                    </ul>

                    <ul class="table-priv">
                            <li>
                                    <h3 class="priv-head">Evidence Managment</h3>
                                    <div class="conf-global-table crew_privileges">
										<div class="global-table-title global-table-title-text">
											<p class="priv-title-box">Action permitted in to perform</p>
											<select id="permission_level">
												<option value="0" <?= selected(!empty($permissions->global_permissions)) ?>>Private</option>
												<option value="1" <?= selected(!empty($permissions->global_permissions)) ?>>Global</option>
											</select>
										</div>

										<div class="global-table-content ng-scope" >
											<ul>
												<li class="ng-scope">
													<input type="checkbox" class="priv-chk" value="upload_file" <?= checked(!empty($permissions->upload_file)) ?> />
													<span class="capability-name ng-binding">Upload External Files</span>
												</li>

												<li class="ng-scope">
													<input type="checkbox" class="priv-chk" value="burn_disc" <?= checked(!empty($permissions->burn_disc)) ?> />
													<span class="capability-name ng-binding">Burn to Disc</span>
												</li>

												<li class="ng-scope">
													<input type="checkbox" class="priv-chk" value="download_video" <?= checked(!empty($permissions->download_video)) ?> />
													<span class="capability-name ng-binding">Download Video File</span>
												</li>

												<li class="ng-scope">
													<input type="checkbox" class="priv-chk" value="download_audio" <?= checked(!empty($permissions->download_audio)) ?> />
													<span class="capability-name ng-binding">Download Audio File</span>
												</li>

												<li class="ng-scope">
													<input type="checkbox" class="priv-chk" value="download_picture" <?= checked(!empty($permissions->download_picture)) ?> />
													<span class="capability-name ng-binding">Download Picture File</span>
												</li>

												<li class="ng-scope">
													<input type="checkbox" class="priv-chk" value="download_document" <?= checked(!empty($permissions->download_document)) ?> />
													<span class="capability-name ng-binding">Download Document File</span>
												</li>

												<li class="ng-scope">
													<input type="checkbox" class="priv-chk" value="download_report" <?= checked(!empty($permissions->download_report)) ?> />
													<span class="capability-name ng-binding">Download Audit Report</span>
												</li>

												<li class="ng-scope">
													<input type="checkbox" class="priv-chk" value="update_video" <?= checked(!empty($permissions->update_video)) ?> />
													<span class="capability-name ng-binding">Edit Video Metadata</span>
												</li>

												<li class="ng-scope">
													<input type="checkbox" class="priv-chk" value="update_audio" <?= checked(!empty($permissions->update_audio)) ?> />
													<span class="capability-name ng-binding">Edit Audio Metadata</span>
												</li>

												<li class="ng-scope">
													<input type="checkbox" class="priv-chk" value="update_picture" <?= checked(!empty($permissions->update_picture)) ?> />
													<span class="capability-name ng-binding">Edit Picture Metadata</span>
												</li>

												<li class="ng-scope">
													<input type="checkbox" class="priv-chk" value="update_document" <?= checked(!empty($permissions->update_document)) ?> />
													<span class="capability-name ng-binding">Edit Document Metadata</span>
												</li>

												<li class="ng-scope">
													<input type="checkbox" class="priv-chk" value="create_bookmark" <?= checked(!empty($permissions->create_bookmark)) ?> />
													<span class="capability-name ng-binding">Add Bookmark</span>
												</li>

												<!--<li class="ng-scope">
													<input type="checkbox" class="priv-chk" value="read_bookmark" <?= checked(!empty($permissions->read_bookmark)) ?> />
													<span class="capability-name ng-binding">View Bookmark</span>
												</li>-->

												<li class="ng-scope">
													<input type="checkbox" class="priv-chk" value="snapshot_video" <?= checked(!empty($permissions->snapshot_video)) ?> />
													<span class="capability-name ng-binding">Video Snapshot</span>
												</li>

												<li class="ng-scope">
													<input type="checkbox" class="priv-chk" value="crop_video" <?= checked(!empty($permissions->crop_video)) ?> />
													<span class="capability-name ng-binding">Crop Video</span>
												</li>
											</ul>
										</div>

										<div class="global-table-content ng-scope" >
											<ul>
												<li class="ng-scope">
													<input type="checkbox" class="priv-chk" value="read_video" <?= checked(!empty($permissions->read_video)) ?> />
													<span class="capability-name ng-binding">View Video File</span>
												</li>

												<li class="ng-scope">
													<input type="checkbox" class="priv-chk" value="read_audio" <?= checked(!empty($permissions->read_audio)) ?> />
													<span class="capability-name ng-binding">Listen Audio File</span>
												</li>

												<li class="ng-scope">
													<input type="checkbox" class="priv-chk" value="read_picture" <?= checked(!empty($permissions->read_picture)) ?> />
													<span class="capability-name ng-binding">View Picture File</span>
												</li>

												<li class="ng-scope">
													<input type="checkbox" class="priv-chk" value="read_document" <?= checked(!empty($permissions->read_document)) ?> />
													<span class="capability-name ng-binding">View Document File</span>
												</li>

												<li class="ng-scope">
													<input type="checkbox" class="priv-chk" value="read_report" <?= checked(!empty($permissions->read_report)) ?> />
													<span class="capability-name ng-binding">Preview Audit Report</span>
												</li>

												<li class="ng-scope">
													<input type="checkbox" class="priv-chk" value="delete_video" <?= checked(!empty($permissions->delete_video)) ?> />
													<span class="capability-name ng-binding">Delete Video</span>
												</li>

												<li class="ng-scope">
													<input type="checkbox" class="priv-chk" value="delete_audio" <?= checked(!empty($permissions->delete_audio)) ?> />
													<span class="capability-name ng-binding">Delete Audio</span>
												</li>

												<li class="ng-scope">
													<input type="checkbox" class="priv-chk" value="delete_picture" <?= checked(!empty($permissions->delete_picture)) ?> />
													<span class="capability-name ng-binding">Delete Picture</span>
												</li>

												<li class="ng-scope">
													<input type="checkbox" class="priv-chk" value="delete_document" <?= checked(!empty($permissions->delete_document)) ?> />
													<span class="capability-name ng-binding">Delete Document</span>
												</li>

												<li class="ng-scope">
													<input type="checkbox" class="priv-chk" value="update_bookmark" <?= checked(!empty($permissions->update_bookmark)) ?> />
													<span class="capability-name ng-binding">Update Bookmark</span>
												</li>

												<li class="ng-scope">
													<input type="checkbox" class="priv-chk" value="delete_bookmark" <?= checked(!empty($permissions->delete_bookmark)) ?> />
													<span class="capability-name ng-binding">Delete Bookmark</span>
												</li>

												<li class="ng-scope">
													<input type="checkbox" class="priv-chk" value="map_video" <?= checked(!empty($permissions->map_video)) ?> />
													<span class="capability-name ng-binding">Open Map</span>
												</li>
											</ul>
										</div>
                                    </div>
                            </li>
                    </ul>

                    <ul class="table-priv">
                            <li>
                                    <h3 class="priv-head">Search</h3>
                                    <div class="conf-global-table crew_privileges">
                                                    <div class="global-table-title global-table-title-text">Action permitted in to perform</div>
                                                   <div class="global-table-content ng-scope" >
                                                                <ul>
                                                                    <li  class="ng-scope">
                                                                        <input type="checkbox" class="priv-chk" value="basic_search" <?= checked(!empty($permissions->basic_search)) ?>/>
                                                                        <span class="capability-name ng-binding">Basic Search</span>
                                                                    </li>
                                                                    <li  class="ng-scope">
                                                                        <input type="checkbox" class="priv-chk" value="user_search" <?= checked(!empty($permissions->user_search)) ?>/>
                                                                        <span class="capability-name ng-binding">Search By User</span>
                                                                    </li>
                                                                </ul>
                                                            </div>
                                                            <div class="global-table-content ng-scope" >
                                                                <ul>
                                                                    <li  class="ng-scope">
                                                                        <input type="checkbox" class="priv-chk" value="advanced_search" <?= checked(!empty($permissions->advanced_search)) ?> />
                                                                        <span class="capability-name ng-binding">Advanced Search</span>
                                                                    </li>
                                                                </ul>
                                                            </div>
                                    </div>
                            </li>
                    </ul>
                    <ul class="table-priv">
                            <li>
                                    <h3 class="priv-head">Group Managment</h3>
                                    <div class="conf-global-table crew_privileges">
                                                    <div class="global-table-title global-table-title-text">Action permitted in to perform</div>
                                                   <div class="global-table-content ng-scope" >
                                                                <ul>
                                                                    <li  class="ng-scope">
                                                                        <input type="checkbox" class="priv-chk" value="create_group" <?= checked(!empty($permissions->create_group)) ?>/>
                                                                        <span class="capability-name ng-binding">Add New Group</span>
                                                                    </li>
                                                                    
																	<li  class="ng-scope">
                                                                        <input type="checkbox" class="priv-chk" value="permissions_group" <?= checked(!empty($permissions->permissions_group)) ?>/>
                                                                        <span class="capability-name ng-binding">Assign Permissions</span>
                                                                    </li>
                                                                </ul>
                                                   </div>

                                                   <div class="global-table-content ng-scope" >
                                                                <ul>																	
																	<li  class="ng-scope">
                                                                        <input type="checkbox" class="priv-chk" value="delete_group" <?= checked(!empty($permissions->delete_group)) ?>/>
                                                                        <span class="capability-name ng-binding">Delete Group</span>
                                                                    </li>

                                                                    <li  class="ng-scope">
                                                                        <input type="checkbox" class="priv-chk" value="update_group" <?= checked(!empty($permissions->update_group)) ?>/>
                                                                        <span class="capability-name ng-binding">Edit Group</span>
                                                                    </li>
                                                                </ul>
                                                   </div>
                                    </div>
                            </li>
                    </ul>
                    <ul class="table-priv">
                            <li>
                                    <h3 class="priv-head">User Managment</h3>
                                    <div class="conf-global-table crew_privileges">
                                                    <div class="global-table-title global-table-title-text">Action permitted in to perform</div>
                                                   <div class="global-table-content ng-scope" >
                                                                <ul>
                                                                    <li  class="ng-scope">
                                                                        <input type="checkbox" class="priv-chk" value="create_user" <?= checked(!empty($permissions->create_user)) ?>/>
                                                                        <span class="capability-name ng-binding">Add New User</span>
                                                                    </li>

                                                                    <li  class="ng-scope">
                                                                        <input type="checkbox" class="priv-chk" value="delete_user" <?= checked(!empty($permissions->delete_user)) ?>/>
                                                                        <span class="capability-name ng-binding">Delete User</span>
                                                                    </li>

                                                                    <li  class="ng-scope">
                                                                        <input type="checkbox" class="priv-chk" value="activate_user" <?= checked(!empty($permissions->activate_user)) ?> />
                                                                        <span class="capability-name ng-binding">Activate User</span>
                                                                    </li>
                                                                </ul>
                                                            </div>
                                            	   <div class="global-table-content ng-scope" >
                                                                <ul>
																	<li  class="ng-scope">
                                                                        <input type="checkbox" class="priv-chk" value="report_user" <?= checked(!empty($permissions->report_user)) ?> />
                                                                        <span class="capability-name ng-binding">View User Activity Report</span>
                                                                    </li>

                                                                    <li  class="ng-scope">
                                                                        <input type="checkbox" class="priv-chk" value="update_user" <?= checked(!empty($permissions->update_user)) ?>/>
                                                                        <span class="capability-name ng-binding">Edit User</span>
                                                                    </li>
                                                                </ul>
                                                            </div>
                                    </div>
                            </li>
                    </ul>

                    
                    <div class="button-area">
                   
                        <button type="button" class="btn-parent btn-default" onclick = "javascript:update_permissions('<?= $group->id ?>');">Save</button>
                    </div>
                    
                </div>
            </div>
        </div>
    </div>
</div>

</div> <!-- end privige container -->
