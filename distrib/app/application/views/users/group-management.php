<div class="col-sm-10 main-content body-wrapper tab">
	<div class="container-fluid">
		<div class="row">
			<?php require(dirname(__FILE__) . '/../template/navigation.php'); ?>

			<div  class="admin-content-page admin-groups-list ng-scope">
			    <div class="feature-area">
					<?php if (!empty($current_permissions->create_group)) { ?>
						<div class="add-new-button">
							<a title="Add" href="javascript:void(0)" id="open"><i class="fa fa-plus"></i></a>
						</div>
					<?php } ?>

					<?php if (!empty(!empty($current_permissions->update_group))) { ?>
						<div class="add-new-button">
							<a title="Edit" href="javascript:void(0)" onclick="$('#dialog-edit-' + get_selected_record()).dialog('open')"><i class="fa fa-pencil"></i></a>
						</div>
					<?php } ?>

					<?php if (!empty(!empty($current_permissions->delete_group))) { ?>
						<div class="add-new-button">
							<a title="Delete" href="javascript:void(0)" onclick="delete_group(get_selected_record())"><i class="fa fa-trash"></i></a>
						</div>
					<?php } ?>

					<div class="add-new-button"><a title="More" href="javascript:void(0)" id="more-info"><i class="fa fa-ellipsis-h"></i></a>
						<ul class="user-more-sub">
							<?php if (!empty($current_permissions->permissions_group) && !empty($current_features->security)) { ?>
								<li>
									<a href="javascript:void(0)" onclick="permission_editor(get_selected_record())">Assign Permissions</a>
								</li>
							<?php } ?>
						</ul>
					</div>
				</div>

				<div class="table-list table-bg">
					<div class="table-responsive">
						<table class="table  table-user-groupe">
							<thead>
								<tr>
									<th>
										<input type="checkbox" class="priv-chk" />
									</th>

									<th>Group Name</th>
									<th>Members</th>
									<th>Creation Date</th>
								</tr>
							</thead>

							<tbody>
								<?php
									foreach ($group_list as $group) {
										$members = array(); ?>
										<tr>
											<td>
												<input type="checkbox" class="priv-chk record" value="<?= $group->id ?>" />
											</td>

											<td><?= $group->name ?></td>
											<td>
												<?php if (empty($group->users)) { ?>
													No Members
												<?php } else {
														foreach ($group->users as $user) {
															$members []= $user->id;	?>
															<?= $user->first_name ?> <?= $user->last_name ?><?= $user !== end($group->users) ? ',' : '' ?>
												<?php
														}
													} ?>
											</td>
											<td>
												<span class="datetime"><?= $group->created ?></span>
												
												<!-- Edit group -->
												<div id="dialog-edit-<?= $group->id ?>" class="dialog-edit modal fade admin-user-dialog in edit_group" style="z-index: 1050;">
													<div class="modal-header ng-scope">
														<div>Edit Group</div>
														<span class="modal-close-icon  modal-close-icon icon-icn_close_x_01 secondaryColorTextActive" ></span>
													</div>

													<div class="modal-body ng-scope">
														<div  class="modal-inner">
															<form class="edit-add-form ng-pristine ng-invalid ng-invalid-required">
																<section>
																	<div class="label-field primaryColorWhite">Group Name</div>
																	<input value="<?= $group->name ?>" name="name" class="input-field" type="text"  maxlength="80" required>
																</section>

																<section>
																	<div class="label-field primaryColorWhite">Members</div>
																	<select class="multiple-user-select" name="members[]" multiple="multiple">
																		<?php foreach (array_merge($user_list, $group->users) as $user) {
																				$is_selected = '';
																				foreach ($group->users as $group_user) {
																					if ($group_user->id == $user->id) {
																						$is_selected = 'selected';
																						break;
																					}
																				}
																		?>
																			<option value="<?= $user->id ?>" <?= $is_selected ?>><?= $user->last_name ?> <?= $user->first_name ?> (<?= $user->login ?>)</option>
																		<?php } ?>
																	</select>
																</section>
															</form>
														</div>

														<div class="modal-footer ng-scope">
															<button class="btn-parent btn-default" onclick="javascript:update_group('<?= $group->id ?>');">Update</button>
															<button class="btn-parent btn-optional btn-close-edit">Cancel</button>
														</div>
													</div>
												</div>
											</td>
										</tr>
								<?php
									} ?>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>

<!-- Add new group -->
<div id="dialog" class="dialog-edit modal fade admin-user-dialog in edit_group" style="z-index: 1050;">
	<div class="modal-header ng-scope">
		<div>Add Group</div>
		<span class="modal-close-icon icon-icn_close_x_01  secondaryColorTextActive" ></span>
	</div>

	<div class="modal-body ng-scope">
		<div  class="">
			<form id="createGroup" class="ng-pristine ng-invalid ng-invalid-required">
				<section>
					<div class="label-field primaryColorWhite">Group Name</div>
					<input name="name" class="input-field" type="text"  maxlength="80" required>
				</section>

				<section>
					<div class="label-field primaryColorWhite">Members</div>
					<select class="multiple-user-select" name="members[]" multiple="multiple">
					<?php foreach ($user_list as $user) { ?>
						<option value="<?= $user->id ?>"><?= $user->last_name ?> <?= $user->first_name ?> (<?= $user->login ?>)</option>
					<?php } ?>
					</select>
				</section>
			</form>
		</div>

		<div class="modal-footer ng-scope">
			<button class="btn-parent btn-default" onclick="javascript:create_group();">Update</button>
			<button class="btn-parent btn-optional btn-close-edit">Cancel</button>
		</div>
	</div>
</div>
</div>
