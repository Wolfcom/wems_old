<div class="col-sm-10 main-content body-wrapper tab">
	<div class="camera-setting-container">
		<h2>Device Setup</h2>
		<div class="col-sm-3">
			<h3>Initial Settings</h3>
			<label for="#" class="badge-label">badge id:</label>
			<input type="text" class="badge-input">
			<label for="#" class="badge-label">device id:</label>
			<input type="text" class="device-input"><br>
			<p class="first-set">Time zone:</p>
			<p class="second-set">-8</p><br>
			<p class="first-set">Time set:</p>
			<p class="second-set">11:11:11 PM</p><br>
			<p class="first-set">Date set:</p>
			<p class="second-set">11/01/2016</p><br>
				<a href="#" class="btn-change-password btn-popup">Training Videos</a>
				<a href="#" class="btn-change-password btn-popup">Troubleshoot/Help</a>
		</div>
		<div class="col-sm-3">
			<h3>Mode Select and Settings</h3>
			<label for="#" class="set-label">Password</label>
			<select name="#" id="#" class="set-select">
				<option value="1" selected></option>
				<option value="1">Test</option>
				<option value="1">Test</option>
			</select>
			<label for="#" class="set-label">Video Resolution</label>
			<select name="#" id="#" class="set-select">
				<option value="1" selected></option>
				<option value="1">Test</option>
				<option value="1">Test</option>
			</select>
			<label for="#" class="set-label">Compression Rate</label>
			<select name="#" id="#" class="set-select">
				<option value="1" selected></option>
				<option value="1">Test</option>
				<option value="1">Test</option>
			</select>
			<label for="#" class="set-label">Video Format</label>
			<select name="#" id="#" class="set-select">
				<option value="1" selected></option>
				<option value="1">Test</option>
				<option value="1">Test</option>
			</select>
			<label for="#" class="set-label">Audio Record</label>
			<select name="#" id="#" class="set-select">
				<option value="1" selected></option>
				<option value="1">Test</option>
				<option value="1">Test</option>
			</select>
			<label for="#" class="set-label">Recycle Record</label>
			<select name="#" id="#" class="set-select">
				<option value="1" selected></option>
				<option value="1">Test</option>
				<option value="1">Test</option>
			</select>
			<label for="#" class="set-label">Pre-record Audio</label>
			<select name="#" id="#" class="set-select">
				<option value="1" selected></option>
				<option value="1">Test</option>
				<option value="1">Test</option>
			</select>
			<label for="#" class="set-label">Loop Record</label>
			<select name="#" id="#" class="set-select">
				<option value="1" selected></option>
				<option value="1">Test</option>
				<option value="1">Test</option>
			</select>
			<label for="#" class="set-label">Photo Resolution</label>
			<select name="#" id="#" class="set-select">
				<option value="1" selected></option>
				<option value="1">Test</option>
				<option value="1">Test</option>
			</select>
			<label for="#" class="set-label">Public Awareness Indicator</label>
			<select name="#" id="#" class="set-select">
				<option value="1" selected></option>
				<option value="1">Test</option>
				<option value="1">Test</option>
			</select>
			<label for="#" class="set-label">Date & Time Stamp</label>
			<select name="#" id="#" class="set-select">
				<option value="1" selected></option>
				<option value="1">Test</option>
				<option value="1">Test</option>
			</select>
			<label for="#" class="set-label">GPS Stamp</label>
			<select name="#" id="#" class="set-select">
				<option value="1" selected></option>
				<option value="1">Test</option>
				<option value="1">Test</option>
			</select>
			<label for="#" class="set-label">One Touch Record</label>
			<select name="#" id="#" class="set-select">
				<option value="1" selected></option>
				<option value="1">Test</option>
				<option value="1">Test</option>
			</select>
		</div>
		<div class="col-sm-6">
			<label for="#" class="set-label language-label">Language</label>
			<select name="#" id="#" class="set-select language-select">
				<option value="1" selected></option>
				<option value="1">Test</option>
				<option value="1">Test</option>
			</select>
			<h3>Current Setting</h3>
			<h4>Connected to devive</h4>
			<div class="col-sm-6">
				<p class="first-set">Police ID is:</p>
				<p class="second-set">111111111111</p><br>
				<p class="first-set">Device ID is:</p>
				<p class="second-set">123456</p><br>
				<p class="first-set">Device Setup is:</p>
				<p class="second-set">9632587410</p><br>
				<p class="first-set">Mode Selected:</p>
				<p class="second-set">EasyMode</p><br>
				<p class="first-set">Password:</p>
				<p class="second-set">OFF</p><br>
				<p class="first-set">Video Resolution:</p>
				<p class="second-set">1280*720(30FPS)</p><br>
				<p class="first-set">Compression Rate:</p>
				<p class="second-set">NORMAL</p><br>
				<p class="first-set">Video Format:</p>
				<p class="second-set">MOV</p><br>
				<p class="first-set">Audio Record:</p>
				<p class="second-set">YES</p><br>
			</div>
			<div class="col-sm-6">
				<p class="first-set">Recycle Record:</p>
				<p class="second-set">30min</p><br>
				<p class="first-set">Pre-record Audio:</p>
				<p class="second-set">ON</p><br>
				<p class="first-set">loop Record:</p>
				<p class="second-set">NO</p><br>
				<p class="first-set">Photo Resolution:</p>
				<p class="second-set">5M</p><br>
				<p class="first-set">Public Awereness Indicator:</p>
				<p class="second-set">FLASH</p><br>
				<p class="first-set">Date & Time Stamp:</p>
				<p class="second-set">YES</p><br>
				<p class="first-set">GPS Stamp:</p>
				<p class="second-set">ON</p><br>
				<p class="first-set">One Touch Record:</p>
				<p class="second-set">Normal</p><br>
				<p class="first-set">Time Zone:</p>
				<p class="second-set">-8</p><br>				
			</div>
			<div class="reset-container">
				<label for="#" class="set-label rest-label">Reset to Factory Default</label>
				<select name="#" id="#" class="set-select rest-select">
					<option value="1" selected></option>
					<option value="1">Test</option>
					<option value="1">Test</option>
				</select>				
			</div>
			<div class="reset-container">
				<label for="#" class="set-label format-label">Format</label>
				<select name="#" id="#" class="set-select format-select">
					<option value="1" selected></option>
					<option value="1">Test</option>
					<option value="1">Test</option>
				</select>				
			</div>
			<div class="reset-container">
				<a href="#" class="btn-change-password btn-popup">Connect to Computer</a>
			</div>
			<div class="reset-container">
				<a href="#" class="btn-change-password btn-popup">Apply Setting</a>
			</div>
		</div>
	</div>
</div>

