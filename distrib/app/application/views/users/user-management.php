<div class="col-sm-10 main-content body-wrapper tab">
	<div class="container-fluid">
		<div class="row">
			<?php require(dirname(__FILE__) . '/../template/navigation.php'); ?>

			<div  class="admin-content-page admin-groups-list ng-scope">
				<div class="feature-area">
					<?php if (!empty($current_permissions->create_user)) { ?>
						<div class="add-new-button"><a title="Add" href="javascript:void(0)" id="open"><i class="fa fa-plus"></i></a></div>
					<?php } ?>

					<?php if (!empty($current_permissions->update_user)) { ?>
						<div class="add-new-button">
							<a title="Edit" href="javascript:void(0)" onclick="user_edit_dialog(get_selected_record())">
								<i class="fa fa-pencil"></i>
							</a>
						</div>
					<?php } ?>

					<?php if (!empty($current_permissions->delete_user)) { ?>
						<div class="add-new-button">
							<a title="Delete" href="javascript:void(0)" onclick="delete_profile(get_selected_record())"><i class="fa fa-trash"></i></a>
						</div>
					<?php } ?>

					<div class="add-new-button"><a title="More" href="javascript:void(0)" id="more-info"><i class="fa fa-ellipsis-h"></i></a>
						<ul class="user-more-sub">
							<?php if (!empty($current_permissions->activate_user)) { ?>
								<li>
									<a href="javascript:void(0)" onclick="activate_user(get_selected_record())">Activate User</a>
								</li>
							<?php } ?>

							<?php if (!empty($current_permissions->report_user) && !empty($current_features->audit)) { ?>
								<li>
									<a href="javascript:void(0)" onclick="display_user_report(get_selected_record())">Activity Report</a>
								</li>
							<?php } ?>
						</ul>
					</div>
				</div>

				<div class="table-list table-bg">
					<div class="table-responsive">
						<table class="table  table-user-groupe">
							<thead>
								<tr>
									<th>
										<input type="checkbox" class="priv-chk" />
									</th>

									<th>Photo</th>
									<th>Username</th>
									<th>First Name</th>
									<th>Last Name</th>
									<th>Email</th>
									<th>Group</th>
									<th>Position</th>
									<th>Department</th>
									<th>Creation Date </th>
									<th>Status</th>
								</tr>
							</thead>

							<tbody>
								<?php
									foreach ($user_list as $user) { ?>
										<tr>
											<td>
												<input type="checkbox" class="priv-chk record" value="<?= $user->id ?>" />
											</td>

											<td id="pict-row">
												<?php if (empty($user->photo)) { ?>
													<img src="<?= site_url('assets') ?>/images/non-profile.jpg" alt="Profile Image" class="img-responsive table-img">
												<?php } else { ?>
													<img src="<?= $user->photo ?>" alt="Profile Image" class="img-responsive table-img">
												<?php } ?>
											</td>

											<td><?= $user->login ?></td>
											<td><?= $user->first_name ?></td>
											<td><?= $user->last_name ?></td>
											<td><?= $user->email ?></td>
											<td><?= empty($user->group->name) ? '-' : $user->group->name ?></td>
											<td><?= $user->position ?> </td>
											<td><?= $user->department ?></td>
											<td><span class="datetime"><?= $user->created ?></span></td>

											<td>
												<?= $user->is_active ? 'Active' : 'Disabled' ?>
												<!-- Edit user profile -->
												<div id="dialog-edit-<?= $user->id ?>" class="dialog-edit modal fade admin-user-dialog in edit_user" style="z-index: 1050;">
													<div class="modal-header ng-scope">
														<div>Edit User</div>
														<span class="modal-close-icon icon-icn_close_x_01  secondaryColorTextActive" ></span>
													</div>
			                                                                                                                            <div class="modal-body ng-scope">

			                                                                                                                                            <div  class="modal-inner">
			                                                                                                                                                    <form id="createProfile" class="edit-add-form ng-pristine ng-invalid ng-invalid-required">
			                                                                                                                                                        <section>
			                                                                                                                                                            <div class="label-field primaryColorWhite">Username (Badge ID)</div>
			                                                                                                                                                            <input name="login" class="input-field" type="text"  maxlength="80" disabled value="<?= $user->login ?>">
			                                                                                                                                                        </section>

			                                                                                                                                                        <section>
			                                                                                                                                                            <div class="label-field primaryColorWhite">Camera ID</div>
			                                                                                                                                                            <input name="device_id" class="input-field" type="text"  maxlength="80" value="<?= $user->device_id ?>">
			                                                                                                                                                        </section>

			                                                                                                                                                        <section>
			                                                                                                                                                            <div class="label-field primaryColorWhite">Status</div>
			                                                                                                                                                            <input class="input-field" name="status" type="text" maxlength="80" disabled value="<?= $user->is_active ? 'Active' : 'Disabled' ?>">
			                                                                                                                                                        </section>

			                                                                                                                                                        <section>
			                                                                                                                                                            <div class="label-field primaryColorWhite">Camera Type</div>
			                                                                                                                                                            <select name="device_type">
																																											<option value="">None</option>
			                                                                                                                                                                <option value="VISION" <?= $user->device_type == 'VISION' ? 'selected' : ''?>>Wolfcom Vision</option>
			                                                                                                                                                                <option value="THIRD_EYE" <?= $user->device_type == 'THIRD_EYE' ? 'selected' : ''?>>Wolfcom 3rd Eye</option>
			                                                                                                                                                            </select>
			                                                                                                                                                        </section>

			                                                                                                                                                        <section>
			                                                                                                                                                            <div class="label-field primaryColorWhite">First Name</div>
			                                                                                                                                                            <input name="first_name" class="input-field" type="text"  maxlength="80" placeholder="Type the First name." required value="<?= $user->first_name ?>">
			                                                                                                                                                        </section>

			                                                                                                                                                        <section>
			                                                                                                                                                            <div class="label-field primaryColorWhite">Last Name</div>
			                                                                                                                                                            <input name="last_name" class="input-field" type="text"  maxlength="80" placeholder="Type the Last name." value="<?= $user->last_name ?>">
			                                                                                                                                                        </section>

			                                                                                                                                                        <section>
			                                                                                                                                                            <div class="label-field primaryColorWhite">Email</div>
			                                                                                                                                                            <input class="input-field" name="email" type="email"  maxlength="80" placeholder="Type the Email." value="<?= $user->email ?>">
			                                                                                                                                                        </section>

			                                                                                                                                                        <section>
			                                                                                                                                                            <div class="label-field primaryColorWhite">Group</div>
			                                                                                                                                                            <select name="group" <?= $user->id == User_model::ADMIN_USER ? 'disabled' : '' ?>>
			                                                                                                                                                                <option value="">None</option>
			                                                                                                                                                                <?php foreach ($group_list as $group) { ?>
			                                                                                                                                                                    <option value="<?= $group->id ?>" <?= !empty($user->group) && $group->id == $user->group->id ? 'selected' : '' ?>>
			                                                                                                                                                                        <?= $group->name ?>
			                                                                                                                                                                    </option>
			                                                                                                                                                                <?php } ?>
			                                                                                                                                                            </select>
			                                                                                                                                                        </section>

			                                                                                                                                                        <section>
			                                                                                                                                                            <div class="label-field primaryColorWhite">Department</div>
			                                                                                                                                                            <input class="input-field" name="department" type="text"  maxlength="80" value="<?= $user->department ?>">
			                                                                                                                                                        </section>

			                                                                                                                                                        <section>
			                                                                                                                                                            <div class="label-field primaryColorWhite">Position</div>
			                                                                                                                                                            <input class="input-field" name="position" type="text"  maxlength="80" value="<?= $user->position ?>">
			                                                                                                                                                        </section>   


			                                                                                                                                                        <section>
			                                                                                                                                                            <div class="label-field primaryColorWhite">Password</div>
			                                                                                                                                                            <input class="input-field" name="password" type="password"  maxlength="80">
			                                                                                                                                                        </section>

			                                                                                                                                                        <section>
			                                                                                                                                                            <div class="label-field primaryColorWhite">Confirm Password</div>
			                                                                                                                                                            <input class="input-field" name="confirm_password" type="password"  maxlength="80">
			                                                                                                                                                        </section>

			                                                                                                                                                        <section>
			                                                                                                                                                            <div class="label-field primaryColorWhite">Photo</div>
			                                                                                                                                                            <input class="input-field" name="photo" type="file" maxlength="80"  onchange="image_preview(this)">
			                                                                                                                                                            <div class="image-preview">
																																											<?php if (!empty($user->photo)) { ?>
																																												<img src="<?= $user->photo ?>" />
																																											<?php } ?>
			                                                                                                                                                            </div>
			                                                                                                                                                        </section>
			                                                                                                                                                    </form>
			                                                                                                                                            </div>

														<div class="modal-footer ng-scope">
															<button class="btn-parent btn-default" onclick="updateProfile('<?= $user->id ?>');">Save</button>
															<button class="btn-parent btn-optional btn-close-edit" >Cancel</button>
														</div>
													</div>
												</div>
											</td>
										</tr>
								<?php
									} ?>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<!-- Add user profile -->
<div id="dialog" class="modal fade admin-user-dialog in add_new_user" style="z-index: 1050;">
	<div class="modal-header ng-scope">
		<div >Add a User</div>
		<span class="modal-close-icon icon-icn_close_x_01  secondaryColorTextActive" ></span>
	</div>

    <div class="modal-body ng-scope">
        <div  class="modal-inner">
                <form id="createProfile" class="edit-add-form ng-pristine ng-invalid ng-invalid-required">
                    <section>
                        <div class="label-field primaryColorWhite">Username (Badge ID)</div>
                        <input name="login" class="input-field ng-pristine ng-invalid ng-invalid-required" type="text"  maxlength="80" required>
                    </section>

                    <section>
                        <div class="label-field primaryColorWhite">Camera ID</div>
                        <input name="device_id" class="input-field ng-pristine ng-invalid ng-invalid-required" type="text"  maxlength="80">
                    </section>

                    <section>
                        <div class="label-field primaryColorWhite">Status</div>
                        <input class="input-field" name="status" type="text"  maxlength="80" disabled value="Active">
                    </section>

                    <section>
                        <div class="label-field primaryColorWhite">Camera Type</div>
                        <select name="device_type">
							<option value="">None</option>
                            <option value="VISION">Wolfcom Vision</option>
                            <option value="THIRD_EYE">Wolfcom 3rd Eye</option>
                        </select>
                    </section>

                    <section>
                        <div class="label-field primaryColorWhite">First Name</div>
                        <input name="first_name" class="input-field ng-pristine ng-invalid ng-invalid-required" type="text"  maxlength="80" placeholder="Type the First name." required>
                    </section>

                    <section>
                        <div class="label-field primaryColorWhite">Last Name</div>
                        <input name="last_name" class="input-field ng-pristine ng-invalid ng-invalid-required" type="text"  maxlength="80" placeholder="Type the Last name.">
                    </section>

                    <section>
                        <div class="label-field primaryColorWhite">Email</div>
                        <input class="input-field ng-pristine ng-invalid ng-invalid-required ng-valid-email" name="email" type="email"  maxlength="80" placeholder="Type the Email.">
                    </section>

                    <section>
                        <div class="label-field primaryColorWhite">Group</div>
                        <select name="group">                    
                            <option value="">None</option>
                            <?php foreach ($group_list as $group) { ?>
                                <option value="<?= $group->id ?>">
                                    <?= $group->name ?>
                                </option>
                            <?php } ?>
                        </select>
                    </section>

                    <section>
                        <div class="label-field primaryColorWhite">Department</div>
                        <input class="input-field" name="department" type="text"  maxlength="80">
                    </section>

                    <section>
                        <div class="label-field primaryColorWhite">Position</div>
                        <input class="input-field" name="position" type="text"  maxlength="80">
                    </section>   


                    <section>
                        <div class="label-field primaryColorWhite">Password</div>
                        <input class="input-field" name="password" type="password"  maxlength="80" required>
                    </section>

                    <section>
                        <div class="label-field primaryColorWhite">Confirm Password</div>
                        <input class="input-field" name="confirm_password" type="password"  maxlength="80" required>
                    </section>

                    <section>
						<div class="label-field primaryColorWhite">Photo</div>
						<input class="input-field" name="photo" type="file" maxlength="80" onchange="image_preview(this)" />
						<div class="image-preview"></div>
					</section>
                </form>
        </div>

		<div class="modal-footer ng-scope">
			<button  class="btn-parent btn-default" onclick="createProfile()">Add</button>
			<button class="btn-parent btn-optional btn-close-add">Cancel</button>
		</div>
    </div>
</div>
