<input type="hidden" id="record-module" value="Video" />
<input type="hidden" id="record-id" value="<?= $id ?> " />
<input type="hidden" id="in-favs" value="<?= $video->in_favs ? 1 : 0 ?> " />
<input type="hidden" id="gps-track" value="<?= htmlentities($video->gps_track) ?> " />

<div class="col-sm-10 main-content body-wrapper tab video-preview">
	<div class="container-fluid">
		<?php require( dirname(__FILE__) . '/../template/navigation.php'); ?>
	</div>

	<div class="video-details-container">
		<div class="head-container">
			<h3><?= empty($video->title) ? 'No Title' : $video->title ?></h3>
			<span class="modal-close-icon icon-icn_close_x_01 secondaryColorTextActive" aria-hidden="true" onclick="location.href = '/index.php/files'"></span>
		</div>

		<div class="video-details-inner">
			<input type="hidden" id="record-module" value="Video" />
			<input type="hidden" id="record-id" value="<?= $id ?> " />
			<input type="hidden" id="in-favs" value="<?= $video->in_favs ? 1 : 0 ?> " />
			<input type="hidden" id="gps-track" value="<?= htmlentities($video->gps_track) ?> " />

			<div id="mediaScreenAndMap">
				<div class="col-sm-4 hidden" id="bookmark-list">
						<div class="nav-book">
							<?php if (!empty($current_permissions->update_bookmark)) { ?>
								<a href="javascript:void(0)" class="book-link" id="EditBookmark">
									<i class="fa fa-pencil"></i>
								</a>
							<?php } ?>
							
							<?php if (!empty($current_permissions->delete_bookmark)) { ?>
								<a href="javascript:void(0)" class="book-link" onclick="remove_bookmark(get_selected_record())">
									<i class="fa fa-trash"></i>
								</a>
							<?php } ?>
						</div>

						<div class="table-responsive">
							<table class="table  table-user-groupe">
								<thead>
									<tr>
										<th>
											<input type="checkbox" class="priv-chk" />
										</th>
										<th>Time</th>
										<th>Description</th>
									</tr>
								</thead>

								<tbody>
								<?php foreach ($video->bookmarks as $bookmark) { ?>
									<tr data-bookmark-id="<?= $bookmark->id ?>">
										<td>
											<input type="checkbox" class="priv-chk record" value="<?= $bookmark->id ?>" />
										</td>
										<td data-bookmark-time="<?= $bookmark->time ?>"><?= date('i:s', $bookmark->time) ?></td>
										<td class="description"><?= $bookmark->description ?></td>
									</tr>
								<?php } ?>
								</tbody>
							</table>
						</div>
				</div>

				<div id="video-player" class="col-lg-12" ><!-- col-lg-7  -->
					<div class="player plyr">
						<video controls preload="true" autoplay class="video-fluid">
							<source src="/index.php/video/index/<?= $id ?>">
						</video>
					</div>
				</div>

				<div class="hidden col-sm-5" id="map-container"><!-- col-lg-5  -->
				</div>
			</div>
			
			<div class="btn-box">
			<?php foreach ($buttons as $button) { ?>
				<button type="button" class="btn btn-primary" id="<?= $button['id'] ?>"><?= $button['title'] ?></button>
			<?php }	?>
			</div>
		</div>
	</div>

</div>
