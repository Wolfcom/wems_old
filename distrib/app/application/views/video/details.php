<input type="hidden" id="record-module" value="Video" />
<input type="hidden" id="record-id" value="<?= $id ?> " />

<div class="col-sm-10 main-content body-wrapper tab video-preview">
	<div class="container-fluid">
		<?php require( dirname(__FILE__) . '/../template/navigation.php'); ?>
	</div>

	<div class="video-details-container">
		<div class="head-container">
			<h3><?= empty($video->title) ? 'No Title' : $video->title ?></h3>
			<span class="modal-close-icon icon-icn_close_x_01 secondaryColorTextActive" aria-hidden="true" onclick="location.href = '/index.php/video/preview/<?= $id ?>'"></span>
		</div>

		<div class="video-details-inner">
			<div class="col-sm-4">
				<div class="detail-video">
					<div id="video-player" class="col-lg-12"><!-- col-lg-7  -->
						<div class="player plyr">
							<video controls class="video-fluid">
								<source src="/index.php/video/index/<?= $id ?>" type="video/webm">
							</video>
						</div>
					</div>
				</div>				

				<div class="details-box">
					<h4>Details</h4>
					<label class="d-tiltle">Size : </label>
					<span class="d-content"><?= format_bytes($video->size) ?></span><br>
					<label class="d-tiltle">Dimensions : </label>
					<span class="d-content"><?= $video->width ?>*<?= $video->height ?> Pixels</span><br>
					<label class="d-tiltle">Duration : </label>
					<span class="d-content"><?= $video->duration ?></span><br>
					<label class="d-tiltle">Date Created : </label>
					<span class="d-content"><?= $video->created ?></span><br>
					<label class="d-tiltle">Date Modified : </label>
					<span class="d-content"></span><br>
					<label class="d-tiltle">Upload By : </label>
					<span class="d-content"><?= $video->first_name ?> <?= $video->last_name ?> (<?= $video->user_login ?>)</span><br>
				</div>
			</div>

			<div class="col-sm-4">
				<label for="record-title" class="s-title">Name</label>
				<input type="text" id="record-title" class="s-select" value="<?= $video->title ?>">

				<label for="record-classification" class="s-title">Classification</label>
				<select name="s-clacification" id="record-classification">
					<option value="">None</option>
					<?php foreach ($classifications as $item) { ?>
						<option value="<?= $item ?>" <?= selected($item == $video->classification) ?>><?= $item ?></option>
					<?php } ?>
				</select>

				<label for="record-caseno" class="s-title">Case</label>
				<input type="text" id="record-caseno" class="s-select" value="<?= $video->caseno ?>">

				<label for="record-description" class="s-title">Description</label>
				<textarea id="record-description" class="text-box"><?= $video->description ?></textarea>

				<div class="btn-group save-meta-group">
					<button type="button" class="btn btn-primary btn-popup btn-detail" id="SaveMetadata">Save</button>
				</div>
			</div>

			<!--<div class="col-sm-4">
				<h4>Bookmarks</h4>
				<label class="d-tiltle">Size : </label>
				<span class="d-content">376.52 KB</span><br>
				<label class="d-tiltle">Dimensions : </label>
				<span class="d-content">400*270 Pxels</span><br>
				<label class="d-tiltle">Date Uploaded : </label>
				<span class="d-content">2016-02-14</span><br>
			</div>-->
		</div>

	</div>
</div>
