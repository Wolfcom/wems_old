<input type="hidden" id="record-id" value="<?= $id ?> " />

<div class="player">
	<video controls preload="true" class="video-fluid">
		<source src="/index.php/video/index/<?= $id ?>">
	</video>
</div>

<form id="videoForm">
	<div class="form-group">
		<label for="video-title">Description:</label>
		<input class="form-control" id="video-title" required type="text" />
	</div>
</form>
