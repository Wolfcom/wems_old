<div class="about-box">
	<p><b>About Wolfcom</b><p>
	<p>Wolfcom Enterprises manufactures the ultimate Law Enforcement Tools. The Wolfcom 3rd Eye, Wolfcom Vision and Wolfcom Evidence Management System (WEMS) are multi-purpose, multi-functional, multi-technology indispensable Law Enforcement tools that will assist officers in their day to day duties and in a days to come.</p>

	<p><b>About Wolfcom Evidence Management System - WEMS</b></p>
	<p>Version: 1.0</p>
	<p>Support Email <a href="mailto:techsupport@wolfcomglobal.com">techsupport@wolfcomglobal.com</a></p>
	<p>Support Phone:  +1 323 962 1061</p>
	<p>Support Center: <a href="https://wolfcom.freshdesk.com/support/login" target="_blank">https://wolfcom.freshdesk.com/support/login</a></p>
</div>
