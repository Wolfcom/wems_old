<input type="hidden" id="record-id" value="<?= $id ?> " />

<div class="snapshot_area" class="row">
	<div id="screenshot"></div>

	<form id="snapshotForm">
		<div class="form-group">
			<label for="snapshot-name">Title:</label>
			<input class="form-control" id="snapshot-name" required>
		</div>
	</form>
</div>
