<div class="row">
	<div id="video-player" class="col-lg-7">
		<div class="player">
		    <video  controls crossorigin preload="true" autoplay>
		        <source src="<?php echo site_url('data_temp/'.$body_data['clip_path']); ?>" type="video/mp4">
		    </video>
		</div>
	</div>
	<div id="map-container" class="col-lg-5">
		<div class="map-canvas"></div>
	</div>
	 
</div>