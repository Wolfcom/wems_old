$baseUrl = window.location.origin + '/index.php/';
$baseRoot = window.location.origin + '/';


$(document).ready(function() {
    
    $('.table_mycase').DataTable();
} );







$('a#open').click(function() {
    $('#dialog').dialog('open');
        $("body").addClass("dialog-open");
});

$('#dialog').dialog({
    autoOpen: false,
    modal: true,
   show:{effect:'drop', direction:'right'},
   hide:{effect:'drop', direction:'right'},
    open: function() {
        $('.btn-close-add').bind('click',function(){
            $('#dialog').dialog('close');
        $("body").removeClass("dialog-open");
        });
        $('.icon-icn_close_x_01').bind('click', function(event) {
            $('#dialog').dialog('close');
        $("body").removeClass("dialog-open");
        });
    }
});   




$('a#open-edit').click(function() {
    $('#dialog-edit').dialog('open');
        $("body").addClass("dialog-open");
});

$('.edit_user').dialog({
    autoOpen: false,
    modal: true,
   show:{effect:'drop', direction:'right'},
   hide:{effect:'drop', direction:'right'},
    open: function() {
        $('.btn-close-edit').bind('click',function(){
            $('.edit_user').dialog('close');
        $("body").removeClass("dialog-open");
        });
        $('.icon-icn_close_x_01').bind('click', function(event) {
            $('.edit_user').dialog('close');
        $("body").removeClass("dialog-open");
        });
    }
});


function openEditDialog(idDialog) {
    
    $('#'+idDialog).dialog('open');
        $("body").addClass("dialog-open");
}










$('a#open-edit-group').click(function() {
    $('.edit_group_modal').dialog('open');
});

$('.edit_group_modal').dialog({
    autoOpen: false,
    modal: true,
   show:{effect:'drop', direction:'right'},
   hide:{effect:'drop', direction:'right'},
    open: function() {
        $('.btn-close-add').bind('click',function(){
            $('.edit_group_modal').dialog('close');
        });
        $('.icon-icn_close_x_01').bind('click', function(event) {
            $('.modal').dialog('close');
        });
    }
});


$('.select_member_modal').dialog({
    autoOpen: false,
    modal: true,
   show:{effect:'drop', direction:'right'},
   hide:{effect:'drop', direction:'right'},
    open: function() {
        $('.btn-close-add').bind('click',function(){
            $('.select_member_modal').dialog('close');
        });
        $('.icon-icn_close_x_01').bind('click', function(event) {
            $('.modal').dialog('close');
        });
    }
});



function openGroupEditDialog(idDialog) {
    
    $('#'+idDialog).dialog('open');
}

$('a#open-crt-group').click(function() {
    $('.create_group_modal').dialog('open');
});

$('.create_group_modal').dialog({
    autoOpen: false,
    modal: true,
   show:{effect:'drop', direction:'right'},
   hide:{effect:'drop', direction:'right'},
    open: function() {
        $('.btn-close-edit').bind('click',function(){
            $('.create_group_modal').dialog('close');
        });
        $('.icon-icn_close_x_01').bind('click', function(event) {
            $('.create_group_modal').dialog('close');
        });
    }
});












$(".group_list").on('click', function(event) {

    event.preventDefault();
    /* Act on the event */
    var user_id = $(this).attr("user-id");

    //$(".group_list_"+user_id).removeAttr("style");

    if ( $(this).css("background-color") == "rgb(128, 128, 128)" ) {

        $(this).removeAttr("style");
    }
    else {

        $(this).css("background-color","#808080");
        $(this).css("font-weight","bold");
        $(this).css("color","#fff");
    }

    var group_id = $(this).attr("id");

    $("#group_"+user_id).val(group_id);
});


$(".access").on('click', function(event) {
    event.preventDefault();
    /* Act on the event */
    var user_id = $(this).attr("user-id");

    $(".access > .checkbox").removeClass('checked');
    $(".access > .checkbox").removeClass('unchecked');

    $(".access .checkbox").addClass('unchecked');
    $(this).children('.checkbox').removeClass('unchecked');
    $(this).children('.checkbox').addClass('checked');

    $('.text_access').removeAttr("style");
    $(this).children('.text_access').css("color", "#333333");

    var portal_id = $(this).attr("id");
    $("#portal_"+user_id).val(portal_id);
    
});



function deleteProfile(user_id) {
    
    $.post($baseUrl+'user_new/deleteProfile',{

        user_id:user_id
    },
    function(data,status) {

        if ( data == "pass" ) {

            //alert("Delete Profile Complete."); location.reload();
            location.reload();
        }
        else {

            //$('body').removeClass('loading');
            alert("Something Error : Error Code Below \n\n"+data);
        }
    });
}


function validateField(data, data_id) {
    
    if ( $.trim(data) == "" || $.trim(data) == null ) {  

        alert("Please full fill information.");
        $(data_id).focus();
        return false;
    }
    else {

        return true;
    }
}



function createGroup() {
    
    var memberSelect = [];

    $(".groupSelectMemberList_new .chooseBtn").each(function(index, el) {

        //console.log($(this).attr("user-id"));

        if ( $(this).css("display") != "none" ) {

            memberSelect.push({
                user_id: $(this).attr("user-id")
            });
            //console.log($(this).attr("user-id"));
        };
        
    });

    var memberSelectJSON = null;

    if ( !(memberSelect.length === 0) ) {

        memberSelectJSON = JSON.stringify(memberSelect);
    };

    console.log(memberSelectJSON);


    var title = $("#titleText_new").val();

    //var member = $("#lastName_new").val();

    if ( validateField(title, "#titleText_new") ) {

        $.post($baseUrl+'user_new/createGroup',{

            title:title,
            memberSelect:memberSelectJSON
        },
        function(data,status) {

            if ( data == "pass" ) {

                alert("Create Group Complete."); location.reload();
            }
            else {

                //$('body').removeClass('loading');
                alert("Something Error : Error Code Below \n\n"+data);
            }
        });
    };
}


function updateGroup(group_id) {
    
    var memberUnSelect = [];
    var memberSelect = [];

    $(".groupSelectMemberList_"+group_id+" .chooseBtn").each(function(index, el) {

        //console.log($(this).attr("user-id"));

        if ( $(this).css("display") != "none" ) {

            memberSelect.push({
                user_id: $(this).attr("user-id")
            });
            //console.log($(this).attr("user-id"));
        }
        else {

            memberUnSelect.push({
                user_id: $(this).attr("user-id")
            });
            //console.log($(this).attr("user-id"));
        }
        
    });

    var memberSelectJSON = null;
    var memberUnSelectJSON = null;

    if ( !(memberSelect.length === 0) ) {

        memberSelectJSON = JSON.stringify(memberSelect);
    };

    if ( !(memberUnSelect.length === 0) ) {

        memberUnSelectJSON = JSON.stringify(memberUnSelect);
    };

    console.log(memberSelectJSON);
    console.log(memberUnSelectJSON);

    var title = $("#edit_group_modal_"+group_id+" #titleText_"+group_id).val();
    //var member = $("#lastName_new").val();

    if ( validateField(title, "#edit_group_modal_"+group_id+" #titleText_"+group_id) ) {

        $.post($baseUrl+'user_new/updateGroup',{

            group_id:group_id,
            title:title,
            memberSelect:memberSelectJSON,
            memberUnSelect:memberUnSelectJSON
        },
        function(data,status) {

            if ( data == "pass" ) {

                alert("Update Group Complete."); location.reload();
            }
            else {

                //$('body').removeClass('loading');
                alert("Something Error : Error Code Below \n\n"+data);
            }
        });
    }
}


function deleteGroup(group_id) {
    
    $.post($baseUrl+'user_new/deleteGroup',{

        group_id:group_id
    },
    function(data,status) {

        if ( data == "pass" ) {

            //alert("Delete Group Complete."); 
            location.reload();
        }
        else {

            //$('body').removeClass('loading');
            alert("Something Error : Error Code Below \n\n"+data);
        }
    });
}



$(function() {

    var navigation = $("#navigation").val();
    var crew_privileges = $("#crew_privileges").val();
    var passenger_privileges = $("#passenger_privileges").val();

    if ( navigation == "User Privileges" && crew_privileges != "" && passenger_privileges != "" ) {

        var crew_privileges_arr = jQuery.parseJSON(crew_privileges);
        var passenger_privileges_arr = jQuery.parseJSON(passenger_privileges);

        //console.log(privileges_arr[0].name);
        var crew_privileges_check = [];
        var passenger_privileges_check = [];

        $.each( crew_privileges_arr, function( key, value ) {

            crew_privileges_check[value.name] = value.status;
            //console.log(value.name+" - "+value.status);
        });

        $.each( passenger_privileges_arr, function( key, value ) {

            passenger_privileges_check[value.name] = value.status;
            //console.log(value.name+" - "+value.status);
        });

        $('.crew_privileges li.ng-scope').each(function() {

            var name = $(this).children('span').text();

            if ( crew_privileges_check[name] == "checked" ) {

                console.log(name);
                $(this).children('div').addClass('checked');
            };

        });

        $('.passenger_privileges li.ng-scope').each(function() {

            var name = $(this).children('span').text();

            if ( passenger_privileges_check[name] == "checked" ) {

                console.log(name);
                $(this).children('div').addClass('checked');
            };

        });
    }
});



function savePrivileges() {
    
    var crew_privileges_arr = [];

    $('.crew_privileges li.ng-scope').each(function() {

        var status = $(this).children('div').hasClass('checked');

        if ( status ) { status = "checked"; } else { status = ""; }

        var name = $(this).children('span').text();

        console.log(name);

        crew_privileges_arr.push({
            name: name,
            status: status
        });
    });

    var passenger_privileges_arr = [];

    $('.passenger_privileges li.ng-scope').each(function() {

        var status = $(this).children('div').hasClass('checked');

        if ( status ) { status = "checked"; } else { status = ""; }

        var name = $(this).children('span').text();

        console.log(name);

        passenger_privileges_arr.push({
            name: name,
            status: status
        });
    });








    var crew_privilegesJSON = JSON.stringify(crew_privileges_arr);
    var passenger_privilegesJSON = JSON.stringify(passenger_privileges_arr);

    //console.log(privilegesJSON);

    $.post($baseUrl+'user_new/setUserPrivileges',{

        crew_privilegesJSON:crew_privilegesJSON,
        passenger_privilegesJSON:passenger_privilegesJSON
    },
    function(data,status) {

        if ( data == "pass" ) {

            alert("Set Privileges Complete."); 
            location.reload();
        }
        else {

            //$('body').removeClass('loading');
            alert("Something Error : Error Code Below \n\n"+data);
        }
    });
}


function unassigneDevice(device_id) {
    
    $.post($baseUrl+'device_new/unassigneDevice',{

        device_id:device_id
    },
    function(data,status) {

        if ( data == "pass" ) {

            alert("Unassigne Complete."); 
            location.reload();
        }
        else {

            //$('body').removeClass('loading');
            alert("Something Error : Error Code Below \n\n"+data);
        }
    });
}

function assigneDevice(device_id) {
    
    $('.select_member_modal').dialog('open');
    $("#device_select").val(device_id);
}

function deviceSelectMember(zoneID) {
    
    var checkStatus = $("."+zoneID).css("display");
    $(".chooseBtn").css("display", "none");

    if ( checkStatus == "none" ) {

        var user_id = $("."+zoneID).attr("user-id");
        $("#user_select").val(user_id);

        $("."+zoneID).css("display", "block");
    }
    else {

        $("."+zoneID).css("display", "none");
    }
}

function assigneDeviceConfirm() {
    
    var device_id = $("#device_select").val();
    var user_id = $("#user_select").val();

    $.post($baseUrl+'device_new/assigneDevice',{

        device_id:device_id,
        user_id:user_id
    },
    function(data,status) {

        if ( data == "pass" ) {

            //alert("Assigne Complete."); 
            location.reload();
        }
        else if ( data == "already" ) {

            alert("This user already assigne to another camera. Please select another user or unassigne and try to assigne again."); 
        }
        else {

            //$('body').removeClass('loading');
            alert("Something Error : Error Code Below \n\n"+data);
        }
    });
}




function groupFilter(user_id) {
    
    var filterString = $("#groupFilter_"+user_id).val();

    $('.group_list_'+user_id).each(function() {

        var group_name = $(this).attr("name");
        //console.log(group_name);


        if ( group_name.toLowerCase().indexOf(filterString) >= 0 ) {

            $(this).css("display","block");
        }
        else {

            $(this).css("display","none");
        }


    });

    //console.log(filterString);
}



  $(function() {
    $( "#first-datepicker" ).datepicker();
  });
  $(function() {
    $( "#scd-datepicker" ).datepicker();
  });
  $(function() {
    $( "#thrd-datepicker" ).datepicker();
  });
