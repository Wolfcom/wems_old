	$(document).ready(function() {
		$(function(){
			$(".date-head").click(function () {
				$header = $(this);
				$content = $header.next();
				$content.slideToggle(500);
			});

			$('.date-head').bind('click', function(event) {
				$(this).toggleClass('active');
			});
		});
	});

/**
 * Displays an about box
 */
function show_about() {
	jQuery('body').append('<div id="modal-box"></div>');
	jQuery('#modal-box').load('/index.php/App/about', function(response, status) {
		if (status == 'error') {
			// Clean modal box
			jQuery(this).remove();
			return;
		}

		jQuery('#about-box').modal({
			show: true,
			keyboard: false,
			backdrop: 'static'
		});

		jQuery('#about-box').on('hidden.bs.modal', function (e) {
			jQuery('#modal-box').remove();
		});
	});
}

/**
 * Displays an dialog to submit a ticket
 */
function show_ticket() {
	jQuery('body').append('<div id="modal-box"></div>');
	jQuery('#modal-box').load('/index.php/App/ticket', function(response, status) {
		if (status == 'error') {
			// Clean modal box
			jQuery(this).remove();
			return;
		}

		jQuery('#ticket-box').modal({
			show: true,
			keyboard: false,
			backdrop: 'static'
		});

		jQuery('#ticket-box').on('hidden.bs.modal', function (e) {
			jQuery('#modal-box').remove();
		});
	});
}


/**
 * Resets a state for specified button
 */
function reset_button_state(button) {
	jQuery(button).data('value', null);

	for (var node = button.firstChild; node; node = node.nextSibling) {
		if (node.nodeType == 3) {
			var pos = node.nodeValue.indexOf(':');
			if (pos != -1) {
				node.nodeValue = node.nodeValue.substring(0, pos);
			}
			break;
		}
	}
}


/**
 * Validates the specified form.
 */
function validate_form(form) {
	if (!form.checkValidity()) {
		jQuery('[required]', form).each(function() {
			var element = jQuery(this);
			if (!element.val()) {
				element.focus();
				return false;
			}
		});

		return false;
	}

	return true;
}

/**
 * Validates an active profile form.
 * Return an instance of form if the form is valid.
 */
function validate_profile_form() {
	// Detect and validate an active form
	var form = document.getElementById("updateProfile");
	if (!form) {
		form = jQuery('.ui-dialog').filter(function() {
			return $(this).css('display') == 'block';
		});

		if (form.length > 1) {
			// Multiple active dialogs isn't supported
			return;
		}
		else {
			form = form.find('form')[0];
		}
	}

	if (!validate_form(form)) {
		return false;
	}

	// Validate a password of user
	if ((form.password.value || form.confirm_password.value) && (form.password.value != form.confirm_password.value)) {
		alert('Passwords are not equal');
		return false;
	}

	// Check the file of user's photo
	if (form.photo.files.length) {
		var photo = form.photo.files[0];
		if (photo.size > 1000000) {
			alert("File size exceeds maximum upload limit 1Mb");
			return false;
		}

		var images = ['image/png', 'image/jpeg', 'image/gif', 'image/bmp', 'image/gif'];
		if (images.indexOf(photo.type) == -1) {
			alert("Unknown type of image: " + photo.type);
			return false;
		}
	}

	return form;
}

/**
 * Creates the profile of user
 */
function createProfile() {
	// Valid an user input
	var form = validate_profile_form();
	if (!form) {
		return;
	}

	// Send a request
	var form_data = new FormData(form);
	var action_url = '/index.php/user/create_profile';
	if (baseurl) {
		action_url = baseurl + action_url;
	}

	jQuery.ajax({
		type: 'POST',
		url: action_url,

		contentType: false,
		processData: false,
		data: form_data,
		dataType: 'json',

		success: function(data) {
			if (data.status == 'success') {
				location.reload();
			}
			else {
				alert("Unable to create an user profile: " + data.message);
			}
		},

		error: function(data){
			console.log(data.responseText);
			alert("Unable to create an user profile");
		}
	});
}

/**
 * Displays the dialog for editing user fields.
 */
function user_edit_dialog(id) {
	if (!id) {
		return;
	}

	jQuery(document.body).addClass('dialog-open');
	jQuery('#dialog-edit-' + id).dialog('open');
}

/**
 * Updates the details of user profile
 */
function updateProfile(user_id) {
	if (typeof user_id === 'undefined') {
		user_id = '';
	}

	var form = validate_profile_form();
	if (!form) {
		return;
	}

	var form_data = new FormData(form);
	$.ajax({
		type: 'POST',
		url: '/index.php/user/update_profile/' + user_id,

		contentType: false,
		processData: false,
		data: form_data,
		dataType: 'json',

		success: function(data) {
			if (data.status == 'success') {
				location.reload();
			}
			else {
				alert("Unable to update: " + data.message);
			}
		},

		error: function(data){
			console.log("error");
			console.log(data);
		}
	});
}

/**
 * Changes the password for current user
 */
function update_password() {
	// Validate passwords
	var new_password = $("#newPassword").val();
	var confirm_password = $("#confirmPassword").val();

	if (new_password != confirm_password) {
		alert('Passwords are not equal.');
		return;
	}

	// Send a request
	$.ajax({
		type: 'POST',
		url: '/index.php/user/update_password',

		data: {password: new_password},
		dataType: 'json',

		success: function(data) {
			$('body').removeClass('loading');

			if (data.status == 'success') {
				alert("Password is updated.");
				$('[data-dismiss]', '.modal-dialog').click();
			}
			else {
				alert("Unable to update a password: " + data.message);
			}
		},

		error: function(data){
			console.log(data);
			alert("Unable to update a password");
		}
	});
}

/**
 * Deletes the specified user
 */
function delete_profile(user_id) {
	if (!user_id) {
		return;
	}

	if (!confirm('Are you sure that you want to disable selected user account(s)?')) {
		return;
	}

	jQuery.ajax({
		type: 'POST',
		url: '/index.php/user/delete_profile/' + user_id,
		dataType: 'json',

		success: function(data) {
			if (data.status == 'success') {
				location.reload();
			}
			else {
				alert("Unable to disable an user: " + data.message);
			}
		},

		error: function(data){
			console.log("error");
			console.log(data);
			alert("Unable to disable an user");
		}
	});
}


// DEPRECATED
function changePicture(option) {	
	var photo = $('#picture_file');
	if (photo.val().length < 1) {
		alert("Please Select Picture");
		return;
	}

	if (photo[0].files[0].size > 1000000 ) {
		alert("You have exceeded the size limit!");
		return;
	}

	$("#previewPicture").html('<label>Preview Picture</label><br/><i class="fa fa-cog fa-spin fa-5x"></i>');

	var form_data = new FormData("#changePictureForm");
	form_data.append('file', $('#picture_file')[0].files[0]);

	if (option == "save") {
		$.ajax({
			type: 'POST',
			url: '/index.php/user/update_photo',

			contentType: false,
			processData: false,
			data: form_data,
			dataType: 'json',

			success: function(data) {
				if (data.status == 'success') {
					location.reload();
				}
				else {
					$('body').removeClass('loading');
					alert("Unexpected error: " + data.message);
				}
			},

			error: function(data){
				console.log("error");
				console.log(data);
			}
		});
	}
	else {
		var reader = new FileReader();
		reader.onload = function (e) {
			$("#previewPicture").html('<label>Preview Picture</label><br/><img src="' + e.target.result + '" width = "200px"/>');
		}
		reader.readAsDataURL($('#picture_file')[0].files[0]);
	}
}

/**
 * Previews the specified media file
 */
var preview_handler = function() {
	// Prepare link of viewer
	var row = jQuery(this);
	var url = '';

	switch (row.data('record-type')) {
		case 'picture': 
			url = "/index.php/picture/preview/" + row.data('record-id');
			break;

		case 'video':
			url = "/index.php/video/preview/" + row.data('record-id');

			var bookmark_id = row.parents('tr').data('bookmark');
			if (bookmark_id) {
				url += '#bookmark=' + bookmark_id;
			}
			break;

		case 'audio':
			url = "/index.php/audio/preview/" + row.data('record-id');
			break;

		case 'document':
			url = "/index.php/document/details/" + row.data('record-id');
			break;
	}

	location.href = url;
}

/**
 * Displays a modal box for the bookmark editor
 */
var bookmark_editor_handler = function() {
	// Pause a video before adding bookmark
	var player = document.querySelectorAll(".player")[0];
	if (player) {
		var media_player = player.plyr;
		media_player.pause();
	}

	// Determine an action
	var action;
	if (this.id == 'EditBookmark') {
		var record_id = get_selected_record();
		if (!record_id) {
			return;
		}

		action = '/index.php/bookmarks/edit/' + record_id;
	}
	else {
		var record_id = jQuery('#record-id').val();
		action = '/index.php/bookmarks/add/' + record_id;
	}

	if (baseurl) {
		action = baseurl + action;
	}

	// Send a request
	jQuery('body').append('<div class="bookmark_modalBox"></div>');
	jQuery('.bookmark_modalBox').load(action, function(data){
		// Render a bookmark dialog with snapshot
		jQuery('#bookmarkEditor').modal("show");

		if (media_player) {
			var canvas = get_snapshot(media_player, 585, 400);
			jQuery('#bookmark-thumbnail').append(canvas);
		}

		// Release resources
		jQuery('#bookmarkEditor').on('hidden.bs.modal', function(e) {
			jQuery('.bookmark_modalBox').remove();
		});
	});
}

/**
 * Displays the list of bookmarks
 */
var bookmark_list_handler = function() {
	var button = jQuery(this);
	var list = jQuery('#bookmark-list');
	var parent_container = jQuery('#mediaScreenAndMap');
	var video_player = jQuery('#video-player');

	if (list.hasClass('hidden')) {
		if (parent_container.hasClass('map_open')) {
			// Close active map
			jQuery('#ShowMap').click();
		}
		parent_container.addClass('bookmarks_open');

		list.removeClass('hidden');
		button.text('Hide Bookmarks');

		video_player.removeClass('col-lg-12');
		video_player.addClass('col-sm-7');
	}
	else {
		parent_container.removeClass('bookmarks_open');

		list.addClass('hidden');
		button.text('Show Bookmarks');

		video_player.removeClass('col-sm-7');
		video_player.addClass('col-lg-12');
	}
}

/**
 * Creates a new bookmark
 */
var bookmark_handler = function() {
	var button = this;

	// Validate form
	var form = document.getElementById("bookmarkForm");
	if (!validate_form(form)) {
		return;
	}

	// Show notification
	var message_container = document.getElementById('notification-message');
	if (message_container) {
		message_container.innerHTML = 'Processing...';
	}

	// Determine an action
	var params = {};
	var record_id = jQuery('#record-id', jQuery('.bookmark_modalBox')).val();
	var description = jQuery('#bookmark-description').val();

	if (button.id == 'UpdateBookmark') {
		var action = '/index.php/bookmarks/update/' + record_id;
		params.description = description;
	}
	else {
		var player = document.querySelectorAll(".player")[0].plyr;
		var current_time = player.media.currentTime;
		var action = '/index.php/bookmarks/create/';

		params.id = record_id;
		params.description = description;
		params.time = current_time;
	}

	// Send a request
	jQuery.ajax({
		url: action,
		type: 'POST',
		dataType: 'json',
		data: params
	})
	.done(function(data) {
		if (data.status == 'success') {
			// Add new row to the list
			if (button.id == 'CreateBookmark') {
				jQuery('#bookmark-list').find('tbody').append(
					'<tr data-bookmark-id="' + data.bookmark.id + '">' +
						'<td>' +
							'<input type="checkbox" class="priv-chk record" value="' + data.bookmark.id + '" style="display: none" />' +
							'<div class="class_checkbox" />' +
						'</td>' +
						'<td data-bookmark-time="' + data.bookmark.raw_time + '">' + data.bookmark.time + '</td>' +
						'<td class="description">' + data.bookmark.description + '</td>' +
					'</tr>'
				);
			}

			// Update existing row
			else {
				var row = jQuery('#bookmark-list').find('tr[data-bookmark-id="' + data.bookmark.id + '"]');
				row.find('td.description').html(data.bookmark.description);
			}

			jQuery('#bookmarkEditor').modal('hide');
		}
		else {
			console.log(data);
			alert('Unable to save a bookmark: ' + data.message);
		}
	})
	.fail(function(data) {
		console.log(data.responseText);
		alert('Unable to save a bookmark');
	});
}

/**
 * Selects a video fragment related to the selected bookmark
 */
var bookmark_preview_handler = function() {
	var bookmark_time = jQuery(this).find('td[data-bookmark-time]').data('bookmark-time');
	var media_player = document.querySelectorAll(".player")[0].plyr;

	media_player.pause();
	media_player.seek(bookmark_time);
}

/**
 * Removes the selected bookmark
 */
function remove_bookmark(id) {
	if (!id) {
		return;
	}
	if (!confirm('Are you sure that you want to delete selected bookmark?')) {
		return;
	}

	jQuery.ajax({
		url: '/index.php/bookmarks/delete/' + id,
		type: 'POST',
		dataType: 'json'
	})
	.done(function(data) {
		if (data.status == 'success') {
			jQuery('[data-bookmark-id="' + id + '"]').remove();
		}
		else {
			console.log(data);
			alert('Unable to delete a bookmark');
		}
	})
	.fail(function(data) {
		console.log(data.responseText);
		alert('Unable to delete a bookmark');
	});
}


/**
 * Manages the current state of media file in the favorites list
 */
var favorite_handler = function() {
	var button = jQuery(this);
	var record_id = jQuery('#record-id').val();
	var in_favs = jQuery('#in-favs');

	var action_url = (in_favs.val() == 1)
		? '/index.php/Favourites/remove/' + record_id
		: '/index.php/Favourites/add/' + record_id;

	jQuery.ajax({
		url: action_url,
		type: 'POST',
		dataType: 'json'
	})
	.done(function(data) {
		if (data.status != 'success') {
			alert('Unable to perform an operation: ' + data.message);
		}

		// Change button name
		if (in_favs.val() == 1) {
			in_favs.val(0);
			button.text('Add to Favorites');
		}
		else {
			in_favs.val(1);
			button.text('Remove from Favorites');
		}
	})
	.fail(function(data) {
		console.log(data.responseText);
		alert('Unable to perform an operation');
	});
}

/**
 * Returns the canvas element with capture wideo frame
 */
function get_snapshot(player, width, height) {
	if (!width) {
		width = player.media.videoWidth;
	}
	if (!height) {
		height = player.media.videoHeight;
	}

	var canvas = document.createElement('canvas')
	canvas.width = width;
	canvas.height = height;

	canvas.getContext('2d').drawImage(player.media, 0, 0, width, height);

	return canvas;
}

/**
 * Displays a modal box for the captured video frame
 */
var snapshot_editor_handler = function() {
	var record_id = jQuery('#record-id').val();
	jQuery('body').append('<div class="Snapshot_ModalBox"></div>');
	jQuery('.Snapshot_ModalBox').load('/index.php/video/snapshot/' + record_id, function() {
		// Pause the video before make a screenshot
		var player = document.querySelectorAll(".player")[0].plyr;
		player.pause();

		// Render an snapshot
		var canvas = get_snapshot(player, 585, 400);
		jQuery('#screenshot').html(canvas);
		jQuery('#snapshotEditor').modal("show");

		// Release resources
		jQuery('#snapshotEditor').on('hidden.bs.modal', function (e) {
			$('.Snapshot_ModalBox').remove();
		});
	});
}

/**
 * Saves the captured video frame
 */
var snapshot_handler = function() {
	// Validate form
	var form = document.getElementById('snapshotForm');
	if (!validate_form(form)) {
		return;
	}

	var record_id = jQuery('#record-id', jQuery('#snapshotEditor')).val();
	var title = jQuery('#snapshot-name').val();

	// Get time of current video fragment
	var player = document.querySelectorAll(".player")[0].plyr;
	var current_time = player.media.currentTime;

	// Send a request
	jQuery.ajax({
		url: '/index.php/picture/save_snapshot/',
		type: 'POST',
		dataType: 'json',
		data: {
			id: record_id,
			title: title,
			time: current_time
		}
	})
	.done(function(data) {
		if (data.status == 'success') {
			jQuery('#snapshotEditor').modal('hide');
		}
		else {
			console.log(data);
			alert('Unable to save a snapshot');
		}
	})
	.fail(function(data) {
		console.log(data.responseText);
		alert('Unable to save a snapshot');
	});
}

/**
 * Displays the video editor
 */
var video_editor_handler = function() {
	// Pause player before edit
	var player = document.querySelectorAll("#video-player video")[0];
	player.pause();

	// Send a request
	var record_id = jQuery('#record-id').val();
	jQuery('body').append('<div class="redact_modalBox"></div>');

	jQuery('.redact_modalBox').load("/index.php/Video/editor/" + record_id, function(data) {
		// Render a editor dialog
		jQuery('#videoEditor').modal("show");
		

		// Release resources
		jQuery('#videoEditor').on('hidden.bs.modal', function(e) {
			jQuery('.redact_modalBox').remove();
		});
	});
}

/**
 * Set start/end time of cropped  video fragment
 */
var crop_time_handler = function() {
	// Detect selected time
	var video_player = jQuery("video", jQuery('#videoEditor'))[0];
	var current_time = parseInt(video_player.currentTime);

	var button = jQuery(this);
	button.data('value', current_time);

	// Extract time parts
	var current_second = current_time % 60;
	current_time = Math.floor(current_time / 60);

	var current_minute = current_time % 60;
	current_time = Math.floor(current_time / 60);

	var current_hour = current_time % 24;

	// Prepare current time
	var time = '';
	if (current_hour) {
		time += (current_hour > 9 ? current_hour : '0' + current_hour) + ':';
	}

	time += (current_minute > 9 ? current_minute : '0' + current_minute) + ':';
	time +=(current_second > 9 ? current_second : '0' + current_second);

	// Change text node of button
	var button = jQuery(this);
	var value = button.html();
	var position = value.indexOf(':');

	if (position > 1) {
		button.html(value.substring(0, position) + ': ' + time);
	}
	else {
		button.html(value + ': ' + time);
	}
}

/**
 * Saves a cropped video
 */
var crop_handler = function() {
	// Validate form
	var form = document.getElementById("videoForm");
	if (!validate_form(form)) {
		return;
	}

	var record_id = jQuery('#record-id').val();
	var title = jQuery('#video-title').val();

	// Validate time interval
	var start_time = jQuery('#SetStartTime').data('value');
	var end_time = jQuery('#SetEndTime').data('value');

	if (start_time == null) {
		alert('Please specify start time');
		return;
	}

	if (end_time == null) {
		alert('Please specify end time');
		return;
	}

	if (start_time > end_time) {
		alert('Start time bigger than end time');
		return;
	}

	// Send request to backend
	jQuery.ajax({
		url: '/index.php/video/crop',
		type: 'POST',
		dataType: 'json',
		data: {
			id: record_id,
			title: title,
			start: start_time,
			end: end_time
		},
	})
	.done(function(data) {
		if (data.status == 'success') {
			jQuery('#videoEditor').modal('hide');
		}
		else {
			console.log(data);
			alert('Unable to crop a video: ' + data.message);
		}
	})
	.fail(function(data) {
		alert('Unable to crop a video');
	});
}

/**
 * Displays the metadata editor
 */
var metadata_editor_handler = function() {
	var record_id = document.getElementById('record-id').value;
	var module = document.getElementById('record-module').value;
	location.href = '/index.php/' + module + '/details/' + record_id;
}

/**
 * Saves the meta information for the current media file
 */
var metadata_handler = function() {
	var module = jQuery('#record-module').val().toLowerCase();
	var record_id = jQuery('#record-id').val();

	var action_url = '/index.php/' + module + '/save_metadata/' + record_id;
	if (baseurl) {
		action_url = baseurl + action_url;
	}

	jQuery.ajax({
		url: action_url,
		type: 'POST',
		dataType: 'json',
		data: {
			caseno: jQuery('#record-caseno').val(),
			title: jQuery('#record-title').val(),
			description: jQuery('#record-description').val(),
			classification: jQuery('#record-classification').val()
		}
	})
	.done(function(data) {
		if (data.status == 'success') {
			var page_url = '';
			if (module == 'document') {
				page_url = '/index.php/' + module + '/details/' + record_id;
			}
			else {
				page_url = '/index.php/' + module + '/preview/' + record_id;
			}

			if (baseurl) {
				page_url = baseurl + page_url;
			}

			location.href = page_url;
		}
		else {
			console.log(data);
			alert('Unable to update a meta information: ' + data.message);
		}
	})
	.fail(function(data) {
		console.log(data.responseText);
		alert('Unable to update a meta information');
	});
}

// Enable a fancy checkboxes
var show_fancy_checkboxes = function(context) {
	jQuery('.priv-chk', context).each(function(){
		jQuery(this).hide().after('<div class="class_checkbox" />');
	});
}

// Setup a fancy media player
var show_media_player = function() {
	var crop_control = '';
	if (current_user_permissions.crop_video && current_features.redaction) {
		crop_control = "<button type='button' id='RedactVideo' title='Crop'>" +
			'<i class="fa fa-crop fa-2x"></i>' +
        "</button>";
	}

	var snapshot_control = '';
	if (current_user_permissions.snapshot_video) {
		snapshot_control = "<button type='button' id='SnapCapture' title='Snapshot'>" +
			'<i class="fa fa-camera-retro fa-2x"></i>' +
        "</button>";
	}

	var bookmark_control = '';
	if (current_user_permissions.create_bookmark) {
		bookmark_control = "<button type='button' id='AddBookmark' title='Bookmark'>" +
			'<i class="fa fa-bookmark-o fa-2x"></i>' + 
        "</button>";
	}

	var controls = ["<div class='plyr__controls'>",
    "<div class='plyr__progress'>",
        "<label for='seek{id}' class='plyr__sr-only'>Seek</label>",
        "<input id='seek{id}' class='plyr__progress--seek' type='range' min='0' max='100' step='0.1' value='0' data-plyr='seek'>",
        "<progress class='plyr__progress--played' max='100' value='0'>",
            "<span>0</span>% played",
        "</progress>",
        "<progress class='plyr__progress--buffer' max='100' value='0'>",
            "<span>0</span>% buffered",
        "</progress>",
        "<span class='plyr__tooltip'>--:--</span>",
    "</div>",
    "<span class='plyr__controls--left'>",
        "<button type='button' data-plyr='restart'>",
            "<svg><use xlink:href='#icon-restart'></use></svg>",
            "<span class='plyr__sr-only'>Restart</span>",
        "</button>",
        "<button type='button' data-plyr='rewind'>",
            "<svg><use xlink:href='#icon-rewind'></use></svg>",
            "<span class='plyr__sr-only'>Rewind {seektime} secs</span>",
        "</button>",
        "<button type='button' data-plyr='play'>",
            "<svg><use xlink:href='#icon-play'></use></svg>",
            "<span class='plyr__sr-only'>Play</span>",
        "</button>",
        "<button type='button' data-plyr='pause'>",
            "<svg><use xlink:href='#icon-pause'></use></svg>",
            "<span class='plyr__sr-only'>Pause</span>",
        "</button>",
        "<button type='button' data-plyr='fast-forward'>",
            "<svg><use xlink:href='#icon-fast-forward'></use></svg>",
            "<span class='plyr__sr-only'>Forward {seektime} secs</span>",
        "</button>",
        "<span class='plyr__time'>",
            "<span class='plyr__sr-only'>Current time</span>",
            "<span class='plyr__time--current'>00:00</span>",
        "</span>",
        "<span class='plyr__time'>",
            "<span class='plyr__sr-only'>Duration</span>",
            "<span class='plyr__time--duration'>--:--</span>",
        "</span>",
		bookmark_control,
        snapshot_control,
        crop_control,
    "</span>",
    "<span class='plyr__controls--right'>",
        "<button type='button' data-plyr='mute'>",
            "<svg class='icon--muted'><use xlink:href='#icon-muted'></use></svg>",
            "<svg><use xlink:href='#icon-volume'></use></svg>",
            "<span class='plyr__sr-only'>Toggle Mute</span>",
        "</button>",
        "<label for='volume{id}' class='plyr__sr-only'>Volume</label>",
        "<input id='volume{id}' class='plyr__volume' type='range' min='0' max='10' value='5' data-plyr='volume'>",
        "<button type='button' data-plyr='captions'>",
            "<svg class='icon--captions-on'><use xlink:href='#icon-captions-on'></use></svg>",
            "<svg><use xlink:href='#icon-captions-off'></use></svg>",
            "<span class='plyr__sr-only'>Toggle Captions</span>",
        "</button>",
        "<button type='button' data-plyr='fullscreen'>",
            "<svg class='icon--exit-fullscreen'><use xlink:href='#icon-exit-fullscreen'></use></svg>",
            "<svg><use xlink:href='#icon-enter-fullscreen'></use></svg>",
            "<span class='plyr__sr-only'>Toggle Fullscreen</span>",
        "</button>",
    "</span>",
"</div>"].join("\n");

	plyr.setup({
		html: controls
	});
}

/**
 * Performs a search request
 */
function perform_search(keyword) {
	if (!keyword) {
		keyword = jQuery('#advanced-search-keyword').val();
	}
	var value = keyword.replace(/[\u00A0-\u9999<>\&]/gim, function(i) {
		return '&#' + i.charCodeAt(0) + ';';
	});

	var action_url = '/index.php/files/search';
	if (baseurl) {
		action_url = baseurl + action_url;
	}

	var form = jQuery(
		'<form action="' + action_url + '" method="POST" style="display: hidden">' +
		'<input name="keyword" value="' + keyword + '" />' +
		'</form>'
	);

	// Check for type of files
	var media_types = jQuery('[name="advanced-search-type"]:checked');
	media_types.each(function(){ 
		form.append(jQuery('<input name="type[]" value="' + jQuery(this).val() + '" />'));
	});

	// Check for date range
	var range_type = jQuery('[name="advanced-search-date"]:checked').val();
	if (range_type == 'exact') {
		var exact_date = jQuery('[name="advanced-search-exact-date"]').val();
		form.append(jQuery('<input name="exact_date" value="' + exact_date + '" />'));
	}
	else if (range_type == 'range') {
		var start_date = jQuery('[name="advanced-search-start-date"]').val();
		var end_date = jQuery('[name="advanced-search-end-date"]').val();

		form.append(jQuery('<input name="start_date" value="' + start_date + '" />'));
		form.append(jQuery('<input name="end_date" value="' + end_date + '" />'));
	}

	// Check for owner
	var media_owner = jQuery('[name="advanced-search-owner"]:checked').val();
	if (media_owner == 'SELECT') {
		media_owner = jQuery('#search-user-select').val();
		form.append(jQuery('<input name="owner" value="' + media_owner + '" />'));
	}
	else if (media_owner) {
		form.append(jQuery('<input name="owner" value="' + media_owner + '" />'));
	}

	jQuery('body').append(form);
	form.submit();
}

/**
 * Activates the specified inactive user
 */
function activate_user(user_id) {
	if (!user_id) {
		return;
	}

	$.ajax({
		type: 'POST',
		url: '/index.php/user/activate/' + user_id,
		dataType: 'json',

		success: function(data) {
			if (data.status == 'success') {
				location.reload();
			}
			else {
				alert("Unable to activate an user: " + data.message);
			}
		},

		error: function(data){
			console.log("error");
			console.log(data);
			alert("Unable to activate an user");
		}
	});
}

/**
 * Displays the permission table for the specified group
 */
function permission_editor(id) {
	if (!id) {
		return;
	}

	location.href = '/index.php/group/privileges/' + id;
}

/**
 * Creates a group of users
 */
function create_group() {
	// Validate form
	var form = document.getElementById("createGroup");
	if (!validate_form(form)) {
		return;
	}

	var action_url = '/index.php/group/create';
	if (baseurl) {
		action_url = baseurl + action_url;
	}

	// Send a request
	var form_data = new FormData(form);
	$.ajax({
		type: 'POST',
		url: action_url,

		contentType: false,
		processData: false,
		data: form_data,
		dataType: 'json',

		success: function(data) {
			if (data.status == 'success') {
				location.reload();
			}
			else {
				alert("Unable to create a group: " + data.message);
			}
		},

		error: function(data){
			console.log("error");
			console.log(data);
			alert("Unable to create a group");
		}
	});
}

/**
 * Updates the details of group
 */
function update_group(id) {
	// Detect an active form
	var form = document.getElementById("updateGroup");
	if (!form) {
		form = jQuery('.ui-dialog').filter(function() {
			return $(this).css('display') == 'block';
		});

		if (form.length > 1) {
			// Multiple active dialogs isn't supported
			return;
		}
		else {
			form = form.find('form')[0];
		}
	}

	// Validate form
	if (!validate_form(form)) {
		return;
	}

	var form_data = new FormData(form);
	jQuery.ajax({
		type: 'POST',
		url: '/index.php/group/update/' + id,

		contentType: false,
		processData: false,
		data: form_data,
		dataType: 'json',

		success: function(data) {
			if (data.status == 'success') {
				location.reload();
			}
			else {
				alert("Unable to update a selected group: " + data.message);
			}
		},

		error: function(data){
			console.log(data);
			alert("Unable to update a selected group");
		}
	});
}

/**
 * Delete the specified group
 */
function delete_group(group_id) {
	if (!group_id) {
		return;
	}

	if (!confirm('Are you sure want delete selected groups?')) {
		return;
	}

	jQuery.ajax({
		type: 'POST',
		url: '/index.php/group/delete/' + group_id,
		dataType: 'json',

		success: function(data) {
			if (data.status == 'success') {
				location.reload();
			}
			else {
				alert("Unable to delete a selected group: " + data.message);
			}
		},

		error: function(data){
			console.log(data);
			alert("Unable to delete a selected group");
		}
	});
}

/**
 * Returns the identifier of selected record.
 */
function get_selected_record() {
	var checkbox = jQuery('input[type="checkbox"].record:checked');
	if (checkbox.length > 1) {
		return null;
	}
	else {
		return checkbox.val();
	}
}

/**
 * Returns the list of selected identifiers.
 */
function get_selected_records() {
	var checkboxes = jQuery('input[type="checkbox"].record:checked');
	var records = checkboxes.map(function(index, element) {
		return jQuery(element).val();
	}).get();

	if (!records.length) {
		return null;
	}
	else {
		return records;
	}
}

/**
 * Displays the dialog to burn DVD disk.
 */
function burn_dialog(id) {
	if (!id) {
		return;
	}

	// Check access for selected files
	var has_access = true;
	id.forEach(function(id) {
		var record_type = jQuery('input.record[value="' + id + '"]').data('record-type');
		var privilege = current_user_permissions["download_" + record_type];
		if (!privilege) {
			alert('You dont have permission to download this type of media file: ' + record_type);
			has_access = false;
			return;
		}
	});

	if (!has_access) {
		return;
	}
	jQuery('#burn-dialog').dialog('open');

	// Send request
	var action_url = '/index.php/files/burn_disc/';
	if (action_url) {
		action_url = baseurl + action_url;
	}

	jQuery.ajax({
		type: 'POST',
		url: action_url,
		data: {
			"file": id
		},
		dataType: 'json',

		success: function(data) {
			if (data.status == 'success') {
				jQuery('#burn-dialog').dialog('close');
			}
			else {
				jQuery('.burn-inner').html("Unable to burn DVD: " + data.message);
			}
		},

		error: function(data) {
			console.log(data);
			jQuery('.burn-inner').html("Unable to burn DVD");
		}
	});
}

/**
 * Displays the dialog to download an audit report.
 */
function report_dialog(id) {
	if (!id) {
		return;
	}

	jQuery('#report-dialog').dialog('open');
}

/**
 * Displays or downloads the audit report for the specified file
 */
function get_file_report(id) {
	if (!id) {
		return;
	}

	// Hide opened dialog
	jQuery('#report-dialog').dialog('close');

	var report_action = jQuery('input[name="report_action"]:checked').val();
	if (!report_action || report_action == 'display') {
		// Redirect to report page
		location.href = '/index.php/activity/index/?record=' + id;
	}

	else {
		// Download the report
		location.href = '/index.php/activity/export/' + id;
	}
}

/**
 * Displays the audit report for the specified user
 */
function display_user_report(id) {
	if (!id) {
		return;
	}

	// Redirect to page
	location.href = '/index.php/activity/index/?user=' + id;
}

/**
 * Displays the dialog to download a selected files.
 */
function download_dialog_file(id) {
	if (!id) {
		return;
	}

	jQuery('#download-dialog').dialog('open');
}

/**
 * Downloads the specified file.
 */
function download_file(id) {
	// Validate arguments
	if (!id) {
		return;
	}

	if (!id.length) {
		id = [id];
	}

	// Hide opened dialog
	jQuery('#download-dialog').dialog('close');

	// Start downloading as zip file
	if (document.getElementById('download-zip-file').checked) {
		var has_access = true;
		id.forEach(function(id) {
			// Check access
			var record_type = jQuery('input.record[value="' + id + '"]').data('record-type');
			var privilege = current_user_permissions["download_" + record_type];
			if (!privilege) {
				alert('You dont have permission to download this type of media file: ' + record_type);
				has_access = false;
				return;
			}
		});

		if (!has_access) {
			return;
		}
		var args = jQuery(id).map(function(index, value) {
			return 'file[]=' + value;
		}).get().join('&');

		if (document.getElementById('download-include-report').checked) {
			args += '&include_report';
		}

		var download_url = '/index.php/files/download_archive/?' + args;
		if (baseurl) {
			download_url = baseurl + download_url;
		}
		location.href = download_url;
	}

	// Start downloading each file
	else {
		id.forEach(function(id) {
			// Check access
			var record_type = jQuery('input.record[value="' + id + '"]').data('record-type');
			var privilege = current_user_permissions["download_" + record_type];
			if (!privilege) {
				alert('You dont have permission to download this type of media file: ' + record_type);
				return;
			}

			// Simulate a click event
			if (document.getElementById('download-include-report').checked) {
				download_link('/index.php/activity/export/' + id);
			}

			var download_url = '/index.php/' + record_type + '/index/' + id + '?force';
			if (baseurl) {
				download_url = baseurl + download_url;
			}

			var checksum_url = '/index.php/' + record_type + '/checksum/' + id;
			if (baseurl) {
				checksum_url = baseurl + checksum_url;
			}

			download_link(download_url);
			download_link(checksum_url);
		});
	}
}

/**
 * Downloads the specified URL address
 */
function download_link(address) {
	var link = document.createElement('a');
	link.href = address;
	link.download = "download";

	document.body.appendChild(link);
	link.click();
	link.remove();
}

/**
 * Enables/Disables a set of modules
 */
function update_modules() {
	// Build a current set of features
	var features = {};
	jQuery('#privileges-page').find('input[type="checkbox"]').each(function() {
		var checkbox = jQuery(this);
		var type = checkbox.val();
		features[type] = + checkbox.is(':checked');
	});

	// Send a request
	var action_url = '/index.php/features/update/';
	if (baseurl) {
		action_url = baseurl + action_url;
	}

	jQuery.ajax({
		url: action_url,
		type: 'POST',
		dataType: 'json',
		data: {
			features: features
		}
	})
	.done(function(data) {
		if (data.status == 'success') {
			window.location.reload();
		}
		else {
			alert('Unable to update a set of features: ' + data.message);
		}
	})
	.fail(function(data) {
		console.log(data);
		alert('Unable to update a set of features');
	});
}

/**
 * Updates the list of permissions
 */
function update_permissions(group_id) {
	if (!group_id) {
		return;
	}

	// Build a current set of permissions
	var permissions = {};
	jQuery('#privileges-page').find('input[type="checkbox"]').each(function() {
		var checkbox = jQuery(this);
		var type = checkbox.val();
		permissions[type] = + checkbox.is(':checked');
	});
	permissions["global_permissions"] = jQuery('#permission_level').val();

	var action_url = '/index.php/group/update_permissions/' + group_id;
	if (baseurl) {
		action_url = baseurl + action_url;
	}

	// Send a request
	jQuery.ajax({
		url: action_url,
		type: 'POST',
		dataType: 'json',
		data: {
			permissions: permissions
		}
	})
	.done(function(data) {
		if (data.status == 'success') {
			window.location.reload();
		}
		else {
			alert('Unable to update a set of permissions: ' + data.message);
		}
	})
	.fail(function(data) {
		console.log("error");
		alert('Unable to update a set of permissions');
	});
}

/**
 * Parses the GPS coordinates that related with time of video
 */
function parse_gps_timepoint(timepoint) {
	var match = timepoint.coordinate.match(/^\((-?\d+(?:\.\d+)?),(-?\d+(?:\.\d+)?)\)$/);
	var time = timepoint.time.split(":");

	return {
		latitude: parseFloat(match[2]),
		longitude: parseFloat(match[1]),
		time: new Date(1900, 0, 0, time[0], time[1], time[2])
	};
}

/**
 * Displays or hides the map for the current video recording
 */
function map_handler() {
	// Determine an action
	var button = jQuery(this);
	var parent_container = jQuery('#mediaScreenAndMap');
	var map_container = jQuery('#map-container');
	var video_player = jQuery('#video-player');

	if (map_container.hasClass('hidden')) {
		if (parent_container.hasClass('bookmarks_open')) {
			// Close bookmark list
			jQuery('#ShowBookmarks').click();
		}
		parent_container.addClass('map_open');

		map_container.removeClass('hidden');
		button.text('Hide Map');

		video_player.removeClass('col-lg-12');
		video_player.addClass('col-sm-7');
	}
	else {
		parent_container.removeClass('map_open');

		map_container.addClass('hidden');
		button.text('Show Map');

		video_player.removeClass('col-sm-7');
		video_player.addClass('col-lg-12');
	}

	if (typeof map !== 'undefined') {
		// Map is initialized
		return;
	}

	// Prepare GPS data
	var gps_track = [];
	var raw_gps_data = jQuery('#gps-track').val().trim();
	if (raw_gps_data.length) {
		JSON.parse(raw_gps_data).forEach(function(value, index) {
			var record = parse_gps_timepoint(value);
			gps_track.push(record);
		});
	}

	// Prepare center of map
	var center;
	if (gps_track.length) {
		center = new google.maps.LatLng(
			gps_track[0].latitude, gps_track[0].longitude
		);
	}
	else {
		center = new google.maps.LatLng(0, 0);
	}

	// Initialiaze map
	var settings = {
		center: center,
		zoom: 16,
		draggable: true,
		scrollwheel: true,
		mapTypeId: google.maps.MapTypeId.ROADMAP
	};

	map = new google.maps.Map(map_container[0], settings);

	// Display GPS marker
	var marker;
	video_player.find('video')[0].ontimeupdate = function() {
		var current_time = parseInt(this.currentTime);

		// only eval once per second inc, since timeupdate pops ~4 times per second
		if (this.last_time != current_time) {
			// Extract time parts
			var time = current_time;
			var current_seconds = time % 60;
			time = Math.floor(time / 60);

			var current_minute = time % 60;
			time = Math.floor(time / 60);

			var current_hour = time % 24;

			// Find record related to current time
			var time_point;
			for (var i = 0; i < gps_track.length; i++) {
				var record = gps_track[i].time;
				if (
					current_hour == record.getHours() &&
					current_minute == record.getMinutes() &&
					current_seconds == record.getSeconds()
				) {
					time_point = gps_track[i];
					break;
				}
			}

			// Display a marker
			if (time_point) {
				var position = new google.maps.LatLng(
					time_point.latitude, time_point.longitude
				);

				if (marker) {
					marker.setPosition(position);
				}
				else {
					marker = new google.maps.Marker({
						position: position,
						map: map
					});
				}
			}
		}

		this.last_time = current_time;
	}
}

/**
 * Displays the uploaded picture.
 */
function image_preview(input) {
	var reader = new FileReader();
	reader.onload = function(e) {
		var img_container = jQuery(input).next('.image-preview');
		img_container.html('<img src="' + e.target.result + '" />');
	}
	reader.readAsDataURL(input.files[0]);
}

/**
 * Displays the metadata editor
 */
function edit_media_file(id) {
	if (!id) {
		return;
	}

	// Detect type of selected file
	var record_type = jQuery('input.record[value="' + id + '"]').data('record-type');

	// Check access
	var privilege = current_user_permissions["update_" + record_type];
	if (!privilege) {
		alert('Access Denied');
		return;
	}

	// Redirect to editor page
	location.href = '/index.php/' + record_type + '/details/' + id;
}

/**
 * Deletes the specified media file
 */
function delete_media_file(id) {
	// Validate arguments
	if (!id) {
		return;
	}

	if (!id.length) {
		id = [id];
	}

	if (!confirm('Are you sure that you want to delete selected media file(s)?')) {
		return;
	}

	// Try to delete every file
	id.forEach(function(id) {
		// Detect type of selected file
		var record_type = jQuery('input.record[value="' + id + '"]').data('record-type');

		// Check access
		var privilege = current_user_permissions["delete_" + record_type];
		if (!privilege) {
			alert('You dont have permission to delete this file!');
			return;
		}

		var action_url = '/index.php/' + record_type + '/delete/' + id;
		if (baseurl) {
			action_url = baseurl + action_url;
		}

		// Try to delete
		jQuery.ajax({
			type: 'POST',
			url: action_url,
			dataType: 'json',

			success: function(data) {
				if (data.status != 'success') {
					alert('Unable to delete a selected file: ' + data.message);
				}
			},

			error: function(data){
				console.log(data.responseText);
				alert('Unable to delete a selected file');
			}
		});
	});

	location.reload();
}

/**
 * Displays the settings page for the specified device
 */
function device_settings(id) {
	if (!id) {
		return;
	}

	// Check status
	var status = jQuery('#device-status-' + id).attr('class');
	if (!status) {
		alert('Unknown device');
		return;
	}
	else if (status == 'device-OFFLINE') {
		alert('Device is offline');
		return;
	}
	else if (status == 'device-SYNC') {
		alert('Device is busy');
		return;
	}

	// Check the type of device
	var type = jQuery('#device-type-' + id).data('device-type');
	if (!type) {
		alert('Unknown device');
		return;
	}
	else if (type != 'VISION') {
		alert('Unsupported device');
		return;
	}

	// Check the number of active devices
	var active_devices = jQuery('.device-ONLINE');
	if (active_devices.length > 1) {
		alert('Unable to update a settings for multiple devices');
		return;
	}

	location.href = '/index.php/device/settings/' + id;
}

/**
 * Updates the set of settings for the current device
 */
var update_device_handler = function() {
	var form_data = new FormData(document.getElementById('device-settings-form'));
	jQuery.ajax({
		type: 'POST',
		url: '/index.php/device/update/',

		contentType: false,
		processData: false,
		data: form_data,
		dataType: 'json',

		success: function(data) {
			if (data.status != 'success') {
				alert('Unable to update device settings: ' + data.message);
			}
			else {
				location.href = '/index.php/device/';
			}
		},

		error: function(data){
			console.log(data.responseText);
			alert('Unable to update device settings');
		}
	});	
}

/**
 * Checks for the new statuses of devices
 */
function check_device_statuses() {
	// Check the current page
	var path = location.pathname;
	if (!path.match(/\/index.php\/device\/?$/)) {
		return;
	}

	jQuery.ajax({
		type: 'GET',
		url: '/index.php/device/status/',
		dataType: 'json',

		success: function(data) {
			if (data.status != 'success') {
				console.log(data);
				return;
			}

			data.devices.forEach(function(device) {
				// Update device statuses
				var device_status = jQuery('#device-status-' + device.id);
				if (device_status.length < 1) {
					// Add new device
					// TODO
					return;
				}

				device_status.removeClass('device-OFFLINE device-ONLINE device-SYNC');
				device_status.addClass('device-' + device.status);

				// Update sync progress
				if (device.status == 'SYNC') {
					jQuery('#device-progress-' + device.id).html(
						Math.round(device.progress) + '%'
					);
				}
				else {
					jQuery('#device-progress-' + device.id).html('');
				}
			});
		},

		error: function(data){
			console.log(data.responseText);
		}
	});
}

/**
 * Displays the classifications list
 */
function picklist_editor_handler() {
	// Prepare data
	var classification_list = jQuery("#classification-list");
	if (!classification_list.length) {
		return;
	}

	var tags = [];
	classification_list.data('tags').forEach(function(value) {
		tags.push({id: value, text: value});
	});

	// Initialize picklist
	classification_list.select2({
		tags: true,

		createSearchChoice: function (term, data) {
			var value = $.trim(term);
			return {
				id: value,
				text: value
            };
		},

		data: {
			text: 'text',
			results: []
		}
	}).select2('data', tags);
}

/**
 * Saves the classifications list
 */
var picklist_handler = function() {
	var items = [];
	jQuery("#classification-list").select2('data').forEach(function(item) {
		items.push(item.id);
	});

	jQuery.ajax({
		type: 'POST',
		url: '/index.php/settings/update/',
		dataType: 'json',
		data: {
			classification: items
		},

		success: function(data) {
			if (data.status != 'success') {
				alert('Unable to update classification list: ' + data.message);
			}
		},

		error: function(data){
			console.log(data.responseText);
			alert('Unable to update classification list');
		}
	});
}

/**
 * Filters the list of system logs
 */
var activity_handler = function() {
	// Prepare request
	var form = jQuery(
		'<form action="/index.php/activity" method="POST" style="display: hidden">' +
			'<input name="filter-field" value="' + jQuery('select[name="filter-field"]').val() + '" style="display: none" />' +
			'<input name="filter-value" value="' + jQuery('input[name="filter-value"]').val() + '" style="display: none" />' +
		'</form>'
	);

	// Send request
	jQuery('body').append(form);
	form.submit();
}

/**
 * Deletes the archive retention policy
 */
var delete_policy_handler = function() {
	// Prepare request
	var category = jQuery('#archive_classifications').val();
	if (!category.length) {
		return;
	}

	var action_url = '/index.php/archive/unset_archive_policy/';
	if (baseurl) {
		action_url = baseurl + action_url;
	}

	jQuery.ajax({
		type: 'POST',
		url: action_url,
		dataType: 'json',
		data: {
			category: category
		},

		success: function(data) {
			if (data.status != 'success') {
				alert('Unable to update a retention policy: ' + data.message);
			}
			else {
				location.reload();
			}
		},

		error: function(data){
			console.log(data.responseText);
			alert('Unable to update a retention policy');
		}
	});
}

/**
 * Saves the retention policy
 */
var save_policy_handler = function() {
	// Prepare request
	var category = jQuery('#archive_classifications').val();
	if (!category.length) {
		return;
	}

	var action_url = '/index.php/archive/set_archive_policy/';
	if (baseurl) {
		action_url = baseurl + action_url;
	}

	jQuery.ajax({
		type: 'POST',
		url: action_url,
		dataType: 'json',
		data: {
			category: category,
			duration: jQuery('#archive_duration').val() + ' days'
		},

		success: function(data) {
			if (data.status != 'success') {
				alert('Unable to update a retention policy: ' + data.message);
			}
			else {
				location.reload();
			}
		},

		error: function(data){
			console.log(data.responseText);
			alert('Unable to update a retention policy');
		}
	});
}

/**
 * Deletes the post archive retention policy
 */
var delete_post_policy_handler = function() {
	// Prepare request
	var category = jQuery('#post_archive_classifications').val();
	if (!category.length) {
		return;
	}

	var action_url = '/index.php/archive/unset_post_archive_policy/';
	if (baseurl) {
		action_url = baseurl + action_url;
	}

	jQuery.ajax({
		type: 'POST',
		url: action_url,
		dataType: 'json',
		data: {
			category: category
		},

		success: function(data) {
			if (data.status != 'success') {
				alert('Unable to update a retention policy: ' + data.message);
			}
			else {
				location.reload();
			}
		},

		error: function(data){
			console.log(data.responseText);
			alert('Unable to update a retention policy');
		}
	});
}

/**
 * Saves the  post archive retention policy
 */
var save_post_policy_handler = function() {
	// Prepare request
	var category = jQuery('#post_archive_classifications').val();
	if (!category.length) {
		return;
	}

	var action_url = '/index.php/archive/set_post_archive_policy/';
	if (baseurl) {
		action_url = baseurl + action_url;
	}

	jQuery.ajax({
		type: 'POST',
		url: action_url,
		dataType: 'json',
		data: {
			category: category,
			duration: jQuery('#post_archive_duration').val() + ' days'
		},

		success: function(data) {
			if (data.status != 'success') {
				alert('Unable to update a retention policy: ' + data.message);
			}
			else {
				location.reload();
			}
		},

		error: function(data){
			console.log(data.responseText);
			alert('Unable to update a retention policy');
		}
	});
}

/**
 * Select the specifed classification for archive table
 */
function select_archive_category(category) {
	var values = jQuery('#archive_classifications').select2("val");
	values.push(category);
	jQuery('#archive_classifications').select2("val", values);
}

/**
 * Select the specifed classification for post archive table
 */
function select_post_archive_category(category) {
	var values = jQuery('#post_archive_classifications').select2("val");
	values.push(category);
	jQuery('#post_archive_classifications').select2("val", values);
}


/**
 * Displays the dialog to upload a document.
 */
function upload_document_dialog() {
	jQuery('#upload-dialog').dialog('open');
}

/**
 * Uploads a document to the system storage
 */
function upload_document_handler(event) {
	event.preventDefault();

	var action_url = '/index.php/files/upload/';
	if (baseurl) {
		action_url = baseurl + action_url;
	}
	
	jQuery.ajax({
		url: action_url,
		type: "POST",
		data: new FormData(this),
		dataType: 'json',

		contentType: false,
		processData: false,

		success: function(data) {
			console.log(data);
			if (data.status == 'success') {
				location.reload();
			}
			else {
				alert("Unable to upload a document: " + data.message);
			}
		},

		error: function(data){
			console.log(data.responseText);
			alert("Unable to upload a document");
		}
	});
}

jQuery(function() {
	if (!window.current_user_permissions) {
		window.current_user_permissions = {};
	}
	if (!window.current_features) {
		window.current_features = {};
	}

	setInterval(check_device_statuses, 3000);
	picklist_editor_handler();

	// Setup media player
	show_media_player();
	show_fancy_checkboxes(document);

	// Setup application handlers
	jQuery(document).on('click', '#SaveClassification', picklist_handler);
	jQuery(document).on('click', '#ActivityFilter', activity_handler);
	jQuery(document).on('click', '#UpdateDeviceSettings', update_device_handler);

	jQuery(document).on('submit', '#upload-dialog form', upload_document_handler);

	jQuery(document).on('click', '.video-row, .picture-row, .audio-row, .document-row', preview_handler);
	jQuery(document).on('click', '#AddFavorite', favorite_handler);
	jQuery(document).on('click', '#ShowMap', map_handler);

	jQuery(document).on('click', '#SnapCapture', snapshot_editor_handler);
	jQuery(document).on('click', '#SavePicture', snapshot_handler);

	jQuery(document).on('click', '#AddBookmark, #EditBookmark', bookmark_editor_handler);
	jQuery(document).on('click', '#ShowBookmarks', bookmark_list_handler);
	jQuery(document).on('click', '#CreateBookmark, #UpdateBookmark', bookmark_handler);
	jQuery(document).on('click', '[data-bookmark-id]', bookmark_preview_handler);

	jQuery(document).on('click', '#RedactVideo', video_editor_handler);
	jQuery(document).on('click', '#SetStartTime, #SetEndTime', crop_time_handler);
	jQuery(document).on('click', '#CropVideo', crop_handler);

	jQuery(document).on('click', '#EditMetadata', metadata_editor_handler);
	jQuery(document).on('click', '#SaveMetadata', metadata_handler);

	jQuery(document).on('click', '#DeleteArchivePolicy', delete_policy_handler);
	jQuery(document).on('click', '#SaveArchivePolicy', save_policy_handler);
	jQuery(document).on('click', '#DeletePostArchivePolicy', delete_post_policy_handler);
	jQuery(document).on('click', '#SavePostArchivePolicy', save_post_policy_handler);

	jQuery(document).on('dblclick', '#pict-row', function() {
		var user_id = jQuery(this).parent().find('.record').val();
		user_edit_dialog(user_id);
	});

	// Set all checkboxes
	jQuery(document).on('click', '.class_checkbox', function() {
		jQuery(this).toggleClass('checked').prev().prop('checked', jQuery(this).is('.checked'))
	});

	jQuery(document).on('click', '.class_checkbox', function() {
		if (!jQuery(this).parent('th').length) {
			// Ignore event
			return;
		}

		var main_checkbox = jQuery(this).parent().find('input[type="checkbox"]');
		var checkboxes = main_checkbox.parents('table').find('tbody').find(".class_checkbox");

		checkboxes.each(function() {
			var checkbox = jQuery(this);
			var checkbox_input = checkbox.parent().find('input[type="checkbox"]'); 

			if (main_checkbox.is(':checked')) {
				// select element
				if (!checkbox_input.is(':checked')) {
					checkbox.click();
				}
			}
			else {
				// Unselect element
				if (checkbox_input.is(':checked')) {
					checkbox.click();
				}
			}
		});
	});

	// Disable multiple selection on users/groups page
	if (document.body.id == 'users_page' || document.body.id == 'groups_page') {
		// TODO
	}

	// Close search dialog
	jQuery(document).click(function(event) {
		if (event.target.id == 'open-advanced') {
			return;
		}
		if (jQuery(event.target).parents('.advanced-search-content-area').length) {
			return;
		}

		if ($('#dialog-advanced').dialog('isOpen')) {
			$('#dialog-advanced').dialog('close');
		}
	});

	// Send a search request
	jQuery(document).on('keypress', '#nav-keyword', function(event) {
		if (event.which != 13) {
			return;
		}

		var keyword = jQuery(this).val();
		perform_search(keyword);
	});

	// Enable multiple select of users
	jQuery('.multiple-user-select').select2();

	// Load skin of media player
	(function(d, p){
		var a = new XMLHttpRequest(),  b = d.body;
		a.open("GET", p, true);
		a.send();

		a.onload = function(){
			var c = d.createElement("div");
			c.style.display = "none";
			c.innerHTML = a.responseText;
			b.insertBefore(c, b.childNodes[0]);
		}
	})(document, "/assets/images/sprite.svg");

	// Logout on close
	jQuery(window).on('beforeunload', function() {
		//jQuery.post('/index.php/user/logout');
	});

	// Setup search datepickers
	jQuery("#first-datepicker").datepicker({ dateFormat: 'yy-mm-dd' });
	jQuery("#scd-datepicker").datepicker({ dateFormat: 'yy-mm-dd' });
	jQuery("#thrd-datepicker").datepicker({ dateFormat: 'yy-mm-dd' });

	// Set checkboxes on a privilege page
	jQuery(document).find('input[type="checkbox"]:checked').each(function() {
		jQuery(this).next('.class_checkbox').addClass('checked');
	});

	// Check for opened bookmark
	var matches = /#bookmark=(.+)/.exec(location.hash);
	if (matches) {
		jQuery('tr[data-bookmark-id="' + matches[1] + '"]').click();
	}

	// Convert to current user timezone
	jQuery('span.datetime').each(function() {
		// Parse date/time
		var element = jQuery(this);
		var date = new Date(element.html().replace(' ', 'T') + ':00');

		// Prepare date
		var user_date = date.getFullYear() + '-' 
			+ (date.getMonth() + 1 > 9 ? (date.getMonth() + 1) : '0' + (date.getMonth() + 1)) + '-'
			+ (date.getDate() > 9 ? date.getDate() : '0' + date.getDate()) + ' ';

		// Prepare time
		user_date += (date.getHours() > 9 ? date.getHours() : '0' + date.getHours()) + ':'
			+ (date.getMinutes() > 9 ? date.getMinutes() : '0' + date.getMinutes()) + ':'
			+ (date.getSeconds() > 9 ? date.getSeconds() : '0' + date.getSeconds());

		var ms = date.getMilliseconds();
		if (ms) {
			user_date += '.' + ms.toString().replace(/0+$/,'');
		}

		// Update date/time of field
		element.html(user_date);
	});
});
