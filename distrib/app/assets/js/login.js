var ramLogin = ramLogin || {};
var baseurl = window.location.origin;
;(function($, window, undefined)
{
	"use strict";
	
	$(document).ready(function()
	{
		ramLogin.$container = $("#form_login");
		
		// Login Form & Validation
		ramLogin.$container.validate({
			rules: {
				username: {
					required: true	
				},
				
				password: {
					required: true
				},
				
			},
			
			highlight: function(element){
				$(element).closest('.input-group').addClass('validate-has-error');
			},
			
			unhighlight: function(element)
			{
				$(element).closest('.input-group').removeClass('validate-has-error');
			},
			
			submitHandler: function(ev)
			{
				$(".login-page").addClass('logging-in'); // This will hide the login form and init the progress bar	
				
				// Hide Errors
				$(".form-login-error").slideUp('fast');

				// wait till the transition ends				
				setTimeout(function()
				{
					var random_pct = 25 + Math.round(Math.random() * 30);
					
					// The form data are subbmitted, we can forward the progress to 70%
					ramLogin.setPercentage(40 + random_pct);

					var myusername = $("input#username").val();
					var mypassword = $("input#password").val();

					$.ajax({
						url: baseurl + '/index.php/user/authenticate',
						type: 'POST',
						dataType: 'json',
						data: {
							username: myusername,
							password: mypassword,
						},
					})
					.done(function(data) {														
						// Form is fully completed, we update the percentage
						ramLogin.setPercentage(100);
						
						// give give some time for the animation to finish, then execute the following procedures	
						setTimeout(function()
						{
							// If login is invalid, we store the 
							if (data.status == 'success') {
								window.location = baseurl;
							}
							else {								
								jQuery('.login-error-title').html(data.title);
								jQuery('.login-error-message').html(data.message);

								$(".login-page").removeClass('logging-in');
								ramLogin.resetProgressBar(true);
							}
						}, 1000);
					})
					.fail(function(data) {
						console.log("error");
					});

				}, 650);
			}
		});


		// Login Form Setup
		ramLogin.$body = $(".login-page");
		ramLogin.$login_progressbar_indicator = $(".login-progressbar-indicator h3");
		ramLogin.$login_progressbar = ramLogin.$body.find(".login-progressbar div");
		
		ramLogin.$login_progressbar_indicator.html('0%');
		
		if(ramLogin.$body.hasClass('login-form-fall'))
		{
			var focus_set = false;
			
			setTimeout(function(){ 
				ramLogin.$body.addClass('login-form-fall-init')
				
				setTimeout(function()
				{
					if( !focus_set)
					{
						ramLogin.$container.find('input:first').focus();
						focus_set = true;
					}
					
				}, 550);
				
			}, 0);
		}
		else
		{
			ramLogin.$container.find('input:first').focus();
		}
		
		// Focus Class
		ramLogin.$container.find('.form-control').each(function(i, el)
		{
			var $this = $(el),
				$group = $this.closest('.input-group');
			
			$this.prev('.input-group-addon').click(function()
			{
				$this.focus();
			});
			
			$this.on({
				focus: function()
				{
					$group.addClass('focused');
				},
				
				blur: function()
				{
					$group.removeClass('focused');
				}
			});
		});
		
		// Functions
		$.extend(ramLogin, {
			setPercentage: function(pct, callback)
			{
				pct = parseInt(pct / 100 * 100, 10) + '%';
				
				// Lockscreen
				if(is_lockscreen)
				{
					ramLogin.$lockscreen_progress_indicator.html(pct);
					
					var o = {
						pct: currentProgress
					};
					
					TweenMax.to(o, .7, {
						pct: parseInt(pct, 10),
						roundProps: ["pct"],
						ease: Sine.easeOut,
						onUpdate: function()
						{
							ramLogin.$lockscreen_progress_indicator.html(o.pct + '%');
							drawProgress(parseInt(o.pct, 10)/100);
						},
						onComplete: callback
					});	
					return;
				}
				
				// Normal Login
				ramLogin.$login_progressbar_indicator.html(pct);
				ramLogin.$login_progressbar.width(pct);
				
				var o = {
					pct: parseInt(ramLogin.$login_progressbar.width() / ramLogin.$login_progressbar.parent().width() * 100, 10)
				};
				
				TweenMax.to(o, .7, {
					pct: parseInt(pct, 10),
					roundProps: ["pct"],
					ease: Sine.easeOut,
					onUpdate: function()
					{
						ramLogin.$login_progressbar_indicator.html(o.pct + '%');
					},
					onComplete: callback
				});
			},
			
			resetProgressBar: function(display_errors)
			{
				TweenMax.set(ramLogin.$container, {css: {opacity: 0}});
				
				setTimeout(function()
				{
					TweenMax.to(ramLogin.$container, .6, {css: {opacity: 1}, onComplete: function()
					{
						ramLogin.$container.attr('style', '');
					}});
					
					ramLogin.$login_progressbar_indicator.html('0%');
					ramLogin.$login_progressbar.width(0);
					
					if(display_errors)
					{
						var $errors_container = $(".form-login-error");
						
						$errors_container.show();
						var height = $errors_container.outerHeight();
						
						$errors_container.css({
							height: 0
						});
						
						TweenMax.to($errors_container, .45, {css: {height: height}, onComplete: function()
						{
							$errors_container.css({height: 'auto'});
						}});
						
						// Reset password fields
						ramLogin.$container.find('input[type="password"]').val('');
					}
					
				}, 800);
			}
		});
		
		// Lockscreen & Validation
		var is_lockscreen = $(".login-page").hasClass('is-lockscreen');
		
		// Lockscreen Create Canvas
		if(is_lockscreen)
		{
			ramLogin.$lockscreen_progress_canvas = $('<canvas></canvas>');
			ramLogin.$lockscreen_progress_indicator =  ramLogin.$container.find('.lockscreen-progress-indicator');
			
			ramLogin.$lockscreen_progress_canvas.appendTo(ramLogin.$ls_thumb);
			
			var thumb_size = ramLogin.$ls_thumb.width();
			
			ramLogin.$lockscreen_progress_canvas.attr({
				width: thumb_size,
				height: thumb_size
			});
			
			
			ramLogin.lockscreen_progress_canvas = ramLogin.$lockscreen_progress_canvas.get(0);
			
			// Create Progress Circle
			var bg = ramLogin.lockscreen_progress_canvas,
				ctx = ctx = bg.getContext('2d'),
				imd = null,
				circ = Math.PI * 2,
				quart = Math.PI / 2,
				currentProgress = 0;
			
			ctx.beginPath();
			ctx.strokeStyle = '#eb7067';
			ctx.lineCap = 'square';
			ctx.closePath();
			ctx.fill();
			ctx.lineWidth = 3.0;
			
			imd = ctx.getImageData(0, 0, thumb_size, thumb_size);
			
			var drawProgress = function(current) {
			    ctx.putImageData(imd, 0, 0);
			    ctx.beginPath();
			    ctx.arc(thumb_size/2, thumb_size/2, 70, -(quart), ((circ) * current) - quart, false);
			    ctx.stroke();
			    
			    currentProgress = current * 100;
			}
			
			drawProgress(0/100);
			
			
			ramLogin.$lockscreen_progress_indicator.html('0%');
			
			ctx.restore();
		}
		
	});
	
})(jQuery, window);
