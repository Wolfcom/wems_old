Set shell = WScript.CreateObject("WScript.Shell")

'Generate self-signed certificate
shell.Run "dpscat.exe", 0, true

'Try to install driver in legacy mode
if Len(shell.ExpandEnvironmentStrings("%ProgramW6432%")) then
	shell.Run "DPInst64.exe /LM /SW", 0, true
else
	shell.Run "DPInst32.exe /LM /SW", 0, true
end if
