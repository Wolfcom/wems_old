Set shell = CreateObject("WScript.Shell")
Set file_system = CreateObject("Scripting.FileSystemObject")
Set temp_file = new TempFile

'Choose installation folder
Set shell_app = CreateObject("Shell.Application")
Set install_folder = shell_app.BrowseForFolder(0, "Please select the folder 'WolfCom Evidence Management Solution'", 1, "")

If (install_folder Is Nothing) Then
	die "Unable to find an installation folder"
End If

If Not (file_system.FileExists("tools\psql.bat")) Then
	die "Unable to find a database client"
End If

'Read configuration
Dim db_host, db_port, db_name, db_user, db_password
Set pattern = new Regexp
pattern.Pattern = "^ER_DSN\s*=\s*host\s*=\s*([^ ]+)\s+port\s*=\s*([^ ]+)\s+user\s*=\s*([^ ]+)\s+password=([^ ]+)\s+dbname=([^ ]+)"
pattern.IgnoreCase = true

config_file = install_folder.Self.path + "\app.cfg"
If Not (file_system.FileExists(config_file)) Then
	die "Unable to find a configuration of application: " + config_file
End If

Set stream = file_system.OpenTextFile(config_file)
Do Until stream.AtEndOfStream
	Dim line : line = Trim(stream.ReadLine())
	Dim match : Set match = pattern.Execute(line)

	If (match.Count = 1) Then
		db_host = match.Item(0).SubMatches(0)
		db_port = match.Item(0).SubMatches(1)
		db_user = match.Item(0).SubMatches(2)
		db_password = match.Item(0).SubMatches(3)
		db_name = match.Item(0).SubMatches(4)
	End If
Loop

If (Len(db_host) = 0) Then
	die "Unable to find a configuration string for database in the file: " + config_file
End If


'Calculate checksums of pictures
execute_query "ALTER TABLE pictures ADD COLUMN checksum bytea"
execute_query "UPDATE pictures SET checksum = digest(content, 'sha256')"
execute_query "ALTER TABLE pictures ALTER COLUMN checksum SET NOT NULL"

shell.LogEvent 4, "Checksums of pictures are updated"
MsgBox "Checksums of pictures are updated"

'Calculate checksums of video files
execute_query "ALTER TABLE videos ADD COLUMN checksum bytea"

lines = Split(execute_query("select id, content from videos"), vbCrLf)
For Each line in lines
	If Len(line) Then
		Dim video_item : video_item = Split(line, ",")
		Dim video_filename, video_stream

		'Prepare temporary file
		temp_file.Create video_filename, video_stream
		video_stream.Close
		video_filename = Replace(video_filename, "\", "/")

		'Export file and calculate checksum
		execute_query "\lo_export " + video_item(1) + " '" + video_filename + "'"
		Dim video_checksum : video_checksum = calc_checksum(video_filename)
		video_checksum = Left(video_checksum, InStr(video_checksum, " "))

		'Save a new checksum
		execute_query "UPDATE videos SET checksum = '" + video_checksum + "' WHERE id = '" + video_item(0) + "'"
	End If
Next

execute_query "ALTER TABLE videos ALTER COLUMN checksum SET NOT NULL"

shell.LogEvent 4, "Checksums of videos are updated"
MsgBox "Checksums of videos are updated"

'Calculate checksums of audio files
execute_query "ALTER TABLE audios ADD COLUMN checksum bytea"

lines = Split(execute_query("select id, content from audios"), vbCrLf)
For Each line in lines
	If Len(line) Then
		Dim audio_item : audio_item = Split(line, ",")
		Dim audio_filename, stream

		'Prepare temporary file
		temp_file.Create audio_filename, audio_stream
		audio_stream.Close
		audio_filename = Replace(audio_filename, "\", "/")

		'Export file and calculate checksum
		execute_query "\lo_export " + audio_item(1) + " '" + audio_filename + "'"
		Dim audio_checksum : audio_checksum = calc_checksum(audio_filename)
		audio_checksum = Left(audio_checksum, InStr(audio_checksum, " "))

		'Save a new checksum
		execute_query "UPDATE audios SET checksum = '" + audio_checksum + "' WHERE id = '" + audio_item(0) + "'"
	End If
Next

execute_query "ALTER TABLE audios ALTER COLUMN checksum SET NOT NULL"

shell.LogEvent 4, "Checksums of audios are updated"
MsgBox "Checksums of audios are updated"


' Executes a specified function
Function execute_query(query)
	Dim command : command = "tools\psql.bat " + db_host + " " + db_port + " " + db_name + " " + db_user + " " + db_password + " """ + query + """" 
	Set result = shell.Exec(command)

	Select Case result.Status
		Case WshFinished
			Dim output : output = Trim(result.StdOut.ReadAll)
			If result.ExitCode <> 0 Then
				die "Unable to execute a query: " + output 
			Else
				execute_query = output
			End If

		Case WshFailed
			die "Unable to execute a query: " & result.StdErr.ReadAll
	End Select
End Function

' Calculates sha256 checksum
Function calc_checksum(filename)
	Dim command : command = "tools\sha256sum.exe " + filename
	Set result = shell.Exec(command)

	Select Case result.Status
		Case WshFinished
			Dim output : output = Trim(result.StdOut.ReadAll)
			If result.ExitCode <> 0 Then
				die "Unable to calculate a checksum: " + output 
			Else
				calc_checksum = output
			End If

		Case WshFailed
			die "Unable to calculate a checksum: " & result.StdErr.ReadAll
	End Select
End Function


'Throw an error
Sub die(message)
	Msgbox message, vbOKOnly + vbCritical
	shell.LogEvent 1, message
	WScript.Quit 1
End Sub

'Implements temporary files
Class TempFile

	'An instance of file system service
	Dim file_system

	'An instance of temporary folder
	Dim temp_folder

	'A list of temporary files
	Dim files

	'Creates a temporary file.
	Function Create(filename, stream)
		Dim temp_file
		temp_file = file_system.GetTempName
		Set stream = temp_folder.CreateTextFile(temp_file)

		'Register the temporary file to future deletion
		filename = temp_folder + "\" + temp_file
		files.Add filename, stream
	End Function

	'Called when the instance is created
	Private Sub Class_Initialize()
		Set files = CreateObject("Scripting.Dictionary")
		Set file_system = CreateObject("Scripting.FileSystemObject")
		Set temp_folder = file_system.GetSpecialFolder(2)
	End Sub

	'Deletes all temporary files after the object is destroyed
	Private Sub Class_Terminate()
		Dim stream, file

		For i = 0 To files.Count - 1
			file = files.Keys()(i)
			Set stream = files.Items()(i)

			stream.Close
			file_system.DeleteFile(file)
		Next
	End Sub

End Class
