@echo off
cd /d %~dp0
Setlocal EnableDelayedExpansion

set db_host=%1
set db_port=%2
set db_name=%3
set db_user=%4
set PGPASSWORD=%5
set db_query=%6

psql.exe -t -A -F, -h %db_host% -p %db_port% -d %db_name% -U %db_user% -c %db_query% > %TEMP%\psql.stdout.txt 2> %TEMP%\psql.stderr.txt
if errorlevel 1	(
	if %ERRORLEVEL% equ 1 (
		set /p OUTPUT=<%TEMP%\psql.stderr.txt
		echo SQL query is failed: %db_query% !OUTPUT!
	) else (
		echo Unable to run a PostgreSQl client: %ERRORLEVEL%
	)
	exit /b 255
)

for /f "tokens=1 delims=" %%i in (%TEMP%\psql.stdout.txt) do echo %%i
exit /b 0
