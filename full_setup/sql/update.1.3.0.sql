UPDATE profiles SET first_name = 'Wolfcom Administrator' WHERE id ='0a89862a-ceec-11e5-903c-303a64a40499';

CREATE TYPE features AS (
  security boolean,
  gps_map boolean,
  redaction boolean,
  audit boolean
);
COMMENT ON TYPE features IS 'a set of enabled/disabled features';

