ALTER TYPE action ADD VALUE 'burn_video';
ALTER TYPE action ADD VALUE 'burn_audio';
ALTER TYPE action ADD VALUE 'burn_picture';

ALTER TYPE features ADD ATTRIBUTE burn_disc boolean;
ALTER TYPE privilege_type ADD ATTRIBUTE burn_disc boolean;
UPDATE groups SET permissions.burn_disc = true WHERE id = 'd2ee53ca-cfa0-11e5-b5dc-303a64a40499';
