CREATE TABLE public.retention_policy
(
  category classification NOT NULL,
  archive_duration interval,
  post_archive_duration interval,
  CONSTRAINT retention_policy_pkey PRIMARY KEY (category)
)
WITH (
  OIDS=FALSE
);

CREATE OR REPLACE VIEW public.archive AS 
 SELECT t.type,
    t.id,
    t.created,
    t.archived,
    t.user_id,
    t.size,
    t.title,
    t.caseno
   FROM ( SELECT 'picture'::text AS type,
            p.id,
            p.created,
            p.created + rp.archive_duration AS archived,
            p.user_id,
            octet_length(p.content) AS size,
            p.title,
            p.caseno
           FROM pictures p
             JOIN retention_policy rp ON rp.archive_duration <> '178000000 years'::interval AND rp.category = p.classification AND (now() - rp.archive_duration) > p.created
          WHERE p.deleted = false
        UNION ALL
         SELECT 'video'::text AS type,
            v.id,
            v.created,
            v.created + rp.archive_duration AS archived,
            v.user_id,
            lo_size(v.content) AS size,
            v.title,
            v.caseno
           FROM videos v
             JOIN retention_policy rp ON rp.archive_duration <> '178000000 years'::interval AND rp.category = v.classification AND (now() - rp.archive_duration) > v.created
          WHERE v.deleted = false
        UNION ALL
         SELECT 'audio'::text AS type,
            a.id,
            a.created,
            a.created + rp.archive_duration AS archived,
            a.user_id,
            lo_size(a.raw_content) AS size,
            a.title,
            a.caseno
           FROM audios a
             JOIN retention_policy rp ON rp.archive_duration <> '178000000 years'::interval AND rp.category = a.classification AND (now() - rp.archive_duration) > a.created
          WHERE a.deleted = false) t;


CREATE OR REPLACE FUNCTION public.delete_outdated_files()
  RETURNS void AS
$BODY$
DECLARE
    r record;
BEGIN
	-- Delete outdated pictures
	FOR r IN (
		SELECT p.id FROM pictures p
		JOIN retention_policy rp ON rp.category = p.classification AND (now() - rp.archive_duration - rp.post_archive_duration) > p.created
		WHERE p.deleted = false AND rp.archive_duration <> '178000000 years' AND rp.post_archive_duration <> '178000000 years'
	)
	LOOP
		UPDATE pictures SET content = '', deleted = true WHERE id = r.id;
	END LOOP;

	-- Delete outdated audios
	FOR r IN (
		SELECT a.id, a.content FROM audios a
		JOIN retention_policy rp ON rp.category = a.classification AND (now() - rp.archive_duration - rp.post_archive_duration) > a.created
		WHERE a.deleted = false AND rp.archive_duration <> '178000000 years' AND rp.post_archive_duration <> '178000000 years'
	)
	LOOP
		BEGIN
			PERFORM lo_unlink(r.content);
		EXCEPTION WHEN SQLSTATE '42704' THEN
		END;
		UPDATE audios SET content = 0, deleted = true WHERE id = r.id;
	END LOOP;

	-- Delete outdated videos
	FOR r IN (
		SELECT v.id, v.content FROM videos v
		JOIN retention_policy rp ON rp.category = v.classification AND (now() - rp.archive_duration - rp.post_archive_duration) > v.created
		WHERE v.deleted = false AND rp.archive_duration <> '178000000 years' AND rp.post_archive_duration <> '178000000 years'
	)
	LOOP
		BEGIN
			PERFORM lo_unlink(r.content);
		EXCEPTION WHEN SQLSTATE '42704' THEN
		END;
		UPDATE pictures SET thumbnail = '', content = 0, deleted = true WHERE id = r.id;
	END LOOP;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
