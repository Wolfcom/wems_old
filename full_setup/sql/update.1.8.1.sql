ALTER TYPE features ADD ATTRIBUTE upload_file boolean;
ALTER TYPE privilege_type ADD ATTRIBUTE upload_file boolean;
ALTER TYPE privilege_type ADD ATTRIBUTE read_document boolean;
ALTER TYPE privilege_type ADD ATTRIBUTE delete_document boolean;
ALTER TYPE privilege_type ADD ATTRIBUTE update_document boolean;
ALTER TYPE privilege_type ADD ATTRIBUTE download_document boolean;

UPDATE groups SET
	permissions.upload_file = true,
	permissions.read_document = true,
	permissions.delete_document = true,
	permissions.update_document = true,
	permissions.download_document = true
WHERE id = 'd2ee53ca-cfa0-11e5-b5dc-303a64a40499';

ALTER TYPE action ADD VALUE 'view_document';
ALTER TYPE action ADD VALUE 'delete_document';
ALTER TYPE action ADD VALUE 'update_document';
ALTER TYPE action ADD VALUE 'upload_document';
ALTER TYPE action ADD VALUE 'download_document';
ALTER TYPE action ADD VALUE 'burn_document';

CREATE TYPE public.mime AS ENUM
   ('image/gif',
    'image/jpeg',
    'image/png',
    'application/pdf',
    'text/plain',
    'application/msword',
    'vnd.openxmlformats-officedocument.wordprocessingml.document',
    'application/vnd.ms-excel',
    'vnd.openxmlformats-officedocument.spreadsheetml.sheet',
    'application/vnd.ms-powerpoint',
    'vnd.openxmlformats-officedocument.presentationml.presentation');
   
COMMENT ON TYPE mime IS 'a set of allowed files to upload';

CREATE TABLE documents
(
  id uuid NOT NULL, -- A primary identifier
  created timestamp(0) with time zone NOT NULL DEFAULT now(), -- a date and time when document was created
  user_id uuid NOT NULL, -- an identifier of user who upload this document
  content bytea NOT NULL, -- a data of document in various format
  modified timestamp without time zone, -- a last date and time when document was modified
  description text, -- an optional description of document
  caseno character varying(32), -- an optional identifier of case
  classification classification, -- an optional classification of case
  title character varying(200), -- an optional tittle of document
  checksum bytea NOT NULL, -- a mandatory SHA-256 checksum
  deleted boolean NOT NULL DEFAULT false, -- true if the file is deleted, otherwise false
  mime_type mime NOT NULL, -- a mime type of document
  CONSTRAINT documents_pkey PRIMARY KEY (id)
)
WITH (
  OIDS=FALSE
);

COMMENT ON TABLE public.documents  IS 'A list of documents';
COMMENT ON COLUMN public.documents.id IS 'A primary identifier';
COMMENT ON COLUMN public.documents.created IS 'a date and time when document was created';
COMMENT ON COLUMN public.documents.user_id IS 'an identifier of user who upload this document';
COMMENT ON COLUMN public.documents.content IS ' a data of document in various format';
COMMENT ON COLUMN public.documents.modified IS 'a last date and time when document was modified';
COMMENT ON COLUMN public.documents.description IS 'an optional description of document';
COMMENT ON COLUMN public.documents.caseno IS 'an optional identifier of case';
COMMENT ON COLUMN public.documents.classification IS 'an optional classification of case';
COMMENT ON COLUMN public.documents.title IS 'an optional tittle of document';
COMMENT ON COLUMN public.documents.checksum IS 'a mandatory SHA-256 checksum';
COMMENT ON COLUMN public.documents.deleted IS 'true if the file is deleted, otherwise false';
COMMENT ON COLUMN public.documents.mime_type IS 'a mime type of document';

CREATE INDEX documents_user_id_idx
  ON public.documents
  USING btree
  (user_id);


CREATE OR REPLACE VIEW public.archive AS 
 SELECT t.type,
    t.id,
    t.created,
    t.archived,
    t.user_id,
    t.size,
    t.title,
    t.caseno
   FROM (
          SELECT 'document'::text AS type,
            d.id,
            d.created,
            d.created + rp.archive_duration AS archived,
            d.user_id,
            octet_length(d.content) AS size,
            d.title,
            d.caseno
           FROM documents d
             JOIN retention_policy rp ON rp.archive_duration <> '178000000 years'::interval AND rp.category = d.classification AND (now() - rp.archive_duration) > d.created
          WHERE d.deleted = false
        UNION ALL
         SELECT 'picture'::text AS type,
            p.id,
            p.created,
            p.created + rp.archive_duration AS archived,
            p.user_id,
            octet_length(p.content) AS size,
            p.title,
            p.caseno
           FROM pictures p
             JOIN retention_policy rp ON rp.archive_duration <> '178000000 years'::interval AND rp.category = p.classification AND (now() - rp.archive_duration) > p.created
          WHERE p.deleted = false
        UNION ALL
         SELECT 'video'::text AS type,
            v.id,
            v.created,
            v.created + rp.archive_duration AS archived,
            v.user_id,
            lo_size(v.content) AS size,
            v.title,
            v.caseno
           FROM videos v
             JOIN retention_policy rp ON rp.archive_duration <> '178000000 years'::interval AND rp.category = v.classification AND (now() - rp.archive_duration) > v.created
          WHERE v.deleted = false
        UNION ALL
         SELECT 'audio'::text AS type,
            a.id,
            a.created,
            a.created + rp.archive_duration AS archived,
            a.user_id,
            lo_size(a.raw_content) AS size,
            a.title,
            a.caseno
           FROM audios a
             JOIN retention_policy rp ON rp.archive_duration <> '178000000 years'::interval AND rp.category = a.classification AND (now() - rp.archive_duration) > a.created
          WHERE a.deleted = false) t;

CREATE OR REPLACE FUNCTION public.delete_outdated_files()
  RETURNS void AS
$BODY$
DECLARE
    r record;
BEGIN
	-- Delete outdated pictures
	FOR r IN (
		SELECT p.id FROM pictures p
		JOIN retention_policy rp ON rp.category = p.classification AND (now() - rp.archive_duration - rp.post_archive_duration) > p.created
		WHERE p.deleted = false AND rp.archive_duration <> '178000000 years' AND rp.post_archive_duration <> '178000000 years'
	)
	LOOP
		UPDATE pictures SET content = '', deleted = true WHERE id = r.id;
	END LOOP;

	-- Delete outdated audios
	FOR r IN (
		SELECT a.id, a.content FROM audios a
		JOIN retention_policy rp ON rp.category = a.classification AND (now() - rp.archive_duration - rp.post_archive_duration) > a.created
		WHERE a.deleted = false AND rp.archive_duration <> '178000000 years' AND rp.post_archive_duration <> '178000000 years'
	)
	LOOP
		BEGIN
			PERFORM lo_unlink(r.content);
		EXCEPTION WHEN SQLSTATE '42704' THEN
		END;

		UPDATE audios SET content = 0, deleted = true WHERE id = r.id;
	END LOOP;

	-- Delete outdated videos
	FOR r IN (
		SELECT v.id, v.content FROM videos v
		JOIN retention_policy rp ON rp.category = v.classification AND (now() - rp.archive_duration - rp.post_archive_duration) > v.created
		WHERE v.deleted = false AND rp.archive_duration <> '178000000 years' AND rp.post_archive_duration <> '178000000 years'
	)
	LOOP
		BEGIN
			PERFORM lo_unlink(r.content);
		EXCEPTION WHEN SQLSTATE '42704' THEN
		END;

		UPDATE videos SET thumbnail = '', content = 0, deleted = true WHERE id = r.id;
	END LOOP;

	-- Delete outdated documents
	FOR r IN (
		SELECT d.id FROM documents d
		JOIN retention_policy rp ON rp.category = d.classification AND (now() - rp.archive_duration - rp.post_archive_duration) > d.created
		WHERE d.deleted = false AND rp.archive_duration <> '178000000 years' AND rp.post_archive_duration <> '178000000 years'
	)
	LOOP
		UPDATE documents SET thumbnail = '', content = 0, deleted = true WHERE id = r.id;
	END LOOP;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
