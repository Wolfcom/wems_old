@echo off
cd %~dp0

:: Prepare compilation parameters (remove  -ext WixSystemToolsExtension)
set EXT=-ext WixUIExtension -ext WixUtilExtension

set APP_MANUFACTURER=WolfCom
set APP_MANUFACTURER_TITLE=WolfCom Enterprises, Inc.

set APP_PRODUCT=Evidence Management Solution x86-x64
set APP_VERSION=1.8.5


echo Prepare headers for WEB Server
set HEAT_WEB_PATH=../distrib/apache
heat dir %HEAT_WEB_PATH% -nologo -o build\web.wxs -scom -sfrag -srd -sreg -gg -var env.HEAT_WEB_PATH -cg WEB_GROUP -dr APP_WEB
candle.exe -arch x86 -nologo -out build\x86\web.wixobj build\web.wxs

echo Prepare headers for WEB Application
set HEAT_WEB_APP_PATH=../distrib/app
heat dir %HEAT_WEB_APP_PATH% -nologo -o build\web_app.wxs -scom -sfrag -srd -sreg -gg -var env.HEAT_WEB_APP_PATH -cg WEB_APP_GROUP -dr APP_WEB_APP
candle.exe -arch x86 -nologo -out build\x86\web_app.wixobj build\web_app.wxs

echo Make installation package (x86)
set OBJ=build\x86\setup_x32.wixobj build\x86\web_app.wixobj build\x86\web.wixobj
candle.exe -arch x86 -nologo -out build\x86\setup_x32.wixobj %EXT% src\setup_x32.wxs
light.exe -nologo -out bin\setup.%APP_VERSION%.msi %EXT% %OBJ%
